import sys
import numpy as np
import matplotlib
import pickle
from scipy.signal import savgol_filter
from scipy.optimize import fsolve
from scipy.optimize import least_squares
from scipy.optimize import brentq
import scipy.optimize as optimize
import scipy.integrate as integrate
from matplotlib.legend import Legend
from matplotlib.lines import Line2D
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from check_collision_times import *
from dynamical_background_gammie1999 import *
from scipy import special
import geokerr_interface as gi
import calcg_lnrf as calcg

small_size = 22
medium_size = 14
# medium_size = 18
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
# matplotlib.rc('legend', fontsize=16)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)
matplotlib.rc('text', usetex=True)

class equilibrium_finder:
    def __init__(self, bg, setup="lia_hp", **kwargs):
        """
        TODO:
        """
        overwrite_self = kwargs.get("overwrite_self", False)
        self.Fthetaphi = bg.Fthetaphi
        self.bg = bg
        self.minimum_gamma1 = kwargs.get("minimum_gamma1", 2.0)
        self.no_qnt_tau_decrease = kwargs.get("no_qnt_tau_decrease", False)

        # NOTE: everything below this point will be overwritten
        # if equilibrium file already exists. That's good
        # because otherwise the user might think they changed
        # a parameter when really they didn't.
        self.electron_heating_fraction = kwargs.get("electron_heating_fraction", 0.5)

        # --------------------------------------------------------------
        # Input PIC prescriptions to use
        # --------------------------------------------------------------
        # PL_type and gamma2_type determine the prescription used to set
        # the power-law index and high-energy cutoff, respectively.
        # They can be set to constant or to use a PIC prescription.
        # PL_type options: "WUBCN18", "WPU19", "constant"
        # gamma2_type options: "fixed", "4sigma", "6sigma"
        self.PL_type = kwargs.get("PL_type", "WUBCN18")
        self.gamma2_type = kwargs.get("gamma2_type", "4sigma")
        # --------------------------------------------------------------
        # Handle prescriptions for p and gamma2
        # --------------------------------------------------------------
        if self.PL_type == "constant":
            self.PL_index_constant = 2.9
            self.PL_filestr = "Constant{:.2f}".format(self.PL_index_constant)
            self.PL_titstr = "$p={:.2f}$".format(self.PL_index_constant)
        else:
            self.PL_filestr = self.PL_type
            self.PL_titstr = self.PL_type + " PL prescription"

        if self.gamma2_type == "fixed":
            self.gamma2_constant = kwargs.get("gamma2_constant", 1000)
            self.gamma2_str = "{:.2e}".format(self.gamma2_constant)
            self.gamma2_titstr = r"$\gamma_2={:.2e}$".format(self.gamma2_constant)
        else:
            self.gamma2_str = self.gamma2_type
            self.gamma2_titstr = r"$\gamma_2$ " + self.gamma2_str

        # -----------------------------------
        #      Add default setup information here
        # -----------------------------------
        if setup == "lia_hp":
            path_to_figures = "/mnt/c/Users/liaha/research/projects/"\
                "summer2021/figures/multizone_model/Fthetaphi{:.2f}a{:.2f}/".format(self.Fthetaphi, bg.a)
            path_to_figures2 = path_to_figures + "delta{:.2f}_cutoff".format(self.electron_heating_fraction) + self.gamma2_str + "_index" + self.PL_filestr + "/gamma1minimum{:.2f}/".format(self.minimum_gamma1)
            path_to_reduced_data = "/mnt/c/Users/liaha/research/projects/"\
                "summer2021/data_reduced/Fthetaphi{:.2f}a{:.2f}/".format(self.Fthetaphi, bg.a)
            pickle_path = path_to_reduced_data + "equilibrium_solution_delta{:.2f}_cutoff".format(self.electron_heating_fraction) + self.gamma2_str + "_index" + self.PL_filestr + "_gamma1minimum{:.2f}".format(self.minimum_gamma1)
            spectrum_path_base = path_to_reduced_data + "spectrum_delta{:.2f}_cutoff".format(self.electron_heating_fraction) + self.gamma2_str + "_index" + self.PL_filestr + "_gamma1minimum{:.2f}".format(self.minimum_gamma1)
            disk_spectrum_dir = "/mnt/c/Users/liaha/research/projects/summer2021/data_reduced/disk_spectra/a{:.2f}/".format(self.bg.a)
            # TEST
            if self.no_qnt_tau_decrease:
                path_to_figures2 += "noQntTauDecreaseTest/"
                pickle_path += "_noQntTauDecreaseTest"
                spectrum_path_base += "_noQntTauDecreaseTest"
            pickle_path += ".p"

        # -----------------------------------
        # If paths are non-standard, set them here using kwargs
        # Otherwise, use the defaults from above
        # -----------------------------------
        self.path_to_figures = kwargs.get("path_to_figures", path_to_figures)
        self.path_to_figures2 = kwargs.get("path_to_figures", path_to_figures2)
        self.path_to_reduced_data = kwargs.get("path_to_reduced_data",
                                               path_to_reduced_data)
        self.dir_to_disk_spectrum = kwargs.get("disk_spectrum_dir", disk_spectrum_dir)
        self.pickle_path = kwargs.get("pickle_path", pickle_path)
        self.spectrum_path_base = kwargs.get("spectrum_path_base", spectrum_path_base)

        need_to_initialize = True
        if os.path.exists(self.pickle_path) and (not overwrite_self):
            print("Loading equilibrium solution from " + self.pickle_path)
            success = self._load_from_pickle()
            if not success:
                print("Something went wrong with loading pickle file.")
            else:
                print("Done.")
            need_to_initialize = not success
            self.dir_to_disk_spectrum = kwargs.get("disk_spectrum_dir", disk_spectrum_dir)

        if need_to_initialize:
            self.temperature_dictionary = {}
            self.max_T_dictionary = {}
            self.gamma1_dictionary = {}
            self.peff_dictionary = {}
            self.norm_dictionary = {}
            self.nonthermal_cooling_rate_dictionary = {}
            self.thermal_cooling_rate_dictionary = {}
            self.cooling_max_T_dictionary = {}
            self._initialize(**kwargs)

        self.quick = kwargs.get("quick", False)
        self.jump = kwargs.get("jump", 1)
        redo_at_r = False
        # redo_at_r = True

        # --------------------------------------------------------------
        # Do some parameter regime scans first
        # --------------------------------------------------------------
        # self.plot_min_collision_time_at_r_over_T(index=0)
        # self.plot_min_collision_time_at_r_over_T(index=self.r_range.size - 1)
        # self.scan_solution_space(index=0)
        # self.scan_solution_space(index=67)
        # self.scan_solution_space(index=200)
        # self.scan_solution_space(index=self.r_range.size - 1)
        # exit()

        # --------------------------------------------------------------
        # Main loop: calculate equilibrium temperature at each radius
        # --------------------------------------------------------------
        for i, r in enumerate(self.r_range):
            if self.quick and not (i%self.jump == 0):
                continue
            if i in self.temperature_dictionary:
                # print("i={:d} already in dictionary. Skipping.".format(i))
                continue

            redo_at_r = True
            if self.ratio_over_r[i] < 1:
                print("Using IC cooling time for i={:d}.".format(i))
            else:
                print("Using synchrotron cooling time for i={:d}.".format(i))

            # TODO: set bounds using previous radii sols?
            Tmin = 1e6; Tmax = 0.9*Consts.max_temp
            try:
                temp_solve = brentq(self.solve_for_equilibrium, Tmin, Tmax, args=i)
            except:
                print("Heating/cooling is non-monotonic! Scanning for solution...")
                self.scan_solution_space(index=i)
                try:
                    Tmax = 0.85*Consts.max_temp
                    temp_solve = brentq(self.solve_for_equilibrium, Tmin, Tmax, args=i)
                    print("Reducing Tmax made heating/cooling monotonic.")
                except:
                    print("Reducing Tmax did not make heating/cooling non-monotonic.")
                    exit()

            eq_temperature = temp_solve
            if eq_temperature == Tmin or eq_temperature == Tmax:
                print("Temperature is hitting the upper/lower bounds!")
            fractional_error = self.solve_for_equilibrium(eq_temperature, i)
            if not np.allclose(fractional_error, 0.0):
                print("WARNING: fractional error is {:.2f}% at index={:d}".format(fractional_error*100, i))
            self.temperature_dictionary[i] = eq_temperature
            ndensity = self.number_density_over_r[i]
            gamma2 = self.gamma2_over_r[i]
            PL_val = self.PL_indices[i]
            theta_e = Consts.kB_cgs * eq_temperature/Consts.restmass_cgs
            cooling_const = self.cooling_consts_over_r[i]
            eq_gamma1 = self.solve_for_gamma1(eq_temperature, ndensity, gamma2, cooling_const, i, quiet=True)
            self.gamma1_dictionary[i] = eq_gamma1
            # print("Final gamma1: {:.3f}".format(eq_gamma1))
            theta_e = Consts.kB_cgs * eq_temperature/Consts.restmass_cgs
            if self.PL_type == "WUBCN18p":
                eq_peff = PL_val
            else:
                eff_args = (theta_e, eq_gamma1, self.gamma2_over_r[i], PL_val, self.number_density_over_r[i])
                eq_peff = solve_for_PL_eff(eff_args)
            self.peff_dictionary[i] = eq_peff
            eq_norm = self.solve_for_PL_norm(eq_gamma1, theta_e, ndensity, eq_peff)
            self.norm_dictionary[i] = eq_norm

            if self.tsync_const_over_r[i] < self.tIC_const_over_r[i]:
                nt_rate = self.calculate_sync_cooling_rate(eq_norm, eq_gamma1, eq_peff, i)
            else:
                nt_rate = self.calculate_IC_cooling_rate(eq_norm, eq_gamma1, eq_peff, i)
            eq_nonthermal_cooling_rate = nt_rate/self.tau_es[i]

            # TEST
            if self.no_qnt_tau_decrease:
                eq_nonthermal_cooling_rate = nt_rate

            self.nonthermal_cooling_rate_dictionary[i] = eq_nonthermal_cooling_rate

            amplification_factor = calculate_amplification_factor(ndensity, eq_temperature)
            bremsstrahlung_emissivity = calculate_bremsstrahlung_emissivity(ndensity, eq_temperature)
            eq_thermal_cooling_rate = amplification_factor * bremsstrahlung_emissivity
            self.thermal_cooling_rate_dictionary[i] = eq_thermal_cooling_rate
            # -----------------------------------
            # Dump to file every ~100 jumps
            # -----------------------------------
            if i%100 == 0:
                with open(self.pickle_path, 'wb') as fh:
                    pickle.dump(self, fh)

        # -----------------------------------
        # Calculate temperature-dependent quantities
        # -----------------------------------
        self.get_quick_arrays()
        if redo_at_r:
            print("Calculating temperature-dependent quantities...")
            self.calculate_tEnergyLoss_at_gamma1_over_r()
            self.calculate_tThermalize_over_r()
            self.get_total_distribution_function()

        # -----------------------------------
        # Dump to file
        # -----------------------------------
        if redo_at_r:
            with open(self.pickle_path, 'wb') as fh:
                pickle.dump(self, fh)


    def _initialize(self, **kwargs):
        print("Creating new equilibrium solution.")
        self.to_print = kwargs.get("printing", True)
        self.accretion_efficiency = 1.0 + self.bg.FEval
        # --------------------------------------------------------------
        # Input parameters using rough estimates
        # --------------------------------------------------------------
        self.M_in_Msun = kwargs.get("M_in_Msun", 10.0)
        self.f_Ledd = kwargs.get("f_Ledd", 0.1)
        self.f_Mdot = kwargs.get("f_Mdot", 0.1)
        self.H_over_R = kwargs.get("H_over_R", 1.0)
        # Starting/ending radii will influence the heating rate that balances
        # the cooling rate in the equilibrium solver, since the total heating
        # rate is divided by the volume of the relevant region
        self.starting_r = kwargs.get("starting_r", self.bg.rISCO - 0.1) # in rg. Use to avoid density spike at r=1.9rg
        self.ending_r = kwargs.get("ending_r", self.bg.rEH) # in rg. Use to avoid density weirdness at r=1.3rg
        self.Te_estimate = kwargs.get("Te_estimate", 1e9)

        # -----------------------------------
        # Create directories if need be
        # -----------------------------------
        if not os.path.exists(self.path_to_figures):
            os.makedirs(self.path_to_figures)
        if not os.path.exists(self.path_to_figures2):
            os.makedirs(self.path_to_figures2)
        if not os.path.exists(self.path_to_reduced_data):
            os.makedirs(self.path_to_reduced_data)
        if not os.path.exists(self.dir_to_disk_spectrum):
            os.makedirs(self.dir_to_disk_spectrum)

        # -----------------------------------
        # Create shortened arrays
        # -----------------------------------
        inner_index = (np.abs(self.starting_r - self.bg.r_range)).argmin()
        outer_index = (np.abs(self.ending_r - self.bg.r_range)).argmin()
        # outer_index = -1
        # These arrays do not take into account the jump
        # used for faster running
        self.r_range = self.bg.r_range[inner_index:outer_index]
        self.Br_over_r = self.bg.get_Br_in_cgs(self.M_in_Msun, self.f_Ledd, self.f_Mdot, self.H_over_R)[inner_index:outer_index]
        self.mass_density_over_r = self.bg.get_mass_density_in_cgs(self.M_in_Msun, self.f_Ledd, self.f_Mdot, self.H_over_R)[inner_index:outer_index]
        self.number_density_over_r = self.mass_density_over_r / Consts.mp_in_g
        self.UB_over_r = self.Br_over_r**2.0/(8.0*np.pi)
        # Don't calculate assuming equipartition; use virial theorem instead.
        # self.ion_temperature_over_r = self.UB_over_r/self.number_density_over_r/Consts.kB_cgs
        self.ion_temperature_over_r = 2.0/5.0*Consts.G_in_cgs*self.M_in_Msun * Consts.solar_mass_in_g*Consts.mp_in_g/(Consts.kB_cgs*self.r_range*get_rg_in_cm(self.M_in_Msun))
        self.plasma_freq_over_r = 1.0e4*np.sqrt(self.number_density_over_r)
        Br_ISCO = self.bg.get_Br_in_cgs(self.M_in_Msun, self.f_Ledd, self.f_Mdot, self.H_over_R)[0]
        n_ISCO = self.bg.get_mass_density_in_cgs(self.M_in_Msun, self.f_Ledd, self.f_Mdot, self.H_over_R)[0]
        self.ISCO_equipartition_T = Br_ISCO**2/(8.0*np.pi)/n_ISCO/Consts.kB_cgs

        # --------------------------------------------------------------
        # Basic calculated parameters
        # --------------------------------------------------------------
        eddington_luminosity = get_eddington_lum(self.M_in_Msun)
        self.luminosity = self.f_Ledd * eddington_luminosity
        # r1 = self.starting_r * get_rg_in_cm(self.M_in_Msun)
        # r2 = self.ending_r * get_rg_in_cm(self.M_in_Msun)
        r1 = self.r_range[0] * get_rg_in_cm(self.M_in_Msun)
        r2 = self.r_range[-1] * get_rg_in_cm(self.M_in_Msun)
        self.emitting_volume = 4.0*np.pi/3.0*np.abs(r1**3 - r2**3)
        # Photon energy density comes from outermost surface area
        self.Uph_cgs = self.luminosity / (4*np.pi*r2**2.0*Consts.c_in_cgs)
        self.volume_heating_rate = self.calculate_heating_rate(eddington_luminosity)
        self.ratio_over_r = self.UB_over_r/self.Uph_cgs
        self.tsync_const_over_r = self.calculate_tsync_constant()
        self.tIC_const_over_r = self.calculate_tIC_constant()

        # -----------------------------------
        # Create quantities that depend only on the
        # dynamical background, not on the temperature
        # or PIC prescriptions
        # -----------------------------------
        # This definition of electron magnetization from
        # Werner, Philippov, Uzdensky 2019 (cold), also WUBCN18
        self.electron_magnetization_over_r = self.Br_over_r**2.0/(4.0*np.pi*self.number_density_over_r*Consts.restmass_cgs)
        # This definition of magnetization from
        # Werner, Uzdensky, Begelman + 2018 (cold)
        self.ion_magnetization_over_r = self.Br_over_r**2.0/(4.0*np.pi*self.number_density_over_r*Consts.restmass_proton_cgs)
        # This beta parameter is ion only
        # self.ion_beta_over_r = 8.0*self.number_density_over_r*Consts.kB_cgs*self.ISCO_equipartition_T/(self.Br_over_r**2.0)
        self.ion_beta_over_r = 8.0*np.pi*self.number_density_over_r*Consts.kB_cgs*self.ion_temperature_over_r/self.Br_over_r**2.0
        # NOTE gamma_rad is defined for *Compton* cooling, not synchrotron
        # See e.g. Mehlhaff+ 2021 eq. 8
        self.gamma_rad_over_r = np.sqrt(3*0.1*Consts.e_in_cgs*np.abs(self.Br_over_r)/(4.0*Consts.thomson_cross_section_cgs*self.Uph_cgs))
        self.cooling_consts_over_r = np.minimum(self.tIC_const_over_r, self.tsync_const_over_r)
        # Electron scattering optical depth
        self.tau_es = self.number_density_over_r * Consts.thomson_cross_section_cgs * self.H_over_R*self.r_range*get_rg_in_cm(self.M_in_Msun)

        # -----------------------------------
        # Create quantities that depend on the
        # PIC prescriptions but not on the temperature
        # -----------------------------------
        if (self.PL_type == "WUBCN18") or (self.PL_type == "WUBCN18p"):
            # Add 1 to account for cooling
            self.PL_indices = 1.9 + 0.7/np.sqrt(self.ion_magnetization_over_r) + 1
        elif self.PL_type == "WPU19":
            radiaction = self.gamma_rad_over_r/self.electron_magnetization_over_r
            self.PL_indices = 1.6 * np.ones(self.r_range.shape)
            self.PL_indices[radiaction <= 6] = 3.5
        elif self.PL_type == "constant":
            self.PL_indices = self.PL_indices_constant*np.ones(self.r_range.shape)
        else:
            print("ERROR: Unknown prescription " + self.PL_type + " for power-law index.")
            exit()

        if self.gamma2_type == "4sigma":
            self.gamma2_over_r = 4*self.electron_magnetization_over_r
        elif self.gamma2_type == "6sigma":
            self.gamma2_over_r = 6*self.electron_magnetization_over_r
        # elif self.gamma2_type == "WPU19":
            # gamma2_over_r = 1
        elif self.gamma2_type == "fixed":
            self.gamma2_over_r = self.gamma2_constant*np.ones(self.r_range.shape)
        else:
            print("ERROR: Unknown prescription " + self.gamma2_type + " for high-energy cutoff.")
            exit()
        self.get_gamma_bins()

        # -----------------------------------
        # Dump to file
        # -----------------------------------
        with open(self.pickle_path, 'wb') as fh:
            pickle.dump(self, fh)

    def _load_from_pickle(self):
        try:
            with open(self.pickle_path, 'rb') as fh:
                self.__dict__.update(pickle.load(fh).__dict__)
                if not hasattr(self, 'cooling_max_T_dictionary'):
                    self.cooling_max_T_dictionary = {}
                return True
        except:
            return False

    def get_spectral_types(self):
        self.list_of_spectral_quantities = [
            "nonthermal",
            "thermal",
            "disk"
        ]
        self.list_of_spectral_type = ["", "1eV-100keV",
                                      "1keV", "1keV-2keV", "1keV-100keV", "1keV-1MeV",
                                      "2keV-20keV",
                                      "50keV"]
        self.list_of_energy_ranges = {"":[None, None], "1eV-100keV":[0.001, 100.0],
                                      "1keV":[1.0, None], "1keV-2keV": [1.0, 2.0],
                                      "1keV-100keV":[1.0, 100.0], "1keV-1MeV":[1.0, 1000.0],
                                      "2keV-20keV":[2.0, 20.0],
                                      "50keV":[50.0, None]}
        return

    def calculate_heating_rate(self, eddington_luminosity):
        total_energy_dissipated = self.accretion_efficiency * eddington_luminosity
        self.volume_heating_rate = self.electron_heating_fraction * total_energy_dissipated / self.emitting_volume
        return self.volume_heating_rate

    def calculate_tsync_constant(self):
        # Full cooling time = calculate_tsync_constant/gamma
        # Note this is in the ultrarelativistic limit!
        # Rybicki & Lightman problem 6.1
        t_sync_constant = 5.1e8/(self.Br_over_r**2.0)
        return t_sync_constant

    def calculate_tIC_constant(self):
        # Full cooling time = calculate_tIC_constant/gamma
        # Assume is IC cooling time, proportional using ratio of power loss
        return self.ratio_over_r*self.calculate_tsync_constant()


    def solve_for_equilibrium(self, temperature, index):
        ndensity = self.number_density_over_r[index]
        gamma2 = self.gamma2_over_r[index]
        PL_val = self.PL_indices[index]
        theta_e = Consts.kB_cgs * temperature/Consts.restmass_cgs
        cooling_const = self.cooling_consts_over_r[index]

        # Get lower bound based on cooling rates. Only collisional time
        # changes with temperature
        gamma1 = self.solve_for_gamma1(temperature, ndensity, gamma2, cooling_const, index)

        if self.PL_type == "WUBCN18p":
            PL_eff = PL_val
        # Get effective slope by requiring fPL(gamma2) = fPL0(gamma2)
        else:
            eff_args = (theta_e, gamma1, gamma2, PL_val, ndensity)
            PL_eff = solve_for_PL_eff(eff_args)

        # Get normalization by requiring continuity of distribution function
        norm = self.solve_for_PL_norm(gamma1, theta_e, ndensity, PL_eff)

        if self.ratio_over_r[index] < 1:
            nonthermal_cooling_rate = self.calculate_IC_cooling_rate(norm, gamma1, PL_eff, index)
        else:
            nonthermal_cooling_rate = self.calculate_sync_cooling_rate(norm, gamma1, PL_eff, index)
        # Decrease nonthermal cooling rate by 1/tau to emulate compton scattering
        nonthermal_cooling_rate = nonthermal_cooling_rate / self.tau_es[index]

        # TEST
        if self.no_qnt_tau_decrease:
            nonthermal_cooling_rate = nonthermal_cooling_rate * self.tau_es[index]
        # Calculate thermal cooling
        amplification_factor = calculate_amplification_factor(ndensity, temperature)
        bremsstrahlung_emissivity = calculate_bremsstrahlung_emissivity(ndensity, temperature)
        thermal_cooling_rate = amplification_factor * bremsstrahlung_emissivity

        cooling_rate = nonthermal_cooling_rate + thermal_cooling_rate

        # print("heating - cooling:")
        # print((cooling_rate - self.volume_heating_rate)/self.volume_heating_rate)
        # return np.abs((cooling_rate - self.volume_heating_rate)/self.volume_heating_rate)
        return (cooling_rate - self.volume_heating_rate)/self.volume_heating_rate

    def solve_for_gamma1(self, temperature, number_density, gamma2, cooling_const, index, quiet=True, to_check=False):
        theta_e = Consts.kB_cgs * temperature/Consts.restmass_cgs
        minimum_beta1 = np.sqrt(1.0 - 1.0/self.minimum_gamma1**2)
        beta2 = np.sqrt(1.0 - 1.0/gamma2**2)
        # Find the velocity where the electron-electron energy loss frequency is zero
        turnover_beta, turnover_gamma = get_turnover_beta_gamma(theta_e)
        minimum_gamma1 = np.max([self.minimum_gamma1, turnover_gamma])
        max_tcool = cooling_const/minimum_gamma1

        # First check if there is any overlap of tcool and tcoll for this temperature.
        coll_freq_solution = calculate_max_energy_loss_freq(theta_e, number_density)
        min_tcoll = -1.0/coll_freq_solution.fun
        min_coll_beta = coll_freq_solution.x[0]
        # At high enough temperatures, min_coll_beta \to 1.
        # In this case, if min_tcoll is greater than max_tcool, still need
        #  to hit lower bound.
        try:
            min_coll_gamma = 1.0/np.sqrt(1.0 - min_coll_beta**2)
        except:
            if min_tcoll >= max_tcool:
                min_coll_gamma = minimum_gamma1
            else:
                print("Reached maximum beta but still need to solve for gamma1.")
                exit()
        # print("Beta where tcoll is minimum: {:.3f}".format(min_coll_beta))

        if to_check:
            check_max_nuEnergyLoss(number_density, theta_e, (minimum_beta1, beta2))

        if (min_tcoll >= max_tcool) or (min_coll_gamma < minimum_gamma1):
            if not quiet:
                print_str = "No solution for tcoll(gamma1)=tcool(gamma1) at T={:.2e}".format(temperature)
                if self.minimum_gamma1 == minimum_gamma1:
                    print_str += " Setting to manual gamma1 lower bound ({:.2f})".format(self.minimum_gamma1)
                else:
                    print_str += " Setting to turnover gamma1 ({:.2f})".format(turnover_gamma)
                print(print_str)
            return minimum_gamma1
            # return min_coll_gamma

        # Now there is a solution for tcoll(gamma1) = tcool(gamma1).
        # Since tcoll(gamma) is non-monotonic over gamma, we want to bound
        # the solution to have lowest gamma where tcoll is its minimum.
        minimum_gamma1 = min_coll_gamma
        # We'll use the inverses to avoid the infinity at the turnover gamma.
        # Also need to do in terms of beta because beta can be very small.
        def cooling_time_equals_collision_time(beta):
            gamma = 1.0/np.sqrt(1.0 - beta**2)
            energy_loss_freq = -minus_energy_loss_frequency(beta, number_density, theta_e)
            cooling_freq = gamma/cooling_const
            return np.abs(cooling_freq - energy_loss_freq)
        beta_solution = optimize.minimize(cooling_time_equals_collision_time, x0=beta2, bounds=([min_coll_beta, beta2],))
        beta1_sol = beta_solution.x[0]
        gamma1_sol = 1.0/np.sqrt(1.0 - beta1_sol**2)

        if not quiet:
            print(minimum_gamma1)
            print("Minimized to find gamma1={:.3f}. T={:.2e} K".format(gamma1_sol, temperature))

        if gamma1_sol < minimum_gamma1:
            print(gamma1_sol)
            print("Something went wrong in the solver")
            exit()

        if to_check:
            check_cooling_equals_collision_freq(number_density, theta_e, cooling_const, (beta2, beta1_sol))

        return gamma1_sol

    def solve_for_PL_norm(self, gamma1, theta, number_density, PL_val):
        fMJ_at_gamma1 = maxwell_juettner_at_gamma(gamma1, number_density, theta)
        normalization = fMJ_at_gamma1/number_density * gamma1**(PL_val)
        return normalization

    def calculate_IC_cooling_rate(self, normalization, gamma1, p, index, args=None):
        # Calculation in notebook. Assumes PIC = (Uph/UB) Psync
        power_loss_sync = self.calculate_sync_cooling_rate(normalization, gamma1, p, index, args)

        # Need this bit for the interpolation functions
        if args is not None:
            gamma2, Br, ndensity, r_range, radius = args
            ratio_interp_f = interp1d(r_range, self.ratio_over_r, fill_value='extrapolate')
            ratio_at_r = ratio_interp_f(radius)
        else:
            ratio_at_r = self.ratio_over_r[index]
        power_loss_IC = power_loss_sync/ratio_at_r
        return power_loss_IC

    def calculate_sync_cooling_rate(self, normalization, gamma1, p, index, args=None):
        """
        NOTE: we cannot simply use Rybicki & Lightman Eq. 6.36 integrated over frequency and
        angle because that derivation assumes that the frequencies of interest are far away
        from the cutoff frequencies; that is not true for us looking at frequencies close to
        the critical synchrotron frequency.

        Instead of extending the limits of F(x) to 0 and infinity, we will instead
        integrate over frequency *first*, leading to the simple Larmor formula (RL Eq. 6.17b):
        Ptotal = \int dOmega domega dgamma f(gamma)* Power per electron
            = \int dOmega dgamma f(gamma) \int domega Power per electron
            = \int dOmega dgamma f(gamma) 2e^4 B^2 gamma^2 beta^2 sin^2alpha/(3 m^2 c^3)
        The integral over dOmega = sin\alpha d\phi d\alpha gives 8\pi/3.
        """
        if index is not None:
            gamma2 = self.gamma2_over_r[index]
            Br = self.Br_over_r[index]
            ndensity = self.number_density_over_r[index]
        else:
            gamma2, Br, ndensity, r_range, radius = args

        # Deal with singularity (a point, not asymptotic)
        if p == 3.0:
            integral1 = np.log10(gamma2/gamma1)
        else:
            integral1 = (gamma2**(3.0-p) - gamma1**(3.0-p))/(3.0 - p)

        if p == 1.0:
            integral2 = -1.0*np.log10(gamma2/gamma1)
        else:
            integral2 = -1.0*(gamma2**(1.0-p) - gamma1**(1.0-p))/(1.0 - p)

        constant = 16*np.pi*Consts.e_in_cgs**4*Br**2/(9*Consts.me_in_g**2*Consts.c_in_cgs**3)*normalization*ndensity
        return constant*(integral1 + integral2)


    # ----------------------------------------------------------
    # Functions that are useful once the temperature profile is known
    # ----------------------------------------------------------
    def calculate_tEnergyLoss_at_gamma1_over_r(self):
        """
        tEnergyLossEE at gamma1 is also the minimum electron-electron energy loss
        timescale, because gamma1 is chosen to be greater than the turnover gamma
        where the energy loss frequency switches signs.
        """
        # Calculate energy loss timescale at gamma1 as a function of radius.
        beta1 = np.sqrt(1.0 - 1.0/self.gamma1_over_r**2)
        turnover_beta = get_turnover_beta_gamma(self.theta_e_over_r)[0]

        beta1_turnover_inds = np.where(np.isclose(beta1, turnover_beta))[0]
        theta_e = Consts.kB_cgs*self.temperature_over_r/Consts.restmass_cgs
        theta_p = Consts.kB_cgs*self.quick_ion_temperature_over_r/Consts.restmass_proton_cgs

        self.gamma1_ee_tEnergyLoss_over_r = energy_loss_timescale(beta1, self.quick_number_density_over_r, theta_e)
        self.gamma1_ep_tEnergyLoss_over_r = energy_loss_timescale_EP(beta1, self.quick_number_density_over_r, theta_p)

        self.gamma1_ee_tEnergyLoss_over_r[beta1_turnover_inds] = 0.0
        return


    def calculate_tThermalize_over_r(self, recalculate_Ti=True):
        # THERMALIZATION time scale between two maxwellians with different temperatures:
        # the protons with equipartition temperature and the electrons with the calculated
        # temperature that sets cooling and heating equal.
        convert_K_to_eV = 8.617e-5
        if recalculate_Ti:
            self.ion_temperature_over_r = 2.0/5.0*Consts.G_in_cgs*self.M_in_Msun * Consts.solar_mass_in_g*Consts.mp_in_g/(Consts.kB_cgs*self.r_range*get_rg_in_cm(self.M_in_Msun))
            self.get_quick_arrays()
        ion_temperature_eV = self.quick_ion_temperature_over_r*convert_K_to_eV
        electron_temperature_eV = self.temperature_over_r*convert_K_to_eV
        nu_thermalize = 1.8e-19*np.sqrt(Consts.mp_in_g*Consts.me_in_g)*self.quick_number_density_over_r*Consts.coulomb_log/(Consts.me_in_g*ion_temperature_eV+Consts.mp_in_g*electron_temperature_eV)**1.5
        self.t_thermalize = 1.0/nu_thermalize
        # self.t_thermalize = self.temperature_over_r/(nu_thermalize*(ion_temperature - self.temperature_over_r))
        return


    def get_quick_arrays(self):
        # Cut down magnetic field, etc. according to indices in temperature dict
        indices = np.array(list(self.temperature_dictionary.keys()))
        # Just make sure the extraction from dictionary to array is in order.
        sorted_indices = indices[np.argsort(indices)]
        temperature_over_r = list(self.temperature_dictionary.values())
        self.temperature_over_r = np.array(temperature_over_r)[np.argsort(indices)]
        gamma1_inds = np.array(list(self.gamma1_dictionary.keys()))
        gamma1_vals = list(self.gamma1_dictionary.values())
        self.gamma1_over_r = np.array(gamma1_vals)[np.argsort(gamma1_inds)]
        peff_inds = np.array(list(self.peff_dictionary.keys()))
        peff_vals = list(self.peff_dictionary.values())
        self.peff_over_r = np.array(peff_vals)[np.argsort(peff_inds)]
        try:
            self.normalization_over_r = np.array(list(self.norm_dictionary.values()))
        except:
            self.normalization_over_r = None
        self.theta_e_over_r = Consts.kB_cgs * self.temperature_over_r/Consts.restmass_cgs
        nt_cooling_inds = np.array(list(self.nonthermal_cooling_rate_dictionary.keys()))
        nt_vals = list(self.nonthermal_cooling_rate_dictionary.values())
        self.nonthermal_cooling_rate_over_r = np.array(nt_vals)[np.argsort(nt_cooling_inds)]
        thermal_cooling_inds = np.array(list(self.thermal_cooling_rate_dictionary.keys()))
        thermal_vals = list(self.thermal_cooling_rate_dictionary.values())
        self.thermal_cooling_rate_over_r = np.array(thermal_vals)[np.argsort(thermal_cooling_inds)]

        self.quick_r_range = self.r_range[sorted_indices]
        self.quick_number_density_over_r = self.number_density_over_r[sorted_indices]
        self.quick_ion_temperature_over_r = self.ion_temperature_over_r[sorted_indices]
        self.quick_Br_over_r = self.Br_over_r[sorted_indices]
        self.quick_UB_over_r = self.UB_over_r[sorted_indices]
        # self.quick_electron_magnetization_over_r = self.electron_magnetization_over_r[sorted_indices]
        self.quick_cooling_consts = self.cooling_consts_over_r[sorted_indices]
        self.quick_PL_indices = self.PL_indices[sorted_indices]
        self.quick_gamma2_over_r = self.gamma2_over_r[sorted_indices]
        self.quick_tIC_const_over_r = self.tIC_const_over_r[sorted_indices]
        self.quick_tsync_const_over_r = self.tsync_const_over_r[sorted_indices]
        self.quick_tau_es = self.tau_es[sorted_indices]
        self.quick_plasma_freq_over_r = self.plasma_freq_over_r[sorted_indices]
        self.compton_y = 4.0 * self.theta_e_over_r * np.max([self.quick_tau_es, self.quick_tau_es**2.0])

    def plot_gammie_quantities(self):
        figure_dir = self.path_to_figures
        title_str = r"$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$, ".format(self.bg.Fthetaphi, self.bg.a) + self.PL_titstr + ", " + self.gamma2_titstr
        plt.figure()
        plt.plot(self.r_range, self.number_density_over_r)
        plt.ylabel(r'$n~{\rm cm}^{-3}$')
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "number_density_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.ion_temperature_over_r)
        plt.ylabel(r'$T_i$ K')
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "ion_temperature_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.plasma_freq_over_r)
        plt.ylabel(r'$\omega_{pe}~{\rm s}^{-1}$')
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "electron_plasma_freq_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.ratio_over_r)
        plt.ylabel(r'$U_B/U_{ph}$')
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "synchrotron_IC_energy_ratio_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.tIC_const_over_r/self.tsync_const_over_r)
        plt.xlabel(r'$r/r_G$')
        plt.ylabel(r'$t_{\rm IC}/t_{\rm sync}$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "synchrotron_IC_cooling_time_ratio_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.electron_magnetization_over_r)
        plt.ylabel(r"$\sigma_{\rm cold}$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "magnetization_electron_cold_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.ion_magnetization_over_r)
        plt.ylabel(r"$\sigma_{i, \rm{cold}}$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "magnetization_ion_cold_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.ion_beta_over_r)
        plt.ylabel(r"$\beta_i$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "ion_beta_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.gamma_rad_over_r)
        plt.ylabel(r"$\gamma_{\rm rad}$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "gamma_rad_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.gamma_rad_over_r/self.electron_magnetization_over_r)
        plt.ylabel(r"$\gamma_{\rm rad}/\sigma_{\rm e,cold}$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "gamma_rad_over_sigma_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.cooling_consts_over_r)
        plt.ylabel(r'$t_{\rm cool}$ s')
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "cooling_time_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.tau_es)
        plt.ylabel(r'$\tau_{\rm es}$')
        plt.xlabel(r'$r/r_G$')
        plt.ylim(bottom=0)
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "electron_scattering_depth_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.Br_over_r)
        plt.ylabel(r'$B_r$ [G]')
        plt.xlabel(r'$r/r_G$')
        # plt.ylim(bottom=0)
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "Br_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.Br_over_r**2*self.number_density_over_r)
        plt.ylabel(r'$B_r^2n_0$')
        plt.xlabel(r'$r/r_G$')
        plt.ylim(bottom=0)
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "Br2n_over_r.png")
        plt.close()

        fig = plt.figure(figsize=(12,3))
        axs = fig.subplots(1, 4)
        axs[0].plot(self.r_range, self.Br_over_r)
        axs[0].set_ylabel(r"$B_r$ [G]")
        axs[1].plot(self.r_range, self.tau_es)
        if self.tau_es.min() > 1.0:
            axs[1].set_ylim([1.0, None])
        else:
            axs[1].axhline([1.0], color='black', ls='--')
        axs[1].set_ylabel(r'$\tau_{\rm es}$')
        axs[1].set_yscale('log')
        theta_i = Consts.kB_cgs * self.ion_temperature_over_r/Consts.restmass_proton_cgs
        axs[2].plot(self.r_range, theta_i)
        axs[2].set_ylabel(r'$\theta_i$')
        axs[3].plot(self.r_range, self.ion_magnetization_over_r)
        axs[3].set_ylabel(r"$\sigma_{i, \rm{cold}}$")
        # axs[3].set_yscale('log')

        # Calculate decoupling radius
        tinfall_over_r_interp_f = interp1d(self.bg.r_range, self.bg.times_at_radius[::-1])
        tinfall_over_r = tinfall_over_r_interp_f(self.quick_r_range)
        tinfall_over_r_s = tinfall_over_r*self.bg.get_tg_in_s(self.M_in_Msun)
        decoupling_index = (np.abs(self.t_thermalize - tinfall_over_r_s)).argmin()
        self.decoupling_radius = equilibrium.quick_r_range[decoupling_index]
        print("Decoupling radius: {:.2f}".format(self.decoupling_radius))
        # try:
            # self.half_light_radius
        # except:
            # self.get_spectrum_over_r()
        for ax in axs:
            ax.tick_params(top=True, right=True, direction='in', which='both')
            # ax.axvline([self.half_light_radius], color='black', ls='--')
            ax.axvline([self.decoupling_radius], color='black', ls='-.')
            ax.set_xlabel(r'$r/r_g$')
            ax.set_xlim([self.r_range.min(), self.r_range.max()])
        plt.tight_layout()
        plt.savefig(figure_dir + "gammie_quantities.png")
        if not os.path.isdir(figure_dir + "pdfs/"):
            os.makedirs(figure_dir + "pdfs/")
        plt.title('')
        plt.savefig(figure_dir + "pdfs/gammie_quantities.pdf", bbox_inches='tight')
        plt.close()

    def plot_prescription_quantities(self):
        figure_dir2 = self.path_to_figures2
        title_str = r"$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$, ".format(self.bg.Fthetaphi, self.bg.a) + self.PL_titstr + ", " + self.gamma2_titstr

        plt.figure()
        plt.plot(self.r_range, self.PL_indices)
        plt.ylabel(r"Power-law index $p$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "power_law_index_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.gamma2_over_r)
        plt.ylabel(r"High-energy cut-off $\gamma_2$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "high_energy_cutoff_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.Br_over_r**2*self.number_density_over_r*self.gamma2_over_r**(3.0-self.PL_indices), label="$B_r^2n_0\gamma_2^{3-p}$")
        plt.plot(self.r_range, self.Br_over_r**2*self.number_density_over_r*self.gamma2_over_r**(3.0-self.PL_indices)/self.tau_es, label=r"$B_r^2n_0\gamma_2^{3-p}/\tau_{\rm es}$")
        plt.ylabel(r"Cooling rate proportionality")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.yscale('log')
        plt.legend()
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "Qminus_proportionality_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.gamma2_over_r**(2.0-self.PL_indices))
        plt.ylabel(r"$\langle U\rangle$ proportionality $\gamma_2^{2-p}$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.yscale('log')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "gamma2_proportionality_over_r.png")
        plt.close()

    def plot_temperature_dependent_quantities(self):
        figure_dir2 = self.path_to_figures2
        title_str = r"$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$, ".format(self.bg.Fthetaphi, self.bg.a) + self.PL_titstr + ", " + self.gamma2_titstr  + r", $\gamma_{{1, {{\rm min}}}}={:.2f}$".format(self.minimum_gamma1)

        plt.figure()
        plt.plot(self.quick_r_range, self.number_density_over_r, label=r"$n_e$ Gammie")
        plt.plot(self.quick_r_range, self.total_density_over_r, label=r"$n_e$ total")
        plt.plot(self.quick_r_range, self.nonthermal_density_over_r, label=r"$n_{e,PL}$")
        plt.yscale('log')
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.ylabel(r'$n~{\rm cm^{-3}}$')
        plt.title(title_str)
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "total-nonthermal_density_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.quick_r_range, self.total_density_over_r/self.number_density_over_r, ls='--', color='black', label=r"$n_{e,tot}/n_0$")
        plt.plot(self.quick_r_range, self.thermal_density_over_r/self.total_density_over_r, label=r"$n_{e,therm}/n_{e,tot}$")
        plt.plot(self.quick_r_range, self.nonthermal_density_over_r/self.total_density_over_r, label=r"$n_{e,PL}/n_{e,tot}$")
        plt.yscale('log')
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "total-nonthermal_number_fraction_over_r.png")
        plt.close()

        plt.figure()
        initial_val = self.average_energy_over_r[0]
        plt.plot(self.quick_r_range, self.average_energy_over_r/initial_val, label=r"$\langle U_{\rm PL}\rangle/U_0$")
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.ylabel(r'$\langle U\rangle/U_0$')
        plt.title(title_str)
        plt.yscale('log')
        plt.tight_layout()
        plt.savefig(figure_dir2 + "average_energy_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.quick_r_range, self.nonthermal_energy_over_r/self.average_energy_over_r, label=r"$\langle U_{\rm PL}\rangle/\langle U_{\rm tot}\rangle$")
        plt.plot(self.quick_r_range, self.thermal_energy_over_r/self.average_energy_over_r, label=r"$\langle U_{\rm thermal}\rangle/\langle U_{\rm tot}\rangle$")
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        # plt.ylabel(r'$\langle U\rangle$ K')
        plt.title(title_str)
        plt.yscale('log')
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "energy_fraction_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.quick_r_range, self.temperature_over_r)
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.ylabel(r'$T_{eq}$ K')
        plt.title(title_str)
        plt.yscale('log')
        plt.tight_layout()
        plt.savefig(figure_dir2 + "equilibrium_temperature_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.quick_r_range, self.gamma1_over_r, label=r"$\gamma_1$")
        plt.plot(self.quick_r_range, self.minimum_gamma1*np.ones(self.gamma1_over_r.shape), label=r"$\gamma_{1, {\rm min}}$", ls="--", color='black')
        plt.plot(self.quick_r_range, get_turnover_beta_gamma(self.theta_e_over_r)[1], label=r"$\gamma_{\rm turnover}$", ls=':', color='black')
        # plt.plot(self.quick_r_range, 1.0 + self.thermal_energy_over_r/Consts.restmass_cgs, label=r"$\gamma_{1, {\rm min}}$", ls='--', color='black')
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.ylabel(r'$\gamma_1$')
        plt.title(title_str)
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "gamma1_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.quick_r_range, self.peff_over_r, label=r"$p_{\rm eff}$")
        plt.plot(self.quick_r_range, self.quick_PL_indices, ls=":", color='black', label=r"$p(\sigma_i)$")
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        # plt.ylabel(r'$p_{\rm eff}$')
        plt.legend()
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "peff_over_r.png")
        plt.close()

        # Get normalization by requiring continuity of distribution function
        norm = self.solve_for_PL_norm(self.gamma1_over_r, self.theta_e_over_r, self.number_density_over_r, self.peff_over_r)
        plt.figure()
        plt.plot(self.r_range, norm)
        plt.ylabel(r"Power-law Normalization")
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.yscale('log')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "normalization_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.Br_over_r**2*self.number_density_over_r*self.gamma2_over_r**(3.0-self.peff_over_r), label=r"$B_r^2n_0\gamma_2^{3-p_{\rm eff}}$")
        plt.plot(self.r_range, self.Br_over_r**2*self.number_density_over_r*self.gamma2_over_r**(3.0-self.peff_over_r)/self.tau_es, label=r"$B_r^2n_0\gamma_2^{3-p_{\rm eff}}/\tau_{\rm es}$")
        plt.ylabel(r"Cooling rate proportionality ")
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.yscale('log')
        plt.title(title_str)
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "Qminus_proportionalityEff_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.gamma2_over_r**(2.0-self.peff_over_r))
        plt.ylabel(r"$\langle U\rangle$ proportionality $\gamma_2^{2-p_{\rm eff}}$")
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.yscale('log')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "gamma2_proportionalityEff_over_r.png")
        plt.close()

        # Calculate collision times over radius
        tinfall = self.bg.t_infall
        tinfall_s = tinfall * self.bg.get_tg_in_s(self.M_in_Msun)
        # bg.times_at_radius[0] is at THE EVENT HORIZON but bg.r_range[0] is at the ISCO
        # so need to reverse
        tinfall_over_r_interp_f = interp1d(bg.r_range, bg.times_at_radius[::-1])
        tinfall_over_r = tinfall_over_r_interp_f(self.quick_r_range)
        tinfall_over_r_s = tinfall_over_r * self.bg.get_tg_in_s(self.M_in_Msun)
        print("Total infall time: {:.3e} s".format(tinfall_s))
        # Approximate acceleration timescale
        # Using Comisso & Sironi 2019, tacc ~ 3/sigma l/c, where l/c=100 1/wpe0
        # TODO: multiply by 3/sigma, check which sigma.
        t_acceleration = 100/self.quick_plasma_freq_over_r

        plt.figure()
        plt.plot(self.quick_r_range, self.t_thermalize/tinfall_over_r_s, label=r"$t_{\rm therm}^{ei}(r)/t_{\rm infall}(r)$")
        plt.plot(self.quick_r_range, self.gamma1_ee_tEnergyLoss_over_r/tinfall_over_r_s, label=r"$t_{\rm coll}^{ee}(\gamma_1,r)/t_{\rm infall}(r)$")
        plt.plot(self.quick_r_range, self.gamma1_ep_tEnergyLoss_over_r/tinfall_s, label=r"$t_{\rm coll}^{ep}(\gamma_1,r)/t_{\rm infall}$")
        plt.plot(self.quick_r_range, self.quick_cooling_consts/self.gamma1_over_r/tinfall_over_r_s, label=r"$t_{\rm cool}(\gamma_1,r)/t_{\rm infall}(r)$")
        plt.plot(self.r_range, 1.0e2*t_acceleration/tinfall_over_r_s, label=r"$10^2t_{\rm accel}(r)/t_{\rm infall}(r)$")
        plt.gca().axhline([1.0], color='black', ls='--')
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.yscale('log')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.title(title_str)
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "timescales_over_r.png")
        plt.close()

        # Calculate importance of Compton cooling
        # Compton y parameter (assuming non-relativistic thermal electrons)
        plt.figure()
        plt.plot(self.quick_r_range, self.compton_y)
        # plt.plot(self.quick_r_range, self.compton_y, marker='o')
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.ylabel("Compton $y$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "compton_y_over_r.png")
        plt.close()

        # Calculate importance of inverse Compton
        # RL Eq. 7.65b
        xcoh = calculate_xcoh(self.quick_number_density_over_r, self.temperature_over_r)
        plt.figure()
        plt.plot(self.quick_r_range, xcoh)
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.ylabel(r"$x_{\rm coh}$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "xcoh_over_r.png")
        plt.close()

        # RL eq. 7.74b
        amplification_factor = calculate_amplification_factor(self.quick_number_density_over_r, self.temperature_over_r)
        plt.figure()
        plt.plot(self.quick_r_range, amplification_factor)
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.ylabel(r"$A(\rho,T)$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "amplification_factor_over_r.png")
        plt.close()

        # Plot change in fPL0_norm
        fPL0_norm_over_r = self.number_density_over_r * (self.PL_indices - 1.0)/(1.0 - self.gamma2_over_r**(1.0 - self.PL_indices))
        fPL00 = fPL0_norm_over_r[0]
        plt.figure()
        plt.ylabel(r'$A_{\rm PL,0}(r)/A_{\rm PL,0}(r=r_{\rm ISCO})$')
        plt.plot(self.quick_r_range, fPL0_norm_over_r/fPL00, ls='-')
        # plt.plot(self.quick_r_range, self.thermal_cooling_rate_over_r/self.thermal_cooling_rate_over_r[0], label=r"$Q^-_{\rm th}/Q^-_{\rm th,0}$", ls='--')
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.yscale('log')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "APL0_over_r.png")
        plt.close()

        fPL0_at_gamma2 = fPL0_norm_over_r * self.gamma2_over_r**(-self.PL_indices)
        fPL00 = fPL0_at_gamma2[0]
        plt.figure()
        plt.ylabel(r'$f_{\rm PL,0}(r,\gamma_2)/f_{\rm PL,0}(r=r_{\rm ISCO},\gamma_2)$')
        plt.plot(self.quick_r_range, fPL0_at_gamma2/fPL00, ls='-')
        # plt.plot(self.quick_r_range, self.thermal_cooling_rate_over_r/self.thermal_cooling_rate_over_r[0], label=r"$Q^-_{\rm th}/Q^-_{\rm th,0}$", ls='--')
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.yscale('log')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "fPL0_at_gamma2_over_r.png")
        plt.close()

        # cooling_rate_p1 = []
        # cooling_rate_p2 = []
        # cooling_rate_p3 = []
        # for i in np.arange(self.r_range.size):
            # cooling_rate_p1.append(self.calculate_sync_cooling_rate(1.0, self.minimum_gamma1, 1.0, i))
            # cooling_rate_p2.append(self.calculate_sync_cooling_rate(1.0, self.minimum_gamma1, 2.0, i))
            # cooling_rate_p3.append(self.calculate_sync_cooling_rate(1.0, self.minimum_gamma1, 3.0, i))
        # print("p=1: {:.2e}".format(cooling_rate_p1[0]))
        # print("p=2: {:.2e}".format(cooling_rate_p2[0]))
        # print("p=3: {:.2e}".format(cooling_rate_p3[0]))
        # cooling_rate_p1 = np.array(cooling_rate_p1)/cooling_rate_p1[0]
        # cooling_rate_p2 = np.array(cooling_rate_p2)/cooling_rate_p2[0]
        # cooling_rate_p3 = np.array(cooling_rate_p3)/cooling_rate_p3[0]
        # plt.figure()
        # plt.ylabel(r'$Q^-_{\rm NT}(A_0, p)$')
        # plt.plot(self.quick_r_range, cooling_rate_p1, ls='-', label="Fixed $p=1.0$")
        # plt.plot(self.quick_r_range, cooling_rate_p2, ls='-', label="Fixed $p=2.0$")
        # plt.plot(self.quick_r_range, cooling_rate_p3, ls='-', label="Fixed $p=3.0$")
        # plt.xlabel(r'$r/r_G$')
        # plt.legend()
        # plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        # plt.yscale('log')
        # plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        # plt.gca().grid(color='.9', ls='--')
        # plt.title(title_str)
        # plt.tight_layout()
        # plt.savefig(figure_dir2 + "cooling_rate_fixed_pA_over_r.png")
        # plt.close()

        cooling_rate_fixed_norm = []
        for i in np.arange(self.r_range.size):
            cooling_rate_fixed_norm.append(self.calculate_sync_cooling_rate(1.0, self.minimum_gamma1, self.peff_over_r[i], i))
        cooling_rate_fixed_norm = np.array(cooling_rate_fixed_norm)
        Q0 = cooling_rate_fixed_norm[0]
        plt.figure()
        plt.ylabel(r'$Q^-_{\rm NT}(A_0, p_{\rm eff}(r))$')
        plt.plot(self.quick_r_range, cooling_rate_fixed_norm/Q0, ls='-')
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.yscale('log')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "cooling_rate_fixed_norm_over_r.png")
        plt.close()

        cooling_rate_p1 = []
        cooling_rate_p2 = []
        cooling_rate_p3 = []
        for i in np.arange(self.r_range.size):
            cooling_rate_p1.append(self.calculate_sync_cooling_rate(self.normalization_over_r[i], self.minimum_gamma1, 1.0, i))
            cooling_rate_p2.append(self.calculate_sync_cooling_rate(self.normalization_over_r[i], self.minimum_gamma1, 2.0, i))
            cooling_rate_p3.append(self.calculate_sync_cooling_rate(self.normalization_over_r[i], self.minimum_gamma1, 3.0, i))
        print("p=1: {:.2e}".format(cooling_rate_p1[0]))
        print("p=2: {:.2e}".format(cooling_rate_p2[0]))
        print("p=3: {:.2e}".format(cooling_rate_p3[0]))
        cooling_rate_p1 = np.array(cooling_rate_p1)/cooling_rate_p1[0]
        cooling_rate_p2 = np.array(cooling_rate_p2)/cooling_rate_p2[0]
        cooling_rate_p3 = np.array(cooling_rate_p3)/cooling_rate_p3[0]
        plt.figure()
        plt.ylabel(r'$Q^-_{\rm NT}(A(r), p_0)$')
        plt.plot(self.quick_r_range, cooling_rate_p1, ls='-', label="Fixed $p=1.0$")
        plt.plot(self.quick_r_range, cooling_rate_p2, ls='-', label="Fixed $p=2.0$")
        plt.plot(self.quick_r_range, cooling_rate_p3, ls='-', label="Fixed $p=3.0$")
        plt.xlabel(r'$r/r_G$')
        plt.legend()
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.yscale('log')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "cooling_rate_fixed_p_over_r.png")
        plt.close()

        p_range = np.linspace(0.5, 4.5, 1000)
        cooling_rate_r0 = []
        cooling_rate_rf = []
        for i in np.arange(p_range.size):
            cooling_rate_r0.append(self.calculate_sync_cooling_rate(1.0, self.minimum_gamma1, p_range[i], 0))
            cooling_rate_rf.append(self.calculate_sync_cooling_rate(1.0, self.minimum_gamma1, p_range[i], -1))
        plt.figure()
        plt.ylabel(r'$Q^-_{\rm NT}(A_0, p)$')
        plt.plot(p_range, cooling_rate_r0, ls='-', label="$r={:.2f}r_g$".format(self.r_range[0]))
        plt.plot(p_range, cooling_rate_rf, ls='-', label="$r={:.2f}r_g$".format(self.r_range[-1]))
        plt.xlabel(r'$p$')
        plt.legend()
        plt.yscale('log')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "cooling_rate_fixed_norm_over_p.png")
        plt.close()


        # Plot change in nonthermal and thermal cooling over r
        plt.figure()
        plt.axhline([1.0], ls=':', color='black')
        # plt.ylabel(r'$Q^-_{\rm nth}/Q^-_{\rm nth,0}$')
        plt.plot(self.quick_r_range, self.nonthermal_cooling_rate_over_r/self.nonthermal_cooling_rate_over_r[0], label=r"$Q^-_{\rm nth}/Q^-_{\rm nth,0}$", ls='-')
        plt.plot(self.quick_r_range, self.thermal_cooling_rate_over_r/self.thermal_cooling_rate_over_r[0], label=r"$Q^-_{\rm th}/Q^-_{\rm th,0}$", ls='--')
        plt.legend()
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.yscale('log')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "cooling_rate_over_r.png")
        plt.close()

        # Plot thermal vs. nonthermal cooling as fraction of total cooling
        total_cooling = self.nonthermal_cooling_rate_over_r + self.thermal_cooling_rate_over_r
        plt.figure()
        plt.axhline([1.0], ls=':', color='black')
        # plt.plot(adjusted_r_range, thermal_cooling_rate_eq/nt_cooling_rate_eq, ls='-')
        # plt.ylabel(r'$Q^-_{\rm th}/Q^-_{\rm nth}$')
        plt.plot(self.quick_r_range, self.nonthermal_cooling_rate_over_r/total_cooling, label=r"$Q^-_{\rm nth}/Q^-$", ls='-')
        plt.plot(self.quick_r_range, self.thermal_cooling_rate_over_r/total_cooling, label=r"$Q^-_{\rm th}/Q^-$", ls='--')
        plt.legend()
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.yscale('log')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "fractional_cooling_rate_over_r.png")
        plt.close()

        # Calculate distribution function at inner/outer radii
        # Outermost radius
        fPL_outer = self.f_dict["nonthermal"][0]
        fMJ_outer = self.f_dict["thermal"][0]
        # Innermost radius
        last_index = self.quick_r_range.size - 1
        fPL_inner = self.f_dict["nonthermal"][last_index]
        fMJ_inner = self.f_dict["thermal"][last_index]

        fPL0_outer = fPL0_norm_over_r[0]*self.gamma_bins**(-self.PL_indices[0])
        fPL0_inner = fPL0_norm_over_r[-1]*self.gamma_bins**(-self.PL_indices[-1])
        fPL0_outer[self.gamma_bins > self.gamma2_over_r[0]] = 0
        fPL0_inner[self.gamma_bins > self.gamma2_over_r[-1]] = 0

        print("Inner radius: {:.2f}rg. p={:.2f}, peff={:.2f}".format(self.quick_r_range.min(), self.PL_indices[-1], self.peff_over_r[-1]))

        max_val = np.max([fMJ_outer.max(), fPL_outer.max(), fMJ_inner.max(), fPL_inner.max()])
        # color_legend_elements = [Line2D([0], [0], color='C0', label='Power-law component'),
                        # Line2D([0], [0], color='C1', label='Thermal component')]
        # ls_legend_elements = [Line2D([0], [0], ls='-', color='black'),
                            # Line2D([0], [0], ls='--', color='black')]
        plt.figure()
        # plt.plot(self.gamma_bins, self.gamma_bins*fMJ_outer, color='C1', ls='-', label="Thermal component")
        # plt.plot(self.gamma_bins, self.gamma_bins*fPL_outer, color='C0', ls='-', label="Power-law component")
        # plt.plot(self.gamma_bins, self.gamma_bins*fPL0_outer, color='C2', ls='-', label="Initial power-law component")
        plt.plot(self.gamma_bins, fMJ_outer, color=colorblind_colors[1], ls='-', label=r"$f_{\rm MJ}$", lw=3)
        plt.plot(self.gamma_bins, fPL_outer, color=colorblind_colors[0], ls='-', label=r"$f_{\rm PL}$, $\gamma_1<\gamma<\gamma_2$", lw=3)
        # plt.plot(self.gamma_bins, fPL0_outer, color=colorblind_colors[3], ls='-', label=r"$f_{\rm PL,0}$, $1<\gamma<\gamma_2$")
        plt.xscale('log')
        plt.yscale('log')
        # plt.legend(frameon=False, loc='upper right')
        # plt.ylim([1, max_val*1.1])
        # plt.ylim([1, 1e20])
        plt.ylim([1, 1e20])
        plt.xlim([None, 3e3])
        plt.xlabel(r'$\gamma$')
        # plt.ylabel(r"$\gamma f(\gamma)$")
        plt.ylabel(r"$f(\gamma)$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        # plt.legend(title=r"$r={:.2f}r_g$".format(self.quick_r_range[0]))
        # plt.legend()
        # plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        # plt.savefig(figure_dir2 + "electron_distribution_function_outer.png")
        plt.savefig(figure_dir2 + "electron_distribution_function_outer_with_fPL0.png")
        if not os.path.isdir(figure_dir2 + "pdfs/"):
            os.makedirs(figure_dir2 + "pdfs/")
        plt.title('')
        plt.savefig(figure_dir2 + "pdfs/electron_distribution_function_outer_with_fPL0.pdf", bbox_inches='tight')
        plt.close()

        plt.figure()
        # plt.plot(self.gamma_bins, self.gamma_bins*fMJ_inner, color='C1', ls='-', label="Thermal component at $r={:.2f}r_g$".format(self.quick_r_range[-1]))
        # plt.plot(self.gamma_bins, self.gamma_bins*fPL_inner, color='C0', ls='-', label="Power-law component at $r={:.2f}r_g$".format(self.quick_r_range[-1]))
        # plt.plot(self.gamma_bins, self.gamma_bins*fPL0_inner, color='C2', ls='-', label="Initial power-law component")
        plt.plot(self.gamma_bins, fPL0_inner, color=colorblind_colors[3], ls='--', label=r"$f_{\rm PL,0}(\gamma)$, $1<\gamma<\gamma_2$")
        plt.plot(self.gamma_bins, fPL_inner, color=colorblind_colors[0], ls='-', label=r"$f_{\rm PL}(\gamma)$, $\gamma_1<\gamma<\gamma_2$")
        plt.plot(self.gamma_bins, fMJ_inner, color=colorblind_colors[1], ls='-', label=r"$f_{\rm MJ}(\gamma)$")
        # plt.plot(self.gamma_bins, PLi+fMJi, color='black', label="Total")
        plt.xscale('log')
        plt.yscale('log')
        # plt.ylim([1, max_val*1.1])
        plt.ylim([1, 1e20])
        plt.xlabel(r'$\gamma$')
        plt.ylabel(r"$f(\gamma)$")
        # plt.ylabel(r"$\gamma f(\gamma)$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.legend(frameon=False, loc='upper right')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        # plt.savefig(figure_dir2 + "electron_distribution_function_inner.png")
        plt.savefig(figure_dir2 + "electron_distribution_function_inner_with_fPL0.png")
        if not os.path.isdir(figure_dir2 + "pdfs/"):
            os.makedirs(figure_dir2 + "pdfs/")
        plt.title('')
        plt.savefig(figure_dir2 + "pdfs/electron_distribution_function_inner_with_fPL0.pdf", bbox_inches='tight')
        plt.close()

        fig = plt.figure(figsize=(12,3))
        axs = fig.subplots(1, 4)
        axs[0].plot(self.quick_r_range, self.quick_gamma2_over_r)
        axs[0].set_ylabel(r"High-energy cut-off $\gamma_2$")
        axs[0].set_yscale('log')
        if self.quick_gamma2_over_r.min() > 1e3:
            axs[0].set_ylim([1e3, None])
        axs[1].plot(self.quick_r_range, self.peff_over_r, label=r"$p_{\rm eff}$")
        axs[1].plot(self.quick_r_range, self.quick_PL_indices, ls=":", color='black', label=r"$p(\sigma_i)$")
        axs[1].legend(frameon=False)
        axs[2].plot(self.quick_r_range, self.theta_e_over_r)
        axs[2].set_ylabel(r'$\theta_e$')
        axs[3].plot(self.quick_r_range, self.nonthermal_cooling_rate_over_r/total_cooling, label=r"$Q^-_{\rm PL}/Q^-$", ls='-')
        axs[3].plot(self.quick_r_range, self.thermal_cooling_rate_over_r/total_cooling, label=r"$Q^-_{\rm MJ}/Q^-$", ls='--')
        # axs[3].axhline([1.0], color='black', ls='--')
        axs[3].legend(frameon=False)
        plt.yscale('log')
        # Calculate decoupling radius
        tinfall_over_r_interp_f = interp1d(self.bg.r_range, self.bg.times_at_radius[::-1])
        tinfall_over_r = tinfall_over_r_interp_f(self.quick_r_range)
        tinfall_over_r_s = tinfall_over_r*self.bg.get_tg_in_s(self.M_in_Msun)
        decoupling_index = (np.abs(self.t_thermalize - tinfall_over_r_s)).argmin()
        self.decoupling_radius = equilibrium.quick_r_range[decoupling_index]
        try:
            self.half_light_radius
        except:
            self.get_spectrum_over_r()
        for ax in axs:
            ax.tick_params(top=True, right=True, direction='in', which='both')
            ax.axvline([self.half_light_radius], color='black', ls='--')
            ax.axvline([self.decoupling_radius], color='black', ls='-.')
            ax.set_xlabel(r'$r/r_g$')
            ax.set_xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.tight_layout()
        print(self.half_light_radius)
        print(self.decoupling_radius)
        plt.savefig(figure_dir2 + "prescription_quantities.png")
        if not os.path.isdir(figure_dir2 + "pdfs/"):
            os.makedirs(figure_dir2 + "pdfs/")
        plt.title('')
        plt.savefig(figure_dir2 + "pdfs/prescription_quantities.pdf", bbox_inches='tight')
        plt.close()

    def plot_energy_scales(self):
        figure_dir2 = self.path_to_figures2
        title_str = r"$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$, ".format(self.bg.Fthetaphi, self.bg.a) + self.PL_titstr + ", " + self.gamma2_titstr  + r", $\gamma_{{1, {{\rm min}}}}={:.2f}$".format(self.minimum_gamma1)

        gamma_KN = 100 # Mehlhaff+ 2021 eq. 117
        john_gamma_rad = 6e3
        john_gamma_max = 3e9 # eq 125
        john_gamma_cool = 0.01
        john_sigma_min = 100; john_sigma_max = 1e4; john_sigma = 1e3;
        john_avg_gamma = john_sigma/4.0
        john_gamma_X = 4.0*john_sigma
        john_gamma_cool = john_gamma_rad**2/john_gamma_max

        gamma_rad = self.gamma_rad_over_r
        gamma_max = 0.1*Consts.e_in_cgs * np.abs(self.Br_over_r)*self.r_range*get_rg_in_cm(self.M_in_Msun)/(Consts.restmass_cgs)
        gamma_cool = gamma_rad**2/gamma_max
        avg_gamma = self.electron_magnetization_over_r/4.0
        gamma_X = 4.0*self.electron_magnetization_over_r
        gamma_cool = gamma_rad**2/gamma_max

        sigma0 = self.electron_magnetization_over_r[0]
        gamma_rad_over_sigma = gamma_rad[0]/sigma0
        gammaKN_over_sigma = gamma_KN/sigma0
        # print(gamma_rad_over_sigma) # 18
        # print(gammaKN_over_sigma) # 0.2
        # print(np.log10(gamma_rad_over_sigma))
        # print(np.log10(gammaKN_over_sigma))
        pair_optical_depth = 3.0*gamma_KN/(5.0*gamma_cool)
        print(pair_optical_depth[0])


        plt.figure()
        plt.plot(self.r_range, gamma_max, label=r"$\gamma_{\rm max}$", color="C0")
        plt.plot(self.r_range, gamma_rad, label=r"$\gamma_{\rm rad}$", color="C1")
        plt.plot(self.r_range, gamma_X, label=r"$\gamma_{\rm X}$", color="C3")
        plt.plot(self.r_range, avg_gamma, label=r"$\langle\gamma\rangle$", color="C4")
        plt.plot(self.r_range, gamma_cool, label=r"$\gamma_{\rm cool}$", color="C2")
        plt.gca().axhline([john_gamma_max], label=None, color="C0", ls="--")
        plt.gca().axhline([john_gamma_rad], label=None, color="C1", ls="--")
        plt.gca().axhline([john_gamma_cool], label=None, color="C2", ls="--")
        plt.gca().axhline([john_gamma_X], label=None, color="C3", ls="--")
        plt.gca().axhline([john_avg_gamma], label=None, color="C4", ls="--")
        plt.yscale('log')
        plt.xlabel(r'$r/r_G$')
        plt.xlim([self.quick_r_range.min(), self.quick_r_range.max()])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.ylabel(r'Lorentz factor')
        plt.title(title_str)
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "lorentz_factors_over_r.png")
        plt.close()

        constant_val = Consts.solar_mass_in_g*Consts.G_in_cgs*Consts.e_in_cgs*np.sqrt(8.0*np.pi)/(Consts.me_in_g*Consts.c_in_cgs**4)*np.sqrt(0.04*2e15)
        print("{:.2e}".format(constant_val))

    def get_spectral_bins(self):
        """
        Old version: calculated numin/numax from the power-law emission.
        That resulted in different spectral bins depending on Fthetaphi, which then
        also messed up the disk spectra.

        New version: make spectral bins the same for every spectrum.
        """
        # omega_const = 3*Consts.e_in_cgs*np.abs(self.quick_Br_over_r)/(2*Consts.me_in_g*Consts.c_in_cgs)
        # nu1_list = omega_const*self.gamma1_over_r**2/(2.0*np.pi)
        # nu2_list = omega_const*self.quick_gamma2_over_r**2/(2.0*np.pi)
#
        # nu_min = nu1_list.min()
        # nu_max = nu2_list.max()
        # nu_max = np.max([nu_max, 1e22])
        # nu_min = np.min([nu_min, 1e14])
        # spectral_bins = np.logspace(np.log10(nu_min), np.log10(nu_max), 10000)
        # New version
        # nu_max = 5.0e23
        # nu_min = 1.0e14
        # spectral_bins = np.logspace(np.log10(nu_min), np.log10(nu_max), 10000)
        spectral_bins = get_fixed_spectral_bins()
        return spectral_bins

    def retrieve_spectra(self, quiet=False, spectrum_decrease_by_tau=False, overwrite=False, inc_angle_deg=60, **kwargs):
        redshift = inc_angle_deg is not None
        spectrum_path = self.spectrum_path_base
        disk_spectrum_path = self.dir_to_disk_spectrum
        disk_only = kwargs.get("disk_only", False)

        if not os.path.exists(self.dir_to_disk_spectrum):
            os.makedirs(self.dir_to_disk_spectrum)
        if redshift:
            spectrum_path += "_redshifted_i{:d}".format(inc_angle_deg)
            disk_spectrum_path += "redshifted_i{:d}.p".format(inc_angle_deg)
        else:
            disk_spectrum_path += "not_redshifted.p"
        spectrum_path += ".p"

        if not disk_only:
            if overwrite or (not os.path.exists(spectrum_path)):
                print("Calculating power-law spectrum " + spectrum_path)
                if redshift:
                    spectrum_dict = self.calculate_redshifted_spectra(inc_angle_deg, spectrum_decrease_by_tau)
                else:
                    spectrum_dict = self.calculate_spectra(quiet, spectrum_decrease_by_tau)
            else:
                print("Loading power-law spectrum from pickle file " + spectrum_path)
                with open(spectrum_path, 'rb') as file:
                    spectrum_dict = pickle.load(file)
        else:
            spectrum_dict = {}

        if overwrite or (not os.path.exists(disk_spectrum_path)):
            print("Calculating disk spectrum " + disk_spectrum_path)
            if redshift:
                disk_spectrum_dict = self.calculate_redshifted_disk_spectrum(inc_angle_deg)
            else:
                disk_spectrum_dict = self.calculate_spectra(quiet, spectrum_decrease_by_tau)
        else:
            print("Loading disk spectrum from pickle file " + disk_spectrum_path)
            with open(disk_spectrum_path, 'rb') as file:
                disk_spectrum_dict = pickle.load(file)
        return (spectrum_dict, disk_spectrum_dict)


    def retrieve_spectral_power(self, quiet=False, spectrum_decrease_by_tau=False, overwrite=False, inc_angle_deg=60):
        self.spectral_power = {"inc_angle_deg":inc_angle_deg}
        (spectrum_dict, disk_spectrum_dict) = self.retrieve_spectra(quiet, spectrum_decrease_by_tau, overwrite, inc_angle_deg)
        self.get_spectral_types()
        spectral_bins = self.get_spectral_bins()

        for quantity in self.list_of_spectral_quantities:
            for spectral_type in self.list_of_spectral_type:
                if "disk" in quantity:
                    quantity_spectrum = disk_spectrum_dict["Lnu_" + quantity]
                else:
                    quantity_spectrum = spectrum_dict["Lnu_" + quantity]
                # Cut spectra between specified min/max indices
                keV_min, keV_max = self.list_of_energy_ranges[spectral_type]
                if keV_min is None:
                    min_ind = 0
                else:
                    min_ind = (np.abs(spectral_bins - kevToNu(keV_min))).argmin()
                if keV_max is None:
                    max_ind = -1
                else:
                    max_ind = (np.abs(spectral_bins - kevToNu(keV_max))).argmin()

                cut_spectral_bins = spectral_bins[min_ind:max_ind]
                cut_spectrum = quantity_spectrum[min_ind:max_ind]

                # Find the integrated luminosity
                spectrum_power = np.trapz(cut_spectrum*cut_spectral_bins, x=np.log(cut_spectral_bins))
                self.spectral_power["power_in_" + spectral_type + "_" + quantity] = spectrum_power

        return


    def plot_spectra(self, quiet=False, spectrum_decrease_by_tau=False, overwrite=False, inc_angle_deg=60, **kwargs):
        redshift = inc_angle_deg is not None
        figdir = kwargs.get("figdir", self.path_to_figures2)
        dpi = kwargs.get("dpi", 150)
        if not os.path.exists(figdir):
            os.makedirs(figdir)
        fname = "spectrum_added_over_r"
        title = kwargs.get("title", True)
        if spectrum_decrease_by_tau:
            fname += "_withSpectrumTauDecrease"
        if redshift:
            fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
        figname = kwargs.get("figname", fname)
        pdf_replace_str = kwargs.get("pdf_replace_str", None)
        xlims = kwargs.get("xlims", None)
        ylims = kwargs.get("ylims", None)
        keV2_in_Hz = kevToNu(2.0)
        keV20_in_Hz = kevToNu(20.0)
        MeV_in_Hz = kevToNu(1000.0)
        # axvspans: y-coordinates for shaded gray regions
        # axvspans = [[keV2_in_Hz, keV20_in_Hz], [MeV_in_Hz, None]]
        axvspans = [[MeV_in_Hz, None]]
        axvspans = kwargs.get("axvspans", axvspans)

        spectrum_dict, disk_spectrum_dict = self.retrieve_spectra(quiet, spectrum_decrease_by_tau, overwrite, inc_angle_deg)

        Lnu_thermal = spectrum_dict["Lnu_thermal"]
        Lnu_nonthermal = spectrum_dict["Lnu_nonthermal"]
        spectral_bins = spectrum_dict["spectral_bins"]
        total_spectrum = Lnu_nonthermal + Lnu_thermal
        Lnu_disk = disk_spectrum_dict["Lnu_disk"]
        total_spectrum += Lnu_disk

        max_val = (total_spectrum*spectral_bins).max()
        # max_val = (Lnu_thermal*spectral_bins).max()
        # label_str = r"$\nu L_{{\nu, {{\rm disk}}}}(T(r))$"

        plt.figure()
        plt.plot(spectral_bins, Lnu_nonthermal*spectral_bins,
                # label=r'$\nu L_{\nu, {\rm PL}}$', color=colorblind_colors[0], ls="-")
                label=r'Nonthermal electrons', color=colorblind_colors[0], ls="-")
        plt.plot(spectral_bins, Lnu_thermal*spectral_bins,
                 # label=r'$\nu L_{\nu, {\rm MJ}}$', color=colorblind_colors[1], ls=':')
                 label=r'Thermal electrons', color=colorblind_colors[1], ls=':')
        plt.plot(spectral_bins, Lnu_disk*spectral_bins,
                 # label=label_str, color=colorblind_colors[2], ls='--')
                 label="Thin disk", color=colorblind_colors[2], ls='--')
        plt.plot(spectral_bins, total_spectrum*spectral_bins, color='black', label='Total', lw=1)
        plt.xscale('log')
        plt.yscale('log')
        plt.xlabel(r"Frequency $\nu$ [Hz]")
        plt.ylabel(r"Monochromatic Luminosity $\nu L_\nu$ [erg/s]")
        title_str = r"$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$, $\gamma_2=$".format(self.bg.Fthetaphi, self.bg.a) + self.gamma2_str + ", " + self.PL_titstr + r", $\gamma_{{1, {{\rm min}}}}={:.2f}$".format(self.minimum_gamma1) + "\n"
        if redshift:
            title_str += r"Redshifted, $i=${:.0f} degrees".format(inc_angle_deg)
        else:
            title_str += "Not Redshifted"
        if title:
            plt.title(title_str)
        plt.gca().tick_params(right=True, direction='in', which='both')
        secax = plt.gca().secondary_xaxis('top', functions=(nuToKev, kevToNu))
        secax.tick_params(top=True, direction='in', which='both')
        secax.set_xlabel(r'$\nu$ [keV]')
        if ylims is None:
            plt.ylim([1e34, max_val*1.1])
            # plt.ylim([1e34, 2e38])
            # plt.ylim([1e30, 3e38])
        else:
            plt.ylim(ylims)
        if xlims is None:
            plt.xlim([1e15, 2e23])
            # plt.xlim([spectral_bins.min(), spectral_bins.max()])
            # plt.xlim([1e15, 2e19])
        else:
            plt.xlim(xlims)
        for vspans in axvspans:
            if vspans[0] is None:
                vspans[0] = plt.gca().get_xlim()[0]
            if vspans[1] is None:
                vspans[1] = plt.gca().get_xlim()[1]
            plt.gca().axvspan(vspans[0], vspans[1], color='gray', alpha=0.3, hatch='/')
        plt.legend(frameon=True)
        plt.tight_layout()
        print("Saving in " + figdir + figname)
        plt.savefig(figdir + figname, dpi=dpi)
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.title('')
        if pdf_replace_str:
            plt.savefig(figdir + "pdfs/" + figname.replace(pdf_replace_str, ".pdf"), bbox_inches='tight', dpi=dpi)
        else:
            plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
        if not quiet:
            self.retrieve_spectral_power(quiet, spectrum_decrease_by_tau, overwrite, inc_angle_deg)
            print(self.spectral_power)

            total_luminosity = 0.0
            pr_luminosity = 0.0
            for quantity in self.list_of_spectral_quantities:
                total_luminosity += self.spectral_power["power_in__" + quantity]
                if "disk" not in quantity:
                    pr_luminosity += self.spectral_power["power_in__" + quantity]
            total_heating = self.volume_heating_rate * self.emitting_volume

            # print("L_PL/L_thermal_pr: {:.4e}".format(power_in_powerlaw/power_in_thermal_pr))
            # print("L_PL/L_disk: {:.4e}".format(power_in_powerlaw/power_in_disk))
            # print("L_PL(nu > 1keV)/L_disk(nu>1keV): {:.4e}".format(Xray_power_in_powerlaw/Xray_power_in_disk))
            # print("L_PL(nu > 1keV)/L_thermal(nu>1keV): {:.4e}".format(Xray_power_in_powerlaw/Xray_power_in_thermal_pr))
            # print("L_PL(1keV < nu < 1MeV)/L_disk(1keV < nu < 1MeV): {:.4e}".format(Xray_nopair_power_in_powerlaw/Xray_power_in_disk))
            # print("L_PL(1keV < nu < 1MeV)/L_thermal(1keV < nu < 1MeV): {:.4e}".format(Xray_nopair_power_in_powerlaw/Xray_power_in_thermal_pr))
            # print("L_PL/P_tot: {:.4e}".format(power_in_powerlaw/total_power))
            # print("Integrated Lnu from plunging region: {:.4e}".format(power_in_powerlaw + power_in_thermal_pr))
            print("Total disk luminosity: {:.4e} erg/s".format(self.spectral_power["power_in__disk"]))
            print("Total observed luminosity: {:.4e}".format(total_luminosity))
            print("Total observed plunging region luminosity: {:.4e}".format(pr_luminosity))
            print("Heating rate*volume: {:.4e}".format(total_heating))


    def get_spectrum_over_r(self, quiet=False, spectrum_decrease_by_tau=False, overwrite=False, inc_angle_deg=60, num_bins=100):
        redshift = inc_angle_deg is not None
        spectrum_dict, disk_spectrum_dict = self.retrieve_spectra(quiet, spectrum_decrease_by_tau, overwrite, inc_angle_deg)

        # Need to sort the emitted radii
        photon_emitted_radii = spectrum_dict["photon_emitted_radii"]
        g3I_nonthermal = spectrum_dict["g3I_nonthermal_over_r"]
        g3I_thermal = spectrum_dict["g3I_thermal_over_r"]
        I_nonthermal = spectrum_dict["I_nonthermal_over_r"]
        I_thermal = spectrum_dict["I_thermal_over_r"]
        dalpha = spectrum_dict["dalpha"]
        dbeta = spectrum_dict["dbeta"]

        sorted_radii_inds = np.argsort(photon_emitted_radii)
        sorted_radii = photon_emitted_radii[sorted_radii_inds]
        sorted_g3I_nt = g3I_nonthermal[sorted_radii_inds]
        sorted_g3I_thermal = g3I_thermal[sorted_radii_inds]
        sorted_I_nt = I_nonthermal[sorted_radii_inds]

        # Bin into evenly-spaced radii and sum to get flux.
        # Since this is flux on the camera, we need to multiply by dalpha*dbeta
        # to get the portion of flux coming from that radial bin.
        # We want to plot F(r)=\int_rEH^r DeltaF so we need to cumsum.
        delta_luminosity, bin_edges = np.histogram(sorted_radii, bins=num_bins, weights=sorted_g3I_nt*dalpha*dbeta)
        delta_luminosity_nog, bin_edges = np.histogram(sorted_radii, bins=num_bins, weights=sorted_I_nt*dalpha*dbeta)
        delta_luminosity_thermal, bin_edges = np.histogram(sorted_radii, bins=num_bins, weights=sorted_g3I_thermal*dalpha*dbeta)
        radii_bins = (bin_edges[1:] + bin_edges[:-1])/2.0
        # F(r) has 1/D^2 in it so convert to luminosity
        # by multiplying by 4\pi D^2.
        luminosity_over_r = np.cumsum(delta_luminosity)*4.0*np.pi
        luminosity_over_r_nog = np.cumsum(delta_luminosity_nog)*4.0*np.pi
        luminosity_over_r_thermal = np.cumsum(delta_luminosity_thermal)*4.0*np.pi

        total_luminosity_over_r = luminosity_over_r + luminosity_over_r_thermal
        total_luminosity = total_luminosity_over_r[-1]
        normalized_total_luminosity = total_luminosity_over_r/total_luminosity
        # With cumsum, half-light radius is super easy to calculate.
        # Here, define as 50% of the TOTAL luminosity (thermal + nonthermal).
        # Could also do just power-law though.
        half_light_ind = (np.abs(normalized_total_luminosity - 0.5)).argmin()
        ten_light_ind = (np.abs(normalized_total_luminosity - 0.1)).argmin()
        self.half_light_radius = radii_bins[half_light_ind]
        self.ten_light_radius = radii_bins[ten_light_ind]

        # An extra plot, just to show where photons are emitted
        plt.figure()
        plt.gca().hist(photon_emitted_radii, bins=num_bins)
        plt.gca().set_xlabel("$r/r_g$")
        plt.gca().set_ylabel("Counts")
        plt.tight_layout()
        plt.savefig(self.path_to_figures2 + "photon_emission_radii_histogram.png")
        plt.close()

        # # Save to pickle
        # spectrum_path = self.path_to_reduced_data + "spectrum"
        # if spectrum_decrease_by_tau:
            # spectrum_path += "_withSpectrumTauDecrease"
        # spectrum_path += "_gamma1minimum{:.2f}".format(self.minimum_gamma1)
        # spectrum_path += ".p"
        # with open(spectrum_path, 'wb') as file:
            # pickle.dump(spectrum_dict, file)

        print("Total luminosity integrated over r: {:.2e} erg/s".format(luminosity_over_r[-1]))
        # print("From other spectrum: {:.2e} erg/s".format(spectrum_dict["power_in_powerlaw"]))

        return (luminosity_over_r, luminosity_over_r_thermal, luminosity_over_r_nog, radii_bins)

    def plot_spectra_over_r(self, quiet=False, spectrum_decrease_by_tau=False, overwrite=False, inc_angle_deg=60, num_bins=100, **kwargs):
        redshift = inc_angle_deg is not None
        pdf_replace_str = kwargs.get("pdf_replace_str", None)
        figdir = kwargs.get("figdir", self.path_to_figures2)
        dpi = kwargs.get("dpi", 150)
        fname = "luminosity_over_r"
        if spectrum_decrease_by_tau:
            fname += "_withSpectrumTauDecrease"
        if redshift:
            fname += "_redshifted_i{:d}".format(inc_angle_deg)
        fname += "_nBins{:d}.png".format(num_bins)
        if spectrum_decrease_by_tau:
            fname += "_withSpectrumTauDecrease"
        if redshift:
            fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
        figname = kwargs.get("figname", fname)
        title = kwargs.get("title", True)
        xlims = kwargs.get("xlims", None)
        ylims = kwargs.get("ylims", None)

        luminosity_over_r, luminosity_over_r_thermal, luminosity_over_r_nog, radii_bins = self.get_spectrum_over_r(quiet, spectrum_decrease_by_tau, overwrite, inc_angle_deg, num_bins)

        total_luminosity_over_r = luminosity_over_r + luminosity_over_r_thermal
        total_luminosity = total_luminosity_over_r[-1]
        normalized_total_luminosity = total_luminosity_over_r/total_luminosity
        normalized_luminosity = luminosity_over_r/total_luminosity
        normalized_luminosity_nog = luminosity_over_r_nog/luminosity_over_r_nog[-1]
        normalized_luminosity_thermal = luminosity_over_r_thermal/total_luminosity

        interp_physical_to_norm = interp1d(total_luminosity_over_r, normalized_total_luminosity, fill_value='extrapolate')
        interp_norm_to_physical = interp1d(normalized_total_luminosity, total_luminosity_over_r, fill_value='extrapolate')

        # Calculate decoupling radius
        tinfall_over_r_interp_f = interp1d(self.bg.r_range, self.bg.times_at_radius[::-1])
        tinfall_over_r = tinfall_over_r_interp_f(self.quick_r_range)
        tinfall_over_r_s = tinfall_over_r*self.bg.get_tg_in_s(self.M_in_Msun)
        decoupling_index = (np.abs(self.t_thermalize - tinfall_over_r_s)).argmin()
        self.decoupling_radius = self.quick_r_range[decoupling_index]

        plt.figure()
        plt.plot(radii_bins, luminosity_over_r, color=colorblind_colors[0], lw=2, label=r"$L_{\rm PL}$")
        # plt.plot(radii_bins, luminosity_over_r, color=colorblind_colors[0], lw=2, label=r"Nonthermal Electrons")
        plt.gca().axvline([self.half_light_radius], color='black', ls='--', label=r"$r_{{1/2}}$")
        # plt.plot(radii_bins, luminosity_over_r + luminosity_over_r_thermal, color="black", lw=2)
        # plt.gca().axvline([self.half_light_radius], color='black', ls='--', label=r"$r_{{1/2}}/r_g={:.2f}$".format(half_light_radius))
        # plt.gca().axvline([self.ten_light_radius], color='black', ls=':', label=r"$r_{{10}}/r_g={:.2f}$".format(ten_light_radius))
        plt.gca().axvline([self.ten_light_radius], color='black', ls=':', label=r"$r_{{1/10}}$")
        plt.plot(radii_bins, luminosity_over_r_thermal, color=colorblind_colors[1], lw=2, label=r"$L_{\rm MJ}$")
        # plt.plot(radii_bins, luminosity_over_r_thermal, color=colorblind_colors[1], lw=2, label=r"Thermal Electrons")
        plt.gca().axvline([self.decoupling_radius], color='black', ls='-.', label=r"$r_{\rm decoupling}$")
        plt.yscale('log')
        plt.xlabel(r"Radius $r/r_g$ ")
        plt.ylabel("Luminosity [erg/s]")
        if ylims is None:
            plt.ylim([1e31, 1.5e37])
        else:
            plt.ylim(ylims)
        plt.gca().tick_params(right=True, direction='in', which='both')
        plt.legend(frameon=False, ncol=2)

        physical_min, physical_max = plt.ylim()
        norm_axis_max = interp_physical_to_norm(physical_max)
        norm_axis_min = interp_physical_to_norm(physical_min)
        secax = plt.gca().twinx()
        secax.set_yscale('log')
        secax.set_ylim([norm_axis_min, norm_axis_max])
        # secax.set_ylabel(r"$L(r)/L(r_{\rm ISCO})$")
        secax.set_ylabel(r"Fraction of Total Luminosity")
        secax.tick_params(right=True, direction='in', which='both')
        # secax.plot(radii_bins, normalized_luminosity_nog, color="C1", label="Not redshifted")
        # plt.legend()
        if xlims is None:
            plt.xlim([radii_bins.min(), radii_bins.max()])
        else:
            plt.xlim(xlims)

        title_str = r"$F_{{\theta\phi}}={:.2f}$, $\gamma_2=$".format(self.bg.Fthetaphi) + self.gamma2_str + ", " + self.PL_titstr + r", $\gamma_{{1, {{\rm min}}}}={:.2f}$".format(self.minimum_gamma1) + "\n"
        if redshift:
            title_str += r"Redshifted, $i={:.0f}$ degrees".format(inc_angle_deg)
        else:
            title_str += "Not Redshifted"
        title_str += "\n Half-light radius={:.2f}rg, 10\%={:.2f}rg".format(self.half_light_radius, self.ten_light_radius)
        if title:
            plt.title(title_str)
        # plt.gca().grid(color='.9', ls='--')

        plt.tight_layout()

        print("Saving in " + figdir + figname)
        plt.savefig(figdir + figname, dpi=dpi)
        if not os.path.isdir(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        plt.title('')
        if pdf_replace_str:
            plt.savefig(figdir + "pdfs/" + figname.replace(pdf_replace_str, ".pdf"), bbox_inches='tight', dpi=dpi)
        else:
            plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
        plt.close()

    def plot_interpolations_over_r(self, quiet=False, spectrum_decrease_by_tau=False, overwrite=False, inc_angle_deg=60):
        figure_dir = self.path_to_figures2 + "interp_at_photon_radii/"
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)
        # Load in camera properties
        redshift_dict = self.trace_geodesics(inc_angle_deg)
        photon_emitted_radii = redshift_dict["photon_emitted_radii"]
        redshifts = redshift_dict["redshifts"]
        dalpha = redshift_dict["dalpha"]
        dbeta = redshift_dict["dbeta"]

        # Load in multizone equilibria quantites
        r_range = self.quick_r_range
        temperature_over_r = self.temperature_over_r
        ndensity_over_r = self.quick_number_density_over_r
        Br_over_r = self.quick_Br_over_r
        gamma1_over_r = self.gamma1_over_r
        gamma2_over_r = self.quick_gamma2_over_r
        peff_over_r = self.peff_over_r
        tau_es_over_r = self.quick_tau_es
        tsync_const_over_r = self.quick_tsync_const_over_r
        tIC_const_over_r = self.quick_tIC_const_over_r

        spectral_bins = self.get_spectral_bins()

        # Create interpolation functions at photon emitted radii
        temp_interp_f = interp1d(r_range, temperature_over_r, fill_value="extrapolate")
        n_interp_f = interp1d(r_range, ndensity_over_r, fill_value="extrapolate")
        Br_interp_f = interp1d(r_range, Br_over_r, fill_value="extrapolate")
        gamma1_interp_f = interp1d(r_range, gamma1_over_r, fill_value="extrapolate")
        gamma2_interp_f = interp1d(r_range, gamma2_over_r, fill_value="extrapolate")
        peff_interp_f = interp1d(r_range, peff_over_r, fill_value="extrapolate")
        tau_interp_f = interp1d(r_range, tau_es_over_r, fill_value='extrapolate')
        tsync_interp_f = interp1d(r_range, tsync_const_over_r, fill_value='extrapolate')
        tIC_interp_f = interp1d(r_range, tIC_const_over_r, fill_value='extrapolate')

        title_str = r"$F_{{\theta\phi}}={:.2f}$, $\gamma_2=$".format(self.bg.Fthetaphi) + self.gamma2_str + ", " + self.PL_titstr + r", $\gamma_{{1, {{\rm min}}}}={:.2f}$".format(self.minimum_gamma1) + "\n"
        title_str += r"Redshifted, $i=${:.0f} degrees".format(inc_angle_deg)

        plt.figure()
        plt.plot(photon_emitted_radii, temp_interp_f(photon_emitted_radii),
                label="Interpolated", color="C0", ls='-')
        plt.plot(self.r_range, self.temperature_over_r,
                label="Solved", color="black", ls='--')
        plt.yscale('log')
        plt.xlabel(r"$r/r_g$ ")
        plt.ylabel(r"$T_e(r)$ K")
        plt.title(title_str)
        plt.gca().tick_params(right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.xlim([photon_emitted_radii.min(), photon_emitted_radii.max()])
        plt.legend()
        plt.tight_layout()
        fname = "temperature"
        if spectrum_decrease_by_tau:
            fname += "_withSpectrumTauDecrease"
        fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
        print("Saving in " + figure_dir + fname)
        plt.savefig(figure_dir + fname)
        plt.close()

        plt.figure()
        plt.plot(photon_emitted_radii, n_interp_f(photon_emitted_radii),
                label="Interpolated", color="C0", ls='-')
        plt.plot(self.r_range, self.number_density_over_r,
                label="Solved", color="black", ls='--')
        plt.yscale('log')
        plt.xlabel(r"$r/r_g$ ")
        plt.ylabel(r"$n_e(r)$")
        plt.title(title_str)
        plt.gca().tick_params(right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.xlim([photon_emitted_radii.min(), photon_emitted_radii.max()])
        plt.legend()
        plt.tight_layout()
        fname = "ndensity"
        if spectrum_decrease_by_tau:
            fname += "_withSpectrumTauDecrease"
        fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
        print("Saving in " + figure_dir + fname)
        plt.savefig(figure_dir + fname)
        plt.close()
        plt.figure()
        plt.plot(photon_emitted_radii, tau_interp_f(photon_emitted_radii),
                label="Interpolated", color="C0", ls='-')
        plt.plot(self.r_range, self.tau_es,
                label="Solved", color="black", ls='--')
        plt.xlabel(r"$r/r_g$ ")
        plt.ylabel(r"$\tau_e(r)$")
        plt.title(title_str)
        plt.gca().tick_params(right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.xlim([photon_emitted_radii.min(), photon_emitted_radii.max()])
        plt.legend()
        plt.tight_layout()
        fname = "tau"
        if spectrum_decrease_by_tau:
            fname += "_withSpectrumTauDecrease"
        fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
        print("Saving in " + figure_dir + fname)
        plt.savefig(figure_dir + fname)
        plt.close()
        plt.figure()
        plt.plot(photon_emitted_radii, Br_interp_f(photon_emitted_radii),
                label="Interpolated", color="C0", ls='-')
        plt.plot(self.r_range, self.Br_over_r,
                label="Solved", color="black", ls='--')
        plt.xlabel(r"$r/r_g$ ")
        plt.ylabel(r"$B_r(r)$")
        plt.title(title_str)
        plt.gca().tick_params(right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.xlim([photon_emitted_radii.min(), photon_emitted_radii.max()])
        plt.legend()
        plt.tight_layout()
        fname = "Br"
        if spectrum_decrease_by_tau:
            fname += "_withSpectrumTauDecrease"
        fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
        print("Saving in " + figure_dir + fname)
        plt.savefig(figure_dir + fname)
        plt.close()


        #  NOTE: the below calculate_spectra does NOT include redshift.
    # def calculate_spectra(self, quiet=False, spectrum_decrease_by_tau=False):
        # spectrum_dict = {}
        # # if spectrum_decrease_by_tau = True, then decrease the nonthermal intensity by 1/tau
        # # presumably this option is only used if the equilibrium's self.no_qnt_tau_decrease
        # # is FALSE (a test)
#
        # if self.quick_r_range.shape != self.r_range.shape:
            # print("Not all radii have been solved for yet, so there is no point in calculating the spectra. Exiting...")
            # return
        # spectral_bins = self.get_spectral_bins()
        # nonthermal_integrands = np.zeros(spectral_bins.shape)
        # thermal_integrands = np.zeros(spectral_bins.shape)
        # r_range_cm = self.r_range * get_rg_in_cm(self.M_in_Msun)
        # # self.temperature_over_r = np.logspace(np.log10(1e7), np.log10(1e9), r_range_cm.size)
        # # self.temperature_over_r = np.linspace(1e8, 6e8, r_range_cm.size)
        # for j, r_cm in enumerate(r_range_cm):
            # # --------------------------------------------------------------
            # # Spectrum from plunging region power-law of electrons
            # # --------------------------------------------------------------
            # # Assume spectrum is A*nu^(-s) between nu1 and nu2.
            # # Find A = spectrum_constant by integrating and set equal to cooling rate
            # gamma1 = self.gamma1_over_r[j]
            # gamma2 = self.gamma2_over_r[j]
            # Br = self.quick_Br_over_r[j]
            # n_at_r = self.quick_number_density_over_r[j]
            # temp_at_r = self.temperature_over_r[j]
            # height = r_cm*self.H_over_R
            # PL_eff = self.peff_over_r[j]
            # thetaj = Consts.kB_cgs*temp_at_r/Consts.restmass_cgs
            # norm_eq = self.solve_for_PL_norm(gamma1, thetaj, n_at_r, PL_eff)
#
            # nt_cooling_rate = self.nonthermal_cooling_rate_over_r[j]
            # omega_const = 3*Consts.e_in_cgs*np.abs(Br)/(2*Consts.me_in_g*Consts.c_in_cgs)
            # nu1 = omega_const*gamma1**2/(2*np.pi)
            # nu2 = omega_const*gamma2**2/(2*np.pi)
            # s_index = (self.peff_over_r[j] - 1.)/2.
#
            # start_index = (np.abs(nu1 - spectral_bins)).argmin()
            # end_index = (np.abs(nu2 - spectral_bins)).argmin()
            # nu1_bin = spectral_bins[start_index]
            # nu2_bin = spectral_bins[end_index]
            # spectrum_constant = nt_cooling_rate * (1.-s_index)/(nu2_bin**(1.-s_index) - nu1_bin**(1.-s_index))
#
            # Pnu = spectrum_constant * spectral_bins**(-s_index)
            # Pnu[:start_index] = 0
            # Pnu[end_index+1:] = 0
#
            # # Because the integration/split between bins isn't perfect,
            # # use residual factor to ensure the total power is correct.
            # P_nonthermal = np.trapz(Pnu, x=spectral_bins)
            # residual_factor =  nt_cooling_rate/P_nonthermal
            # Pnu = Pnu * residual_factor
            # P_nonthermal = np.trapz(Pnu, x=spectral_bins)
#
            # # Isotropic emission --> jnu = Pnu/4pi
            # # dInu = jnu*ds (assuming no absorption).
            # # Set ds to be height (same as radius for H/R=1)
            # Inu_nonthermal_orig = Pnu/(4.0*np.pi)*height
#
            # # Decrease nonthermal intensity by 1/tau
            # # NOTE: doing this will make power_in_powerlaw != heating_rate*volume
            # # unless accounted for in thermal.
            # if spectrum_decrease_by_tau:
                # Inu_thermalized = Inu_nonthermal_orig*(1.0 - 1.0/self.quick_tau_es[j])
                # Inu_nonthermal = Inu_nonthermal_orig/self.quick_tau_es[j]
                # nt_cooling_rate = nt_cooling_rate/self.quick_tau_es[j]
            # else:
                # Inu_nonthermal = Inu_nonthermal_orig
#
            # nonthermal_integrands = np.vstack([nonthermal_integrands, 2.0*np.pi*r_cm*Inu_nonthermal])
#
            # # ---------------------------------
            # # Calculate Inu of thermal electrons
            # # ---------------------------------
            # thermal_cooling_rate = self.thermal_cooling_rate_over_r[j]
            # # # Account for energy in power law thermalized by scattering
            # # if spectrum_decrease_by_tau:
                # # thermalized_cooling_rate = np.trapz(Inu_thermalized*spectral_bins, x=np.log(spectral_bins))*np.pi*4./height
                # # thermal_cooling_rate += thermalized_cooling_rate
            # thermal_jnu = thermal_cooling_rate/4.0/np.pi
#
            # # Make sure that I=j*H = RL eq. 7.71, j = P/4\pi = A\epsilon/4\pi
            # intensity_constant = 12.0*Consts.kB_cgs**4.*temp_at_r**4./(Consts.c_in_cgs**2.*Consts.h_cgs**3.)
            # exponential_decrease = thermal_jnu*height/intensity_constant
            # Inu_thermal = 2*Consts.h_cgs * spectral_bins**3./Consts.c_in_cgs**2. * np.exp(-Consts.h_cgs*spectral_bins/(Consts.kB_cgs*temp_at_r))
            # Inu_thermal = Inu_thermal*exponential_decrease
#
            # # Because the integration/split between bins isn't perfect,
            # # use residual factor to ensure the total power is correct.
            # P_thermal = np.trapz(Inu_thermal, x=spectral_bins)*4.0*np.pi/height
            # residual_factor =  thermal_cooling_rate/P_thermal
            # Inu_thermal = Inu_thermal * residual_factor
#
            # thermal_integrands = np.vstack([thermal_integrands, 2.0*np.pi*r_cm*Inu_thermal])
#
            # # ---------------------------------
            # # Check cooling rate: at this radius, is the power equal to cooling rate
            # # calculated in equilibrium model?
            # # ---------------------------------
            # nonthermal_Inu_power = np.trapz(Inu_nonthermal, x=spectral_bins)*4.*np.pi/height
            # thermal_Inu_power = np.trapz(Inu_thermal, x=spectral_bins)*4.*np.pi/height
            # if not (np.allclose(nonthermal_Inu_power + thermal_Inu_power, self.volume_heating_rate)):
                # print("Heating != cooling at j={:d}".format(j)); exit()
            # if not (np.allclose(nonthermal_Inu_power, nt_cooling_rate)):
                # print("Nonthermal radiation power != nonthermal equilibrium cooling rate at j={:d}.".format(j))
                # exit()
            # if not (np.allclose(thermal_Inu_power, thermal_cooling_rate)):
                # print("Thermal radiation power != thermal equilibrium cooling rate at j={:d}.".format(j))
                # exit()
#
        # nonthermal_integrands = nonthermal_integrands[1:, :]
        # thermal_integrands = thermal_integrands[1:, :]
        # # Fnu = \int Inu dA/D^2 = \int Inu 2\pi rdr/D^2, D is distance
        # # Lnu = 4\pi D^2 Fnu = independent of D
        # # Integrate all_intensity over radius
        # total_flux_const_nonthermal = np.trapz(nonthermal_integrands[::-1, :], x=r_range_cm[::-1], axis=0)
        # total_flux_const_thermal = np.trapz(thermal_integrands[::-1, :], x=r_range_cm[::-1], axis=0)
        # # Factor of 2 because wedge shape has two sides
        # Lnu_nonthermal = 2.0*4.0*np.pi*total_flux_const_nonthermal
        # # Factor of 2 included in FW
        # Lnu_thermal = 2.0*4.0*np.pi*total_flux_const_thermal
#
        # power_in_powerlaw = np.trapz(spectral_bins*Lnu_nonthermal, x=np.log(spectral_bins))
        # power_in_thermal_pr = np.trapz(spectral_bins*Lnu_thermal, x=np.log(spectral_bins))
#
        # # Only Xrays, i.e. above 1keV
        # keV_ind = (np.abs(nuToKev(spectral_bins) - 1.0)).argmin()
        # Xray_spectral_bins = spectral_bins[keV_ind:]
        # Xray_Lnu_nonthermal = Lnu_nonthermal[keV_ind:]
        # Xray_Lnu_disk = Lnu_disk[keV_ind:]
        # Xray_power_in_powerlaw = np.trapz(Xray_spectral_bins*Xray_Lnu_nonthermal, x=np.log(Xray_spectral_bins))
        # Xray_power_in_disk = np.trapz(Xray_spectral_bins*Xray_Lnu_disk, x=np.log(Xray_spectral_bins))
        # pair_ind = (np.abs(nuToKev(Xray_spectral_bins) - 1000.0)).argmin()
        # Xray_nopair_spectral_bins = Xray_spectral_bins[:pair_ind]
        # Xray_nopair_Lnu_nonthermal = Xray_Lnu_nonthermal[:pair_ind]
        # Xray_nopair_Lnu_disk = Xray_Lnu_disk[:pair_ind]
        # Xray_nopair_power_in_powerlaw = np.trapz(Xray_nopair_spectral_bins*Xray_nopair_Lnu_nonthermal, x=np.log(Xray_nopair_spectral_bins))
        # Xray_nopair_power_in_disk = np.trapz(Xray_nopair_spectral_bins*Xray_nopair_Lnu_disk, x=np.log(Xray_nopair_spectral_bins))
#
        # total_pr_emitted_power = power_in_powerlaw + power_in_thermal_pr
        # total_heating = self.volume_heating_rate * self.emitting_volume
        # fractional_error = np.abs(total_pr_emitted_power - total_heating)/total_heating
        # print("Error in total power: {:.3e}".format(fractional_error))
        # if not quiet:
            # print("Integrated Lnu over power-law: {:.4e}".format(power_in_powerlaw))
            # print("Integrated Lnu over thermal: {:.4e}".format(power_in_thermal_pr))
            # print("P_PL/P_thermal: {:.4e}".format(power_in_powerlaw/power_in_thermal_pr))
            # print("Integrated Lnu from plunging region: {:.4e}".format(total_pr_emitted_power))
            # print("Heating rate*volume: {:.4e}".format(total_heating))
        # if not np.allclose(total_pr_emitted_power, total_heating, rtol=1e-3):
            # print("---> Sum of emitted power is not right!!")
            # if spectrum_decrease_by_tau:
                # print("     due to 1/tau decrease not being added to thermal electrons?")
            # else:
                # print("     not due to 1/tau decrease (spectrum_decrease_by_tau=False).")
#
        # # --------------------------------------------------------------
        # # Disk spectrum
        # # --------------------------------------------------------------
        # disk_outer_edge = 1e10 # cm
        # disk_inner_edge = self.bg.rISCO * get_rg_in_cm(self.M_in_Msun)
        # rstar = self.bg.rEH * get_rg_in_cm(self.M_in_Msun)
        # disk_radial_bins = np.logspace(np.log10(disk_inner_edge), np.log10(disk_outer_edge), 1000)
        # L_edd = get_eddington_lum(self.M_in_Msun)
        # Mdot_edd = L_edd/(self.f_Ledd*Consts.c_in_cgs**2.0)
        # Mdot = self.f_Mdot*Mdot_edd  # g/s
        # disk_temp_over_r = (3*Consts.G_in_cgs*self.M_in_Msun*Consts.solar_mass_in_g*Mdot/(8*np.pi*disk_radial_bins**3*Consts.SB_cgs)*(1.0 - np.sqrt(rstar/disk_radial_bins)))**0.25
        # # print("Maximum disk temperature: {:.2e} K".format(disk_temp_over_r.max()))
        # # print("Minimum disk temperature: {:.2e} K".format(disk_temp_over_r.min()))
        # disk_function = []
        # for nu in spectral_bins:
            # planck_vals = []
            # for i, r in enumerate(disk_radial_bins):
                # try:
                    # planck_val = 1.0/(np.exp(Consts.h_cgs*nu/(Consts.kB_cgs*disk_temp_over_r[i])) - 1)
                # except:
                    # planck_val = 0
                # planck_vals.append(planck_val)
            # integral_value = np.trapz(disk_radial_bins*planck_vals, disk_radial_bins)
            # # integral_value = 0.5*(disk_outer_edge**2 - disk_inner_edge**2)*planck_val
            # luminosity_val = 16.0*np.pi**2*nu**3*Consts.h_cgs/Consts.c_in_cgs**2*integral_value
            # disk_function.append(luminosity_val)
        # disk_function = np.array(disk_function)
#
        # if not quiet:
            # print("Luminosity: {:.2e}".format(self.luminosity))
            # print("Integrated disk luminosity: {:.2e}".format(np.trapz(disk_function, spectral_bins)))
#
        # spectrum_dict["spectral_bins"] = spectral_bins
        # spectrum_dict["Lnu_nonthermal"] = Lnu_nonthermal
        # spectrum_dict["Lnu_thermal"] = Lnu_thermal
        # spectrum_dict["Lnu_disk"] = disk_function
        # spectrum_dict["power_in_powerlaw"] = power_in_powerlaw
        # spectrum_dict["Xray_power_in_powerlaw"] = Xray_power_in_powerlaw
        # spectrum_dict["Xray_nopair_power_in_powerlaw"] = Xray_nopair_power_in_powerlaw
        # disk_spectrum_dict["Xray_power_in_disk"] = Xray_power_in_disk
        # disk_spectrum_dict["Xray_nopair_power_in_disk"] = Xray_nopair_power_in_disk
        # spectrum_dict["power_in_thermal_pr"] = power_in_thermal_pr
        # spectrum_dict["fractional_error"] = fractional_error
#
        # # Save to pickle
        # spectrum_path = self.path_to_reduced_data + "spectrum"
        # if spectrum_decrease_by_tau:
            # spectrum_path += "_withSpectrumTauDecrease"
        # spectrum_path += "_gamma1minimum{:.2f}".format(self.minimum_gamma1)
        # spectrum_path += ".p"
        # with open(spectrum_path, 'wb') as file:
            # pickle.dump(spectrum_dict, file)
#
        # return spectrum_dict

    def trace_geodesics(self, inc_angle_deg=60, quiet=False):
        print("Tracing geodesics for i={:d}".format(inc_angle_deg))
        # --------------------------------------------------------------
        # Trace geodesics
        # --------------------------------------------------------------
        # Create a camera at infinity
        g = gi.geokerr()
        # For each pixel in the camera, find whether the geodesic hits the midplane
        cos_inclination = np.cos(inc_angle_deg*np.pi/180) # set the inclination angle of the camera
        # avals = [-4., 4., -4., 4.] # xy extent of the camera in rg
        avals = [-8., 8., -8., 8.] # xy extent of the camera in rg
        nvals = [64, 64, 1] # number of pixels on the camera
        # nvals = [96, 96, 1] # number of pixels on the camera
        # nvals = [128, 128, 1] # number of pixels on the camera
        dalpha = (avals[1] - avals[0])*get_rg_in_cm(self.M_in_Msun)/nvals[0]
        dbeta = (avals[3] - avals[2])*get_rg_in_cm(self.M_in_Msun)/nvals[1]
        u_locations, mu_locations, dt, out_l, dphi, tpm, tpr, i1, i2 = g.run_camera(mu0=cos_inclination, a=self.bg.a, standard=2, avals=avals, n=nvals)

        q2 = g.q2
        su = g.su
        sm = g.sm
        l = g.l # NOT the output

        # If a pixel i's geodesic misses the midplane, u_locations[i] = -1
        # Otherwise, u_locations[i] is the radius r=1/u where the photon hit.
        # We assume that the gas at that radius emitted the photon.
        # To calculate redshift, we need to know the photon momentum in the
        # locally non-rotating frame. We need to transform the gas's velocity at that location
        # (given by the Gammie 1999 trajectories).
        # -------------------------------------------
        # Eliminate photons that don't hit the midplane
        indices_to_remove = u_locations == -1.0
        if not quiet:
            print("{:d} pixels' geodesics did not hit the midplane.".format(indices_to_remove.sum()))
        u_locations = u_locations[~indices_to_remove]
        mu_locations = mu_locations[~indices_to_remove]
        dt = dt[~indices_to_remove]
        dphi = dphi[~indices_to_remove]
        tpm = tpm[~indices_to_remove]
        tpr = tpr[~indices_to_remove]
        indices_to_remove = np.squeeze(indices_to_remove)
        i1 = i1[~indices_to_remove]
        i2 = i2[~indices_to_remove]
        q2 = q2[~indices_to_remove]
        su = su[~indices_to_remove]
        sm = sm[~indices_to_remove]
        l = l[~indices_to_remove]

        # Eliminate photons that hit the disk outside the plunging region starting radius
        photon_emitted_radii = 1./u_locations
        indices_to_remove = photon_emitted_radii >= self.starting_r

        disk_emitted_radii = photon_emitted_radii[indices_to_remove]
        disk_u_locations = u_locations[indices_to_remove]
        disk_mu_locations = mu_locations[indices_to_remove]
        disk_dt = dt[indices_to_remove]
        disk_dphi = dphi[indices_to_remove]
        disk_tpm = tpm[indices_to_remove]
        disk_tpr = tpr[indices_to_remove]
        disk_i1 = i1[np.squeeze(indices_to_remove)]
        disk_i2 = i2[np.squeeze(indices_to_remove)]
        disk_q2 = q2[np.squeeze(indices_to_remove)]
        disk_su = su[np.squeeze(indices_to_remove)]
        disk_sm = sm[np.squeeze(indices_to_remove)]
        disk_l = l[np.squeeze(indices_to_remove)]

        photon_emitted_radii = photon_emitted_radii[~indices_to_remove]
        u_locations = u_locations[~indices_to_remove]
        mu_locations = mu_locations[~indices_to_remove]
        dt = dt[~indices_to_remove]
        dphi = dphi[~indices_to_remove]
        tpm = tpm[~indices_to_remove]
        tpr = tpr[~indices_to_remove]
        indices_to_remove = np.squeeze(indices_to_remove)
        i1 = i1[~indices_to_remove]
        i2 = i2[~indices_to_remove]
        q2 = q2[~indices_to_remove]
        su = su[~indices_to_remove]
        sm = sm[~indices_to_remove]
        l = l[~indices_to_remove]

        # If starting_r != rISCO, need to remove photons that hit at starting_r < r < rISCO
        indices_to_keep = disk_emitted_radii > self.bg.rISCO
        if not quiet:
            print("{:d} pixels' geodesics hit the disk midplane.".format(indices_to_keep.size - indices_to_keep.sum(), indices_to_keep.sum()))

        disk_emitted_radii = disk_emitted_radii[indices_to_keep]
        disk_u_locations = disk_u_locations[indices_to_keep]
        disk_mu_locations = disk_mu_locations[indices_to_keep]
        disk_dt = disk_dt[indices_to_keep]
        disk_dphi = disk_dphi[indices_to_keep]
        disk_tpm = disk_tpm[indices_to_keep]
        disk_tpr = disk_tpr[indices_to_keep]
        disk_i1 = disk_i1[np.squeeze(indices_to_keep)]
        disk_i2 = disk_i2[np.squeeze(indices_to_keep)]
        disk_q2 = disk_q2[np.squeeze(indices_to_keep)]
        disk_su = disk_su[np.squeeze(indices_to_keep)]
        disk_sm = disk_sm[np.squeeze(indices_to_keep)]
        disk_l = disk_l[np.squeeze(indices_to_keep)]

        # ------------------------------
        # Get the Gammie 1999 velocities
        # ------------------------------
        # Interpolate between radii
        uT_interp_f = interp1d(self.bg.r_range, self.bg.uTsolution)
        uR_interp_f = interp1d(self.bg.r_range, self.bg.uRsolution)
        uPhi_interp_f = interp1d(self.bg.r_range, self.bg.uPhisolution)
        # Use interpolation at emitted radii (boyer-lindquist coords)
        uT_photons = uT_interp_f(photon_emitted_radii)
        uR_photons = uR_interp_f(photon_emitted_radii)
        uPhi_photons = uPhi_interp_f(photon_emitted_radii)
        uTheta_photons = 0.0*np.ones(uR_photons.shape)

        # ------------------------------
        # Transform to locally non-rotating frame
        # ------------------------------
        theta_array = np.pi/2.*np.ones(uR_photons.shape)
        # Calculate three-velocities
        vR_bl = uR_photons/uT_photons
        vTheta_bl = uTheta_photons/uT_photons
        vPhi_bl = uPhi_photons/uT_photons
        vMag2 = vR_bl**2 + vTheta_bl**2 + vPhi_bl**2
        vR_lnrf, vTheta_lnrf, vPhi_lnrf = calcg.lnrf_frame(vR_bl, vTheta_bl, vPhi_bl, photon_emitted_radii, self.bg.a, theta_array)

        # For some reason the transformed velocity can be faster than c?
        # TODO
        vMag2 = vR_lnrf**2 + vTheta_lnrf**2 + vPhi_lnrf**2
        if not quiet:
            print("Number of velocities with v > c: {:d}".format((vMag2 >= 1).sum()))
        indices_to_remove = vMag2 >= 1
        photon_emitted_radii = photon_emitted_radii[~indices_to_remove]
        u_locations = u_locations[~indices_to_remove]
        mu_locations = mu_locations[~indices_to_remove]
        dt = dt[~indices_to_remove]
        dphi = dphi[~indices_to_remove]
        tpm = tpm[~indices_to_remove]
        tpr = tpr[~indices_to_remove]
        i1 = i1[~indices_to_remove]
        i2 = i2[~indices_to_remove]
        q2 = q2[~indices_to_remove]
        su = su[~indices_to_remove]
        sm = sm[~indices_to_remove]
        l = l[~indices_to_remove]
        vR_lnrf = vR_lnrf[~indices_to_remove]
        vTheta_lnrf = vTheta_lnrf[~indices_to_remove]
        vPhi_lnrf = vPhi_lnrf[~indices_to_remove]

        # ------------------------------
        # Calculate redshift from each photon
        # ------------------------------
        if not quiet:
            print("{:d} photons hit the plunging region midplane.".format(photon_emitted_radii.size))
        redshifts = calcg.calcg(u_locations, mu_locations, q2, l, self.bg.a, tpm, tpr, su, sm, vR_lnrf, vTheta_lnrf, vPhi_lnrf)

        # Sort redshifts to be increasing radius
        sorted_inds = photon_emitted_radii.argsort()
        photon_emitted_radii = photon_emitted_radii[sorted_inds[::-1]]
        redshifts = redshifts[sorted_inds[::-1]]

        figure_dir = self.path_to_figures
        plt.plot(photon_emitted_radii, redshifts, ls='None', marker='o')
        plt.ylabel(r"Redshift $g$")
        plt.xlabel(r"$r/r_g$")
        plt.title(r"$i=${:.0f} degrees".format(inc_angle_deg))
        plt.savefig(figure_dir + "redshifts_over_r_i{:d}.png".format(inc_angle_deg))
        plt.close()

        # ------------------------------
        # Get the disk velocities
        # ------------------------------
        M = self.bg.M
        a = self.bg.a*M # remember Gammie a, which this is, is unitless whereas NT a has units of length
        # Novikov Thorne gives the thin disk velocity already in the LNRF,
        # so we don't need to transform it. Eq. 5.4.4a
        [vR_lnrf, vTheta_lnrf, vPhi_lnrf] = get_thin_disk_lnrf_velocities(a, M, disk_emitted_radii)
        # [vR_lnrf, vTheta_lnrf, vPhi_lnrf] = get_thin_disk_lnrf_velocities_old(a, M, disk_emitted_radii)

        # For some reason the transformed velocity can be faster than c, just for one?
        # TODO
        vMag2 = vR_lnrf**2 + vTheta_lnrf**2 + vPhi_lnrf**2
        if not quiet:
            print("Number of velocities with v > c: {:d}".format((vMag2 >= 1).sum()))
        indices_to_remove = vMag2 >= 1
        disk_emitted_radii = disk_emitted_radii[~indices_to_remove]
        disk_u_locations = disk_u_locations[~indices_to_remove]
        disk_mu_locations = disk_mu_locations[~indices_to_remove]
        disk_dt = disk_dt[~indices_to_remove]
        disk_dphi = disk_dphi[~indices_to_remove]
        disk_tpm = disk_tpm[~indices_to_remove]
        disk_tpr = disk_tpr[~indices_to_remove]
        disk_i1 = disk_i1[~indices_to_remove]
        disk_i2 = disk_i2[~indices_to_remove]
        disk_q2 = disk_q2[~indices_to_remove]
        disk_su = disk_su[~indices_to_remove]
        disk_sm = disk_sm[~indices_to_remove]
        disk_l = disk_l[~indices_to_remove]
        vR_lnrf = vR_lnrf[~indices_to_remove]
        vTheta_lnrf = vTheta_lnrf[~indices_to_remove]
        vPhi_lnrf = vPhi_lnrf[~indices_to_remove]

        # ------------------------------
        # Calculate redshift from each photon
        # ------------------------------
        disk_redshifts = calcg.calcg(disk_u_locations, disk_mu_locations, disk_q2, disk_l, self.bg.a, disk_tpm, disk_tpr, disk_su, disk_sm, vR_lnrf, vTheta_lnrf, vPhi_lnrf)

        # Sort redshifts to be increasing radius
        sorted_inds = disk_emitted_radii.argsort()
        disk_emitted_radii = disk_emitted_radii[sorted_inds[::-1]]
        disk_redshifts = disk_redshifts[sorted_inds[::-1]]

        redshift_dict = {"photon_emitted_radii":photon_emitted_radii,
                        "redshifts":redshifts,
                         "disk_emitted_radii":disk_emitted_radii,
                         "disk_redshifts":disk_redshifts,
                         "dalpha":dalpha,
                         "dbeta":dbeta}
        return redshift_dict

    def calculate_redshifted_spectra(self, inc_angle_deg=60, spectrum_decrease_by_tau=False):
        spectrum_path = self.spectrum_path_base
        spectrum_path += "_redshifted_i{:d}".format(inc_angle_deg)
        spectrum_path += ".p"

        if os.path.exists(spectrum_path):
            with open(spectrum_path, 'rb') as file:
                spectrum_dict = pickle.load(file)
        else:
            spectrum_dict = {}
        # Load in camera properties
        redshift_dict = self.trace_geodesics(inc_angle_deg)
        photon_emitted_radii = redshift_dict["photon_emitted_radii"]
        redshifts = redshift_dict["redshifts"]
        dalpha = redshift_dict["dalpha"]
        dbeta = redshift_dict["dbeta"]

        # Load in multizone equilibria quantites
        temperature_over_r = self.temperature_over_r
        gamma1_over_r = self.gamma1_over_r
        peff_over_r = self.peff_over_r
        gamma2_over_r = self.quick_gamma2_over_r
        r_range = self.quick_r_range
        ndensity_over_r = self.quick_number_density_over_r
        Br_over_r = self.quick_Br_over_r
        tau_es_over_r = self.quick_tau_es
        tsync_const_over_r = self.quick_tsync_const_over_r
        tIC_const_over_r = self.quick_tIC_const_over_r

        spectral_bins = self.get_spectral_bins()

        # Create interpolation functions at photon emitted radii
        temp_interp_f = interp1d(r_range, temperature_over_r, fill_value="extrapolate")
        n_interp_f = interp1d(r_range, ndensity_over_r, fill_value="extrapolate")
        Br_interp_f = interp1d(r_range, Br_over_r, fill_value="extrapolate")
        gamma1_interp_f = interp1d(r_range, gamma1_over_r, fill_value="extrapolate")
        gamma2_interp_f = interp1d(r_range, gamma2_over_r, fill_value="extrapolate")
        peff_interp_f = interp1d(r_range, peff_over_r, fill_value="extrapolate")
        tau_interp_f = interp1d(r_range, tau_es_over_r, fill_value='extrapolate')
        tsync_interp_f = interp1d(r_range, tsync_const_over_r, fill_value='extrapolate')
        tIC_interp_f = interp1d(r_range, tIC_const_over_r, fill_value='extrapolate')
        print("Done interpolating")

        nonthermal_integrands = np.zeros(spectral_bins.shape)
        thermal_integrands = np.zeros(spectral_bins.shape)
        nonthermal_redshift_integrands = np.zeros(spectral_bins.shape)
        thermal_redshift_integrands = np.zeros(spectral_bins.shape)
        r_range_cm = photon_emitted_radii * get_rg_in_cm(self.M_in_Msun)
        # For each pixel, calculate Inu(r).
        for j, radius in enumerate(photon_emitted_radii):
            print(j)
            gamma1 = gamma1_interp_f(radius)
            gamma2 = gamma2_interp_f(radius)
            Br = Br_interp_f(radius)
            n_at_r = n_interp_f(radius)
            temp_at_r = temp_interp_f(radius)
            theta = Consts.kB_cgs*temp_at_r/Consts.restmass_cgs
            peff = peff_interp_f(radius)
            norm = self.solve_for_PL_norm(gamma1, theta, n_at_r, peff)
            args = (gamma2, Br, n_at_r, r_range, radius)
            tau = tau_interp_f(radius)

            if tsync_interp_f(radius) < tIC_interp_f(radius):
                nt_cooling_rate = self.calculate_sync_cooling_rate(norm, gamma1, peff, None, args)
            else:
                nt_cooling_rate = self.calculate_IC_cooling_rate(norm, gamma1, peff, None, args)
            nt_cooling_rate = nt_cooling_rate/tau

            # ---------------------------------
            # Calculate Inu of power-law electrons
            # ---------------------------------
            # Assume spectrum is A*nu^(-s) between nu1 and nu2.
            # Find A = spectrum_constant by integrating and set equal to cooling rate
            omega_const = 3*Consts.e_in_cgs*np.abs(Br)/(2*Consts.me_in_g*Consts.c_in_cgs)
            nu1 = omega_const*gamma1**2/(2*np.pi)
            nu2 = omega_const*gamma2**2/(2*np.pi)
            s_index = (peff - 1.)/2.
            start_index = (np.abs(nu1 - spectral_bins)).argmin()
            end_index = (np.abs(nu2 - spectral_bins)).argmin()
            nu1_bin = spectral_bins[start_index]
            nu2_bin = spectral_bins[end_index]

            if nt_cooling_rate == 0.0:
                Inu_nonthermal_orig = np.zeros(spectral_bins.shape)
            else:
                spectrum_constant = nt_cooling_rate * (1.-s_index)/(nu2_bin**(1.-s_index) - nu1_bin**(1.-s_index))

                Pnu = spectrum_constant * spectral_bins**(-s_index)
                Pnu[:start_index] = 0
                Pnu[end_index:] = 0

                # Because the integration/split between bins isn't perfect,
                # use residual factor to ensure the total power is correct.
                P_nonthermal = np.trapz(Pnu, x=spectral_bins)
                residual_factor =  nt_cooling_rate/P_nonthermal
                Pnu = Pnu * residual_factor
                P_nonthermal = np.trapz(Pnu, x=spectral_bins)

                # Isotropic emission --> jnu = Pnu/4pi
                # dInu = jnu*ds. Set ds to be height (same as radius for H/R=1)
                # Inu thus has units of (erg/s)/(cm^2 Hz str)
                height = radius*get_rg_in_cm(self.M_in_Msun)*self.H_over_R
                Inu_nonthermal_orig = Pnu/(4.0*np.pi)*height

            # Decrease nonthermal intensity by 1/tau
            # NOTE: doing this will make power_in_powerlaw != heating_rate*volume
            # unless accounted for in thermal.
            # spectrum_decrease_by_tau is a TEST. It has been included in the nonthermal cooling rate
            # already so does not need to be decreased in the spectrum.
            if spectrum_decrease_by_tau:
                Inu_thermalized = Inu_nonthermal_orig*(1.0 - 1.0/tau)
                Inu_nonthermal = Inu_nonthermal_orig/tau
                nt_cooling_rate = nt_cooling_rate/tau
            else:
                Inu_nonthermal = Inu_nonthermal_orig

            # Redshift
            Inu_nonthermal_redshift = Inu_nonthermal*redshifts[j]**3 # multiply by g^3

            # Shift the frequencies. Can result in a large amount of power in lowest bins
            # if the original spectral_bins' lowest value doesn't include the lowest
            # redshifted bin.
            spectral_bins_obs = spectral_bins*redshifts[j]
            Inu_bin_inds = np.digitize(spectral_bins_obs, spectral_bins)
            Inu_nonthermal_redshift = [Inu_nonthermal_redshift[Inu_bin_inds == i].sum() for i in range(0, len(Inu_bin_inds))]

            nonthermal_integrands = np.vstack([nonthermal_integrands, Inu_nonthermal])
            nonthermal_redshift_integrands = np.vstack([nonthermal_redshift_integrands, Inu_nonthermal_redshift])

            # ---------------------------------
            # Calculate Inu of thermal electrons
            # ---------------------------------
            amplification_factor = calculate_amplification_factor(n_at_r, temp_at_r)
            bremsstrahlung_emissivity = calculate_bremsstrahlung_emissivity(n_at_r, temp_at_r)
            thermal_cooling_rate = amplification_factor * bremsstrahlung_emissivity
            # Account for energy in power law thermalized by scattering
            if spectrum_decrease_by_tau:
                thermalized_cooling_rate = np.trapz(Inu_thermalized*spectral_bins, x=np.log(spectral_bins))*np.pi*4./height
                thermal_cooling_rate += thermalized_cooling_rate
            thermal_jnu = thermal_cooling_rate/4.0/np.pi

            # Make sure that I=j*H = RL eq. 7.71, j = P/4\pi = A\epsilon/4\pi
            intensity_constant = 12.0*Consts.kB_cgs**4.*temp_at_r**4./(Consts.c_in_cgs**2.*Consts.h_cgs**3.)
            exponential_decrease = thermal_jnu*height/intensity_constant
            Inu_thermal = 2*Consts.h_cgs * spectral_bins**3./Consts.c_in_cgs**2. * np.exp(-Consts.h_cgs*spectral_bins/(Consts.kB_cgs*temp_at_r))
            Inu_thermal = Inu_thermal*exponential_decrease

            # Because the integration/split between bins isn't perfect,
            # use residual factor to ensure the total power is correct.
            P_thermal = np.trapz(Inu_thermal, x=spectral_bins)*4.0*np.pi/height
            residual_factor =  thermal_cooling_rate/P_thermal
            Inu_thermal = Inu_thermal * residual_factor

            # Redshift
            Inu_thermal_redshift = Inu_thermal * redshifts[j]**3 # multiply by g^3
            # Shift the frequencies.
            Inu_thermal_redshift = [Inu_thermal_redshift[Inu_bin_inds == i].sum() for i in range(0, len(Inu_bin_inds))]
            thermal_integrands = np.vstack([thermal_integrands, Inu_thermal])
            thermal_redshift_integrands = np.vstack([thermal_redshift_integrands, Inu_thermal_redshift])

        nonthermal_integrands = nonthermal_integrands[1:, :]
        thermal_integrands = thermal_integrands[1:, :]
        nonthermal_redshift_integrands = nonthermal_redshift_integrands[1:, :]
        thermal_redshift_integrands = thermal_redshift_integrands[1:, :]

        # For distant observer:
        # Fnu = \int Inu dA/D^2 = \int Inu 2pi rdr/D^2, D is distance
        # Lnu = 4\pi D^2 Fnu = independent of D
        # For camera: Fnu = sum over each pixel
        #                 = \Delta\alpha\Delta\beta/D^2*sum(intensity at each pixel)
        # And pixels all have the same size, so can pull out of integral (yay)
        flux_const_nonthermal_redshift = dalpha*dbeta*np.sum(nonthermal_redshift_integrands, axis=0)
        flux_const_thermal_redshift = dalpha*dbeta*np.sum(thermal_redshift_integrands, axis=0)
        flux_const_nonthermal = dalpha*dbeta*np.sum(nonthermal_integrands, axis=0)
        flux_const_thermal = dalpha*dbeta*np.sum(thermal_integrands, axis=0)

        # Isotropic Lnu = 4\pi D^2 Fnu
        Lnu_nonthermal_redshift = 4.0*np.pi*flux_const_nonthermal_redshift
        Lnu_thermal_redshift = 4.0*np.pi*flux_const_thermal_redshift
        Lnu_nonthermal = 4.0*np.pi*flux_const_nonthermal
        Lnu_thermal = 4.0*np.pi*flux_const_thermal

        spectrum_dict["spectral_bins"] = spectral_bins
        spectrum_dict["Lnu_nonthermal"] = Lnu_nonthermal_redshift
        spectrum_dict["Lnu_thermal"] = Lnu_thermal_redshift
        spectrum_dict["g3I_nonthermal_over_r"] = np.trapz(nonthermal_redshift_integrands, x=spectral_bins, axis=1)
        spectrum_dict["g3I_thermal_over_r"] = np.trapz(thermal_redshift_integrands, x=spectral_bins, axis=1)
        spectrum_dict["I_nonthermal_over_r"] = np.trapz(nonthermal_integrands, x=spectral_bins, axis=1)
        spectrum_dict["I_thermal_over_r"] = np.trapz(thermal_integrands, x=spectral_bins, axis=1)
        spectrum_dict["photon_emitted_radii"] = photon_emitted_radii
        spectrum_dict["dalpha"] = dalpha
        spectrum_dict["dbeta"] = dbeta

        with open(spectrum_path, 'wb') as file:
            pickle.dump(spectrum_dict, file)
        return spectrum_dict

    def calculate_redshifted_disk_spectrum(self, inc_angle_deg=60, spectrum_decrease_by_tau=False):
        disk_spectrum_path = self.dir_to_disk_spectrum + "redshifted_i{:d}.p".format(inc_angle_deg)
        if os.path.exists(disk_spectrum_path):
            with open(disk_spectrum_path, 'rb') as file:
                disk_spectrum_dict = pickle.load(file)
        else:
            disk_spectrum_dict = {}

        # Load in camera properties
        redshift_dict = self.trace_geodesics(inc_angle_deg)
        disk_emitted_radii = redshift_dict["disk_emitted_radii"] # should probably cut off r < rISCO here
        redshifts = redshift_dict["disk_redshifts"]
        dalpha = redshift_dict["dalpha"]
        dbeta = redshift_dict["dbeta"]

        spectral_bins = self.get_spectral_bins()
        disk_outer_edge = disk_emitted_radii.max() * get_rg_in_cm(self.M_in_Msun) # cm
        disk_inner_edge = self.bg.rISCO * get_rg_in_cm(self.M_in_Msun)
        rstar = self.bg.rEH * get_rg_in_cm(self.M_in_Msun)
        # Maybe rstar should be rISCO instead of rEH?
        # Note it doesn't matter with rel. corrections.
        disk_radial_bins = np.logspace(np.log10(disk_inner_edge), np.log10(disk_outer_edge), 1000)
        L_edd = get_eddington_lum(self.M_in_Msun)
        Mdot_edd = L_edd/(self.f_Ledd*Consts.c_in_cgs**2.0)
        Mdot = self.f_Mdot*Mdot_edd  # g/s
        # disk_temp_over_r = (3*Consts.G_in_cgs*self.M_in_Msun*Consts.solar_mass_in_g*Mdot/(8*np.pi*disk_radial_bins**3*Consts.SB_cgs)*(1.0 - np.sqrt(rstar/disk_radial_bins)))**0.25
        disk_temp_over_r = (3*Consts.G_in_cgs*self.M_in_Msun*Consts.solar_mass_in_g*Mdot/(8*np.pi*disk_radial_bins**3*Consts.SB_cgs))**0.25
        a = self.bg.a
        r_in_rg = disk_radial_bins/get_rg_in_cm(self.M_in_Msun)
        # See tools.py; equations from Novikov/Thorne and Page/Thorne.
        relativistic_flux_correction = funcFromHell(a, r_in_rg)/Bfunc(a, r_in_rg)/np.sqrt(Cfunc(a, r_in_rg))
        if (relativistic_flux_correction < 0).any():
            zero_inds = np.where(relativistic_flux_correction < 0)
            if np.allclose(relativistic_flux_correction[zero_inds], 0.0):
                # print("Close to zero.")
                relativistic_flux_correction[zero_inds] = 0.0
            else:
                print("Warning: relativistic flux correction is negative for some reason.")
                print(relativistic_flux_correction[zero_inds])
                print(r_in_rg[zero_inds])
                print(self.bg.rEH)
                print(self.bg.rISCO)
                exit()
        relativistic_temp_correction = relativistic_flux_correction**0.25
        disk_temp_over_r = disk_temp_over_r * relativistic_temp_correction

        # Note the temp interp using argument in physical units, not rg
        temp_interp_f = interp1d(disk_radial_bins, disk_temp_over_r, fill_value='extrapolate')
        disk_redshift_integrands = np.zeros(spectral_bins.shape)

        for i, r in enumerate(disk_emitted_radii):
            print(i)
            # Note that disk_emitted_radii includes photons that were emitted
            # in starting_r < r < rISCO. We completely neglect these photons.
            if r < self.bg.rISCO:
                # print("Photon in neglected ISCO.")
                Inu_disk = np.zeros(spectral_bins.shape)
            else:
                temp_at_r = temp_interp_f(r*get_rg_in_cm(self.M_in_Msun))
                if temp_at_r < 0:
                    print("Negative temperature for r > rISCO.")
                    print(r)
                    print(self.bg.rISCO)
                    print(i)
                    print(disk_emitted_radii.size)
                    print(temp_at_r)
                    exit()
                Inu_disk = 2.0*Consts.h_cgs * spectral_bins**3./Consts.c_in_cgs**2. * np.exp(-Consts.h_cgs*spectral_bins/(Consts.kB_cgs*temp_at_r))
            # Redshift
            Inu_disk_redshift = Inu_disk * redshifts[i]**3 # multiply by g^3
            # Shift the frequencies.
            spectral_bins_obs = spectral_bins*redshifts[i]
            Inu_bin_inds = np.digitize(spectral_bins_obs, spectral_bins)
            Inu_disk_redshift = [Inu_disk_redshift[Inu_bin_inds == i].sum() for i in range(0, len(Inu_bin_inds))]
            disk_redshift_integrands = np.vstack([disk_redshift_integrands, Inu_disk_redshift])

        disk_redshift_integrands = disk_redshift_integrands[1:, :]
        flux_const_disk_redshift = dalpha*dbeta*np.sum(disk_redshift_integrands, axis=0)
        Lnu_disk_redshift = 4.0*np.pi*flux_const_disk_redshift

        disk_spectrum_dict["spectral_bins"] = spectral_bins
        disk_spectrum_dict["Lnu_disk"] = Lnu_disk_redshift
        with open(disk_spectrum_path, 'wb') as file:
            pickle.dump(disk_spectrum_dict, file)
        return disk_spectrum_dict


    def print_consistency_check(self):
        # Infall time
        tinfall = self.bg.t_infall
        tinfall_s = tinfall * self.bg.get_tg_in_s(self.M_in_Msun)

        j = 0
        tcool = self.quick_cooling_consts[j]
        gamma1_eq = self.gamma1_over_r[j]
        temp = self.temperature_over_r[j]
        PL_eff = self.peff_over_r[j]
        ndensity = self.quick_number_density_over_r[j]
        UB = self.quick_UB_over_r[j]
        ion_temp = self.quick_ion_temperature_over_r[j]
        thetaj = Consts.kB_cgs*temp/Consts.restmass_cgs
        norm_eq = self.solve_for_PL_norm(gamma1_eq, thetaj, ndensity, PL_eff)
        if self.quick_tsync_const_over_r[j] < self.quick_tIC_const_over_r[j]:
            nt_cooling_rate_eq = self.calculate_sync_cooling_rate(norm_eq, gamma1_eq, PL_eff, j)
        else:
            nt_cooling_rate_eq = self.calculate_IC_cooling_rate(norm_eq, gamma1_eq, PL_eff, j)

        nt_cooling_rate_eq = nt_cooling_rate_eq/self.quick_tau_es[j]
        xcoh = calculate_xcoh(ndensity, temp)
        # RL eq. 7.74b
        amplification_factor = calculate_amplification_factor(ndensity, temp)
        # RL eq. 5.15b
        bremsstrahlung_emissivity = calculate_bremsstrahlung_emissivity(ndensity, temp)
        thermal_cooling_rate_eq = amplification_factor * bremsstrahlung_emissivity
        cooling_rate_eq = nt_cooling_rate_eq + thermal_cooling_rate_eq


        print("Dynamical background has an infall time of {:.1e} rg/c, or {:.1e}s".format(tinfall, tinfall_s))
        print("At r={:.3f}rg, T={:.3e} K, theta={:.2e}, gamma1 = {:.3f}:".format(self.quick_r_range[j], temp, thetaj, gamma1_eq))
        print("Equipartition ion temperature: {:.2e} K".format(ion_temp))
        if not np.allclose(self.volume_heating_rate, cooling_rate_eq):
            print("--------------------------------------------------")
            print("ERROR!!!!!!!!!!!!!!!!!!!!!")
            print("Heating is not equal to cooling, i.e. system did NOT equilibrate!")
            print("--------------------------------------------------")
        print("Cooling rate = {:.3e} erg/s/cm3".format(cooling_rate_eq))
        print("Heating rate = {:.3e} erg/s/cm3".format(self.volume_heating_rate))
        print("Normalization = {:.3e}".format(norm_eq))
        print("Number density = {:.2e} 1/cm3".format(ndensity))

        # Check where absorption becomes important. Use RL Eq. 7.64b.
        # This is the frequency where the plasma becomes optically thin to
        # ELECTRON SCATTERING
        def xt_equations(xt, *args):
            T, ndensity, tau = args
            return xt**3/(1.0 - np.exp(-xt)) - 4e25/(T**(7.0/2.0))*Consts.mp_in_g*ndensity*tau**2
        # Check at inner radius first, then outer.
        Ti = self.temperature_over_r[0]
        taui = self.quick_tau_es[0]
        ni = self.quick_number_density_over_r[0]
        args = (Ti, ni, taui)
        xt_solve = least_squares(xt_equations, 1e-4, args=args, bounds=(0, np.inf))
        xti = xt_solve.x[0]
        nutvali = xti*Consts.kB_cgs*Ti/Consts.h_cgs
        Tf = self.temperature_over_r[-1]
        tauf = self.quick_tau_es[-1]
        nf = self.quick_number_density_over_r[-1]
        args = (Tf, nf, tauf)
        xt_solve = least_squares(xt_equations, 1e-4, args=args, bounds=(0, np.inf))
        xtf = xt_solve.x[0]
        nutvalf = xtf*Consts.kB_cgs*Ti/Consts.h_cgs
        print("Absorption becomes important for frequencies below {:.2e} or {:.2e}Hz".format(nutvali, nutvalf))


    def get_gamma_bins(self):
        gamma_min = 1.0
        gamma_max = self.gamma2_over_r.max()
        gamma_bins = np.logspace(np.log10(gamma_min), np.log10(gamma_max), 100000)
        self.gamma_bins = gamma_bins
        return gamma_bins

    def get_total_distribution_function(self):
        """
        Calculate f_total = f_MJ + f_PL at each radius
        """
        f_dict = {}
        f_dict["thermal"] = {}
        f_dict["nonthermal"] = {}

        total_density_over_r = []
        nonthermal_density_over_r = []
        thermal_density_over_r = []
        average_energy_over_r = []
        nonthermal_energy_over_r = []
        thermal_energy_over_r = []

        for k, temp in enumerate(self.temperature_over_r):
            ndensity = self.quick_number_density_over_r[k]
            theta = Consts.kB_cgs*temp/Consts.restmass_cgs
            fMJ = maxwell_juettner_over_gamma(self.gamma_bins, ndensity, theta)

            gamma1 = self.gamma1_over_r[k]
            gamma2 = self.quick_gamma2_over_r[k]
            peff = self.peff_over_r[k]
            norm = self.solve_for_PL_norm(gamma1, theta, ndensity, peff)
            fPL = power_law_over_gamma(gamma1, gamma2, peff, norm*ndensity, self.gamma_bins)

            f_dict["nonthermal"][k] = fPL
            f_dict["thermal"][k] = fMJ

            ftotal = fPL + fMJ

            total_density = np.trapz(ftotal, x=self.gamma_bins)
            nonthermal_density = np.trapz(fPL, x=self.gamma_bins)
            thermal_density = np.trapz(fMJ, x=self.gamma_bins)

            total_density_over_r.append(total_density)
            nonthermal_density_over_r.append(nonthermal_density)
            thermal_density_over_r.append(thermal_density)

            # Calculate average electron energy
            average_energy = np.trapz(ftotal*(self.gamma_bins-1.0), x=self.gamma_bins)
            average_energy = average_energy*Consts.restmass_cgs/total_density
            nonthermal_energy = np.trapz(fPL*(self.gamma_bins-1.0), x=self.gamma_bins)
            nonthermal_energy = nonthermal_energy*Consts.restmass_cgs/total_density
            thermal_energy = np.trapz(fMJ*(self.gamma_bins-1.0), x=self.gamma_bins)
            thermal_energy = thermal_energy*Consts.restmass_cgs/total_density

            average_energy_over_r.append(average_energy)
            nonthermal_energy_over_r.append(nonthermal_energy)
            thermal_energy_over_r.append(thermal_energy)

        self.total_density_over_r = np.array(total_density_over_r)
        self.nonthermal_density_over_r = np.array(nonthermal_density_over_r)
        self.thermal_density_over_r = np.array(thermal_density_over_r)
        self.average_energy_over_r = np.array(average_energy_over_r)
        self.nonthermal_energy_over_r = np.array(nonthermal_energy_over_r)
        self.thermal_energy_over_r = np.array(thermal_energy_over_r)

        self.f_dict = f_dict
        return f_dict

    def scan_solution_space(self, Tmin=1e6, Tmax=None, Npoints=1000, index=0):
        # if index not in self.max_T_dictionary:
            # self.max_T_dictionary[index] = self.calculate_T_with_min_tEnergyLossEE(index)
        # max_T_with_gamma1_sol = self.max_T_dictionary[index]
        if Tmax == None:
            Tmax = Consts.max_temp*0.90
        print("Scanning solution space at index {:d}".format(index))
        ndensity = self.number_density_over_r[index]
        gamma2 = self.gamma2_over_r[index]
        PL_val = self.PL_indices[index]
        cooling_const = self.cooling_consts_over_r[index]
        beta2 = np.sqrt(1.0 - 1.0/gamma2**2)
        gamma2_theta = beta2**2/(2.0*Consts.turnover_x)
        gamma2_temp = gamma2_theta*Consts.restmass_cgs/Consts.kB_cgs
        Tmax = np.min([gamma2_temp, Tmax])

        temp_range = np.logspace(np.log10(Tmin), np.log10(Tmax), Npoints)
        gamma1_range = np.logspace(np.log10(1.0), np.log10(10000.0), Npoints)
        fractional_errors = []
        gamma1_over_T = []
        PL_eff_over_T = []
        norm_over_T = []
        turnover_gamma_over_T = []
        figure_dir = self.path_to_figures2 + "scan_solution_space_rInd{:d}/".format(index)
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)
        for temp in temp_range:
            error = self.solve_for_equilibrium(temp, index)
            fractional_errors.append(error)
            theta_e = Consts.kB_cgs * temp/Consts.restmass_cgs

            gamma1 = self.solve_for_gamma1(temp, ndensity, gamma2, cooling_const, index)
            gamma1_over_T.append(gamma1)
            turnover_beta, turnover_gamma = get_turnover_beta_gamma(theta_e)
            turnover_gamma_over_T.append(turnover_gamma)

            eff_args = (theta_e, gamma1, gamma2, PL_val, ndensity)
            PL_eff = solve_for_PL_eff(eff_args)
            PL_eff_over_T.append(PL_eff)

            norm = self.solve_for_PL_norm(gamma1, theta_e, ndensity, PL_eff)
            norm_over_T.append(norm)

        best_ind = (np.abs(fractional_errors)).argmin()
        best_error = fractional_errors[best_ind]
        best_temp = temp_range[best_ind]
        best_gamma1 = gamma1_over_T[best_ind]
        best_peff = PL_eff_over_T[best_ind]
        best_norm = norm_over_T[best_ind]

        title_str = r"$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$, ".format(self.bg.Fthetaphi, self.bg.a) + self.PL_titstr + ", " + self.gamma2_titstr + r", $\gamma_{{1, {{\rm min}}}}={:.2f}$".format(self.minimum_gamma1)
        title_str += ", $r={:.2f}r_g$".format(self.r_range[index])
        plt.figure()
        plt.plot(temp_range, fractional_errors)
        plt.plot(best_temp, best_error, marker='o', color='black')
        plt.xlabel("$T$ [K]")
        plt.ylabel(r"$(Q_- - Q_+)/Q_+$")
        plt.title(title_str)
        plt.xscale('log')
        # plt.yscale('log')
        plt.yscale('symlog')
        # plt.ylim([-1.0, 1.0])
        plt.gca().axhline([0], ls='--', color='black')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        fractional_errors = (np.array(fractional_errors)).flatten()
        # plt.gca().fill_between(temp_range, fractional_errors, where=(temp_range >= max_T_with_gamma1_sol), alpha=0.2, hatch='/', color='red')
        plt.xlim([Tmin, Tmax])
        plt.gca().grid(color='.9', ls='--')
        plt.tight_layout()
        plt.savefig(figure_dir + "heating_cooling_error_over_T.png")
        # plt.show()
        plt.close()

        plt.figure()
        plt.plot(temp_range, gamma1_over_T, label=r"$\gamma$ solution")
        plt.plot(temp_range, turnover_gamma_over_T, color='black', ls=':', label=r"$\gamma_{\rm turnover}$")
        plt.plot(best_temp, best_gamma1, marker='o', color='black')
        plt.gca().axhline([self.minimum_gamma1], color='black', ls='--', label=r"Manual lower $\gamma$ limit")
        plt.legend()
        plt.xlabel("$T$ [K]")
        plt.ylabel(r"$\gamma_1$")
        plt.title(title_str)
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim([1.0, None])
        plt.gca().axhline([0], ls='--', color='black')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        gamma1_over_T = (np.array(gamma1_over_T)).flatten()
        # plt.gca().fill_between(temp_range, gamma1_over_T, where=(temp_range >= max_T_with_gamma1_sol), alpha=0.2, hatch='/', color='red')
        plt.xlim([Tmin, Tmax])
        plt.tight_layout()
        plt.gca().grid(color='.9', ls='--')
        plt.savefig(figure_dir + "gamma1_over_T.png")
        # plt.show()
        plt.close()

        plt.figure()
        plt.plot(temp_range, PL_eff_over_T)
        plt.plot(best_temp, best_peff, marker='o', color='black')
        plt.gca().axhline([self.PL_indices[index]], color='black', ls=':')
        plt.xlabel("$T$ [K]")
        plt.ylabel(r"$p_{\rm eff}$")
        plt.title(title_str)
        plt.xscale('log')
        # plt.yscale('log')
        plt.yscale('symlog')
        # plt.ylim([-1.0, 1.0])
        PL_eff_over_T = (np.array(PL_eff_over_T)).flatten()
        # plt.gca().fill_between(temp_range, PL_eff_over_T, where=(temp_range >= max_T_with_gamma1_sol), alpha=0.2, hatch='/', color='red')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.xlim([Tmin, Tmax])
        plt.tight_layout()
        plt.gca().grid(color='.9', ls='--')
        plt.savefig(figure_dir + "PL_eff_over_T.png")
        # plt.show()
        plt.close()

        plt.figure()
        plt.plot(temp_range, norm_over_T)
        plt.plot(best_temp, best_norm, marker='o', color='black')
        plt.xlabel("$T$ [K]")
        plt.ylabel(r"$A_{\rm PL}$")
        plt.title(title_str)
        plt.xscale('log')
        plt.yscale('log')
        # plt.ylim([-1.0, 1.0])
        plt.gca().axhline([0], ls='--', color='black')
        norm_over_T = (np.array(norm_over_T)).flatten()
        # plt.gca().fill_between(temp_range, norm_over_T, where=(temp_range >= max_T_with_gamma1_sol), alpha=0.2, hatch='/', color='red')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.xlim([Tmin, Tmax])
        plt.tight_layout()
        plt.gca().grid(color='.9', ls='--')
        plt.savefig(figure_dir + "norm_over_T.png")
        # plt.show()
        plt.close()

        return

    # --------------------------------------------------------------
    # Some other models that can be tested out, e.g. what if there
    # are no thermal electrons (PL_only)? What if there are no thermal
    # electrons and fixed gamma1 (PL_only_fixed_gamma1)?
    # Answer: cooling is always too efficient. Just saving these
    # models in case I want to come back to them
    # --------------------------------------------------------------
    def solve_for_PL_only(self, gamma1, *args):
        index = args
        ndensity = self.number_density_over_r[index]
        gamma2 = self.gamma2_over_r[index]
        PL_val = self.PL_indices[index]
        PL_norm = (PL_val - 1.0)/(gamma1**(1.0 - PL_val) - gamma2**(1.0 - PL_val))

        if self.ratio_over_r[index] < 1:
            nonthermal_cooling_rate = self.calculate_IC_cooling_rate(PL_norm, gamma1, PL_val, index)
        else:
            nonthermal_cooling_rate = self.calculate_sync_cooling_rate(PL_norm, gamma1, PL_val, index)
        # Decrease nonthermal cooling rate by 1/tau to emulate compton scattering
        nonthermal_cooling_rate = nonthermal_cooling_rate / self.tau_es[index]
        cooling_rate = nonthermal_cooling_rate
        return (cooling_rate - self.volume_heating_rate)/self.volume_heating_rate

    def solve_for_PL_only_fixed_gamma1(self, PL_norm, args):
        index, gamma1 = args
        ndensity = self.number_density_over_r[index]
        gamma2 = self.gamma2_over_r[index]
        PL_val = self.PL_indices[index]
        if self.ratio_over_r[index] < 1:
            nonthermal_cooling_rate = self.calculate_IC_cooling_rate(PL_norm, gamma1, PL_val, index)
        else:
            nonthermal_cooling_rate = self.calculate_sync_cooling_rate(PL_norm, gamma1, PL_val, index)
        # Decrease nonthermal cooling rate by 1/tau to emulate compton scattering
        nonthermal_cooling_rate = nonthermal_cooling_rate / self.tau_es[index]
        cooling_rate = nonthermal_cooling_rate
        return (cooling_rate - self.volume_heating_rate)/self.volume_heating_rate

    def scan_solution_space_PL_only(self, gamma1min=1.001, gamma1max=100, Npoints=1000, index=0):
        print("Scanning solution space at index {:d}".format(index))
        gamma1_range = np.logspace(np.log10(gamma1min), np.log10(gamma1max), Npoints)
        fractional_errors = []
        PL_norms = []
        figure_dir = self.path_to_figures2 + "scan_solution_space_rInd{:d}/PL_only/".format(index)
        ndensity = self.number_density_over_r[index]
        gamma2 = self.gamma2_over_r[index]
        PL_val = self.PL_indices[index]
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)

        for gamma1 in gamma1_range:
            error = self.solve_for_PL_only(gamma1, [index])
            PL_norm = (PL_val - 1.0)/(gamma1**(1.0 - PL_val) - gamma2**(1.0 - PL_val))
            fractional_errors.append(error)
            PL_norms.append(PL_norm)

        plt.figure()
        title_str = r"$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$, ".format(self.bg.Fthetaphi, self.bg.a) + self.PL_titstr + ", " + self.gamma2_titstr + r", $\gamma_{{1, {{\rm min}}}}={:.2f}$".format(self.minimum_gamma1)
        title_str += ", $r={:.2f}r_g$".format(self.r_range[index])
        plt.plot(gamma1_range, fractional_errors)
        plt.xlabel(r"$\gamma_1$")
        plt.ylabel(r"$(Q_- - Q_+)/Q_+$")
        plt.title(title_str)
        plt.xscale('log')
        # plt.yscale('log')
        plt.yscale('symlog')
        # plt.ylim([-1.0, 1.0])
        plt.gca().axhline([0], ls='--', color='black')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.tight_layout()
        plt.savefig(figure_dir + "heating_cooling_error_over_gamma1.png")
        # plt.show()
        plt.close()

        plt.figure()
        plt.plot(gamma1_range, PL_norms)
        plt.xlabel(r"$\gamma_1$")
        plt.ylabel(r"$A$")
        plt.title(title_str)
        plt.xscale('log')
        plt.yscale('log')
        # plt.ylim([-1.0, 1.0])
        # plt.gca().axhline([0], ls='--', color='black')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.tight_layout()
        plt.savefig(figure_dir + "PL_norm_over_gamma1.png")
        # plt.show()
        plt.close()

        min_ind = (np.abs(fractional_errors)).argmin()
        min_norm = PL_norms[min_ind]
        min_gamma1 = gamma1_range[min_ind]
        fPL = power_law_over_gamma(min_gamma1, gamma2, PL_val, min_norm*ndensity, self.gamma_bins)
        density = np.trapz(fPL, x=self.gamma_bins)
        if not np.allclose(density, ndensity):
            print("Problem with density constraint: ")
            print("Solved for powerlaw density: {:.3e}".format(density))
            print("Given (Gammie) density: {:.3e}".format(ndensity))
        return


def solve_for_PL_eff(eff_args):
    theta_e, gamma1, gamma2, PL_val, number_density = eff_args
    # If gamma1 = gamma2, then PL_eff is zero.
    if gamma1 == gamma2:
        return 0.0

    # Note that fMJ(gamma1)=fPL(gamma1) (continuity) and that
    # fPL(gamma2) = fPL0(gamma2); by dividing fPL(gamma1)/fPL(gamma2),
    # we get rid of the dependence on the norm of fPL and can solve for PL_eff
    # ANALTICALLY!
    fMJ_at_gamma1 = maxwell_juettner_at_gamma(gamma1, number_density, theta_e)
    # fPL0 is normalized to give number_density when integrated over gamma.
    if PL_val == 1.0:
        fPL0_norm = number_density * np.log(gamma2)
    else:
        fPL0_norm = number_density * (PL_val - 1.0)/(1.0 - gamma2**(1.0 - PL_val))

    fPL0_at_gamma2 = fPL0_norm * gamma2**(-PL_val)
    PL_eff = np.log(fPL0_at_gamma2/fMJ_at_gamma1)/np.log(gamma1/gamma2)
    # Lower bound on PL effective.
    if PL_eff < -3.0:
        PL_eff = -3.0
    return PL_eff


if __name__ == '__main__':
    kwargs = {}
    kwargs["Fthetaphi"] = 6.0
    kwargs["minimum_gamma1"] = 2.0
    kwargs["a"] = 0.95
    # kwargs["Fthetaphi"] = 1.0
    bg = dynamical_background(**kwargs)

    kwargs["PL_type"] = "WUBCN18"
    # kwargs["PL_type"] = "WUBCN18p"
    # kwargs["PL_type"] = "WPU19"
    # kwargs["PL_type"] = "constant"
    # kwargs["gamma2_type"] = "fixed"
    kwargs["gamma2_type"] = "4sigma"

    # TEST
    # kwargs["no_tau_decrease"] = True
    # ----------
    kwargs["quick"] = True
    kwargs["jump"] = 1
    spectrum_decrease_by_tau = False
    # inc_angle_deg = None
    inc_angle_deg = 60

    equilibrium = equilibrium_finder(bg, **kwargs)
    # print(equilibrium.r_range[0])
    # print("Equipartition Tmin: {:.3e} K".format(equilibrium.ion_temperature_over_r.min()))
    # print("Equipartition Tmax: {:.3e} K".format(equilibrium.ion_temperature_over_r.max()))
    # print(equilibrium.t_thermalize.min())
    # print(equilibrium.t_thermalize.max())
    # equilibrium.calculate_tThermalize_over_r()
    # print("Virial Tmin: {:.3e} K".format(equilibrium.ion_temperature_over_r.min()))
    # print("Virial Tmax: {:.3e} K".format(equilibrium.ion_temperature_over_r.max()))
    # print(equilibrium.t_thermalize.min())
    # print(equilibrium.t_thermalize.max())


    # --------------------------------------------------------------
    # Plot some constant quantities
    # --------------------------------------------------------------
    # print("Saving prescription-independent figures in " + equilibrium.path_to_figures)
    # equilibrium.plot_gammie_quantities()

    # --------------------------------------------------------------
    # Plot some prescription-dependent quantities
    # --------------------------------------------------------------
    # equilibrium.plot_prescription_quantities()

    # print("Saving temperature-dependent quantities in " + equilibrium.path_to_figures2)
    equilibrium.plot_temperature_dependent_quantities()
    # equilibrium.plot_energy_scales()

    # --------------------------------------------------------------
    # Consistency check: calculate relevant time scales
    # --------------------------------------------------------------
    # equilibrium.print_consistency_check()

    # --------------------------------------------------------------
    # Calculate spectra
    # --------------------------------------------------------------
    # Redshifted
    # NOTE that spectrum decrease by tau should be false by default. 1/tau is already included in
    # the nonthermal cooling rate. Setting to true is only a test.
    # equilibrium.plot_spectra(spectrum_decrease_by_tau=False, inc_angle_deg=inc_angle_deg, overwrite=False)
    # # equilibrium.plot_spectra(spectrum_decrease_by_tau=True, inc_angle_deg=inc_angle_deg, overwrite=False)
    # for inc_angle_deg in [0, 15, 30, 45, 60, 75, 90]:
        # equilibrium.plot_spectra(spectrum_decrease_by_tau=False, inc_angle_deg=inc_angle_deg, overwrite=False)
    # equilibrium.plot_spectra_over_r(spectrum_decrease_by_tau=False, inc_angle_deg=inc_angle_deg, overwrite=False)
    # equilibrium.plot_interpolations_over_r()

    # equilibrium = equilibrium_finder(bg, **kwargs)
    # equilibrium.plot_spectra(spectrum_decrease_by_tau=False, inc_angle_deg=60, overwrite=False)

    # for inc_angle_deg in [60]: # [60, 0, 90, 15, 30, 45, 75]:
        # for spin in [0.95, 0.5, 0.7, 0.85]:
            # kwargs["a"] = spin
            # kwargs["Fthetaphi"] = 5.0
            # bg = dynamical_background(**kwargs)
            # equilibrium = equilibrium_finder(bg, **kwargs)
            # equilibrium.plot_spectra(spectrum_decrease_by_tau=False, inc_angle_deg=inc_angle_deg, overwrite=False)

    # quiet = False
    # spectrum_decrease_by_tau = False
    # overwrite = False
    # spins = [0.95]
    # spins = [0.95, 0.5, 0.85]
    # Fthetaphis = [2.0, 3.0, 4.0, 5.0]
    # Fthetaphis = [6.0]
    # inc_angles = [60]
    # inc_angles = [0, 15, 30, 45, 75, 85, 90, 60]
    # for spin in spins:
        # for Fthetaphi in Fthetaphis:
            # kwargs["a"] = spin
            # kwargs["Fthetaphi"] = Fthetaphi
            # bg = dynamical_background(**kwargs)
            # equilibrium = equilibrium_finder(bg, **kwargs)
            # for inc_angle_deg in inc_angles:
                # equilibrium.plot_spectra(quiet, spectrum_decrease_by_tau, overwrite, inc_angle_deg=inc_angle_deg)

    # for spin in [0.5]:
        # for Fthetaphi in [6.0]:
            # kwargs["a"] = spin
            # kwargs["Fthetaphi"] = Fthetaphi
            # bg = dynamical_background(**kwargs)
            # equilibrium = equilibrium_finder(bg, **kwargs)
            # for inc_angle_deg in [0]:
                # redshift_dict = equilibrium.trace_geodesics(inc_angle_deg)
                # print("Furthest photon hit at r/rISCO={:.2f}".format(redshift_dict["disk_emitted_radii"].max()/equilibrium.bg.rISCO))
#
                # equilibrium.plot_spectra(quiet, spectrum_decrease_by_tau, overwrite, inc_angle_deg=inc_angle_deg)
