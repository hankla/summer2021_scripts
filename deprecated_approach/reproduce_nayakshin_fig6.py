import sys
import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from dynamical_background_gammie1999 import *
import scipy.integrate as integrate

# --------------------------------------------------------------
# TODO:
# - Check convergence of thermalization against analytic solution
# --------------------------------------------------------------

M_in_Msun = 10.0
H_over_R = 0.01
kwargs = {}
kwargs["M_in_Msun"] = M_in_Msun
kwargs["H_over_R"] = H_over_R
bg = dynamical_background(**kwargs)

N_interp = 1000
# n_cgs = 10**-5/Consts.mp_in_g
power_law_index = 3.72
p = power_law_index
gamma_min = 1.007
gamma_max = 101
gamma_range = np.logspace(0, np.log10(gamma_max), 10000)
n_cgs = (gamma_max**(1.0-p) - gamma_min**(1.0-p))/(1.0-p)
print(n_cgs)

# Definition of tc from Nayakshin Fig. 1 caption,
# in units of tg
# t_collision = 1.0/(n_cgs * Consts.c_in_cgs * Consts.thomson_cross_section_cgs*20.0)/bg.tg_in_s
t_collision = 0.1
dt = t_collision/10
end_time = 10*t_collision

kwargs = {}
kwargs["t_maxwellian_collision"] = t_collision
kwargs["gamma_range"] = gamma_range
kwargs["dt"] = dt
kwargs["end_time"] = end_time
# pl = plasma(**kwargs)
# rest_mass = pl.particle_mass_cgs * Consts.c_in_cgs**2.0
# gamma_range = pl.gamma_range
#
# powerlaw_f = power_law(pl, n_cgs, gamma_min, gamma_max, power_law_index)
# density = get_number_density(pl, powerlaw_f)
# avg_E = get_average_energy(pl, powerlaw_f)
# maxwellian_f = maxwellian(pl, n_cgs, avg_E)
# density_max = get_number_density(pl, maxwellian_f)
# avg_E_max = get_average_energy(pl, maxwellian_f)
#
# theory_E = (1-p)*rest_mass*(gamma_max**(2-p) - gamma_min**(2-p))/(gamma_max**(1-p) - gamma_min**(1-p))
# print("Power-law density integrates to {:e}, should get {:e}".format(density, n_cgs))
# print("Power-law energy integrates to {:e}, should get {:e}".format(avg_E/rest_mass, theory_E/rest_mass))
# print("Maxwellian density integrates to {:e}, should get {:e}".format(density_max, n_cgs))
# print("Maxwellian energy integrates to {:e}, should get {:e}".format(avg_E_max/rest_mass, theory_E/rest_mass))
#
# # --------------------------------------------------------------
# # Test thermalization of power-law initial distribution function
# # --------------------------------------------------------------
# print("\n--------------------------------------------------------------------")
# print("Testing thermalization of power-law initial distribution function...")
# initial_f = powerlaw_f
# tcoll_str = "{:.2f}".format(t_collision)
#
# kwargs["injection_type"] = "None"

allf_path = bg.path_to_reduced_data + "nayakshin_fig6_solve.txt"
# if not os.path.exists(allf_path) or False:
    # print("Evolving distribution function...")
    # all_f = evolve_distribution_function(initial_f, bg, pl, **kwargs)
    # np.savetxt(allf_path, all_f)
# else:
    # print("Loading distribution function from file...")
    # all_f = np.loadtxt(allf_path)
# all_f = np.loadtxt(allf_path)
# figure_directory = bg.path_to_figures
# figure_directory += "implementation_tests/"
# figure_directory += "nayakshin_fig6/"
# fig_dir = figure_directory + "timesteps/"
#
# pl = plasma(**kwargs)
# energy_initial = get_average_energy(pl, all_f[0,:])
# energy_final = get_average_energy(pl, all_f[-1,:])
# print((energy_final - energy_initial)/energy_initial)
# exit()
# time_range = np.arange(0, end_time, dt)
#
# if not os.path.exists(fig_dir):
    # os.makedirs(fig_dir)
#
# kinetic_energies = pl.gamma_range - 1.0
# norm_factor = np.max(kinetic_energies * maxwellian_f)
# num_timesteps = np.shape(all_f)[0]
# t = 0
# print("Making plots...")
# # Plot 1
# figure_name = "nayakshin_fig6_same_times"
# time1 = 0.068*t_collision
# time2 = 0.235*t_collision
# time3 = 0.710*t_collision
#
# time1_ind = (np.abs(time_range - time1)).argmin()
# time2_ind = (np.abs(time_range - time2)).argmin()
# time3_ind = (np.abs(time_range - time3)).argmin()
#
# plt.figure()
# plt.plot(kinetic_energies, (kinetic_energies * all_f[t, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(0.0), ls=':')
# plt.plot(kinetic_energies, (kinetic_energies * all_f[time1_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time1_ind]/t_collision), ls = '-.')
# plt.plot(kinetic_energies, (kinetic_energies * all_f[time2_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time2_ind]/t_collision), ls = '--')
# plt.plot(kinetic_energies, (kinetic_energies * all_f[time3_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time3_ind]/t_collision), ls = '--')
# plt.plot(kinetic_energies, (kinetic_energies * all_f[-1, :])/norm_factor, label=r"$t\to\infty$",ls = '-')
#
# plt.xlabel(r'$\gamma-1$')
# plt.ylabel(r'$(\gamma-1)f(\gamma)$ (arb. units)')
# plt.gca().grid(color='.9', ls='--')
# plt.gca().tick_params(top=True, right=True, direction='in', which='both')
# plt.legend()
#
# plt.yscale('log')
# plt.xscale('log')
# plt.ylim([1e-5, 1.1])
# plt.xlim([1e-2, 1.1e2])
# plt.savefig(figure_directory + figure_name + '.png', bbox_inches='tight')
#
# # Plot 2
# figure_name = "nayakshin_fig6_match_shape"
#
# time1_ind = 50
# time2_ind = 200
# time3_ind = 400
#
# plt.figure()
# plt.plot(kinetic_energies, (kinetic_energies * all_f[t, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(0.0), ls=':')
# plt.plot(kinetic_energies, (kinetic_energies * all_f[time1_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time1_ind]/t_collision), ls = '-.')
# plt.plot(kinetic_energies, (kinetic_energies * all_f[time2_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time2_ind]/t_collision), ls = '--')
# plt.plot(kinetic_energies, (kinetic_energies * all_f[time3_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time3_ind]/t_collision), ls = '--')
# plt.plot(kinetic_energies, (kinetic_energies * all_f[-1, :])/norm_factor, label=r"$t\to\infty$",ls = '-')
#
# plt.xlabel(r'$\gamma-1$')
# plt.ylabel(r'$(\gamma-1)f(\gamma)$ (arb. units)')
# plt.gca().grid(color='.9', ls='--')
# plt.gca().tick_params(top=True, right=True, direction='in', which='both')
# plt.legend()
#
# plt.yscale('log')
# plt.xscale('log')
# plt.ylim([1e-5, 1.1])
# plt.xlim([1e-2, 1.1e2])
# plt.savefig(figure_directory + figure_name + '.png', bbox_inches='tight')
#
# while t < 5: #num_timesteps:
    # time = time_range[t]
    # figure_name = "t{:03d}".format(t)
#
    # n_t = get_number_density(pl, all_f[t, :])
    # avg_E_t = get_average_energy(pl, all_f[t, :])
    # maxwellian_t = maxwellian(pl, n_t, avg_E_t)
#
    # plt.figure()
    # plt.plot(kinetic_energies, (kinetic_energies * all_f[t, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time/t_collision))
    # plt.plot(kinetic_energies, kinetic_energies*maxwellian_t/norm_factor, label=r"$f_M=f(t\to\infty)$")
    # # plt.gca().axvline([avg_E/rest_mass], color='black', ls='--', label="Eavg initial")
    # # plt.gca().axvline([avg_E_t/rest_mass], color='blue', ls=':', label="Eavg current")
    # # plt.plot(pl.gamma_range, maxwellian_f/initial_f.max(), 'k--', label="Maxwellian")
    # plt.xlabel(r'$\gamma-1$')
    # plt.ylabel(r'$(\gamma-1)f(\gamma)$ (arb. units)')
    # plt.gca().grid(color='.9', ls='--')
    # # plt.gca().grid(which='minor', color='.9', ls='--')
    # plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    # if t == 1:
        # print(t_collision)
        # print(bg.tg_in_s)
    # title_str = r'$t={:.3f}~t_c$'.format(time/t_collision)
#
    # plt.legend()
#
    # plt.title(title_str)
    # plt.yscale('log')
    # plt.xscale('log')
    # plt.ylim([1e-5, 1.1])
    # plt.xlim([1e-2, 1.1e2])
#
    # plt.savefig(fig_dir + figure_name + '.png', bbox_inches='tight')
    # plt.close()
    # t += 1
# print("Done. {:d} timesteps plotted.".format(t))

# --------------------------------------------------------------
# Test thermalization of power-law initial distribution function
# with energy-dependent collision times
# --------------------------------------------------------------
end_time = 1000*dt
kwargs["end_time"] = end_time
kwargs["energy_dependent_collisions"] = True
pl = plasma(**kwargs)
rest_mass = pl.particle_mass_cgs * Consts.c_in_cgs**2.0
gamma_range = pl.gamma_range

powerlaw_f = power_law(pl, n_cgs, gamma_min, gamma_max, power_law_index)
avg_E = get_average_energy(pl, powerlaw_f)
maxwellian_f = maxwellian(pl, n_cgs, avg_E)

print("\n--------------------------------------------------------------------")
print("Testing thermalization of power-law initial distribution function...")
initial_f = powerlaw_f
tcoll_str = "{:.2f}".format(t_collision)

kwargs["injection_type"] = "None"

allf_path = bg.path_to_reduced_data + "energy_dependent_tcol.txt"
allt_path = bg.path_to_reduced_data + "energy_dependent_tcol_times.txt"
if not os.path.exists(allf_path) or False:
    print("Evolving distribution function...")
    (all_f, all_collision_times) = evolve_distribution_function(initial_f, bg, pl, **kwargs)
    np.savetxt(allf_path, all_f)
    np.savetxt(allt_path, all_collision_times)
else:
    print("Loading distribution function from file...")
    all_f = np.loadtxt(allf_path)
    all_collision_times = np.loadtxt(allt_path)
figure_directory = bg.path_to_figures
figure_directory += "implementation_tests/"
figure_directory += "energy_dependent_collisions/"
fig_dir = figure_directory + "timesteps/"
fig_dir2 = figure_directory + "collision_times/"

time_range = np.arange(0, end_time, dt)

if not os.path.exists(fig_dir):
    os.makedirs(fig_dir)
if not os.path.exists(fig_dir2):
    os.makedirs(fig_dir2)

kinetic_energies = pl.gamma_range - 1.0
norm_factor = np.max(kinetic_energies * maxwellian_f)
num_timesteps = np.shape(all_f)[0]
t = 0
print("Making plots...")

figure_name = "nayakshin_fig6_same_times"
time1 = 0.068*t_collision
time2 = 0.235*t_collision
time3 = 0.710*t_collision

time1_ind = (np.abs(time_range - time1)).argmin()
time2_ind = (np.abs(time_range - time2)).argmin()
time3_ind = (np.abs(time_range - time3)).argmin()

plt.figure()
plt.plot(kinetic_energies, (kinetic_energies * all_f[t, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(0.0), ls=':')
plt.plot(kinetic_energies, (kinetic_energies * all_f[time1_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time1_ind]/t_collision), ls = '-.')
plt.plot(kinetic_energies, (kinetic_energies * all_f[time2_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time2_ind]/t_collision), ls = '--')
plt.plot(kinetic_energies, (kinetic_energies * all_f[time3_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time3_ind]/t_collision), ls = '--')
plt.plot(kinetic_energies, (kinetic_energies * all_f[-1, :])/norm_factor, label=r"$t\to\infty$",ls = '-')

plt.xlabel(r'$\gamma-1$')
plt.ylabel(r'$(\gamma-1)f(\gamma)$ (arb. units)')
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.legend()

plt.yscale('log')
plt.xscale('log')
plt.ylim([1e-5, 1.1])
plt.xlim([1e-2, 1.1e2])
plt.savefig(figure_directory + figure_name + '.png', bbox_inches='tight')
plt.close()

# Plot 2
figure_name = "nayakshin_fig6_match_shape"

time1_ind = 20
time2_ind = 50
time3_ind = 99
plt.figure()
plt.plot(kinetic_energies, (kinetic_energies * all_f[t, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(0.0), ls=':')
plt.plot(kinetic_energies, (kinetic_energies * all_f[time1_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time1_ind]/t_collision), ls = '-.')
plt.plot(kinetic_energies, (kinetic_energies * all_f[time2_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time2_ind]/t_collision), ls = '--')
plt.plot(kinetic_energies, (kinetic_energies * all_f[time3_ind, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time_range[time3_ind]/t_collision), ls = '--')
plt.plot(kinetic_energies, (kinetic_energies * all_f[-1, :])/norm_factor, label=r"$t\to\infty$",ls = '-')

plt.xlabel(r'$\gamma-1$')
plt.ylabel(r'$(\gamma-1)f(\gamma)$ (arb. units)')
plt.gca().grid(color='.9', ls='--')
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.legend()

plt.yscale('log')
plt.xscale('log')
plt.ylim([1e-5, 1.1])
plt.xlim([1e-2, 1.1e2])
plt.savefig(figure_directory + figure_name + '.png', bbox_inches='tight')
plt.close()

energy_initial = get_average_energy(pl, all_f[0,:])
energy_final = get_average_energy(pl, all_f[-1,:])
percent_error = (energy_final - energy_initial)/energy_initial
print("Error in energy conservation: {:.2e}".format(percent_error))
number_density = get_number_density(pl, all_f[0, :])
maxwellian_final = maxwellian(pl, number_density, energy_final)

# Collision times
figure_name = "collision_times"
plt.figure()
plt.plot(kinetic_energies, all_collision_times[1, :]/t_collision)
plt.xlabel(r'$\gamma-1$')
plt.ylabel(r'$t_{\rm coll}/t_{\rm Max}$')
plt.gca().grid(color='.9', ls='--')
plt.gca().axvline([energy_initial/rest_mass], color='blue', ls=':', label=r"$\langle\gamma(t=0)\rangle$")
plt.gca().axvline([energy_final/rest_mass], color='red', ls=':', label=r"$\langle\gamma(t\to\infty)\rangle$")
plt.gca().tick_params(top=True, right=True, direction='in', which='both')
plt.legend()
plt.yscale('log')
plt.xscale('log')
plt.ylim([all_collision_times[1, :].min(), all_collision_times[1, :].max()])
plt.xlim([1e-3, 1.1e2])
plt.savefig(fig_dir2 + figure_name + '.png', bbox_inches='tight')
plt.close()

while t < num_timesteps:
    time = time_range[t]
    figure_name = "t{:03d}".format(t)
    plt.figure()
    plt.plot(kinetic_energies, (kinetic_energies * all_f[t, :])/norm_factor, label=r"$t={:.2f}~t_c$".format(time/t_collision))
    plt.plot(kinetic_energies, kinetic_energies*maxwellian_final/norm_factor, label=r"$f_M=f(t\to\infty)$")
    # plt.gca().axvline([avg_E/rest_mass], color='black', ls='--', label="Eavg initial")
    # plt.gca().axvline([avg_E_t/rest_mass], color='blue', ls=':', label="Eavg current")
    # plt.plot(pl.gamma_range, maxwellian_f/initial_f.max(), 'k--', label="Maxwellian")
    plt.xlabel(r'$\gamma-1$')
    plt.ylabel(r'$(\gamma-1)f(\gamma)$ (arb. units)')
    plt.gca().grid(color='.9', ls='--')
    # plt.gca().grid(which='minor', color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    title_str = r'$t={:.3f}~t_c$'.format(time/t_collision)
    plt.legend()
    plt.title(title_str)
    plt.yscale('log')
    plt.xscale('log')
    plt.ylim([1e-5, 1.1])
    plt.xlim([1e-2, 1.1e2])
    plt.savefig(fig_dir + figure_name + '.png', bbox_inches='tight')
    plt.close()

    t += 1
