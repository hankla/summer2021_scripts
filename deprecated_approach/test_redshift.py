import sys
import numpy as np
import matplotlib
import pickle
from scipy.interpolate import interp1d
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from equilibrium_finder import *
from dynamical_background_gammie1999 import *
import geokerr_interface as gi
import calcg_lnrf as calcg

small_size = 22
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)
matplotlib.rc('text', usetex=True)

def trace_geodesics(eq, inc_angle_deg=60, decrease_by_tau=True):
    # --------------------------------------------------------------
    # Trace geodesics
    # --------------------------------------------------------------
    # Create a camera at infinity
    g = gi.geokerr()
    # For each pixel in the camera, find whether the geodesic hits the midplane
    cos_inclination = np.cos(inc_angle_deg*np.pi/180) # set the inclination angle of the camera
    avals = [-4., 4., -4., 4.] # xy extent of the camera in rg
    nvals = [64, 64, 1] # number of pixels on the camera
    dalpha = (avals[1] - avals[0])*eq.bg.get_rg_in_cm(eq.M_in_Msun)/nvals[0]
    dbeta = (avals[3] - avals[2])*eq.bg.get_rg_in_cm(eq.M_in_Msun)/nvals[1]
    u_locations, mu_locations, dt, out_l, dphi, tpm, tpr, i1, i2 = g.run_camera(mu0=cos_inclination, a=bg.a, standard=2, avals=avals, n=nvals)

    q2 = g.q2
    su = g.su
    sm = g.sm
    l = g.l # NOT the output

    # If a pixel i's geodesic misses the midplane, u_locations[i] = -1
    # Otherwise, u_locations[i] is the radius r=1/u where the photon hit.
    # We assume that the gas at that radiates emitted the photon.
    # To calculate redshift, we need to know the photon momentum in the
    # locally non-rotating frame. We need to transform the gas's velocity at that location
    # (given by the Gammie 1999 trajectories).
    # -------------------------------------------
    # Eliminate photons that don't hit the disk
    indices_to_remove = u_locations == -1.0
    print("{:d} pixels' geodesics did not hit the disk.".format(indices_to_remove.sum()))
    u_locations = u_locations[~indices_to_remove]
    mu_locations = mu_locations[~indices_to_remove]
    dt = dt[~indices_to_remove]
    dphi = dphi[~indices_to_remove]
    tpm = tpm[~indices_to_remove]
    tpr = tpr[~indices_to_remove]
    indices_to_remove = np.squeeze(indices_to_remove)
    i1 = i1[~indices_to_remove]
    i2 = i2[~indices_to_remove]
    q2 = q2[~indices_to_remove]
    su = su[~indices_to_remove]
    sm = sm[~indices_to_remove]
    l = l[~indices_to_remove]

    # Eliminate photons that hit the disk outside the plunging region
    photon_emitted_radii = 1./u_locations
    indices_to_remove = photon_emitted_radii >= eq.starting_r
    print("{:d} pixels' geodesics hit at r>rISCO.".format(indices_to_remove.sum()))
    photon_emitted_radii = photon_emitted_radii[~indices_to_remove]
    u_locations = u_locations[~indices_to_remove]
    mu_locations = mu_locations[~indices_to_remove]
    dt = dt[~indices_to_remove]
    dphi = dphi[~indices_to_remove]
    tpm = tpm[~indices_to_remove]
    tpr = tpr[~indices_to_remove]
    indices_to_remove = np.squeeze(indices_to_remove)
    i1 = i1[~indices_to_remove]
    i2 = i2[~indices_to_remove]
    q2 = q2[~indices_to_remove]
    su = su[~indices_to_remove]
    sm = sm[~indices_to_remove]
    l = l[~indices_to_remove]

    # ------------------------------
    # Get the Gammie 1999 velocities
    # ------------------------------
    # Interpolate between radii
    uT_interp_f = interp1d(eq.bg.r_range, eq.bg.uTsolution)
    uR_interp_f = interp1d(eq.bg.r_range, eq.bg.uRsolution)
    uPhi_interp_f = interp1d(eq.bg.r_range, eq.bg.uPhisolution)
    # Use interpolation at emitted radii (boyer-lindquist coords)
    uT_photons = uT_interp_f(photon_emitted_radii)
    uR_photons = uR_interp_f(photon_emitted_radii)
    uPhi_photons = uPhi_interp_f(photon_emitted_radii)
    uTheta_photons = 0.0*np.ones(uR_photons.shape)

    # ------------------------------
    # Transform to locally non-rotating frame
    # ------------------------------
    theta_array = np.pi/2.*np.ones(uR_photons.shape)
    # Calculate three-velocities
    vR_bl = uR_photons/uT_photons
    vTheta_bl = uTheta_photons/uT_photons
    vPhi_bl = uPhi_photons/uT_photons
    vMag2 = vR_bl**2 + vTheta_bl**2 + vPhi_bl**2
    vR_lnrf, vTheta_lnrf, vPhi_lnrf = calcg.lnrf_frame(vR_bl, vTheta_bl, vPhi_bl, photon_emitted_radii, eq.bg.a, theta_array)

    # For some reason the transformed velocity can be faster than c, just for one?
    # TODO
    vMag2 = vR_lnrf**2 + vTheta_lnrf**2 + vPhi_lnrf**2
    print("Number of velocities with v > c: {:d}".format((vMag2 >= 1).sum()))
    indices_to_remove = vMag2 >= 1
    photon_emitted_radii = photon_emitted_radii[~indices_to_remove]
    u_locations = u_locations[~indices_to_remove]
    mu_locations = mu_locations[~indices_to_remove]
    dt = dt[~indices_to_remove]
    dphi = dphi[~indices_to_remove]
    tpm = tpm[~indices_to_remove]
    tpr = tpr[~indices_to_remove]
    i1 = i1[~indices_to_remove]
    i2 = i2[~indices_to_remove]
    q2 = q2[~indices_to_remove]
    su = su[~indices_to_remove]
    sm = sm[~indices_to_remove]
    l = l[~indices_to_remove]
    vR_lnrf = vR_lnrf[~indices_to_remove]
    vTheta_lnrf = vTheta_lnrf[~indices_to_remove]
    vPhi_lnrf = vPhi_lnrf[~indices_to_remove]

    # ------------------------------
    # Calculate redshift from each photon
    # ------------------------------
    redshifts = calcg.calcg(u_locations, mu_locations, q2, l, eq.bg.a, tpm, tpr, su, sm, vR_lnrf, vTheta_lnrf, vPhi_lnrf)
    # print(np.isnan(redshifts).sum())
    # redshifts = redshifts[~np.isnan(redshifts)]

    # plt.figure()
    # plt.hist(redshifts, bins=100)
    # plt.xlabel(r"Redshift $g$")
    # plt.ylabel("Number of photons")
    # plt.show()

    # Sort redshifts to be increasing radius
    sorted_inds = photon_emitted_radii.argsort()
    photon_emitted_radii = photon_emitted_radii[sorted_inds[::-1]]
    redshifts = redshifts[sorted_inds[::-1]]

    figure_dir = eq.path_to_figures
    plt.plot(photon_emitted_radii, redshifts, ls='None', marker='o')
    plt.ylabel(r"Redshift $g$")
    plt.xlabel(r"$r/r_g$")
    plt.title(r"$i=${:.0f} degrees".format(inc_angle_deg))
    plt.savefig(figure_dir + "redshifts_over_r_i{:d}.png".format(inc_angle_deg))
    plt.close()

    redshift_dict = {"photon_emitted_radii":photon_emitted_radii,
                     "redshifts":redshifts,
                     "dalpha":dalpha,
                     "dbeta":dbeta}
    return redshift_dict

def calculate_redshifted_spectra(eq, redshift_dict, decrease_by_tau=True):
    # Load in camera properties
    photon_emitted_radii = redshift_dict["photon_emitted_radii"]
    redshifts = redshift_dict["redshifts"]
    dalpha = redshift_dict["dalpha"]
    dbeta = redshift_dict["dbeta"]
    spectrum_dict = {}

    # Load in multizone equilibria quantites
    eq.get_quick_arrays()
    temperature_over_r = eq.temperature_over_r
    gamma1_over_r = eq.gamma1_over_r
    peff_over_r = eq.peff_over_r
    gamma2_over_r = eq.quick_gamma2_over_r
    r_range = eq.quick_r_range
    ndensity_over_r = eq.quick_number_density_over_r
    Br_over_r = eq.quick_Br_over_r
    tau_es_over_r = eq.quick_tau_es
    tsync_over_r = eq.quick_tsync_over_r
    tIC_over_r = eq.quick_tIC_over_r

    spectral_bins = eq.get_spectral_bins()

    # Create interpolation functions at photon emitted radii
    temp_interp_f = interp1d(r_range, temperature_over_r, fill_value="extrapolate")
    n_interp_f = interp1d(r_range, ndensity_over_r, fill_value="extrapolate")
    Br_interp_f = interp1d(r_range, Br_over_r, fill_value="extrapolate")
    gamma1_interp_f = interp1d(r_range, gamma1_over_r, fill_value="extrapolate")
    gamma2_interp_f = interp1d(r_range, gamma2_over_r, fill_value="extrapolate")
    peff_interp_f = interp1d(r_range, peff_over_r, fill_value="extrapolate")
    tau_interp_f = interp1d(r_range, tau_es_over_r, fill_value='extrapolate')
    tsync_interp_f = interp1d(r_range, tsync_over_r, fill_value='extrapolate')
    tIC_interp_f = interp1d(r_range, tIC_over_r, fill_value='extrapolate')

    nonthermal_integrands = np.zeros(spectral_bins.shape)
    thermal_integrands = np.zeros(spectral_bins.shape)
    nonthermal_redshift_integrands = np.zeros(spectral_bins.shape)
    thermal_redshift_integrands = np.zeros(spectral_bins.shape)
    r_range_cm = photon_emitted_radii * bg.get_rg_in_cm(eq.M_in_Msun)
    # For each pixel, calculate Inu(r).
    for j, radius in enumerate(photon_emitted_radii):
        gamma1 = gamma1_interp_f(radius)
        gamma2 = gamma2_interp_f(radius)
        Br = Br_interp_f(radius)
        n_at_r = n_interp_f(radius)
        temp_at_r = temp_interp_f(radius)
        theta = Consts.kB_cgs*temp_at_r/Consts.restmass_cgs
        peff = peff_interp_f(radius)
        norm = eq.solve_for_PL_norm(gamma1, theta, n_at_r, peff)
        args = (gamma2, Br, n_at_r)
        if tsync_interp_f(radius) < tIC_interp_f(radius):
            nt_cooling_rate = eq.calculate_sync_cooling_rate(norm, gamma1, peff, None, args)
        else:
            nt_cooling_rate = eq.calculate_IC_cooling_rate(norm, gamma1, peff, None, args)

        # ---------------------------------
        # Calculate Inu of power-law electrons
        # ---------------------------------
        # Assume spectrum is A*nu^(-s) between nu1 and nu2.
        # Find A = spectrum_constant by integrating and set equal to cooling rate
        omega_const = 3*Consts.e_in_cgs*np.abs(Br)/(2*Consts.me_in_g*Consts.c_in_cgs)
        nu1 = omega_const*gamma1**2/(2*np.pi)
        nu2 = omega_const*gamma2**2/(2*np.pi)
        s_index = (peff - 1.)/2.
        start_index = (np.abs(nu1 - spectral_bins)).argmin()
        end_index = (np.abs(nu2 - spectral_bins)).argmin()
        nu1_bin = spectral_bins[start_index]
        nu2_bin = spectral_bins[end_index]

        spectrum_constant = nt_cooling_rate * (1.-s_index)/(nu2_bin**(1.-s_index) - nu1_bin**(1.-s_index))

        Pnu = spectrum_constant * spectral_bins**(-s_index)
        Pnu[:start_index] = 0
        Pnu[end_index:] = 0

        # Because the integration/split between bins isn't perfect,
        # use residual factor to ensure the total power is correct.
        P_nonthermal = np.trapz(Pnu, x=spectral_bins)
        residual_factor =  nt_cooling_rate/P_nonthermal
        Pnu = Pnu * residual_factor
        P_nonthermal = np.trapz(Pnu, x=spectral_bins)

        # Isotropic emission --> jnu = Pnu/4pi
        # dInu = jnu*ds. Set ds to be height (same as radius for H/R=1)
        height = radius*bg.get_rg_in_cm(eq.M_in_Msun)*eq.H_over_R
        Inu_nonthermal_orig = Pnu/(4.0*np.pi)*height

        # Decrease nonthermal intensity by 1/tau
        # NOTE: doing this will make power_in_powerlaw != heating_rate*volume
        # unless accounted for in thermal.
        if decrease_by_tau:
            Inu_thermalized = Inu_nonthermal_orig*(1.0 - 1.0/tau_interp_f(radius))
            Inu_nonthermal = Inu_nonthermal_orig/tau_interp_f(radius)
            nt_cooling_rate = nt_cooling_rate/tau_interp_f(radius)
        else:
            Inu_nonthermal = Inu_nonthermal_orig

        # Redshift
        Inu_nonthermal_redshift = Inu_nonthermal*redshifts[j]**3 # multiply by g^3
        nonthermal_integrands = np.vstack([nonthermal_integrands, Inu_nonthermal])
        nonthermal_redshift_integrands = np.vstack([nonthermal_redshift_integrands, Inu_nonthermal_redshift])

        # ---------------------------------
        # Calculate Inu of thermal electrons
        # ---------------------------------
        # RL eq. 7.65b
        xcoh = 2.4e17 * np.sqrt(Consts.me_in_g*n_at_r)*(temp_at_r)**(-9.0/4.0)
        # RL eq. 7.74b
        amplification_factor = 0.75*(np.log(2.25/xcoh))**2
        # RL eq. 5.15b
        gaunt_factor = 1.2
        bremsstrahlung_emissivity = 1.4e-27*np.sqrt(temp_at_r)*n_at_r**2*gaunt_factor
        thermal_cooling_rate = amplification_factor * bremsstrahlung_emissivity
        # Account for energy in power law thermalized by scattering
        if decrease_by_tau:
            thermalized_cooling_rate = np.trapz(Inu_thermalized*spectral_bins, x=np.log(spectral_bins))*np.pi*4./height
            thermal_cooling_rate += thermalized_cooling_rate
        thermal_jnu = thermal_cooling_rate/4.0/np.pi

        # Make sure that I=j*H = RL eq. 7.71, j = P/4\pi = A\epsilon/4\pi
        intensity_constant = 12.0*Consts.kB_cgs**4.*temp_at_r**4./(Consts.c_in_cgs**2.*Consts.h_cgs**3.)
        exponential_decrease = thermal_jnu*height/intensity_constant
        Inu_thermal = 2*Consts.h_cgs * spectral_bins**3./Consts.c_in_cgs**2. * np.exp(-Consts.h_cgs*spectral_bins/(Consts.kB_cgs*temp_at_r))
        Inu_thermal = Inu_thermal*exponential_decrease

        # Because the integration/split between bins isn't perfect,
        # use residual factor to ensure the total power is correct.
        P_thermal = np.trapz(Inu_thermal, x=spectral_bins)*4.0*np.pi/height
        residual_factor =  thermal_cooling_rate/P_thermal
        Inu_thermal = Inu_thermal * residual_factor

        # Redshift
        Inu_thermal_redshift = Inu_thermal * redshifts[j]**3 # multiply by g^3
        thermal_integrands = np.vstack([thermal_integrands, Inu_thermal])
        thermal_redshift_integrands = np.vstack([thermal_redshift_integrands, Inu_thermal_redshift])

    nonthermal_integrands = nonthermal_integrands[1:, :]
    thermal_integrands = thermal_integrands[1:, :]
    nonthermal_redshift_integrands = nonthermal_redshift_integrands[1:, :]
    thermal_redshift_integrands = thermal_redshift_integrands[1:, :]

    # For distant observer:
    # Fnu = \int Inu dA/D^2 = \int Inu 2\pi rdr/D^2, D is distance
    # Lnu = 4\pi D^2 Fnu = independent of D
    # For camera: Fnu = sum over each pixel
    #                 = \Delta\alpha\Delta\beta/D^2*sum(intensity at each pixel)
    flux_const_nonthermal_redshift = dalpha*dbeta*np.sum(nonthermal_redshift_integrands, axis=0)
    flux_const_thermal_redshift = dalpha*dbeta*np.sum(thermal_redshift_integrands, axis=0)
    flux_const_nonthermal = dalpha*dbeta*np.sum(nonthermal_integrands, axis=0)
    flux_const_thermal = dalpha*dbeta*np.sum(thermal_integrands, axis=0)

    # Isotropic Lnu = 4\pi D^2 Fnu
    Lnu_nonthermal_redshift = 4.0*np.pi*flux_const_nonthermal_redshift
    Lnu_thermal_redshift = 4.0*np.pi*flux_const_thermal_redshift
    Lnu_nonthermal = 4.0*np.pi*flux_const_nonthermal
    Lnu_thermal = 4.0*np.pi*flux_const_thermal

    power_in_powerlaw = np.trapz(Lnu_nonthermal_redshift, spectral_bins)
    power_in_thermal_pr = np.trapz(Lnu_thermal_redshift, spectral_bins)
    print("Integrated Lnu over power-law: {:.2e}".format(power_in_powerlaw))
    print("Integrated Lnu over thermal: {:.2e}".format(power_in_thermal_pr))
    print("Heating rate*volume: {:.2e}".format(eq.heating_rate*eq.emitting_volume))

    spectrum_dict["spectral_bins"] = spectral_bins
    spectrum_dict["Lnu_nonthermal"] = Lnu_nonthermal_redshift
    spectrum_dict["Lnu_thermal"] = Lnu_thermal_redshift
    spectrum_dict["power_in_powerlaw"] = power_in_powerlaw
    spectrum_dict["power_in_thermal_pr"] = power_in_thermal_pr

    spectrum_path = eq.path_to_reduced_data + "spectrum"
    if not decrease_by_tau:
        spectrum_path += "_noTauDecrease"
    spectrum_path += "_redshifted_i{:d}.png".format(inc_angle_deg)
    spectrum_path += ".p"
    with open(spectrum_path, 'wb') as file:
        pickle.dump(spectrum_dict, file)
    return spectrum_dict

def retrieve_redshifted_spectra(eq, inc_angle_deg=60, overwrite=False, decrease_by_tau=True):
    spectrum_path = eq.path_to_reduced_data + "spectrum"
    if not decrease_by_tau:
        spectrum_path += "_noTauDecrease"
    spectrum_path += "_redshifted_i{:d}.png".format(inc_angle_deg)
    spectrum_path += ".p"
    if overwrite or (not os.path.exists(spectrum_path)):
        redshift_dict = trace_geodesics(eq, inc_angle_deg, decrease_by_tau)
        print("Calculating redshifted spectrum...")
        spectrum_dict = calculate_redshifted_spectra(eq, redshift_dict, decrease_by_tau)
    else:
        print("Loading spectrum from pickle file " + spectrum_path)
        with open(spectrum_path, 'rb') as file:
            spectrum_dict = pickle.load(file)
    return spectrum_dict

def plot_redshifted_spectra(eq, inc_angle_deg=60, overwrite=False, decrease_by_tau=True):
    spectrum_dict = retrieve_redshifted_spectra(eq, inc_angle_deg, overwrite, decrease_by_tau)

    def nuToKev(nu):
        return 4.1e-18 * nu
    def kevToNu(keV):
        return keV/(4.1e-18)

    Lnu_thermal = spectrum_dict["Lnu_thermal"]
    Lnu_nonthermal = spectrum_dict["Lnu_nonthermal"]
    spectral_bins = spectrum_dict["spectral_bins"]
    total_spectrum = Lnu_nonthermal + Lnu_thermal
    max_val = (total_spectrum*spectral_bins).max()

    figure_dir = eq.path_to_figures2
    plt.figure()
    plt.plot(spectral_bins, Lnu_nonthermal*spectral_bins,
            label=r'$\nu L_{\nu, {\rm PL}}$', color="C0", ls="-")
    plt.plot(spectral_bins, Lnu_thermal*spectral_bins,
            label=r'$\nu L_{\nu, {\rm thermal}}$', ls=':', color="C2")
    # plt.plot(spectral_bins, disk_function*spectral_bins,
            # label=label_str, ls='--', color="C1")
    plt.plot(spectral_bins, total_spectrum*spectral_bins, color='black', label='Total', lw=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r"$\nu/{\rm Hz}$ ")
    title_str = r"$F_{{\theta\phi}}={:.2f}$, $\gamma_2=$".format(bg.Fthetaphi) + eq.gamma2_str + ", " + eq.PL_titstr + "\n"
    title_str += r"$i=${:.0f} degrees".format(inc_angle_deg)
    plt.title(title_str)
    plt.gca().tick_params(right=True, direction='in', which='both')
    secax = plt.gca().secondary_xaxis('top', functions=(nuToKev, kevToNu))
    secax.tick_params(top=True, direction='in', which='both')
    secax.set_xlabel(r'$\nu/{\rm keV}$')
    plt.ylim([1e34, max_val])
    # plt.ylim([1e25, max_val])
    plt.xlim([spectral_bins.min(), spectral_bins.max()])
    plt.legend()
    plt.tight_layout()
    fname = "spectrum_added_over_r"
    if not decrease_by_tau:
        fname += "_noTauDecrease"
    fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
    plt.savefig(figure_dir + fname)
    plt.close()



if __name__ == '__main__':
    kwargs = {}
    kwargs["Fthetaphi"] = 6.0
    # kwargs["Fthetaphi"] = 4.0
    bg = dynamical_background(**kwargs)
    decrease_by_tau = True
    overwrite = False

    kwargs["PL_type"] = "WUBCN18"
    # kwargs["PL_type"] = "WPU19"
    # kwargs["PL_type"] = "constant"
    # kwargs["gamma2_type"] = "fixed"
    kwargs["gamma2_type"] = "4sigma"

    kwargs["quick"] = True
    kwargs["jump"] = 1

    eq = equilibrium_finder(bg, **kwargs)
    inc_angle_deg = 30
    plot_redshifted_spectra(eq, inc_angle_deg, overwrite, decrease_by_tau)
