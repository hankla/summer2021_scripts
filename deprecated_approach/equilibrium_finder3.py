import sys
import numpy as np
import matplotlib
import pickle
from scipy.optimize import fsolve
from scipy.optimize import least_squares
import scipy.integrate as integrate
from matplotlib.legend import Legend
from matplotlib.lines import Line2D
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from dynamical_background_gammie1999 import *
from scipy import special
import geokerr_interface as gi
import calcg_lnrf as calcg

small_size = 22
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)
matplotlib.rc('text', usetex=True)

class equilibrium_finder:
    def __init__(self, bg, setup="lia_hp", **kwargs):
        """
        TODO:
        """
        overwrite_self = kwargs.get("overwrite_self", False)
        self.Fthetaphi = bg.Fthetaphi
        self.bg = bg
        self.electron_heating_fraction = kwargs.get("electron_heating_fraction", 0.5)

        # NOTE: everything below this point will be overwritten
        # if equilibrium file already exists. That's good
        # because otherwise the user might think they changed
        # a parameter when really they didn't.

        # --------------------------------------------------------------
        # Input PIC prescriptions to use
        # --------------------------------------------------------------
        # PL_type and gamma2_type determine the prescription used to set
        # the power-law index and high-energy cutoff, respectively.
        # They can be set to constant or to use a PIC prescription.
        # PL_type options: "WUBCN18", "WPU19", "constant"
        # gamma2_type options: "fixed", "4sigma", "6sigma"
        self.PL_type = kwargs.get("PL_type", "WUBCN18")
        self.gamma2_type = kwargs.get("gamma2_type", "4sigma")
        # --------------------------------------------------------------
        # Handle prescriptions for p and gamma2
        # --------------------------------------------------------------
        if self.PL_type == "constant":
            self.PL_index_constant = 2.9
            self.PL_filestr = "Constant{:.2f}".format(self.PL_index_constant)
            self.PL_titstr = "$p={:.2f}$".format(self.PL_index_constant)
        else:
            self.PL_filestr = self.PL_type
            self.PL_titstr = self.PL_type + " PL prescription"

        if self.gamma2_type == "fixed":
            self.gamma2_constant = kwargs.get("gamma2_constant", 1000)
            self.gamma2_str = "{:.2e}".format(self.gamma2_constant)
            self.gamma2_titstr = r"$\gamma_2={:.2e}$".format(self.gamma2_constant)
        else:
            self.gamma2_str = self.gamma2_type
            self.gamma2_titstr = r"$\gamma_2$ " + self.gamma2_str

        # -----------------------------------
        #      Add default setup information here
        # -----------------------------------
        if setup == "lia_hp":
            path_to_figures = "/mnt/c/Users/liaha/research/projects/"\
                "summer2021/figures/multizone_model/Fthetaphi{:.2f}a{:.2f}/".format(self.Fthetaphi, bg.a)
            # BRANCH: rename file so don't overwrite anything XX
            path_to_figures2 = path_to_figures + "delta{:.2f}_cutoff".format(self.electron_heating_fraction) + self.gamma2_str + "_index" + self.PL_filestr + "/"
            path_to_figures2 = path_to_figures + "delta{:.2f}_cutoff".format(self.electron_heating_fraction) + self.gamma2_str + "_index" + self.PL_filestr + "/rewrite2/"
            path_to_reduced_data = "/mnt/c/Users/liaha/research/projects/"\
                "summer2021/data_reduced/Fthetaphi{:.2f}a{:.2f}/".format(self.Fthetaphi, bg.a)
            # BRANCH: rename file so don't overwrite anything XX
            pickle_path = path_to_reduced_data + "equilibrium_solution_delta{:.2f}_cutoff".format(self.electron_heating_fraction) + self.gamma2_str + "_index" + self.PL_filestr + ".p"
            pickle_path = path_to_reduced_data + "equilibrium_solution_delta{:.2f}_cutoff".format(self.electron_heating_fraction) + self.gamma2_str + "_index" + self.PL_filestr + "_rewrite2.p"

        # -----------------------------------
        # If paths are non-standard, set them here using kwargs
        # Otherwise, use the defaults from above
        # -----------------------------------
        self.path_to_figures = kwargs.get("path_to_figures", path_to_figures)
        self.path_to_figures2 = kwargs.get("path_to_figures", path_to_figures2)
        self.path_to_reduced_data = kwargs.get("path_to_reduced_data",
                                               path_to_reduced_data)
        self.pickle_path = kwargs.get("pickle_path", pickle_path)

        if os.path.exists(self.pickle_path) and (not overwrite_self):
            print("Loading equilibrium solution from " + self.pickle_path)
            self._load_from_pickle()
            try:
                self.volume_heating_rate = self.heating_rate
            except:
                print('')

        else:
            self.temperature_dictionary = {}
            self.gamma1_dictionary = {}
            self.peff_dictionary = {}
            self._initialize(**kwargs)

        self.quick = kwargs.get("quick", True)
        self.jump = kwargs.get("jump", 50)
        # --------------------------------------------------------------
        # Main loop: calculate equilibrium temperature at each radius
        # --------------------------------------------------------------
        for i, r in enumerate(self.r_range):
            if self.quick and not (i%self.jump == 0):
                continue
            if i in self.temperature_dictionary:
                # print("i={:d} already in dictionary. Skipping.".format(i))
                continue

            if self.ratio_over_r[i] < 1:
                print("Using IC cooling time for i={:d}.".format(i))
            else:
                print("Using synchrotron cooling time for i={:d}.".format(i))

            gamma1_guess = 1.5
            Te_estimate = 1e9
            initial_guesses = (gamma1_guess, Te_estimate)
            lower_bounds = [1.0, 1e7]
            upper_bounds = [self.gamma2_over_r[i], 1e12]
            gamma_bounds = (1.0, self.gamma2_over_r[i])
            temp_bounds = (1e7, 1e12)

            heating_constraint = lambda x: self.calculate_sync_cooling_rate(norm, x[0], PL_eff, index) - self.volume_heating_rate
            cons = ({'type':'eq', 'fun':heating_constraint})

            equilibrium_solve = minimize(self.solve_for_equilibrium, initial_guesses, args=([i]), bounds=(gamma_bounds, temp_bounds), constraints=cons)
            eq_gamma1 = equilibrium_solve.x[0]
            eq_temperature = equilibrium_solve.x[1]
            self.gamma1_dictionary[i] = eq_gamma1
            self.temperature_dictionary[i] = eq_temperature
            theta_e = Consts.kB_cgs * eq_temperature/Consts.restmass_cgs
            eff_args = (theta_e, eq_gamma1, self.gamma2_over_r[i], self.PL_indices[i], self.number_density_over_r[i])
            eq_peff = solve_for_PL_eff(eff_args)
            self.peff_dictionary[i] = eq_peff
            print(equilibrium_solve.x)
            # print(equilibrium_solve.fun)

        # -----------------------------------
        # Dump to file
        # -----------------------------------
        with open(self.pickle_path, 'wb') as fh:
            pickle.dump(self, fh)


    def _initialize(self, **kwargs):
        print("Creating new equilibrium solution.")
        self.to_print = kwargs.get("printing", True)
        self.accretion_efficiency = 1.0 + self.bg.FEval
        # --------------------------------------------------------------
        # Input parameters using rough estimates
        # --------------------------------------------------------------
        self.M_in_Msun = kwargs.get("M_in_Msun", 10.0)
        self.f_Ledd = kwargs.get("f_Ledd", 0.1)
        self.f_Mdot = kwargs.get("f_Mdot", 0.1)
        self.H_over_R = kwargs.get("H_over_R", 1.0)
        # Starting/ending radii will influence the heating rate that balances
        # the cooling rate in the equilibrium solver, since the total heating
        # rate is divided by the volume of the relevant region
        self.starting_r = kwargs.get("starting_r", 1.8) # in rg. Use to avoid density spike at r=1.9rg
        self.ending_r = kwargs.get("ending_r", self.bg.rEH) # in rg. Use to avoid density weirdness at r=1.3rg
        self.Te_estimate = kwargs.get("Te_estimate", 1e9)

        # -----------------------------------
        # Create directories if need be
        # -----------------------------------
        if not os.path.exists(self.path_to_figures):
            os.makedirs(self.path_to_figures)
        if not os.path.exists(self.path_to_figures2):
            os.makedirs(self.path_to_figures2)
        if not os.path.exists(self.path_to_reduced_data):
            os.makedirs(self.path_to_reduced_data)

        # -----------------------------------
        # Create shortened arrays
        # -----------------------------------
        inner_index = (np.abs(self.starting_r - self.bg.r_range)).argmin()
        outer_index = (np.abs(self.ending_r - self.bg.r_range)).argmin()
        # outer_index = -1
        # These arrays do not take into account the jump
        # used for faster running
        self.r_range = self.bg.r_range[inner_index:outer_index]
        self.Br_over_r = self.bg.get_Br_in_cgs(self.M_in_Msun, self.f_Ledd, self.f_Mdot, self.H_over_R)[inner_index:outer_index]
        self.mass_density_over_r = self.bg.get_mass_density_in_cgs(self.M_in_Msun, self.f_Ledd, self.f_Mdot, self.H_over_R)[inner_index:outer_index]
        self.number_density_over_r = self.mass_density_over_r / Consts.mp_in_g
        self.UB_over_r = self.Br_over_r**2.0/(8.0*np.pi)
        Br_ISCO = self.bg.get_Br_in_cgs(self.M_in_Msun, self.f_Ledd, self.f_Mdot, self.H_over_R)[0]
        n_ISCO = self.bg.get_mass_density_in_cgs(self.M_in_Msun, self.f_Ledd, self.f_Mdot, self.H_over_R)[0]
        self.ISCO_equipartition_T = Br_ISCO**2/(8.0*np.pi)/n_ISCO/Consts.kB_cgs

        # --------------------------------------------------------------
        # Basic calculated parameters
        # --------------------------------------------------------------
        eddington_luminosity = get_eddington_lum(self.M_in_Msun)
        self.luminosity = self.f_Ledd * eddington_luminosity
        # r1 = self.starting_r * self.bg.get_rg_in_cm(self.M_in_Msun)
        # r2 = self.ending_r * self.bg.get_rg_in_cm(self.M_in_Msun)
        r1 = self.r_range[0] * self.bg.get_rg_in_cm(self.M_in_Msun)
        r2 = self.r_range[-1] * self.bg.get_rg_in_cm(self.M_in_Msun)
        self.emitting_volume = 4.0*np.pi/3.0*np.abs(r1**3 - r2**3)
        # Photon energy density comes from outermost surface area
        self.Uph_cgs = self.luminosity / (4*np.pi*r2**2.0*Consts.c_in_cgs)
        self.volume_heating_rate = self.calculate_heating_rate(eddington_luminosity)
        self.ratio_over_r = self.UB_over_r/self.Uph_cgs
        self.tsync_over_r = self.calculate_tsync_constant()
        self.tIC_over_r = self.calculate_tIC_constant()

        # -----------------------------------
        # Create quantities that depend only on the
        # dynamical background, not on the temperature
        # or PIC prescriptions
        # -----------------------------------
        # This definition of electron magnetization from
        # Werner, Philippov, Uzdensky 2019 (cold)
        self.magnetization_over_r = self.Br_over_r**2.0/(4.0*np.pi*self.number_density_over_r*Consts.restmass_cgs)
        # This definition of magnetization from
        # Werner, Uzdensky, Begelman + 2018
        self.ion_magnetization_over_r = self.Br_over_r**2.0/(4.0*np.pi*self.number_density_over_r*Consts.restmass_proton_cgs)
        # This beta parameter is ion only
        self.beta_over_r = 8.0*self.number_density_over_r*Consts.kB_cgs*self.ISCO_equipartition_T/(self.Br_over_r**2.0)
        # NOTE gamma_rad is defined for *Compton* cooling, not synchrotron
        self.gamma_rad_over_r = np.sqrt(3*0.1*Consts.e_in_cgs*np.abs(self.Br_over_r)/(4.0*Consts.thomson_cross_section_cgs*self.Uph_cgs))
        self.cooling_times = np.minimum(self.tIC_over_r, self.tsync_over_r)
        # Electron scattering optical depth
        self.tau_es = self.number_density_over_r * Consts.thomson_cross_section_cgs * self.H_over_R*self.r_range*self.bg.get_rg_in_cm(self.M_in_Msun)

        # -----------------------------------
        # Create quantities that depend on the
        # PIC prescriptions but not on the temperature
        # -----------------------------------
        if self.PL_type == "WUBCN18":
            # Add 1 to account for cooling
            self.PL_indices = 1.9 + 0.7/np.sqrt(self.ion_magnetization_over_r) + 1
        elif self.PL_type == "WPU19":
            radiaction = self.gamma_rad_over_r/self.magnetization_over_r
            self.PL_indices = 1.6 * np.ones(self.r_range.shape)
            self.PL_indices[radiaction <= 6] = 3.5
        elif self.PL_type == "constant":
            self.PL_indices = self.PL_indices_constant*np.ones(self.r_range.shape)
        else:
            print("ERROR: Unknown prescription " + self.PL_type + " for power-law index.")
            exit()

        if self.gamma2_type == "4sigma":
            self.gamma2_over_r = 4*self.magnetization_over_r
        elif self.gamma2_type == "6sigma":
            self.gamma2_over_r = 6*self.magnetization_over_r
        # elif self.gamma2_type == "WPU19":
            # gamma2_over_r = 1
        elif self.gamma2_type == "fixed":
            self.gamma2_over_r = self.gamma2_constant*np.ones(self.r_range.shape)
        else:
            print("ERROR: Unknown prescription " + self.gamma2_type + " for high-energy cutoff.")
            exit()

        # -----------------------------------
        # Dump to file
        # -----------------------------------
        with open(self.pickle_path, 'wb') as fh:
            pickle.dump(self, fh)

    def _load_from_pickle(self):
        try:
            with open(self.pickle_path, 'rb') as fh:
                self.__dict__.update(pickle.load(fh).__dict__)
                return True
        except:
            return False


    def calculate_heating_rate(self, eddington_luminosity):
        total_energy_dissipated = self.accretion_efficiency * eddington_luminosity
        self.volume_heating_rate = self.electron_heating_fraction * total_energy_dissipated / self.emitting_volume
        return self.volume_heating_rate

    def calculate_tsync_constant(self):
        # Full cooling time = calculate_tsync_constant/gamma
        # Note this is in the ultrarelativistic limit!
        # Rybicki & Lightman problem 6.1
        t_sync_constant = 5.1e8/(self.Br_over_r**2.0)
        return t_sync_constant

    def calculate_tIC_constant(self):
        # Full cooling time = calculate_tIC_constant/gamma
        # Assume is IC cooling time, proportional using ratio of power loss
        return self.ratio_over_r*self.calculate_tsync_constant()

    def solve_for_equilibrium(self, variables, *args):
        gamma1, temperature = variables
        index = args
        ndensity = self.number_density_over_r[index]
        gamma2 = self.gamma2_over_r[index]
        PL_val = self.PL_indices[index]
        cooling_time = self.cooling_times[index]
        theta_e = Consts.kB_cgs * temperature/Consts.restmass_cgs

        # # Get effective slope by requiring fPL(gamma2) = fPL0(gamma2)
        # eff_args = (theta_e, gamma1, gamma2, PL_val, ndensity)
        # PL_eff = solve_for_PL_eff(eff_args)
#
        # # Get normalization by requiring continuity of distribution function
        # norm = self.solve_for_PL_norm(gamma1, theta_e, ndensity, PL_eff)
#
        # if self.ratio_over_r[index] < 1:
            # nonthermal_cooling_rate = self.calculate_IC_cooling_rate(norm, gamma1, PL_eff, index)
        # else:
            # nonthermal_cooling_rate = self.calculate_sync_cooling_rate(norm, gamma1, PL_eff, index)
#
        # # Calculate thermal cooling
        # xcoh = 2.4e17 * np.sqrt(Consts.me_in_g*ndensity)*(temperature)**(-9.0/4.0)
        # # RL eq. 7.74b
        # amplification_factor = 0.75*(np.log(2.25/xcoh))**2
        # # RL eq. 5.15b
        # gaunt_factor = 1.2
        # bremsstrahlung_emissivity = 1.4e-27*np.sqrt(temperature)*ndensity**2*gaunt_factor
        # thermal_cooling_rate = amplification_factor * bremsstrahlung_emissivity
        # cooling_rate = nonthermal_cooling_rate/self.tau_es[index] + thermal_cooling_rate

        # Residuals. Do fractional difference.
        # heating_residual = (cooling_rate - self.volume_heating_rate)/self.volume_heating_rate
        collision_equation_residual = self.collision_time_equation(gamma1, temperature, ndensity, cooling_time)

        return np.array([collision_equation_residual])

    def solve_for_gamma1(self, temperature, index):
        cooling_time = self.cooling_times[index]
        if not isinstance(temperature, float):
            temperature = temperature[0]
        number_density = self.number_density_over_r[index]
        # Temperature changes only the collisional time.
        # For the general collision time, we need to solve an equation
        # involving psi(x), where x changes with temperature.
        theta_e = Consts.kB_cgs * temperature/Consts.restmass_cgs

        needed_values = (temperature, number_density, cooling_time)
        # Set gamma1 min to be the maximum value of the maxwell-juettner:
        # # Set gamma1 min to be the non-relativistic average gamma from theta,
        # # i.e. 1.5theta + 1
        gamma1_min = 1.5*theta_e + 1.0
        # gamma_solve = least_squares(self.collision_time_equation, gamma1_min, args=needed_values, bounds=(1.0, np.inf))
        # gamma_solve = least_squares(self.collision_time_equation, gamma1_min, args=needed_values, bounds=(gamma1_min, np.inf))
        gamma_solve = least_squares(self.collision_time_equation, gamma1_min*1.5, args=needed_values, bounds=(gamma1_min, np.inf))
        gamma1 = gamma_solve.x[0]

        return gamma1

    def solve_for_PL_norm(self, gamma1, theta, number_density, PL_val):
        fMJ_at_gamma1 = maxwell_juettner_at_gamma(gamma1, number_density, theta)
        normalization = fMJ_at_gamma1/number_density * gamma1**(PL_val)
        return normalization

    def calculate_IC_cooling_rate(self, normalization, gamma1, p, index, args=None):
        # Calculation in notebook. Assumes PIC = (Uph/UB) Psync
        power_loss_sync = self.calculate_sync_cooling_rate(normalization, gamma1, p, index, args)

        # Need this bit for the interpolation functions
        if args is not None:
            gamma2, Br, ndensity, r_range, radius = args
            ratio_interp_f = interp1d(r_range, self.ratio_over_r, fill_value='extrapolate')
            ratio_at_r = ratio_interp_f(radius)
        else:
            ratio_at_r = self.ratio_over_r[index]
        power_loss_IC = power_loss_sync/ratio_at_r
        return power_loss_IC

    def calculate_sync_cooling_rate(self, normalization, gamma1, p, index, args=None):
        """
        NOTE: we cannot simply use Rybicki & Lightman Eq. 6.36 integrated over frequency and
        angle because that derivation assumes that the frequencies of interest are far away
        from the cutoff frequencies; that is not true for us looking at frequencies close to
        the critical synchrotron frequency.

        Instead of extending the limits of F(x) to 0 and infinity, we will instead
        integrate over frequency *first*, leading to the simple Larmor formula (RL Eq. 6.17b):
        Ptotal = \int dOmega domega dgamma f(gamma)* Power per electron
            = \int dOmega dgamma f(gamma) \int domega Power per electron
            = \int dOmega dgamma f(gamma) 2e^4 B^2 gamma^2 beta^2 sin^2alpha/(3 m^2 c^3)
        The integral over dOmega = sin\alpha d\phi d\alpha gives 8\pi/3.
        """
        if index is not None:
            gamma2 = self.gamma2_over_r[index]
            Br = self.Br_over_r[index]
            ndensity = self.number_density_over_r[index]
        else:
            gamma2, Br, ndensity, r_range, radius = args

        # Deal with singularity (a point, not asymptotic)
        if p == 3.0:
            integral1 = np.log10(gamma2/gamma1)
        else:
            integral1 = (gamma2**(3.0-p) - gamma1**(3.0-p))/(3.0 - p)

        if p == 1.0:
            integral2 = -1.0*np.log10(gamma2/gamma1)
        else:
            integral2 = -1.0*(gamma2**(1.0-p) - gamma1**(1.0-p))/(1.0 - p)

        constant = 16*np.pi*Consts.e_in_cgs**4*Br**2/(9*Consts.me_in_g**2*Consts.c_in_cgs**3)*normalization*ndensity
        return constant*(integral1 + integral2)

    def get_quick_arrays(self):
        # Cut down magnetic field, etc. according to indices in temperature dict
        indices = np.array(list(self.temperature_dictionary.keys()))
        # Just make sure the extraction from dictionary to array is in order.
        sorted_indices = indices[np.argsort(indices)]
        temperature_over_r = list(self.temperature_dictionary.values())
        self.temperature_over_r = np.array(temperature_over_r)[np.argsort(indices)]
        gamma1_inds = np.array(list(self.gamma1_dictionary.keys()))
        gamma1_vals = list(self.gamma1_dictionary.values())
        self.gamma1_over_r = np.array(gamma1_vals)[np.argsort(gamma1_inds)]
        peff_inds = np.array(list(self.peff_dictionary.keys()))
        peff_vals = list(self.peff_dictionary.values())
        self.peff_over_r = np.array(peff_vals)[np.argsort(peff_inds)]

        self.quick_r_range = self.r_range[sorted_indices]
        self.quick_number_density_over_r = self.number_density_over_r[sorted_indices]
        self.quick_Br_over_r = self.Br_over_r[sorted_indices]
        self.quick_UB_over_r = self.UB_over_r[sorted_indices]
        self.quick_magnetization_over_r = self.magnetization_over_r[sorted_indices]
        self.quick_cooling_times = self.cooling_times[sorted_indices]
        self.quick_PL_indices = self.PL_indices[sorted_indices]
        self.quick_gamma2_over_r = self.gamma2_over_r[sorted_indices]
        self.quick_tIC_over_r = self.tIC_over_r[sorted_indices]
        self.quick_tsync_over_r = self.tsync_over_r[sorted_indices]
        self.quick_tau_es = self.tau_es[sorted_indices]

    def plot_gammie_quantities(self):
        figure_dir = self.path_to_figures
        title_str = r"$F_{{\theta\phi}}={:.2f}$, ".format(self.bg.Fthetaphi) + self.PL_titstr + ", " + self.gamma2_titstr
        plt.figure()
        plt.plot(self.r_range, self.number_density_over_r)
        plt.ylabel(r'$n~{\rm cm}^{-3}$')
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "number_density_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.ratio_over_r)
        plt.ylabel(r'$U_B/U_{ph}$')
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "synchrotron_IC_energy_ratio_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.tIC_over_r/self.tsync_over_r)
        plt.xlabel(r'$r/r_G$')
        plt.ylabel(r'$t_{\rm IC}/t_{\rm sync}$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "synchrotron_IC_cooling_time_ratio_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.magnetization_over_r)
        plt.ylabel(r"$\sigma_{\rm cold}$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "magnetization_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.ion_magnetization_over_r)
        plt.ylabel(r"$\sigma_{i, \rm{cold}}$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "ion_magnetization_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.beta_over_r)
        plt.ylabel(r"$\beta_i$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "beta_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.gamma_rad_over_r)
        plt.ylabel(r"$\gamma_{\rm rad}$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "gamma_rad_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.gamma_rad_over_r/self.magnetization_over_r)
        plt.ylabel(r"$\gamma_{\rm rad}/\sigma_{\rm cold}$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "gamma_rad_over_sigma_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.cooling_times)
        plt.ylabel(r'$t_{\rm cool}$ s')
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "cooling_time_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.tau_es)
        plt.ylabel(r'$\tau_{\rm es}$')
        plt.xlabel(r'$r/r_G$')
        plt.ylim(bottom=0)
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir + "electron_scattering_depth_over_r.png")
        plt.close()


    def plot_prescription_quantities(self):
        figure_dir2 = self.path_to_figures2
        title_str = r"$F_{{\theta\phi}}={:.2f}$, ".format(self.bg.Fthetaphi) + self.PL_titstr + ", " + self.gamma2_titstr

        plt.figure()
        plt.plot(self.r_range, self.PL_indices)
        plt.ylabel(r"Power-law index $p$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "power_law_index_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.r_range, self.gamma2_over_r)
        plt.ylabel(r"High-energy cut-off $\gamma_2$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "high_energy_cutoff_over_r.png")
        plt.close()

    def plot_temperature_dependent_quantities(self):
        figure_dir2 = self.path_to_figures2
        title_str = r"$F_{{\theta\phi}}={:.2f}$, ".format(self.bg.Fthetaphi) + self.PL_titstr + ", " + self.gamma2_titstr

        self.get_quick_arrays()
        self.get_total_distribution_function()
        plt.figure()
        plt.plot(self.quick_r_range, self.number_density_over_r, label=r"$n_e$ Gammie")
        plt.plot(self.quick_r_range, self.total_density_over_r, label=r"$n_e$ total")
        plt.plot(self.quick_r_range, self.nonthermal_density_over_r, label=r"$n_{e,PL}$")
        plt.yscale('log')
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.ylabel(r'$n~{\rm cm^{-3}}$')
        plt.title(title_str)
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "total-nonthermal_density_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.quick_r_range, self.total_density_over_r/self.number_density_over_r, ls='--', color='black', label=None)
        plt.plot(self.quick_r_range, self.thermal_density_over_r/self.total_density_over_r, label=r"$n_{e,therm}/n_{e,tot}$")
        plt.plot(self.quick_r_range, self.nonthermal_density_over_r/self.total_density_over_r, label=r"$n_{e,PL}/n_{e,tot}$")
        plt.yscale('log')
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "total-nonthermal_number_fraction_over_r.png")
        plt.close()

        # Somewhat tricky to compare thermal energy and temperature, since unclear if
        # U = 3KT or 3/2 kT
        plt.figure()
        initial_val = self.average_energy_over_r[0]
        plt.plot(self.quick_r_range, self.average_energy_over_r/initial_val, label=r"$\langle U_{\rm PL}\rangle/U_0$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.ylabel(r'$\langle U\rangle/U_0$')
        plt.title(title_str)
        plt.yscale('log')
        plt.tight_layout()
        plt.savefig(figure_dir2 + "average_energy_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.quick_r_range, self.nonthermal_energy_over_r/self.average_energy_over_r, label=r"$\langle U_{\rm PL}\rangle/\langle U_{\rm tot}\rangle$")
        plt.plot(self.quick_r_range, self.thermal_energy_over_r/self.average_energy_over_r, label=r"$\langle U_{\rm thermal}\rangle/\langle U_{\rm tot}\rangle$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        # plt.ylabel(r'$\langle U\rangle$ K')
        plt.title(title_str)
        plt.yscale('log')
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "energy_fraction_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.quick_r_range, self.temperature_over_r)
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.ylabel(r'$T_{eq}$ K')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "equilibrium_temperature_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.quick_r_range, self.gamma1_over_r)
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.ylabel(r'$\gamma_1$')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "gamma1_over_r.png")
        plt.close()

        plt.figure()
        plt.plot(self.quick_r_range, self.peff_over_r)
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.ylabel(r'$p_{\rm eff}$')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "peff_over_r.png")
        plt.close()

        # Calculate collision times over radius
        theta_e = Consts.kB_cgs*self.temperature_over_r/Consts.restmass_cgs
        nu0const = Consts.nu0const1 * Consts.coulomb_log * self.quick_number_density_over_r
        kinetic_ratio = (self.gamma1_over_r - 1.)/theta_e
        integral_path = self.bg.path_to_reduced_data + "tabulated_integral.txt"
        tabulated_integral = np.loadtxt(integral_path)
        tcoll_over_r = []
        for k, x in enumerate(kinetic_ratio):
            x_index = (np.abs(x - tabulated_integral[0, :])).argmin()
            psi_value = tabulated_integral[1, x_index]
            tcoll = (self.gamma1_over_r[k] - 1.)**1.5/(2.*nu0const[k]*psi_value)
            tcoll_over_r.append(tcoll)
        tcoll_over_r = np.array(tcoll_over_r)
        tinfall = self.bg.t_infall
        tinfall_s = tinfall * self.bg.get_tg_in_s(self.M_in_Msun)
        # THERMALIZATION time scale between two maxwellians with different temperatures:
        # the protons with equipartition temperature and the electrons with the calculated
        # temperature that sets cooling and heating equal.
        ion_temperature = self.quick_UB_over_r/self.quick_number_density_over_r/Consts.kB_cgs
        nu_thermalize = 1.8e-19*np.sqrt(Consts.mp_in_g*Consts.me_in_g)*self.quick_number_density_over_r*Consts.coulomb_log/(Consts.mp_in_g*ion_temperature+Consts.me_in_g*self.temperature_over_r)**1.5
        t_thermalize = 1.0/nu_thermalize*(self.temperature_over_r)/(ion_temperature - self.temperature_over_r)
        # Acceleration timescale
        # From Comisso & SIroni 2018, eq. 3 with kappa=10, dB=B0, dv=vA
        kappa = 10.0
        dB_over_B0 = 1.0
        # Use electron magnetization...maybe should be ion?
        vA2 = self.magnetization_over_r/(self.magnetization_over_r + 1.0)
        vA2_ion = self.ion_magnetization_over_r/(self.ion_magnetization_over_r + 1.0)
        plasma_freq = 1.0e4*np.sqrt(self.quick_number_density_over_r)
        # Use nonrelativistic eq. 1.5 kT = gamma mc^2 to get gamma=1.5theta
        denominator = 1.0/kappa*dB_over_B0**2*vA2*np.sqrt(self.magnetization_over_r*(1.0+2.0/3.0))*plasma_freq
        denominator_ion = 1.0/kappa*dB_over_B0**2*vA2_ion*np.sqrt(self.ion_magnetization_over_r*(1.0+2.0/3.0))*plasma_freq
        t_accel = 1.0/denominator
        t_accel_ion = 1.0/denominator_ion

        plt.figure()
        plt.plot(self.quick_r_range, tcoll_over_r/tinfall_s, label=r"$t_{\rm coll}^{ee}(\gamma_1)/t_{\rm infall}$")
        plt.plot(self.quick_r_range, t_thermalize/tinfall_s, label=r"$t_{\rm coll}^{ei}(\gamma_1)/t_{\rm infall}$")
        # plt.plot(self.quick_r_range, self.quick_cooling_times/self.gamma1_over_r/tinfall_s, label=r"$t_{\rm cool}(\gamma_1)/t_{\rm infall}$")
        plt.plot(self.r_range, self.tIC_over_r/self.gamma1_over_r/tinfall_s, label=r"$t_{\rm IC}(\gamma_1)/t_{\rm infall}$")
        plt.plot(self.r_range, self.tsync_over_r/self.gamma1_over_r/tinfall_s, label=r"$t_{\rm sync}(\gamma_1)/t_{\rm infall}$")
        plt.plot(self.r_range, t_accel/tinfall_s, label=r"$t_{\rm accel}^e/t_{\rm infall}$")
        plt.plot(self.r_range, t_accel_ion/tinfall_s, label=r"$t_{\rm accel}^i/t_{\rm infall}$")
        plt.xlabel(r'$r/r_G$')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.yscale('log')
        plt.title(title_str)
        plt.legend()
        plt.tight_layout()
        plt.savefig(figure_dir2 + "timescales_over_r.png")
        plt.close()

        # Calculate importance of Compton cooling
        # Compton y parameter (assuming non-relativistic thermal electrons)
        self.compton_y = 4.0 * theta_e * np.max([self.tau_es, self.tau_es**2.0])
        plt.figure()
        plt.plot(self.quick_r_range, self.compton_y)
        # plt.plot(self.quick_r_range, self.compton_y, marker='o')
        plt.xlabel(r'$r/r_G$')
        plt.ylabel("Compton $y$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "compton_y_over_r.png")
        plt.close()

        # Calculate importance of inverse Compton
        # RL Eq. 7.65b
        xcoh = 2.4e17 * np.sqrt(Consts.me_in_g*self.quick_number_density_over_r)*(self.temperature_over_r)**(-9.0/4.0)
        plt.figure()
        plt.plot(self.quick_r_range, xcoh)
        plt.xlabel(r'$r/r_G$')
        plt.ylabel(r"$x_{\rm coh}$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "xcoh_over_r.png")
        plt.close()

        # RL eq. 7.74b
        amplification_factor = 0.75*(np.log(2.25/xcoh))**2
        plt.figure()
        plt.plot(self.quick_r_range, amplification_factor)
        plt.xlabel(r'$r/r_G$')
        plt.ylabel(r"$A(\rho,T)$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "amplification_factor_over_r.png")
        plt.close()

        # Plot thermal vs. nonthermal cooling
        nt_cooling_rate_eq = []
        for k in np.arange(0, self.quick_r_range.size):
            tcool = self.quick_cooling_times[k]
            gamma1_eq = self.gamma1_over_r[k]
            PL_eff = self.peff_over_r[k]
            gamma2_eq = self.quick_gamma2_over_r[k]
            thetak = Consts.kB_cgs*self.temperature_over_r[k]/Consts.restmass_cgs
            norm_eq = self.solve_for_PL_norm(gamma1_eq, thetak, self.quick_number_density_over_r[k], PL_eff)
            if self.quick_tsync_over_r[k] < self.quick_tIC_over_r[k]:
                nt_rate = self.calculate_sync_cooling_rate(norm_eq, gamma1_eq, PL_eff, k)
            else:
                nt_rate = self.calculate_IC_cooling_rate(norm_eq, gamma1_eq, PL_eff, k)
            nt_rate = nt_rate/self.quick_tau_es[k]
            nt_cooling_rate_eq.append(nt_rate)
        nt_cooling_rate_eq = np.array(nt_cooling_rate_eq)

        # RL eq. 7.74b
        amplification_factor = 0.75*(np.log(2.25/xcoh))**2
        # RL eq. 5.15b
        gaunt_factor = 1.2
        bremsstrahlung_emissivity = 1.4e-27*np.sqrt(self.temperature_over_r)*self.quick_number_density_over_r**2*gaunt_factor
        thermal_cooling_rate_eq = amplification_factor * bremsstrahlung_emissivity
        cooling_rate_eq = nt_cooling_rate_eq + thermal_cooling_rate_eq

        plt.figure()
        plt.axhline([1.0], ls=':', color='black')
        # plt.plot(adjusted_r_range, thermal_cooling_rate_eq/nt_cooling_rate_eq, ls='-')
        # plt.ylabel(r'$Q^-_{\rm th}/Q^-_{\rm nth}$')
        plt.plot(self.quick_r_range, nt_cooling_rate_eq/cooling_rate_eq, label=r"$Q^-_{\rm nth}/Q^-$", ls='-')
        plt.plot(self.quick_r_range, thermal_cooling_rate_eq/cooling_rate_eq, label=r"$Q^-_{\rm th}/Q^-$", ls='--')
        plt.legend()
        plt.xlabel(r'$r/r_G$')
        plt.yscale('log')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "cooling_rate_over_r.png")
        plt.close()

        # Calculate distribution function at inner/outer radii
        self.get_gamma_bins()
        # Outermost radius
        T0 = self.temperature_over_r[0]
        n0 = self.quick_number_density_over_r[0]
        theta0 = Consts.kB_cgs*T0/Consts.restmass_cgs
        fMJ0 = maxwell_juettner_over_gamma(self.gamma_bins, n0, theta0)
        gamma20 = self.quick_gamma2_over_r[0]
        gamma10 = self.gamma1_over_r[0]
        p0 = self.peff_over_r[0]
        norm0 = self.solve_for_PL_norm(gamma10, theta0, n0, p0)
        ind1 = (np.abs(self.gamma_bins - gamma10)).argmin()
        ind2 = (np.abs(self.gamma_bins - gamma20)).argmin()
        PL0 = norm0 * n0 * self.gamma_bins**(-p0)
        PL0[:ind1] = 0
        PL0[ind2:] = 0
        # Innermost radius
        Ti = self.temperature_over_r[-1]
        ni = self.quick_number_density_over_r[-1]
        thetai = Consts.kB_cgs*Ti/Consts.restmass_cgs
        fMJi = maxwell_juettner_over_gamma(self.gamma_bins, ni, thetai)
        gamma2i = self.quick_gamma2_over_r[-1]
        gamma1i = self.gamma1_over_r[-1]
        # pi = self.quick_PL_indices[-1]
        pi = self.peff_over_r[-1]
        normi = self.solve_for_PL_norm(gamma1i, thetai, ni, pi)
        ind1 = (np.abs(self.gamma_bins - gamma1i)).argmin()
        ind2 = (np.abs(self.gamma_bins - gamma2i)).argmin()
        PLi = normi * ni * self.gamma_bins**(-pi)
        PLi[:ind1] = 0
        PLi[ind2:] = 0
        max_val = np.max([fMJ0.max(), PL0.max(), fMJi.max(), PLi.max()])
        color_legend_elements = [Line2D([0], [0], color='C0', label='Power-law component'),
                        Line2D([0], [0], color='C1', label='Thermal component')]
        ls_legend_elements = [Line2D([0], [0], ls='-', color='black'),
                            Line2D([0], [0], ls='--', color='black')]
        plt.figure()
        plt.plot(self.gamma_bins, self.gamma_bins*fMJ0, color='C1', ls='-', label="Thermal component at $r={:.2f}r_g$".format(self.quick_r_range[0]))
        plt.plot(self.gamma_bins, self.gamma_bins*PL0, color='C0', ls='-', label="Power-law component at $r={:.2f}r_g$".format(self.quick_r_range[0]))
        plt.xscale('log')
        plt.yscale('log')
        # plt.ylim([1, max_val*1.1])
        plt.ylim([1, 1e20])
        plt.xlabel(r'$\gamma$')
        plt.ylabel(r"$\gamma f(\gamma)$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.legend()
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "electron_distribution_function_outer.png")
        plt.close()

        plt.figure()
        plt.plot(self.gamma_bins, fMJi, color='C1', ls='-', label="Thermal component at $r={:.2f}r_g$".format(self.quick_r_range[-1]))
        plt.plot(self.gamma_bins, PLi, color='C0', ls='-', label="Power-law component at $r={:.2f}r_g$".format(self.quick_r_range[-1]))
        # plt.plot(self.gamma_bins, PLi+fMJi, color='black', label="Total")
        plt.xscale('log')
        plt.yscale('log')
        # plt.ylim([1, max_val*1.1])
        plt.ylim([1, 1e20])
        plt.xlabel(r'$\gamma$')
        plt.ylabel(r"$\gamma f(\gamma)$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.legend()
        plt.gca().grid(color='.9', ls='--')
        plt.title(title_str)
        plt.tight_layout()
        plt.savefig(figure_dir2 + "electron_distribution_function_inner.png")
        plt.close()

    def get_spectral_bins(self):
        self.get_quick_arrays()
        omega_const = 3*Consts.e_in_cgs*np.abs(self.quick_Br_over_r)/(2*Consts.me_in_g*Consts.c_in_cgs)
        nu1_list = omega_const*self.gamma1_over_r**2/(2.0*np.pi)
        nu2_list = omega_const*self.quick_gamma2_over_r**2/(2.0*np.pi)

        nu_min = nu1_list.min()
        nu_max = nu2_list.max()
        spectral_bins = np.logspace(np.log10(nu_min), np.log10(nu_max), 10000)
        # step = (np.log10(nu_max) - np.log10(nu_min))/1000000
        # spectral_bins = np.arange(np.log10(nu_min), np.log10(nu_max), step)
        # spectral_bins = 10.0**spectral_bins
        # spectral_bins = np.linspace(nu_min, nu_max, 1000)
        return spectral_bins

    def retrieve_spectra(self, quiet=False, decrease_by_tau=True, overwrite=False, inc_angle_deg=60):
        redshift = inc_angle_deg is not None
        spectrum_path = self.path_to_reduced_data + "spectrum"
        if not decrease_by_tau:
            spectrum_path += "_noTauDecrease"
        if redshift:
            spectrum_path += "_redshifted_i{:d}".format(inc_angle_deg)
        # BRANCH: change spectrum path XX
        spectrum_path += "_rewrite2"
        spectrum_path += ".p"
        if overwrite or (not os.path.exists(spectrum_path)):
            print("Calculating spectrum " + spectrum_path)
            if redshift:
                spectrum_dict = self.calculate_redshifted_spectra(inc_angle_deg, decrease_by_tau)
            else:
                spectrum_dict = self.calculate_spectra(quiet, decrease_by_tau)
        else:
            print("Loading spectrum from pickle file " + spectrum_path)
            with open(spectrum_path, 'rb') as file:
                spectrum_dict = pickle.load(file)
        return spectrum_dict


    def plot_spectra(self, quiet=False, decrease_by_tau=True, overwrite=False, inc_angle_deg=60):
        redshift = inc_angle_deg is not None
        spectrum_dict = self.retrieve_spectra(quiet, decrease_by_tau, overwrite, inc_angle_deg)

        def nuToKev(nu):
            return 4.1e-18 * nu
        def kevToNu(keV):
            return keV/(4.1e-18)

        Lnu_thermal = spectrum_dict["Lnu_thermal"]
        # Lnu_nonthermal = spectrum_dict["Lnu_nonthermal"]
        spectral_bins = spectrum_dict["spectral_bins"]
        # total_spectrum = Lnu_nonthermal + Lnu_thermal
        if not redshift:
            disk_function = spectrum_dict["disk_function"]
            # total_spectrum += disk_function

        # max_val = (total_spectrum*spectral_bins).max()
        max_val = (Lnu_thermal*spectral_bins).max()
        label_str = r"$\nu L_{{\nu, {{\rm disk}}}}(T(r))$"

        plt.figure()
        # plt.plot(spectral_bins, Lnu_nonthermal*spectral_bins,
                # label=r'$\nu L_{\nu, {\rm PL}}$', color="C0", ls="-")
        plt.plot(spectral_bins, Lnu_thermal*spectral_bins,
                label=r'$\nu L_{\nu, {\rm thermal}}$', ls=':', color="C2")
        # if not redshift:
            # plt.plot(spectral_bins, disk_function*spectral_bins,
                    # label=label_str, ls='--', color="C1")
        # plt.plot(spectral_bins, total_spectrum*spectral_bins, color='black', label='Total', lw=1)
        plt.xscale('log')
        plt.yscale('log')
        plt.xlabel(r"$\nu/{\rm Hz}$ ")
        title_str = r"$F_{{\theta\phi}}={:.2f}$, $\gamma_2=$".format(self.bg.Fthetaphi) + self.gamma2_str + ", " + self.PL_titstr + "\n"
        if redshift:
            title_str += r"Redshifted, $i=${:.0f} degrees".format(inc_angle_deg)
        else:
            title_str += "Not Redshifted"
        plt.title(title_str)
        plt.gca().tick_params(right=True, direction='in', which='both')
        secax = plt.gca().secondary_xaxis('top', functions=(nuToKev, kevToNu))
        secax.tick_params(top=True, direction='in', which='both')
        secax.set_xlabel(r'$\nu/{\rm keV}$')
        # plt.ylim([1e34, max_val])
        plt.ylim([1e34, 2e36])
        # plt.xlim([spectral_bins.min(), spectral_bins.max()])
        plt.xlim([1e17, 1e21])
        plt.legend()
        plt.tight_layout()
        fname = "spectrum_added_over_r"
        if not decrease_by_tau:
            fname += "_noTauDecrease"
        if redshift:
            fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
        plt.savefig(self.path_to_figures2 + fname)
        plt.close()


    def calculate_spectra(self, quiet=False, decrease_by_tau=True):
        spectrum_dict = {}
        # if decrease_by_tau = False, then remove the 1/tau decrease in the nonthermal intensity.
        self.get_quick_arrays()

        if self.quick_r_range.shape != self.r_range.shape:
            print("Not all radii have been solved for yet, so there is no point in calculating the spectra. Exiting...")
            return
        spectral_bins = self.get_spectral_bins()
        nonthermal_integrands = np.zeros(spectral_bins.shape)
        thermal_integrands = np.zeros(spectral_bins.shape)
        r_range_cm = self.r_range * self.bg.get_rg_in_cm(self.M_in_Msun)
        # self.temperature_over_r = np.logspace(np.log10(1e7), np.log10(1e9), r_range_cm.size)
        # self.temperature_over_r = np.linspace(1e8, 6e8, r_range_cm.size)
        for j, r_cm in enumerate(r_range_cm):
            # --------------------------------------------------------------
            # Spectrum from plunging region power-law of electrons
            # --------------------------------------------------------------
            # Assume spectrum is A*nu^(-s) between nu1 and nu2.
            # Find A = spectrum_constant by integrating and set equal to cooling rate
            gamma1 = self.gamma1_over_r[j]
            gamma2 = self.gamma2_over_r[j]
            Br = self.quick_Br_over_r[j]
            n_at_r = self.quick_number_density_over_r[j]
            temp_at_r = self.temperature_over_r[j]
            height = r_cm*self.H_over_R
            PL_eff = self.peff_over_r[j]
            thetaj = Consts.kB_cgs*temp_at_r/Consts.restmass_cgs
            norm_eq = self.solve_for_PL_norm(gamma1, thetaj, n_at_r, PL_eff)
            if self.quick_tsync_over_r[j] < self.quick_tIC_over_r[j]:
                nt_cooling_rate = self.calculate_sync_cooling_rate(norm_eq, gamma1, PL_eff, j)
            else:
                nt_cooling_rate = self.calculate_IC_cooling_rate(norm_eq, gamma1, PL_eff, j)

            # BRANCH: decrease by 1/tau
            nt_cooling_rate = nt_cooling_rate/self.quick_tau_es[j]

            omega_const = 3*Consts.e_in_cgs*np.abs(Br)/(2*Consts.me_in_g*Consts.c_in_cgs)
            nu1 = omega_const*gamma1**2/(2*np.pi)
            nu2 = omega_const*gamma2**2/(2*np.pi)
            s_index = (self.peff_over_r[j] - 1.)/2.

            start_index = (np.abs(nu1 - spectral_bins)).argmin()
            end_index = (np.abs(nu2 - spectral_bins)).argmin()
            nu1_bin = spectral_bins[start_index]
            nu2_bin = spectral_bins[end_index]
            # NOTE: cooling_rate_nt is no longer = heating rate since we included thermal cooling!
            spectrum_constant = nt_cooling_rate * (1.-s_index)/(nu2_bin**(1.-s_index) - nu1_bin**(1.-s_index))

            Pnu = spectrum_constant * spectral_bins**(-s_index)
            Pnu[:start_index] = 0
            Pnu[end_index+1:] = 0

            # Because the integration/split between bins isn't perfect,
            # use residual factor to ensure the total power is correct.
            P_nonthermal = np.trapz(Pnu, x=spectral_bins)
            residual_factor =  nt_cooling_rate/P_nonthermal
            Pnu = Pnu * residual_factor
            P_nonthermal = np.trapz(Pnu, x=spectral_bins)

            # Isotropic emission --> jnu = Pnu/4pi
            # dInu = jnu*ds (assuming no absorption).
            # Set ds to be height (same as radius for H/R=1)
            Inu_nonthermal_orig = Pnu/(4.0*np.pi)*height

            # Decrease nonthermal intensity by 1/tau
            # NOTE: doing this will make power_in_powerlaw != heating_rate*volume
            # unless accounted for in thermal.
            if decrease_by_tau:
                Inu_thermalized = Inu_nonthermal_orig*(1.0 - 1.0/self.quick_tau_es[j])
                Inu_nonthermal = Inu_nonthermal_orig/self.quick_tau_es[j]
                nt_cooling_rate = nt_cooling_rate/self.quick_tau_es[j]
            else:
                Inu_nonthermal = Inu_nonthermal_orig

            nonthermal_integrands = np.vstack([nonthermal_integrands, 2.0*np.pi*r_cm*Inu_nonthermal])

            # ---------------------------------
            # Calculate Inu of thermal electrons
            # ---------------------------------
            # RL eq. 7.65b
            xcoh = 2.4e17 * np.sqrt(Consts.me_in_g*n_at_r)*(temp_at_r)**(-9.0/4.0)
            # RL eq. 7.74b
            amplification_factor = 0.75*(np.log(2.25/xcoh))**2
            # RL eq. 5.15b
            gaunt_factor = 1.2
            bremsstrahlung_emissivity = 1.4e-27*np.sqrt(temp_at_r)*n_at_r**2.*gaunt_factor
            thermal_cooling_rate = amplification_factor * bremsstrahlung_emissivity
            # Account for energy in power law thermalized by scattering
            if decrease_by_tau:
                thermalized_cooling_rate = np.trapz(Inu_thermalized*spectral_bins, x=np.log(spectral_bins))*np.pi*4./height
                thermal_cooling_rate += thermalized_cooling_rate
            thermal_jnu = thermal_cooling_rate/4.0/np.pi

            # Make sure that I=j*H = RL eq. 7.71, j = P/4\pi = A\epsilon/4\pi
            intensity_constant = 12.0*Consts.kB_cgs**4.*temp_at_r**4./(Consts.c_in_cgs**2.*Consts.h_cgs**3.)
            exponential_decrease = thermal_jnu*height/intensity_constant
            Inu_thermal = 2*Consts.h_cgs * spectral_bins**3./Consts.c_in_cgs**2. * np.exp(-Consts.h_cgs*spectral_bins/(Consts.kB_cgs*temp_at_r))
            Inu_thermal = Inu_thermal*exponential_decrease

            # Because the integration/split between bins isn't perfect,
            # use residual factor to ensure the total power is correct.
            P_thermal = np.trapz(Inu_thermal, x=spectral_bins)*4.0*np.pi/height
            residual_factor =  thermal_cooling_rate/P_thermal
            Inu_thermal = Inu_thermal * residual_factor

            thermal_integrands = np.vstack([thermal_integrands, 2.0*np.pi*r_cm*Inu_thermal])

            # ---------------------------------
            # Check cooling rate: at this radius, is the power equal to cooling rate
            # calculated in equilibrium model?
            # ---------------------------------
            nonthermal_Inu_power = np.trapz(Inu_nonthermal, x=spectral_bins)*4.*np.pi/height
            thermal_Inu_power = np.trapz(Inu_thermal, x=spectral_bins)*4.*np.pi/height
            # BRANCH: did not set 1/tau here.
            if not (np.allclose(nonthermal_Inu_power + thermal_Inu_power, self.volume_heating_rate)):
                print("Heating != cooling at j={:d}".format(j)); exit()
            if not (np.allclose(nonthermal_Inu_power, nt_cooling_rate)):
                print("Nonthermal radiation power != nonthermal equilibrium cooling rate at j={:d}.".format(j))
                exit()
            if not (np.allclose(thermal_Inu_power, thermal_cooling_rate)):
                print("Thermal radiation power != thermal equilibrium cooling rate at j={:d}.".format(j))
                exit()

        # nonthermal_integrands = nonthermal_integrands[1:, :]
        thermal_integrands = thermal_integrands[1:, :]
        # Fnu = \int Inu dA/D^2 = \int Inu 2\pi rdr/D^2, D is distance
        # Lnu = 4\pi D^2 Fnu = independent of D
        # Integrate all_intensity over radius
        # total_flux_const_nonthermal = np.trapz(nonthermal_integrands[::-1, :], x=r_range_cm[::-1], axis=0)
        total_flux_const_thermal = np.trapz(thermal_integrands[::-1, :], x=r_range_cm[::-1], axis=0)
        # Factor of 2 because wedge shape has two sides
        # Lnu_nonthermal = 2.0*4.0*np.pi*total_flux_const_nonthermal
        # Factor of 2 included in FW
        Lnu_thermal = 2.0*4.0*np.pi*total_flux_const_thermal

        # power_in_powerlaw = np.trapz(spectral_bins*Lnu_nonthermal, x=np.log(spectral_bins))
        power_in_thermal_pr = np.trapz(spectral_bins*Lnu_thermal, x=np.log(spectral_bins))
        # total_emitted_power = power_in_powerlaw + power_in_thermal_pr
        # total_heating = self.volume_heating_rate * self.emitting_volume
        # fractional_error = np.abs(total_emitted_power - total_heating)/total_heating
        # print("Error in total power: {:.3e}".format(fractional_error))
        # if not quiet:
            # print("Integrated Lnu over power-law: {:.4e}".format(power_in_powerlaw))
            # print("Integrated Lnu over thermal: {:.4e}".format(power_in_thermal_pr))
            # print("P_PL/P_thermal: {:.4e}".format(power_in_powerlaw/power_in_thermal_pr))
            # print("Integrated Lnu: {:.4e}".format(total_emitted_power))
            # print("Heating rate*volume: {:.4e}".format(total_heating))
        # if not np.allclose(total_emitted_power, total_heating, rtol=1e-3):
            # print("---> Sum of emitted power is not right!!")
            # if decrease_by_tau:
                # print("     due to 1/tau decrease not being added to thermal electrons?")
            # else:
                # print("     not due to 1/tau decrease (decrease_by_tau=False).")

        # --------------------------------------------------------------
        # Disk spectrum
        # --------------------------------------------------------------
        disk_outer_edge = 1e10 # cm
        disk_inner_edge = self.bg.rISCO * self.bg.get_rg_in_cm(self.M_in_Msun)
        rstar = self.bg.rEH * self.bg.get_rg_in_cm(self.M_in_Msun)
        disk_radial_bins = np.logspace(np.log10(disk_inner_edge), np.log10(disk_outer_edge), 1000)
        L_edd = get_eddington_lum(self.M_in_Msun)
        Mdot_edd = L_edd/(self.f_Ledd*Consts.c_in_cgs**2.0)
        Mdot = self.f_Mdot*Mdot_edd  # g/s
        disk_temp_over_r = (3*Consts.G_in_cgs*self.M_in_Msun*Consts.solar_mass_in_g*Mdot/(8*np.pi*disk_radial_bins**3*Consts.SB_cgs)*(1.0 - np.sqrt(rstar/disk_radial_bins)))**0.25
        # print("Maximum disk temperature: {:.2e} K".format(disk_temp_over_r.max()))
        # print("Minimum disk temperature: {:.2e} K".format(disk_temp_over_r.min()))
        disk_function = []
        for nu in spectral_bins:
            planck_vals = []
            for i, r in enumerate(disk_radial_bins):
                try:
                    planck_val = 1.0/(np.exp(Consts.h_cgs*nu/(Consts.kB_cgs*disk_temp_over_r[i])) - 1)
                except:
                    planck_val = 0
                planck_vals.append(planck_val)
            integral_value = np.trapz(disk_radial_bins*planck_vals, disk_radial_bins)
            # integral_value = 0.5*(disk_outer_edge**2 - disk_inner_edge**2)*planck_val
            luminosity_val = 16.0*np.pi**2*nu**3*Consts.h_cgs/Consts.c_in_cgs**2*integral_value
            disk_function.append(luminosity_val)
        disk_function = np.array(disk_function)

        if not quiet:
            print("Luminosity: {:.2e}".format(self.luminosity))
            print("Integrated disk luminosity: {:.2e}".format(np.trapz(disk_function, spectral_bins)))

        spectrum_dict["spectral_bins"] = spectral_bins
        # spectrum_dict["Lnu_nonthermal"] = Lnu_nonthermal
        spectrum_dict["Lnu_thermal"] = Lnu_thermal
        spectrum_dict["disk_function"] = disk_function
        # spectrum_dict["power_in_powerlaw"] = power_in_powerlaw
        # spectrum_dict["power_in_thermal_pr"] = power_in_thermal_pr
        # spectrum_dict["fractional_error"] = fractional_error

        # Save to pickle
        spectrum_path = self.path_to_reduced_data + "spectrum"
        if not decrease_by_tau:
            spectrum_path += "_noTauDecrease"
        # BRANCH: change spectrum path XX
        spectrum_path += "_rewrite2"
        spectrum_path += ".p"
        with open(spectrum_path, 'wb') as file:
            pickle.dump(spectrum_dict, file)

        return spectrum_dict

    def trace_geodesics(self, inc_angle_deg=60):
        # --------------------------------------------------------------
        # Trace geodesics
        # --------------------------------------------------------------
        # Create a camera at infinity
        g = gi.geokerr()
        # For each pixel in the camera, find whether the geodesic hits the midplane
        cos_inclination = np.cos(inc_angle_deg*np.pi/180) # set the inclination angle of the camera
        avals = [-4., 4., -4., 4.] # xy extent of the camera in rg
        nvals = [64, 64, 1] # number of pixels on the camera
        dalpha = (avals[1] - avals[0])*self.bg.get_rg_in_cm(self.M_in_Msun)/nvals[0]
        dbeta = (avals[3] - avals[2])*self.bg.get_rg_in_cm(self.M_in_Msun)/nvals[1]
        u_locations, mu_locations, dt, out_l, dphi, tpm, tpr, i1, i2 = g.run_camera(mu0=cos_inclination, a=self.bg.a, standard=2, avals=avals, n=nvals)

        q2 = g.q2
        su = g.su
        sm = g.sm
        l = g.l # NOT the output

        # If a pixel i's geodesic misses the midplane, u_locations[i] = -1
        # Otherwise, u_locations[i] is the radius r=1/u where the photon hit.
        # We assume that the gas at that radiates emitted the photon.
        # To calculate redshift, we need to know the photon momentum in the
        # locally non-rotating frame. We need to transform the gas's velocity at that location
        # (given by the Gammie 1999 trajectories).
        # -------------------------------------------
        # Eliminate photons that don't hit the disk
        indices_to_remove = u_locations == -1.0
        print("{:d} pixels' geodesics did not hit the disk.".format(indices_to_remove.sum()))
        u_locations = u_locations[~indices_to_remove]
        mu_locations = mu_locations[~indices_to_remove]
        dt = dt[~indices_to_remove]
        dphi = dphi[~indices_to_remove]
        tpm = tpm[~indices_to_remove]
        tpr = tpr[~indices_to_remove]
        indices_to_remove = np.squeeze(indices_to_remove)
        i1 = i1[~indices_to_remove]
        i2 = i2[~indices_to_remove]
        q2 = q2[~indices_to_remove]
        su = su[~indices_to_remove]
        sm = sm[~indices_to_remove]
        l = l[~indices_to_remove]

        # Eliminate photons that hit the disk outside the plunging region
        photon_emitted_radii = 1./u_locations
        indices_to_remove = photon_emitted_radii >= self.starting_r
        print("{:d} pixels' geodesics hit at r>rISCO.".format(indices_to_remove.sum()))
        photon_emitted_radii = photon_emitted_radii[~indices_to_remove]
        u_locations = u_locations[~indices_to_remove]
        mu_locations = mu_locations[~indices_to_remove]
        dt = dt[~indices_to_remove]
        dphi = dphi[~indices_to_remove]
        tpm = tpm[~indices_to_remove]
        tpr = tpr[~indices_to_remove]
        indices_to_remove = np.squeeze(indices_to_remove)
        i1 = i1[~indices_to_remove]
        i2 = i2[~indices_to_remove]
        q2 = q2[~indices_to_remove]
        su = su[~indices_to_remove]
        sm = sm[~indices_to_remove]
        l = l[~indices_to_remove]

        # ------------------------------
        # Get the Gammie 1999 velocities
        # ------------------------------
        # Interpolate between radii
        uT_interp_f = interp1d(self.bg.r_range, self.bg.uTsolution)
        uR_interp_f = interp1d(self.bg.r_range, self.bg.uRsolution)
        uPhi_interp_f = interp1d(self.bg.r_range, self.bg.uPhisolution)
        # Use interpolation at emitted radii (boyer-lindquist coords)
        uT_photons = uT_interp_f(photon_emitted_radii)
        uR_photons = uR_interp_f(photon_emitted_radii)
        uPhi_photons = uPhi_interp_f(photon_emitted_radii)
        uTheta_photons = 0.0*np.ones(uR_photons.shape)

        # ------------------------------
        # Transform to locally non-rotating frame
        # ------------------------------
        theta_array = np.pi/2.*np.ones(uR_photons.shape)
        # Calculate three-velocities
        vR_bl = uR_photons/uT_photons
        vTheta_bl = uTheta_photons/uT_photons
        vPhi_bl = uPhi_photons/uT_photons
        vMag2 = vR_bl**2 + vTheta_bl**2 + vPhi_bl**2
        vR_lnrf, vTheta_lnrf, vPhi_lnrf = calcg.lnrf_frame(vR_bl, vTheta_bl, vPhi_bl, photon_emitted_radii, self.bg.a, theta_array)

        # For some reason the transformed velocity can be faster than c, just for one?
        # TODO
        vMag2 = vR_lnrf**2 + vTheta_lnrf**2 + vPhi_lnrf**2
        print("Number of velocities with v > c: {:d}".format((vMag2 >= 1).sum()))
        indices_to_remove = vMag2 >= 1
        photon_emitted_radii = photon_emitted_radii[~indices_to_remove]
        u_locations = u_locations[~indices_to_remove]
        mu_locations = mu_locations[~indices_to_remove]
        dt = dt[~indices_to_remove]
        dphi = dphi[~indices_to_remove]
        tpm = tpm[~indices_to_remove]
        tpr = tpr[~indices_to_remove]
        i1 = i1[~indices_to_remove]
        i2 = i2[~indices_to_remove]
        q2 = q2[~indices_to_remove]
        su = su[~indices_to_remove]
        sm = sm[~indices_to_remove]
        l = l[~indices_to_remove]
        vR_lnrf = vR_lnrf[~indices_to_remove]
        vTheta_lnrf = vTheta_lnrf[~indices_to_remove]
        vPhi_lnrf = vPhi_lnrf[~indices_to_remove]

        # ------------------------------
        # Calculate redshift from each photon
        # ------------------------------
        redshifts = calcg.calcg(u_locations, mu_locations, q2, l, self.bg.a, tpm, tpr, su, sm, vR_lnrf, vTheta_lnrf, vPhi_lnrf)

        # Sort redshifts to be increasing radius
        sorted_inds = photon_emitted_radii.argsort()
        photon_emitted_radii = photon_emitted_radii[sorted_inds[::-1]]
        redshifts = redshifts[sorted_inds[::-1]]

        figure_dir = self.path_to_figures
        plt.plot(photon_emitted_radii, redshifts, ls='None', marker='o')
        plt.ylabel(r"Redshift $g$")
        plt.xlabel(r"$r/r_g$")
        plt.title(r"$i=${:.0f} degrees".format(inc_angle_deg))
        plt.savefig(figure_dir + "redshifts_over_r_i{:d}.png".format(inc_angle_deg))
        plt.close()

        redshift_dict = {"photon_emitted_radii":photon_emitted_radii,
                        "redshifts":redshifts,
                        "dalpha":dalpha,
                        "dbeta":dbeta}
        return redshift_dict

    def calculate_redshifted_spectra(self, inc_angle_deg=60, decrease_by_tau=True):
        # Load in camera properties
        redshift_dict = self.trace_geodesics(inc_angle_deg)
        photon_emitted_radii = redshift_dict["photon_emitted_radii"]
        redshifts = redshift_dict["redshifts"]
        dalpha = redshift_dict["dalpha"]
        dbeta = redshift_dict["dbeta"]
        spectrum_dict = {}

        # Load in multizone equilibria quantites
        self.get_quick_arrays()
        temperature_over_r = self.temperature_over_r
        gamma1_over_r = self.gamma1_over_r
        peff_over_r = self.peff_over_r
        gamma2_over_r = self.quick_gamma2_over_r
        r_range = self.quick_r_range
        ndensity_over_r = self.quick_number_density_over_r
        Br_over_r = self.quick_Br_over_r
        tau_es_over_r = self.quick_tau_es
        tsync_over_r = self.quick_tsync_over_r
        tIC_over_r = self.quick_tIC_over_r

        spectral_bins = self.get_spectral_bins()

        # Create interpolation functions at photon emitted radii
        temp_interp_f = interp1d(r_range, temperature_over_r, fill_value="extrapolate")
        n_interp_f = interp1d(r_range, ndensity_over_r, fill_value="extrapolate")
        Br_interp_f = interp1d(r_range, Br_over_r, fill_value="extrapolate")
        gamma1_interp_f = interp1d(r_range, gamma1_over_r, fill_value="extrapolate")
        gamma2_interp_f = interp1d(r_range, gamma2_over_r, fill_value="extrapolate")
        peff_interp_f = interp1d(r_range, peff_over_r, fill_value="extrapolate")
        tau_interp_f = interp1d(r_range, tau_es_over_r, fill_value='extrapolate')
        tsync_interp_f = interp1d(r_range, tsync_over_r, fill_value='extrapolate')
        tIC_interp_f = interp1d(r_range, tIC_over_r, fill_value='extrapolate')

        nonthermal_integrands = np.zeros(spectral_bins.shape)
        thermal_integrands = np.zeros(spectral_bins.shape)
        nonthermal_redshift_integrands = np.zeros(spectral_bins.shape)
        thermal_redshift_integrands = np.zeros(spectral_bins.shape)
        r_range_cm = photon_emitted_radii * self.bg.get_rg_in_cm(self.M_in_Msun)
        # For each pixel, calculate Inu(r).
        for j, radius in enumerate(photon_emitted_radii):
            gamma1 = gamma1_interp_f(radius)
            gamma2 = gamma2_interp_f(radius)
            Br = Br_interp_f(radius)
            n_at_r = n_interp_f(radius)
            temp_at_r = temp_interp_f(radius)
            theta = Consts.kB_cgs*temp_at_r/Consts.restmass_cgs
            peff = peff_interp_f(radius)
            norm = self.solve_for_PL_norm(gamma1, theta, n_at_r, peff)
            args = (gamma2, Br, n_at_r, r_range, radius)
            tau = tau_interp_f(radius)

            if tsync_interp_f(radius) < tIC_interp_f(radius):
                nt_cooling_rate = self.calculate_sync_cooling_rate(norm, gamma1, peff, None, args)
            else:
                nt_cooling_rate = self.calculate_IC_cooling_rate(norm, gamma1, peff, None, args)

            nt_cooling_rate = nt_cooling_rate/tau

            # ---------------------------------
            # Calculate Inu of power-law electrons
            # ---------------------------------
            # Assume spectrum is A*nu^(-s) between nu1 and nu2.
            # Find A = spectrum_constant by integrating and set equal to cooling rate
            omega_const = 3*Consts.e_in_cgs*np.abs(Br)/(2*Consts.me_in_g*Consts.c_in_cgs)
            nu1 = omega_const*gamma1**2/(2*np.pi)
            nu2 = omega_const*gamma2**2/(2*np.pi)
            s_index = (peff - 1.)/2.
            start_index = (np.abs(nu1 - spectral_bins)).argmin()
            end_index = (np.abs(nu2 - spectral_bins)).argmin()
            nu1_bin = spectral_bins[start_index]
            nu2_bin = spectral_bins[end_index]

            spectrum_constant = nt_cooling_rate * (1.-s_index)/(nu2_bin**(1.-s_index) - nu1_bin**(1.-s_index))

            Pnu = spectrum_constant * spectral_bins**(-s_index)
            Pnu[:start_index] = 0
            Pnu[end_index:] = 0

            # Because the integration/split between bins isn't perfect,
            # use residual factor to ensure the total power is correct.
            P_nonthermal = np.trapz(Pnu, x=spectral_bins)
            residual_factor =  nt_cooling_rate/P_nonthermal
            Pnu = Pnu * residual_factor
            P_nonthermal = np.trapz(Pnu, x=spectral_bins)

            # Isotropic emission --> jnu = Pnu/4pi
            # dInu = jnu*ds. Set ds to be height (same as radius for H/R=1)
            height = radius*self.bg.get_rg_in_cm(self.M_in_Msun)*self.H_over_R
            Inu_nonthermal_orig = Pnu/(4.0*np.pi)*height

            # Decrease nonthermal intensity by 1/tau
            # NOTE: doing this will make power_in_powerlaw != heating_rate*volume
            # unless accounted for in thermal.
            if decrease_by_tau:
                Inu_thermalized = Inu_nonthermal_orig*(1.0 - 1.0/tau)
                Inu_nonthermal = Inu_nonthermal_orig/tau
                nt_cooling_rate = nt_cooling_rate/tau
            else:
                Inu_nonthermal = Inu_nonthermal_orig

            # Redshift
            Inu_nonthermal_redshift = Inu_nonthermal*redshifts[j]**3 # multiply by g^3
            nonthermal_integrands = np.vstack([nonthermal_integrands, Inu_nonthermal])
            nonthermal_redshift_integrands = np.vstack([nonthermal_redshift_integrands, Inu_nonthermal_redshift])

            # ---------------------------------
            # Calculate Inu of thermal electrons
            # ---------------------------------
            # RL eq. 7.65b
            xcoh = 2.4e17 * np.sqrt(Consts.me_in_g*n_at_r)*(temp_at_r)**(-9.0/4.0)
            # RL eq. 7.74b
            amplification_factor = 0.75*(np.log(2.25/xcoh))**2
            # RL eq. 5.15b
            gaunt_factor = 1.2
            bremsstrahlung_emissivity = 1.4e-27*np.sqrt(temp_at_r)*n_at_r**2*gaunt_factor
            thermal_cooling_rate = amplification_factor * bremsstrahlung_emissivity
            # Account for energy in power law thermalized by scattering
            if decrease_by_tau:
                thermalized_cooling_rate = np.trapz(Inu_thermalized*spectral_bins, x=np.log(spectral_bins))*np.pi*4./height
                thermal_cooling_rate += thermalized_cooling_rate
            thermal_jnu = thermal_cooling_rate/4.0/np.pi

            # Make sure that I=j*H = RL eq. 7.71, j = P/4\pi = A\epsilon/4\pi
            intensity_constant = 12.0*Consts.kB_cgs**4.*temp_at_r**4./(Consts.c_in_cgs**2.*Consts.h_cgs**3.)
            exponential_decrease = thermal_jnu*height/intensity_constant
            Inu_thermal = 2*Consts.h_cgs * spectral_bins**3./Consts.c_in_cgs**2. * np.exp(-Consts.h_cgs*spectral_bins/(Consts.kB_cgs*temp_at_r))
            Inu_thermal = Inu_thermal*exponential_decrease

            # Because the integration/split between bins isn't perfect,
            # use residual factor to ensure the total power is correct.
            P_thermal = np.trapz(Inu_thermal, x=spectral_bins)*4.0*np.pi/height
            residual_factor =  thermal_cooling_rate/P_thermal
            Inu_thermal = Inu_thermal * residual_factor

            # Redshift
            Inu_thermal_redshift = Inu_thermal * redshifts[j]**3 # multiply by g^3
            thermal_integrands = np.vstack([thermal_integrands, Inu_thermal])
            thermal_redshift_integrands = np.vstack([thermal_redshift_integrands, Inu_thermal_redshift])

        nonthermal_integrands = nonthermal_integrands[1:, :]
        thermal_integrands = thermal_integrands[1:, :]
        nonthermal_redshift_integrands = nonthermal_redshift_integrands[1:, :]
        thermal_redshift_integrands = thermal_redshift_integrands[1:, :]

        # For distant observer:
        # Fnu = \int Inu dA/D^2 = \int Inu 2\pi rdr/D^2, D is distance
        # Lnu = 4\pi D^2 Fnu = independent of D
        # For camera: Fnu = sum over each pixel
        #                 = \Delta\alpha\Delta\beta/D^2*sum(intensity at each pixel)
        flux_const_nonthermal_redshift = dalpha*dbeta*np.sum(nonthermal_redshift_integrands, axis=0)
        flux_const_thermal_redshift = dalpha*dbeta*np.sum(thermal_redshift_integrands, axis=0)
        flux_const_nonthermal = dalpha*dbeta*np.sum(nonthermal_integrands, axis=0)
        flux_const_thermal = dalpha*dbeta*np.sum(thermal_integrands, axis=0)

        # Isotropic Lnu = 4\pi D^2 Fnu
        Lnu_nonthermal_redshift = 4.0*np.pi*flux_const_nonthermal_redshift
        Lnu_thermal_redshift = 4.0*np.pi*flux_const_thermal_redshift
        Lnu_nonthermal = 4.0*np.pi*flux_const_nonthermal
        Lnu_thermal = 4.0*np.pi*flux_const_thermal

        power_in_powerlaw = np.trapz(Lnu_nonthermal_redshift, spectral_bins)
        power_in_thermal_pr = np.trapz(Lnu_thermal_redshift, spectral_bins)
        total_heating = self.volume_heating_rate * self.emitting_volume
        print("Integrated Lnu over power-law: {:.2e}".format(power_in_powerlaw))
        print("Integrated Lnu over thermal: {:.2e}".format(power_in_thermal_pr))
        print("Heating rate*volume: {:.2e}".format(total_heating))

        spectrum_dict["spectral_bins"] = spectral_bins
        spectrum_dict["Lnu_nonthermal"] = Lnu_nonthermal_redshift
        spectrum_dict["Lnu_thermal"] = Lnu_thermal_redshift
        spectrum_dict["power_in_powerlaw"] = power_in_powerlaw
        spectrum_dict["power_in_thermal_pr"] = power_in_thermal_pr

        spectrum_path = self.path_to_reduced_data + "spectrum"
        if not decrease_by_tau:
            spectrum_path += "_noTauDecrease"
        spectrum_path += "_redshifted_i{:d}".format(inc_angle_deg)
        # BRANCH: change spectrum path XX
        spectrum_path += "_rewrite2"
        spectrum_path += ".p"
        with open(spectrum_path, 'wb') as file:
            pickle.dump(spectrum_dict, file)
        return spectrum_dict

    def print_consistency_check(self):
        self.get_quick_arrays()

        # Infall time
        tinfall = self.bg.t_infall
        tinfall_s = tinfall * self.bg.get_tg_in_s(self.M_in_Msun)

        j = 0
        tcool = self.quick_cooling_times[j]
        gamma1_eq = self.gamma1_over_r[j]
        temp = self.temperature_over_r[j]
        PL_eff = self.peff_over_r[j]
        ndensity = self.quick_number_density_over_r[j]
        UB = self.quick_UB_over_r[j]
        thetaj = Consts.kB_cgs*temp/Consts.restmass_cgs
        norm_eq = self.solve_for_PL_norm(gamma1_eq, thetaj, ndensity, PL_eff)
        if self.quick_tsync_over_r[j] < self.quick_tIC_over_r[j]:
            nt_cooling_rate_eq = self.calculate_sync_cooling_rate(norm_eq, gamma1_eq, PL_eff, j)
        else:
            nt_cooling_rate_eq = self.calculate_IC_cooling_rate(norm_eq, gamma1_eq, PL_eff, j)

        nt_cooling_rate_eq = nt_cooling_rate_eq/self.quick_tau_es[j]
        xcoh = 2.4e17 * np.sqrt(Consts.me_in_g*ndensity)*(temp)**(-9.0/4.0)
        # RL eq. 7.74b
        amplification_factor = 0.75*(np.log(2.25/xcoh))**2
        # RL eq. 5.15b
        gaunt_factor = 1.2
        bremsstrahlung_emissivity = 1.4e-27*np.sqrt(temp)*ndensity**2*gaunt_factor
        thermal_cooling_rate_eq = amplification_factor * bremsstrahlung_emissivity
        cooling_rate_eq = nt_cooling_rate_eq + thermal_cooling_rate_eq

        # Electron-electron Collision time
        nu0const = Consts.nu0const1 * Consts.coulomb_log * ndensity
        kinetic_ratio = (gamma1_eq - 1.)/thetaj
        integral_path = self.bg.path_to_reduced_data + "tabulated_integral.txt"
        tabulated_integral = np.loadtxt(integral_path)
        x_index = (np.abs(kinetic_ratio - tabulated_integral[0, :])).argmin()
        psi_value = tabulated_integral[1, x_index]
        tcoll = (gamma1_eq - 1.)**1.5/(2.*nu0const*psi_value)

        # To see if electrons and protons can exchange energy, we'd like to calculate the
        # THERMALIZATION time scale between two maxwellians with different temperatures:
        # the protons with equipartition temperature and the electrons with the calculated
        # temperature that sets cooling and heating equal.
        ion_temperature = UB/ndensity/Consts.kB_cgs
        nu_thermalize = 1.8e-19*np.sqrt(Consts.mp_in_g*Consts.me_in_g)*ndensity*Consts.coulomb_log/(Consts.mp_in_g*ion_temperature+Consts.me_in_g*temp)**1.5
        Tfinal_isotropize = ion_temperature + (temp - ion_temperature)*np.exp(-nu_thermalize*tinfall_s)
        T_fractional_change = (Tfinal_isotropize - temp)/temp

        # To see if electrons can be efficiently accelerated, calculate the
        # ACCELERATION time scale. We take this estimate from Commiso & Sironi 2018
        # Fig. 5...note it's just an estimate that doesn't depend on magnetization
        plasma_freq = 1.0e4*np.sqrt(ndensity)
        t_accel = 50/plasma_freq

        # To see the thermal cooling timescale, take the average energy and divide
        # by the cooling rate (RL eq. 5.25)
        temp_in_eV = temp*8.62e-5
        t_brems = 3.e27*Consts.kB_cgs*temp/(ndensity*np.sqrt(temp_in_eV))

        print("Dynamical background has an infall time of {:.1e} rg/c, or {:.1e}s".format(tinfall, tinfall_s))
        print("At r={:.3f}rg, T={:.3e} K, theta={:.2e}, gamma1 = {:.3f}:".format(self.quick_r_range[j], temp, thetaj, gamma1_eq))
        print("  t_cool(gamma1) = {:.2e}s = {:.2e} tinfall".format(tcool/gamma1_eq, tcool/gamma1_eq/tinfall_s))
        print("  t_coll(gamma1) = {:.2e}s = {:.2e} tinfall".format(tcoll, tcoll/tinfall_s))
        print("  t_isotropize = {:.2e}s = {:.2e} tinfall".format(1/nu_thermalize, 1/nu_thermalize/tinfall_s))
        print("  t_accel = {:.2e}s = {:.2e} tinfall".format(t_accel, t_accel/tinfall_s))
        print("  t_brems = {:.2e}s = {:.2e} tinfall".format(t_brems, t_brems/tinfall_s))
        print("T will increase to {:.2e} K ({:.3f} %) over the infall time due to thermalization with ions".format(Tfinal_isotropize, T_fractional_change*100))
        print("Equipartition ion temperature: {:.2e} K".format(ion_temperature))
        if not np.allclose(self.volume_heating_rate, cooling_rate_eq):
            print("--------------------------------------------------")
            print("ERROR!!!!!!!!!!!!!!!!!!!!!")
            print("Heating is not equal to cooling, i.e. system did NOT equilibrate!")
            print("--------------------------------------------------")
        print("Cooling rate = {:.3e} erg/s/cm3".format(cooling_rate_eq))
        print("Heating rate = {:.3e} erg/s/cm3".format(self.volume_heating_rate))
        print("Normalization = {:.3e}".format(norm_eq))
        print("Number density = {:.2e} 1/cm3".format(ndensity))

        # Check where absorption becomes important. Use RL Eq. 7.64b.
        def xt_equations(xt, *args):
            T, ndensity, tau = args
            return xt**3/(1.0 - np.exp(-xt)) - 4e25/(T**(7.0/2.0))*Consts.mp_in_g*ndensity*tau**2
        # Check at inner radius first, then outer.
        Ti = self.temperature_over_r[0]
        taui = self.quick_tau_es[0]
        ni = self.quick_number_density_over_r[0]
        args = (Ti, ni, taui)
        xt_solve = least_squares(xt_equations, 1e-4, args=args, bounds=(0, np.inf))
        xti = xt_solve.x[0]
        nutvali = xti*Consts.kB_cgs*Ti/Consts.h_cgs
        Tf = self.temperature_over_r[-1]
        tauf = self.quick_tau_es[-1]
        nf = self.quick_number_density_over_r[-1]
        args = (Tf, nf, tauf)
        xt_solve = least_squares(xt_equations, 1e-4, args=args, bounds=(0, np.inf))
        xtf = xt_solve.x[0]
        nutvalf = xtf*Consts.kB_cgs*Ti/Consts.h_cgs
        print("Absorption becomes important for frequencies below {:.2e} or {:.2e}Hz".format(nutvali, nutvalf))


    def collision_time_equation(self, gamma1, temperature, number_density, cooling_time):
        theta_e = Consts.kB_cgs * temperature/Consts.restmass_cgs
        nu0const = Consts.nu0const1 * Consts.coulomb_log * number_density

        # Tabulate needed integrals
        integral_path = self.bg.path_to_reduced_data + "tabulated_integral.txt"
        integral_path2 = self.bg.path_to_reduced_data + "../tabulated_integral.txt"
        try:
            table_exists = os.path.exists(integral_path2)
            integral_path = integral_path2
        except:
            table_exists = os.path.exists(integral_path)
        if not table_exists:
            # Tabulate \psi(x) on NRL p. 31
            x_values = np.logspace(-5, 2, 10000)
            psi_values = []
            for x in x_values:
                psi_value = psi(x)
                psi_values.append(psi_value)
            tabulated_integral = np.vstack([x_values, psi_values])
            np.savetxt(integral_path, tabulated_integral)
            print("Tabulated psi(x).")
        else:
            tabulated_integral = np.loadtxt(integral_path)

        # Kinetic_ratio is x
        kinetic_ratio = (gamma1 - 1)/theta_e
        x_index = (np.abs(kinetic_ratio - tabulated_integral[0, :])).argmin()
        psi_value = tabulated_integral[1, x_index]

        # Full collision time = 1/(2*nu0const*psi(x))*(gamma-1)^1.5
        collision_time = 1.0/(2*nu0const*psi_value)*(gamma1-1.)**1.5
        equation_to_solve = (cooling_time/gamma1 - collision_time)/(cooling_time/gamma1)

        # For the high-energy limit:
        # high_energy_const = 1.0e6/(7.7*number_density*Consts.coulomb_log)*(5.11e5)**1.5
        # equation_to_solve = cooling_time/gamma - high_energy_const*(gamma-1)**1.5
        # print(equation_to_solve/collision_time)

        # return equation_to_solve/collision_time
        return equation_to_solve

    def get_gamma_bins(self):
        self.get_quick_arrays()
        gamma_min = 1.0
        gamma_max = self.quick_gamma2_over_r.max()
        gamma_bins = np.logspace(np.log10(gamma_min), np.log10(gamma_max), 100000)
        self.gamma_bins = gamma_bins
        return gamma_bins


    def get_total_distribution_function(self):
        """
        Calculate f_total = f_MJ + f_PL at each radius
        """
        self.get_quick_arrays()
        self.get_gamma_bins()

        f_dict = {}
        f_dict["thermal"] = {}
        f_dict["nonthermal"] = {}

        total_density_over_r = []
        nonthermal_density_over_r = []
        thermal_density_over_r = []
        average_energy_over_r = []
        nonthermal_energy_over_r = []
        thermal_energy_over_r = []

        for k, temp in enumerate(self.temperature_over_r):
            ndensity = self.quick_number_density_over_r[k]
            theta = Consts.kB_cgs*temp/Consts.restmass_cgs
            fMJ = maxwell_juettner_over_gamma(self.gamma_bins, ndensity, theta)

            gamma2 = self.quick_gamma2_over_r[k]
            gamma1 = self.gamma1_over_r[k]
            peff = self.peff_over_r[k]
            norm = self.solve_for_PL_norm(gamma1, theta, ndensity, peff)
            ind1 = (np.abs(self.gamma_bins - gamma1)).argmin()
            ind2 = (np.abs(self.gamma_bins - gamma2)).argmin()
            fPL = norm * ndensity * self.gamma_bins**(-peff)
            fPL[:ind1] = 0
            fPL[ind2:] = 0

            f_dict["nonthermal"][k] = fPL
            f_dict["thermal"][k] = fMJ

            ftotal = fPL + fMJ

            total_density = np.trapz(ftotal, x=self.gamma_bins)
            nonthermal_density = np.trapz(fPL, x=self.gamma_bins)
            thermal_density = np.trapz(fMJ, x=self.gamma_bins)

            total_density_over_r.append(total_density)
            nonthermal_density_over_r.append(nonthermal_density)
            thermal_density_over_r.append(thermal_density)

            # Calculate average electron energy
            average_energy = np.trapz(ftotal*(self.gamma_bins-1.0), x=self.gamma_bins)
            average_energy = average_energy*Consts.restmass_cgs/total_density
            nonthermal_energy = np.trapz(fPL*(self.gamma_bins-1.0), x=self.gamma_bins)
            nonthermal_energy = nonthermal_energy*Consts.restmass_cgs/total_density
            thermal_energy = np.trapz(fMJ*(self.gamma_bins-1.0), x=self.gamma_bins)
            thermal_energy = thermal_energy*Consts.restmass_cgs/total_density

            average_energy_over_r.append(average_energy)
            nonthermal_energy_over_r.append(nonthermal_energy)
            thermal_energy_over_r.append(thermal_energy)

        self.total_density_over_r = np.array(total_density_over_r)
        self.nonthermal_density_over_r = np.array(nonthermal_density_over_r)
        self.thermal_density_over_r = np.array(thermal_density_over_r)
        self.average_energy_over_r = np.array(average_energy_over_r)
        self.nonthermal_energy_over_r = np.array(nonthermal_energy_over_r)
        self.thermal_energy_over_r = np.array(thermal_energy_over_r)

        self.f_dict = f_dict
        return f_dict



def solve_for_PL_eff(eff_args):
    theta_e, gamma1, gamma2, PL_val, number_density = eff_args
    PL_eff_solve = least_squares(PL_eff_equation, PL_val, args=eff_args, bounds=(0.0, PL_val))
    PL_eff = PL_eff_solve.x[0]
    return PL_eff

def PL_eff_equation(p_eff, *args):
    # Note that fMJ(gamma1)=fPL(gamma1) (continuity) and that
    # fPL(gamma2) = fPL0(gamma2); by dividing fPL(gamma1)/fPL(gamma2),
    # we get rid of the dependence on the norm of fPL and can solve for PL_eff.
    theta, gamma1, gamma2, PL_val, number_density = args

    fMJ_at_gamma1 = maxwell_juettner_at_gamma(gamma1, number_density, theta)
    denom = (PL_val - 1.0)/(1.0 - gamma2**(1.0 - PL_val))*number_density*gamma2**(-PL_val)
    left_hand_side = (gamma1/gamma2)**(-p_eff)
    return left_hand_side - fMJ_at_gamma1/denom


if __name__ == '__main__':
    kwargs = {}
    kwargs["Fthetaphi"] = 1.0
    # kwargs["Fthetaphi"] = 4.0
    # NOTE: 1.0 does not have equilibrium!!
    bg = dynamical_background(**kwargs)

    kwargs["PL_type"] = "WUBCN18"
    # kwargs["PL_type"] = "WPU19"
    # kwargs["PL_type"] = "constant"
    # kwargs["gamma2_type"] = "fixed"
    kwargs["gamma2_type"] = "4sigma"

    kwargs["quick"] = True
    kwargs["jump"] = 1
    decrease_by_tau = True
    decrease_by_tau = False
    inc_angle_deg = None
    # inc_angle_deg = 30

    equilibrium = equilibrium_finder(bg, **kwargs)

    # --------------------------------------------------------------
    # Plot some constant quantities
    # --------------------------------------------------------------
    # print("Saving prescription-independent figures in " + equilibrium.path_to_figures)
    # equilibrium.plot_gammie_quantities()

    # --------------------------------------------------------------
    # Plot some prescription-dependent quantities
    # --------------------------------------------------------------
    # equilibrium.plot_prescription_quantities()

    # print("Saving temperature-dependent quantities in " + equilibrium.path_to_figures2)
    equilibrium.plot_temperature_dependent_quantities()

    # --------------------------------------------------------------
    # Consistency check: calculate relevant time scales
    # --------------------------------------------------------------
    # equilibrium.print_consistency_check()

    # --------------------------------------------------------------
    # Calculate spectra
    # --------------------------------------------------------------
    # Not redshifted
    # equilibrium.plot_spectra(decrease_by_tau=decrease_by_tau, inc_angle_deg=None)
    # Redshifted
    # equilibrium.plot_spectra(decrease_by_tau=decrease_by_tau, inc_angle_deg=inc_angle_deg)
