import sys
import numpy as np
import matplotlib
import pickle
from scipy.optimize import fsolve
from scipy.optimize import least_squares
import scipy.integrate as integrate
from matplotlib.legend import Legend
from matplotlib.lines import Line2D
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
sys.path.append('..')
from tools import *
from dynamical_background_gammie1999 import *
from scipy import special

small_size = 22
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)
matplotlib.rc('text', usetex=True)

def calculate_spectra():
    # --------------------------------------------------------------
    # Spectrum from plunging region power-law of electrons
    # --------------------------------------------------------------
    PL_spectrum_path = bg.path_to_reduced_data + "PL_spectrum_Fthetaphi{:.2f}_cutoff".format(bg.Fthetaphi) + gamma2_str + "_index" + PL_filestr + ".p"
    if not os.path.exists(PL_spectrum_path):
        PL_spectrum_dictionary = {}
    else:
        print("Loading PL spectrum from pickle file " + PL_spectrum_path)
        with open(PL_spectrum_path, 'rb') as file:
            PL_spectrum_dictionary = pickle.load(file)

    omega_const = 3*Consts.e_in_cgs*np.abs(adjusted_Br_over_r)/(2*Consts.me_in_g*Consts.c_in_cgs)
    nu1_list = omega_const*gamma1_over_r**2/(2.0*np.pi)
    nu2_list = omega_const*adjusted_gamma2**2/(2.0*np.pi)

    nu_min = nu1_list.min()
    nu_max = nu2_list.max()
    spectral_bins = np.logspace(np.log10(nu_min), np.log10(nu_max), 1000)
    integrands = np.zeros(spectral_bins.shape)

    for j, r in enumerate(adjusted_r_range):
        if j in PL_spectrum_dictionary:
            intensity = PL_spectrum_dictionary[j]
            integrands = np.vstack([integrands, 2.0*np.pi*r*bg.get_rg_in_cm(M_in_Msun)*intensity])
            # print("i={:d} already in dictionary. Skipping.".format(i))
            continue
        # Assume spectrum is A*nu^(-s) between nu1 and nu2.
        # Find A = spectrum_constant by integrating and set equal to cooling rate
        tcool = adjusted_cooling_times[j]
        gamma1_eq = gamma1_over_r[j]
        gamma2_eq = gamma2_vals[j]
        omega_const = 3*Consts.e_in_cgs*np.abs(adjusted_Br_over_r[j])/(2*Consts.me_in_g*Consts.c_in_cgs)
        nu1 = omega_const*gamma1_eq**2/(2*np.pi)
        nu2 = omega_const*gamma2_eq**2/(2*np.pi)
        s_index = (adjusted_PL_index[j] - 1)/2
        spectrum_constant = heating_rate * (1-s_index)/(nu2**(1-s_index) - nu1**(1-s_index))

        start_index = (np.abs(nu1 - spectral_bins)).argmin()
        end_index = (np.abs(nu2 - spectral_bins)).argmin()
        Pnu = spectrum_constant * spectral_bins**(-s_index)
        Pnu[:start_index] = 0
        Pnu[end_index:] = 0
        # Isotropic emission --> jnu = Pnu/4pi
        # dInu = jnu*ds. Set ds to be height (same as radius for H/R=1)
        intensity = Pnu/(4.0*np.pi)*r*bg.get_rg_in_cm(M_in_Msun)

        # Decrease intensity by 1/tau
        intensity /= adjusted_tau_es[j]

        integrands = np.vstack([integrands, 2.0*np.pi*r*bg.get_rg_in_cm(M_in_Msun)*intensity])
        PL_spectrum_dictionary[j] = intensity
    integrands = integrands[1:, :]
    # Fnu = \int Inu dA/D^2 = \int Inu 2\pi rdr/D^2, D is distance
    # Lnu = 4\pi D^2 Fnu = independent of D
    # Integrate all_intensity over radius
    total_flux_const = np.trapz(integrands[::-1, :], adjusted_r_range[::-1]*bg.get_rg_in_cm(M_in_Msun), axis=0)
    # Factor of 2 because wedge shape has two sides
    Lnu = 2.0*4.0*np.pi*total_flux_const

    PL_spectrum_dictionary["total_spectrum"] = Lnu
    with open(PL_spectrum_path, 'wb') as fh:
        pickle.dump(PL_spectrum_dictionary, fh)

    # --------------------------------------------------------------
    # Spectrum from plunging region thermal electrons
    # --------------------------------------------------------------
    # Saturated Compton regime: RL eq. 7.71
    # Fnu = \int Inu dA/D^2 = \int Inu 2\pi rdr/D^2, D is distance
    # Lnu = 4\pi D^2 Fnu = 8\pi^2 \int Inu r dr
    Lnu_thermal = []
    # RL eq. 7.65b
    xcoh = 2.4e17 * np.sqrt(Consts.me_in_g*adjusted_number_density_over_r)*(temperature_over_r)**(-9.0/4.0)
    # RL eq. 7.74b
    amplification_factor = 0.75*(np.log(2.25/xcoh))**2
    # RL eq. 5.15b
    gaunt_factor = 1.2
    bremsstrahlung_emissivity = 1.4e-27*np.sqrt(temperature_over_r)*adjusted_number_density_over_r**2*gaunt_factor
    thermal_cooling_rate = amplification_factor * bremsstrahlung_emissivity
    cooling_ratio = np.max(thermal_cooling_rate / heating_rate)
    # Find e^-alpha by setting F^W = A bremsstrahlung-emissivity radial_size_cm
    FW = 12.0*np.pi*Consts.kB_cgs**4*temperature_over_r**4/(Consts.c_in_cgs**2*Consts.h_cgs**3)
    # exponential_decrease = amplification_factor*bremsstrahlung_emissivity*radial_size_cm/FW
    exponential_decrease = amplification_factor*bremsstrahlung_emissivity*adjusted_r_range*bg.get_rg_in_cm(M_in_Msun)/FW

    for nu in spectral_bins:
        Inu_thermal_val = 2*Consts.h_cgs * nu**3/Consts.c_in_cgs**2 * np.exp(-Consts.h_cgs*nu/(Consts.kB_cgs*temperature_over_r))*exponential_decrease
        Lnu_thermal_val = 8.0*np.pi**2 * np.trapz(Inu_thermal_val*adjusted_r_range[::-1]*bg.get_rg_in_cm(M_in_Msun), adjusted_r_range[::-1]*bg.get_rg_in_cm(M_in_Msun))
        Lnu_thermal.append(Lnu_thermal_val)
    Lnu_thermal = np.array(Lnu_thermal)
    print("Integrated Lnu over power-law: {:.2e}".format(np.trapz(Lnu, spectral_bins)))
    print("Integrated Lnu over thermal: {:.2e}".format(np.trapz(Lnu_thermal, spectral_bins)))
    r1 = starting_r * bg.get_rg_in_cm(M_in_Msun)
    r2 = ending_r * bg.get_rg_in_cm(M_in_Msun)
    volume = 4.0*np.pi/3.0*np.abs(r1**3 - r2**3)
    print("Heating rate*volume: {:.2e}".format(heating_rate * volume))

    # --------------------------------------------------------------
    # Disk spectrum
    # --------------------------------------------------------------
    disk_outer_edge = 1e10 # cm
    disk_inner_edge = radial_size_cm
    rstar = bg.rEH * bg.get_rg_in_cm(M_in_Msun)
    disk_radial_bins = np.logspace(np.log10(disk_inner_edge), np.log10(disk_outer_edge), 1000)
    L_edd = get_eddington_lum(M_in_Msun)
    Mdot_edd = L_edd/(f_Ledd*Consts.c_in_cgs**2.0)
    Mdot = f_Mdot*Mdot_edd  # g/s
    disk_temp_over_r = (3*Consts.G_in_cgs*M_in_Msun*Consts.solar_mass_in_g*Mdot/(8*np.pi*disk_radial_bins**3*Consts.SB_cgs)*(1.0 - np.sqrt(rstar/disk_radial_bins)))**0.25
    print("Maximum disk temperature: {:.2e} K".format(disk_temp_over_r.max()))
    print("Minimum disk temperature: {:.2e} K".format(disk_temp_over_r.min()))
    disk_function = []
    for nu in spectral_bins:
        planck_vals = []
        for i, r in enumerate(disk_radial_bins):
            try:
                planck_val = 1.0/(np.exp(Consts.h_cgs*nu/(Consts.kB_cgs*disk_temp_over_r[i])) - 1)
            except:
                planck_val = 0
            planck_vals.append(planck_val)
        integral_value = np.trapz(disk_radial_bins*planck_vals, disk_radial_bins)
        # integral_value = 0.5*(disk_outer_edge**2 - disk_inner_edge**2)*planck_val
        luminosity_val = 16.0*np.pi**2*nu**3*Consts.h_cgs/Consts.c_in_cgs**2*integral_value
        disk_function.append(luminosity_val)
    disk_function = np.array(disk_function)
    print("Eddington Luminosity: {:.2e}".format(L_edd))
    print("Luminosity: {:.2e}".format(luminosity))
    print("Integrated disk luminosity: {:.2e}".format(np.trapz(disk_function, spectral_bins)))


    def nuToKev(nu):
        return 4.1e-18 * nu
    def kevToNu(keV):
        return keV/(4.1e-18)

    # float_str = "{:.2g}".format(disk_temp_over_r.max())
    # base, exponent = float_str.split("e")
    # disk_temp_str = r"{0}\times 10^{{{1}}}".format(base, int(exponent))
    # label_str = r"$\nu L_{{\nu, {{\rm disk}}}}(T=)" + disk_temp_str + "K)$"
    label_str = r"$\nu L_{{\nu, {{\rm disk}}}}(T(r))$"
    total_spectrum = Lnu + Lnu_thermal + disk_function
    # max_val = np.max([np.max(Lnu*spectral_bins), np.max(disk_function*spectral_bins)])
    # max_val = np.max([np.max(Lnu*spectral_bins), np.max(Lnu_thermal*spectral_bins), np.max(disk_function*spectral_bins)])
    max_val = (total_spectrum*spectral_bins).max()
    plt.figure()
    plt.plot(spectral_bins, Lnu*spectral_bins,
             label=r'$\nu L_{\nu, {\rm PL}}$', color="C0", ls="-")
    plt.plot(spectral_bins, Lnu_thermal*spectral_bins,
             label=r'$\nu L_{\nu, {\rm thermal}}$', ls=':', color="C2")
    plt.plot(spectral_bins, disk_function*spectral_bins,
             label=label_str, ls='--', color="C1")
    plt.plot(spectral_bins, total_spectrum*spectral_bins, color='black', label='Total', lw=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r"$\nu/{\rm Hz}$ ")
    title_str = r"$F_{{\theta\phi}}={:.2f}$, $\gamma_2=$".format(bg.Fthetaphi) + gamma2_str + ", " + PL_titstr
    if adjusted_r_range.shape != r_range.shape:
        title_str += "\n Note: spectrum does not include all radii."
    title_str += "\n" + "Max thermal plunging electron cooling rate = {:.3f}*nonthermal cooling".format(cooling_ratio)
    plt.title(title_str)
    plt.gca().tick_params(right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    secax = plt.gca().secondary_xaxis('top', functions=(nuToKev, kevToNu))
    secax.tick_params(top=True, direction='in', which='both')
    secax.set_xlabel(r'$\nu/{\rm keV}$')
    plt.ylim([1e34, max_val])
    # plt.ylim([1e18, max_val])
    plt.xlim([spectral_bins.min(), spectral_bins.max()])
    plt.legend()
    plt.tight_layout()
    # plt.savefig(figure_dir2 + "spectrum_at_r={:.2f}rg.png".format(adjusted_r_range[j]))
    plt.savefig(figure_dir2 + "spectrum_added_over_r.png")


def print_consistency_check():
    # Infall time
    tinfall = bg.t_infall
    tinfall_s = tinfall * bg.get_tg_in_s(M_in_Msun)

    j = 0
    # Cooling time
    tcool = adjusted_cooling_times[j]
    gamma1_eq = gamma1_over_r[j]
    gamma2_eq = adjusted_gamma2[j]
    thetaj = Consts.kB_cgs*temperature_over_r[j]/Consts.restmass_cgs
    norm_eq = solve_for_PL_norm(gamma1_eq, thetaj, adjusted_number_density_over_r[j], adjusted_PL_index[j])
    if tsync_over_r[j] < tIC_over_r[j]:
        cooling_rate_eq = calculate_sync_cooling_rate(norm_eq, gamma1_eq, gamma2_eq, j)
    else:
        cooling_rate_eq = calculate_IC_cooling_rate(norm_eq, gamma1_eq, gamma2_eq, j)

    # Electron-electron Collision time
    nu0const = Consts.nu0const1 * Consts.coulomb_log * adjusted_number_density_over_r[j]
    kinetic_ratio = (gamma1_eq - 1)/thetaj
    integral_path = bg.path_to_reduced_data + "tabulated_integral.txt"
    tabulated_integral = np.loadtxt(integral_path)
    x_index = (np.abs(kinetic_ratio - tabulated_integral[0, :])).argmin()
    psi_value = tabulated_integral[1, x_index]
    tcoll = (gamma1_eq - 1.)**1.5/(2.*nu0const*psi_value)

    # To see if electrons and protons can exchange energy, we'd like to calculate the
    # THERMALIZATION time scale between two maxwellians with different temperatures:
    # the protons with equipartition temperature and the electrons with the calculated
    # temperature that sets cooling and heating equal.
    ion_temperature = (UB_over_r[j])/number_density_over_r[j]/Consts.kB_cgs
    nu_thermalize = 1.8e-19*np.sqrt(Consts.mp_in_g*Consts.me_in_g)*number_density_over_r[j]*Consts.coulomb_log/(Consts.mp_in_g*ion_temperature+Consts.me_in_g*temperature_over_r[j])**1.5
    Tfinal_isotropize = ion_temperature + (temperature_over_r[j] - ion_temperature)*np.exp(-nu_thermalize*tinfall_s)
    T_fractional_change = (Tfinal_isotropize - temperature_over_r[j])/temperature_over_r[j]
    print("Dynamical background has an infall time of {:.1e} rg/c, or {:.1e}s".format(tinfall, tinfall_s))
    print("At r={:.2f}rg, T={:.2e} K, theta={:.1e}, gamma1 = {:.2f}:".format(adjusted_r_range[j], temperature_over_r[j], thetaj, gamma1_eq))
    print("  t_cool(gamma1) = {:.2e}s = {:.2e} tinfall".format(tcool/gamma1_eq, tcool/gamma1_eq/tinfall_s))
    print("  t_coll(gamma1) = {:.2e}s = {:.2e} tinfall".format(tcoll, tcoll/tinfall_s))
    print("  t_isotropize = {:.2e}s = {:.2e} tinfall".format(1/nu_thermalize, 1/nu_thermalize/tinfall_s))
    print("T will increase to {:.2e} K ({:.3f} %) over the infall time due to thermalization with ions".format(Tfinal_isotropize, T_fractional_change*100))
    print("Equipartition ion temperature: {:.2e} K".format(ion_temperature))
    if not np.allclose(heating_rate, cooling_rate_eq):
        print("--------------------------------------------------")
        print("ERROR!!!!!!!!!!!!!!!!!!!!!")
        print("Heating is not equal to cooling, i.e. system did NOT equilibrate!")
        print("--------------------------------------------------")
    print("Cooling rate = {:.2e}".format(cooling_rate_eq))
    print("Heating rate = {:.2e}".format(heating_rate))
    print("Normalization = {:.2e}".format(norm_eq))
    print("Number density = {:.2e}".format(number_density_over_r[j]))

def plot_temperature_dependent_quantities():
    plt.figure()
    plt.plot(adjusted_r_range, temperature_over_r)
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.ylabel(r'$T_{eq}$ K')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir2 + "equilibrium_temperature_over_r.png")
    plt.close()

    plt.figure()
    plt.plot(adjusted_r_range, gamma1_over_r)
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.ylabel(r'$\gamma_1$')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir2 + "gamma1_over_r.png")
    plt.close()

    # Calculate importance of Compton cooling
    # Compton y parameter (assuming non-relativistic thermal electrons)
    theta_e = Consts.kB_cgs * temperature_over_r/Consts.restmass_cgs
    compton_y = 4 * theta_e * np.max([tau_es, tau_es**2.0])
    plt.figure()
    plt.plot(adjusted_r_range, compton_y)
    plt.xlabel(r'$r/r_G$')
    plt.ylabel("Compton $y$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.tight_layout()
    plt.savefig(figure_dir2 + "compton_y_over_r.png")

    # Calculate importance of inverse Compton
    # RL Eq. 7.65b
    xcoh = 2.4e17 * np.sqrt(Consts.me_in_g*adjusted_number_density_over_r)*(temperature_over_r)**(-9.0/4.0)
    plt.figure()
    plt.plot(adjusted_r_range, xcoh)
    plt.xlabel(r'$r/r_G$')
    plt.ylabel(r"$x_{\rm coh}$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.tight_layout()
    plt.savefig(figure_dir2 + "xcoh_over_r.png")

    # Calculate distribution function at inner/outer radii
    gamma_min = 1.0
    gamma_max = adjusted_gamma2.max()
    gamma_bins = np.logspace(np.log10(gamma_min), np.log10(gamma_max), 1000)
    # Outermost radius
    T0 = temperature_over_r[0]
    n0 = adjusted_number_density_over_r[0]
    theta0 = Consts.kB_cgs*T0/Consts.restmass_cgs
    fMJ0 = maxwell_juettner_over_gamma(gamma_bins, n0, theta0)
    gamma20 = adjusted_gamma2[0]
    gamma10 = gamma1_over_r[0]
    p0 = adjusted_PL_index[0]
    norm0 = solve_for_PL_norm(gamma10, theta0, n0, p0)
    ind1 = (np.abs(gamma_bins - gamma10)).argmin()
    ind2 = (np.abs(gamma_bins - gamma20)).argmin()
    PL0 = norm0 * n0 * gamma_bins**(-p0)
    PL0[:ind1] = 0
    PL0[ind2:] = 0
    # Innermost radius
    Ti = temperature_over_r[-1]
    ni = adjusted_number_density_over_r[-1]
    thetai = Consts.kB_cgs*Ti/Consts.restmass_cgs
    fMJi = maxwell_juettner_over_gamma(gamma_bins, ni, thetai)
    gamma2i = adjusted_gamma2[-1]
    gamma1i = gamma1_over_r[-1]
    pi = adjusted_PL_index[-1]
    normi = solve_for_PL_norm(gamma1i, thetai, ni, pi)
    ind1 = (np.abs(gamma_bins - gamma1i)).argmin()
    ind2 = (np.abs(gamma_bins - gamma2i)).argmin()
    PLi = normi * ni * gamma_bins**(-pi)
    PLi[:ind1] = 0
    PLi[ind2:] = 0
    max_val = np.max([fMJ0.max(), PL0.max(), fMJi.max(), PLi.max()])
    color_legend_elements = [Line2D([0], [0], color='C0', label='Power-law component'),
                       Line2D([0], [0], color='C1', label='Thermal component')]
    ls_legend_elements = [Line2D([0], [0], ls='-', color='black'),
                          Line2D([0], [0], ls='--', color='black')]
    plt.figure()
    plt.plot(gamma_bins, gamma_bins*fMJ0, color='C1', ls='-', label="Thermal component at $r={:.2f}r_g$".format(adjusted_r_range[0]))
    plt.plot(gamma_bins, gamma_bins*PL0, color='C0', ls='-', label="Power-law component at $r={:.2f}r_g$".format(adjusted_r_range[0]))
    # plt.plot(gamma_bins, PL0+fMJ0, color='black', ls=":", label="Total")
    # plt.plot(gamma_bins, fMJi, color='C1', ls='-', label="Thermal component at $r={:.2f}r_g$".format(adjusted_r_range[-1]))
    # plt.plot(gamma_bins, PLi, color='C0', ls='-', label="Power-law component at $r={:.2f}r_g$".format(adjusted_r_range[-1]))
    # plt.plot(gamma_bins, PLi+fMJi, color='black', label="Total")
    plt.xscale('log')
    plt.yscale('log')
    # plt.ylim([1, max_val*1.1])
    plt.ylim([1, 1e20])
    plt.xlabel(r'$\gamma$')
    plt.ylabel(r"$\gamma f(\gamma)$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.legend()
    # plt.gca().legend(handles=color_legend_elements, loc='upper right')
    # leg = Legend(plt.gca(), ls_legend_elements, ['$r={:.2f}r_g$'.format(adjusted_r_range[0]), '$r={:.2f}r_g$'.format(adjusted_r_range[-1])], loc='lower right')
    # plt.gca().add_artist(leg)
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.tight_layout()
    plt.savefig(figure_dir2 + "electron_distribtion_function.png")



def plot_prescription_quantities():
    plt.figure()
    plt.plot(r_range, PL_index)
    plt.ylabel(r"Power-law index $p$")
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir2 + "power_law_index_over_r.png")
    plt.close()

    plt.figure()
    plt.plot(r_range, gamma2_vals)
    plt.ylabel(r"High-energy cut-off $\gamma_2$")
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir2 + "high_energy_cutoff_over_r.png")
    plt.close()


def plot_gammie_quantities():
    plt.figure()
    plt.plot(r_range, ratio_over_r)
    plt.ylabel(r'$U_B/U_{ph}$')
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir + "synchrotron_IC_energy_ratio_over_r.png")
    plt.close()

    plt.figure()
    plt.plot(r_range, tIC_over_r/tsync_over_r)
    plt.xlabel(r'$r/r_G$')
    plt.ylabel(r'$t_{\rm IC}/t_{\rm sync}$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir + "synchrotron_IC_cooling_time_ratio_over_r.png")
    plt.close()

    plt.figure()
    plt.plot(r_range, magnetization_over_r)
    plt.ylabel(r"$\sigma_{\rm cold}$")
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir + "magnetization_over_r.png")
    plt.close()

    plt.figure()
    plt.plot(r_range, ion_magnetization_over_r)
    plt.ylabel(r"$\sigma_{i, \rm{cold}}$")
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir + "ion_magnetization_over_r.png")
    plt.close()

    plt.figure()
    plt.plot(r_range, beta_over_r)
    plt.ylabel(r"$\beta_i$")
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir + "beta_over_r.png")
    plt.close()

    plt.figure()
    plt.plot(r_range, gamma_rad_over_r)
    plt.ylabel(r"$\gamma_{\rm rad}$")
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir + "gamma_rad_over_r.png")
    plt.close()

    plt.figure()
    plt.plot(r_range, gamma_rad_over_r/magnetization_over_r)
    plt.ylabel(r"$\gamma_{\rm rad}/\sigma_{\rm cold}$")
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir + "gamma_rad_over_sigma_over_r.png")
    plt.close()

    plt.figure()
    plt.plot(r_range, cooling_times)
    plt.ylabel(r'$t_{\rm cool}$ s')
    plt.xlabel(r'$r/r_G$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir + "cooling_time_over_r.png")
    plt.close()

    plt.figure()
    plt.plot(r_range, tau_es)
    plt.ylabel(r'$\tau_{\rm es}$')
    plt.xlabel(r'$r/r_G$')
    plt.ylim(bottom=0)
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(title_str)
    plt.tight_layout()
    plt.savefig(figure_dir + "electron_scattering_depth_over_r.png")
    plt.close()


def solve_for_equilibrium(temperature, *args):
    number_density, cooling_time, PL_val, gamma2 = args
    theta_e = Consts.kB_cgs * temperature/Consts.restmass_cgs

    # Get lower bound based on cooling rates. Only collisional time
    # changes with temperature
    gamma1 = solve_for_gamma1(temperature, number_density, cooling_time)

    # Get normalization by requiring continuity of distribution function
    norm = solve_for_PL_norm(gamma1, theta_e, number_density, PL_val)

    if ratio_over_r[i] < 1:
        cooling_rate = calculate_IC_cooling_rate(norm, gamma1, gamma2, i)
    else:
        cooling_rate = calculate_sync_cooling_rate(norm, gamma1, gamma2, i)

    # if i==0:
        # print("Cooling/heating, gamma1, norm")
        # print(cooling_rate)
        # print(heating_rate)
        # print(temperature)

    # print("Q-: {:.2e}".format(cooling_rate[0]))
    # print("Q+: {:.2e}".format(heating_rate))
    return cooling_rate - heating_rate


def calculate_tsync_constant():
    # Full cooling time = calculate_tsync_constant/gamma
    # Note this is in the ultrarelativistic limit!
    # Rybicki & Lightman problem 6.1
    t_sync_constant = 5.1e8/(Br_over_r**2.0)
    return t_sync_constant

def calculate_tIC_constant():
    # Full cooling time = calculate_tIC_constant/gamma
    # Assume is IC cooling time, proportional using ratio of power loss
    return ratio_over_r*calculate_tsync_constant()

def collision_time_equation(gamma1, *values):
    temperature, number_density, cooling_time = values

    theta_e = Consts.kB_cgs * temperature/Consts.restmass_cgs
    nu0const = Consts.nu0const1 * Consts.coulomb_log * number_density

    # Tabulate needed integrals
    integral_path = bg.path_to_reduced_data + "tabulated_integral.txt"
    if not os.path.exists(integral_path):
        # Tabulate \psi(x) on NRL p. 31
        x_values = np.logspace(-5, 2, 10000)
        psi_values = []
        for x in x_values:
            psi_value = psi(x)
            psi_values.append(psi_value)
        tabulated_integral = np.vstack([x_values, psi_values])
        np.savetxt(integral_path, tabulated_integral)
        print("Tabulated psi(x).")
    else:
        tabulated_integral = np.loadtxt(integral_path)

    # Kinetic_ratio is x
    kinetic_ratio = (gamma1 - 1)/theta_e
    x_index = (np.abs(kinetic_ratio - tabulated_integral[0, :])).argmin()
    psi_value = tabulated_integral[1, x_index]

    # Full collision time = 1/(2*nu0const*psi(x))*(gamma-1)^1.5
    equation_to_solve = cooling_time/gamma1 - 1.0/(2*nu0const*psi_value)*(gamma1-1)**1.5

    # For the high-energy limit:
    # high_energy_const = 1.0e6/(7.7*number_density*Consts.coulomb_log)*(5.11e5)**1.5
    # equation_to_solve = cooling_time/gamma - high_energy_const*(gamma-1)**1.5

    return equation_to_solve

def solve_for_gamma1(temperature, number_density, cooling_time):
    if not isinstance(temperature, float):
        temperature = temperature[0]
    # Temperature changes only the collisional time.
    # For the general collision time, we need to solve an equation
    # involving psi(x), where x changes with temperature.
    theta_e = Consts.kB_cgs * temperature/Consts.restmass_cgs

    needed_values = (temperature, number_density, cooling_time)
    # Set gamma1 min to be the maximum value of the maxwell-juettner:
    # # Set gamma1 min to be the non-relativistic average gamma from theta,
    # # i.e. 1.5theta + 1
    gamma1_min = 1.5*theta_e + 1.0
    # gamma_solve = least_squares(collision_time_equation, gamma1_min, args=needed_values, bounds=(gamma1_min, np.inf))
    gamma_solve = least_squares(collision_time_equation, gamma1_min*1.5, args=needed_values, bounds=(gamma1_min, np.inf))
    gamma1 = gamma_solve.x[0]

    return gamma1

def solve_for_PL_norm(gamma1, theta, number_density, PL_index=3.0):
    fMJ_at_gamma1 = maxwell_juettner_at_gamma(gamma1, number_density, theta)
    normalization = fMJ_at_gamma1/number_density * gamma1**(PL_index)
    return normalization

def calculate_heating_rate(M_in_Msun, dissipation_efficiency, nonthermal_heating_fraction, eddington_luminosity):
    r1 = starting_r * bg.get_rg_in_cm(M_in_Msun)
    r2 = ending_r * bg.get_rg_in_cm(M_in_Msun)
    volume = 4.0*np.pi/3.0*np.abs(r1**3 - r2**3)
    total_energy_dissipated = dissipation_efficiency * eddington_luminosity
    heating_rate = nonthermal_heating_fraction * total_energy_dissipated / volume
    return heating_rate

def calculate_sync_cooling_rate(normalization, gamma1, gamma2, index):
    """
    NOTE: we cannot simply use Rybicki & Lightman Eq. 6.36 integrated over frequency and
    angle because that derivation assumes that the frequencies of interest are far away
    from the cutoff frequencies; that is not true for us looking at frequencies close to
    the critical synchrotron frequency.

    Instead of extending the limits of F(x) to 0 and infinity, we will instead
    integrate over frequency *first*, leading to the simple Larmor formula (RL Eq. 6.17b):
    Ptotal = \int dOmega domega dgamma f(gamma)* Power per electron
           = \int dOmega dgamma f(gamma) \int domega Power per electron
           = \int dOmega dgamma f(gamma) 2e^4 B^2 gamma^2 beta^2 sin^2alpha/(3 m^2 c^3)
    The integral over dOmega = sin\alpha d\phi d\alpha gives 16\pi/3.
    """
    p = PL_index[index]

    # Deal with singularity (a point, not asymptotic)
    if p == 3.0:
        integral1 = np.log10(gamma2/gamma1)
    else:
        integral1 = (gamma2**(3.0-p) - gamma1**(3.0-p))/(3.0 - p)

    if p == 1.0:
        integral2 = -1.0*np.log10(gamma2/gamma1)
    else:
        integral2 = -1.0*(gamma2**(1.0-p) - gamma1**(1.0-p))/(1.0 - p)

    constant = 16*np.pi*Consts.e_in_cgs**4*Br_over_r[index]**2/(9*Consts.me_in_g**2*Consts.c_in_cgs**3)*normalization*number_density_over_r[index]

    return constant*(integral1 + integral2)


def calculate_IC_cooling_rate(normalization, gamma1, gamma2, index):
    # Calculation in notebook. Assumes PIC = (Uph/UB) Psync

    power_loss_sync = calculate_sync_cooling_rate(normalization, gamma1, gamma2, index)
    power_loss_IC = power_loss_sync/ratio_over_r[index]
    return power_loss_IC


if __name__ == '__main__':
    kwargs = {}
    kwargs["Fthetaphi"] = 6.0
    # kwargs["Fthetaphi"] = 1.0
    bg = dynamical_background(**kwargs)
    accretion_efficiency = 1 + bg.FEval

    # --------------------------------------------------------------
    # Input parameters using rough estimates
    # --------------------------------------------------------------
    M_in_Msun = 10.0
    f_Ledd = 0.1
    f_Mdot = 0.1
    H_over_R = 1.0
    starting_r = 1.8 # in rg. Use to avoid density spike at r=1.9rg
    ending_r = bg.rEH # in rg. Use to avoid density weirdness at r=1.3rg
    nonthermal_heating_fraction = 0.5

    # PL_type and gamma2_type determine the prescription used to set
    # the power-law index and high-energy cutoff, respectively.
    # They can be set to constant or to use a PIC prescription.
    PL_type = "WUBCN18"
    # PL_type = "WPU19"
    # PL_type = "constant"
    gamma2_type = "fixed"
    gamma2_type = "4sigma"

    quick = True
    jump = 50

    # --------------------------------------------------------------
    # Cut arrays to preferred region only
    # --------------------------------------------------------------
    inner_index = (np.abs(starting_r - bg.r_range)).argmin()
    outer_index = (np.abs(ending_r - bg.r_range)).argmin()
    # outer_index = -1
    r_range = bg.r_range[inner_index:outer_index]
    Br_over_r = bg.get_Br_in_cgs(M_in_Msun, f_Ledd, f_Mdot, H_over_R)[inner_index:outer_index]
    mass_density_over_r = bg.get_mass_density_in_cgs(M_in_Msun, f_Ledd, f_Mdot, H_over_R)[inner_index:outer_index]
    Br_inner = Br_over_r[0]
    mass_density_inner = mass_density_over_r[0]
    number_density_over_r = mass_density_over_r / Consts.mp_in_g
    number_density_inner = number_density_over_r[0]
    UB_over_r = Br_over_r**2.0/(8.0*np.pi)
    UB_inner = UB_over_r[0]

    # --------------------------------------------------------------
    # Handle prescriptions for p and gamma2
    # --------------------------------------------------------------
    if PL_type == "constant":
        PL_index_constant = 2.9
        PL_filestr = "Constant{:.2f}".format(PL_index_constant)
        PL_titstr = "$p={:.2f}$".format(PL_index_constant)
    else:
        PL_filestr = PL_type
        PL_titstr = PL_type + " PL prescription"

    if gamma2_type == "fixed":
        gamma2_constant = 1000
        gamma2_str = "{:.2e}".format(gamma2_constant)
        gamma2_titstr = r"$\gamma_2={:.2e}$".format(gamma2_constant)
    else:
        gamma2_str = gamma2_type
        gamma2_titstr = r"$\gamma_2$ " + gamma2_str

    # --------------------------------------------------------------
    # Calculate some constants, like equilibrium ion temperature,
    # luminosity, heating_rate, and cooling times over radius.
    # --------------------------------------------------------------
    equipartition_T = UB_inner/number_density_inner/Consts.kB_cgs
    # Initial guess for electron temperature
    # Te_estimate = equipartition_T # I think 1e12 leads to huge unphysical jumps...
    Te_estimate = 1e9

    eddington_luminosity = get_eddington_lum(M_in_Msun)
    luminosity = f_Ledd * eddington_luminosity

    radial_size_cm = bg.rISCO * bg.get_rg_in_cm(M_in_Msun)
    Uph_cgs = luminosity / (4*np.pi*radial_size_cm**2.0*Consts.c_in_cgs)
    heating_rate = calculate_heating_rate(M_in_Msun, accretion_efficiency, nonthermal_heating_fraction, eddington_luminosity)

    ratio_over_r = UB_over_r/Uph_cgs
    tsync_over_r = calculate_tsync_constant()
    tIC_over_r = calculate_tIC_constant()

    # --------------------------------------------------------------
    # Plot some constant quantities
    # --------------------------------------------------------------
    figure_dir = bg.path_to_figures + "multizone_model/Fthetaphi{:.2f}/".format(bg.Fthetaphi)
    figure_dir2 = figure_dir + "cutoff" + gamma2_str + "_index" + PL_filestr + "/"
    if not os.path.exists(figure_dir2):
        os.makedirs(figure_dir2)
    title_str = r"$F_{{\theta\phi}}={:.2f}$, ".format(bg.Fthetaphi) + PL_titstr + ", " + gamma2_titstr

    # This definition of electron magnetization from Werner, Philippov, Uzdensky 2019 (cold)
    magnetization_over_r = Br_over_r**2.0/(4.0*np.pi*number_density_over_r*Consts.restmass_cgs)
    # This definition of magnetization from Werner, Uzdensky, Begelman + 2018
    ion_magnetization_over_r = Br_over_r**2.0/(4.0*np.pi*number_density_over_r*Consts.restmass_proton_cgs)
    # This beta parameter is ion only
    beta_over_r = 8.0*number_density_over_r*Consts.kB_cgs*equipartition_T/(Br_over_r**2.0)
    gamma_rad_over_r = np.sqrt(3*0.1*Consts.e_in_cgs*np.abs(Br_over_r)/(4.0*Consts.thomson_cross_section_cgs*Uph_cgs))
    cooling_times = np.minimum(tIC_over_r, tsync_over_r)
    # Electron scattering optical depth
    tau_es = number_density_over_r * Consts.thomson_cross_section_cgs * H_over_R*r_range*bg.get_rg_in_cm(M_in_Msun)

    # print("Saving prescription-independent figures in " + figure_dir)
    plot_gammie_quantities()

    # --------------------------------------------------------------
    # Plot some prescription-dependent quantities
    # --------------------------------------------------------------
    if PL_type == "WUBCN18":
        # Add 1 to account for cooling
        PL_index = 1.9 + 0.7/np.sqrt(ion_magnetization_over_r) + 1
    elif PL_type == "WPU19":
        radiaction = gamma_rad_over_r/magnetization_over_r
        PL_index = 1.6 * np.ones(r_range.shape)
        PL_index[radiaction <= 6] = 3.5
    elif PL_type == "constant":
        PL_index = PL_index_constant*np.ones(r_range.shape)
    else:
        print("ERROR: Unknown prescription " + PL_type + " for power-law index.")
        exit()

    if gamma2_type == "4sigma":
        gamma2_vals = 4*magnetization_over_r
    elif gamma2_type == "6sigma":
        gamma2_vals = 6*magnetization_over_r
    # elif gamma2_type == "WPU19":
        # gamma2_vals = 1
    elif gamma2_type == "fixed":
        gamma2_vals = gamma2_constant*np.ones(r_range.shape)
    else:
        print("ERROR: Unknown prescription " + gamma2_type + " for high-energy cutoff.")
        exit()
    plot_prescription_quantities()

    # --------------------------------------------------------------
    # Main loop: calculate equilibrium temperature at each radius
    # --------------------------------------------------------------
    temperature_path = bg.path_to_reduced_data + "temperature_over_r_Fthetaphi{:.2f}_cutoff".format(bg.Fthetaphi) + gamma2_str + "_index" + PL_filestr + ".p"
    gamma1_path = bg.path_to_reduced_data + "gamma1_over_r_Fthetaphi{:.2f}_cutoff".format(bg.Fthetaphi) + gamma2_str + "_index" + PL_filestr + ".p"
    if not os.path.exists(temperature_path):
        temperature_dictionary = {}
        gamma1_dictionary = {}
    else:
        print("Loading eq. temperature from pickle file " + temperature_path)
        with open(temperature_path, 'rb') as file:
            temperature_dictionary = pickle.load(file)
        with open(gamma1_path, 'rb') as file:
            gamma1_dictionary = pickle.load(file)

    for i, r in enumerate(r_range):
        if quick and not (i%jump == 0):
            continue
        if i in temperature_dictionary:
            # print("i={:d} already in dictionary. Skipping.".format(i))
            continue

        if ratio_over_r[i] < 1:
            print("Using IC cooling time for i={:d}.".format(i))
            cooling_time = tIC_over_r[i]
        else:
            print("Using synchrotron cooling time for i={:d}.".format(i))
            cooling_time = tsync_over_r[i]

        gamma2_val = gamma2_vals[i]
        arguments = (number_density_over_r[i], cooling_time, PL_index[i], gamma2_val)

        # eq_temperature = fsolve(solve_for_equilibrium, Te_estimate, args=arguments, xtol=1e-3, factor=0.1)[0]
        temp_solve = least_squares(solve_for_equilibrium, Te_estimate, args=arguments, bounds=(1e7, np.inf))
        eq_temperature = temp_solve.x[0]
        temperature_dictionary[i] = eq_temperature
        with open(temperature_path, 'wb') as fh:
            pickle.dump(temperature_dictionary, fh)
        eq_gamma1 = solve_for_gamma1(eq_temperature, number_density_over_r[i], cooling_time)
        gamma1_dictionary[i] = eq_gamma1
        with open(gamma1_path, 'wb') as fh:
            pickle.dump(gamma1_dictionary, fh)

    # Cut down magnetic field, etc. according to indices in temperature dict
    indices = np.array(list(temperature_dictionary.keys()))
    # Just make sure the extraction from dictionary to array is in order.
    sorted_indices = indices[np.argsort(indices)]
    temperature_over_r = list(temperature_dictionary.values())
    temperature_over_r = np.array(temperature_over_r)[np.argsort(indices)]
    gamma1_inds = np.array(list(gamma1_dictionary.keys()))
    gamma1_vals = list(gamma1_dictionary.values())
    gamma1_over_r = np.array(gamma1_vals)[np.argsort(gamma1_inds)]

    adjusted_r_range = r_range[sorted_indices]
    adjusted_number_density_over_r = number_density_over_r[sorted_indices]
    adjusted_Br_over_r = Br_over_r[sorted_indices]
    adjusted_magnetization_over_r = magnetization_over_r[sorted_indices]
    adjusted_cooling_times = cooling_times[sorted_indices]
    adjusted_PL_index = PL_index[sorted_indices]
    adjusted_gamma2 = gamma2_vals[sorted_indices]
    adjusted_tau_es = tau_es[sorted_indices]

    print("Saving temperature-dependent quantities in " + figure_dir2)
    plot_temperature_dependent_quantities()

    # --------------------------------------------------------------
    # Consistency check: calculate relevant time scales
    # --------------------------------------------------------------
    print_consistency_check()

    # --------------------------------------------------------------
    # Calculate spectra
    # --------------------------------------------------------------
    calculate_spectra()

    # thermal_heating_rate =  (1.0 - nonthermal_heating_fraction)*total_energy_dissipated/volume
