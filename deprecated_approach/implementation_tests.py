import sys
import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from dynamical_background_gammie1999 import *
import scipy.integrate as integrate

# --------------------------------------------------------------
# TODO:
# - Check convergence of thermalization against analytic solution
# --------------------------------------------------------------

M_in_Msun = 10.0
H_over_R = 0.01
kwargs = {}
kwargs["M_in_Msun"] = M_in_Msun
kwargs["H_over_R"] = H_over_R
bg = dynamical_background(**kwargs)

# Estimate parameters from Zhu + 2014
# TODO: change to be equipartition T for Gammie model
Te_in_K = 10.0**9 # electron temperature in eV
Te_in_eV = Te_in_K/(1.1*10**4) # electron temperature in eV
Ti_in_eV = Te_in_eV  # ion temperature in eV
theta_e = Consts.kB_cgs * Te_in_K / (Consts.me_in_g * Consts.c_in_cgs**2.0)

radial_size = bg.rISCO*bg.rg_in_cm
compactness = (bg.L_edd * 0.1) * Consts.thomson_cross_section_cgs/(H_over_R*radial_size * Consts.me_in_g * Consts.c_in_cgs**3.0)
print(bg.L_edd)
print(4*np.pi/3.0*(bg.rISCO*bg.rg_in_cm)**3.0)
exit()

# Convert gammie density to cgs
mass_density_over_r = bg.get_mass_density_in_cgs()  # g/cm^3
number_density = mass_density_over_r/(Consts.mp_in_g + Consts.me_in_g)  # 1/cm^3

coulomb_log = get_ee_coulomb_log(Te_in_eV, number_density)
thomson_time = 1/(number_density * Consts.c_in_cgs * Consts.thomson_cross_section_cgs)
thomson_depth = radial_size * Consts.thomson_cross_section_cgs * number_density
lambda_param = compactness * (1 + thomson_depth)/thomson_depth/coulomb_log * (1.5*theta_e)**1.5*(1 + (6.0*theta_e)**1.5)
thermal_velocity = np.sqrt(2.0*Consts.kB_cgs * Te_in_K/Consts.mp_in_g)
thermal_beta = thermal_velocity/Consts.c_in_cgs
thermal_gamma = 1.0/np.sqrt(1 - thermal_beta**2.0)
thermal_velocity = np.sqrt(2.0*Consts.kB_cgs * Te_in_K/Consts.me_in_g)
thermal_beta = thermal_velocity/Consts.c_in_cgs
thermal_gamma = 1.0/np.sqrt(1 - thermal_beta**2.0)
print(coulomb_log[0])
print(theta_e)
print(compactness)
print(lambda_param[0])
print(thermal_beta)
print(thermal_gamma)
print("Min. value of \lambda: {:.2f}".format(lambda_param.min()))
print("Max. value of \lambda: {:.2f}".format(lambda_param.max()))
magnetic_field = bg.get_Br_in_cgs()
magnetic_compactness = magnetic_field**2.0/(8.0*np.pi) * radial_size * Consts.thomson_cross_section_cgs/(Consts.me_in_g * Consts.c_in_cgs**2.0)
lambda_m = magnetic_compactness/(thomson_depth * coulomb_log) * (1.5*theta_e)**1.5 * (1 + (6.0*theta_e)**1.5)
print("Min. value of \lambda_m: {:.2f}".format(lambda_m.min()))
print("Max. value of \lambda_m: {:.2f}".format(lambda_m.max()))

print("compactness: {:.2f}".format(compactness))
print("Magnetic compactness: {:.2f} to {:.2f}".format(magnetic_compactness.min(), magnetic_compactness.max()))

figure_directory = bg.path_to_figures + "implementation_tests/"
# plt.figure()
# plt.plot(bg.r_range, lambda_m)
# plt.savefig(figure_directory + "test.png")
# plt.show()

# Compare the (proper) infall time and the collision times
t_eicoll_in_s = get_ei_collision_time(Te_in_eV, Ti_in_eV, number_density)
t_eicoll_in_tg = t_eicoll_in_s / bg.tg_in_s
t_eecoll_in_s = get_ee_collision_time(Te_in_eV, number_density)
t_eecoll_in_tg = get_ee_collision_time(Te_in_eV, number_density)/bg.tg_in_s
t_iicoll_in_s = 42*t_eecoll_in_s
t_infall_in_s = bg.t_infall * bg.tg_in_s

def convert_to_tinfall(time_in_s):
    return time_in_s / t_infall_in_s
def convert_from_tinfall(time_in_tf):
    return time_in_tf * t_infall_in_s

if isinstance(number_density, float):
    print("density is a float")
    print(r'Infall time: {:.2f} t_g'.format(bg.t_infall))
    print(r'Electron-ion collision time: {:.2f} t_g'.format(t_eicoll_in_tg))
    print(r'Electron-electron collision time: {:.2f} t_g'.format(t_eecoll_in_tg))
else:
    plt.figure()
    secax = plt.gca().secondary_yaxis('right', functions=(convert_to_tinfall, convert_from_tinfall))
    secax.set_ylabel(r'$\tau/t_{\rm infall}$')
    plt.plot(bg.r_range, t_eecoll_in_s, label=r"$\tau_{ee}$")
    plt.plot(bg.r_range, t_eicoll_in_s, label=r"$\tau_{ep}$")
    plt.plot(bg.r_range, t_iicoll_in_s, label=r"$\tau_{pp}$")
    plt.legend(frameon=False)
    plt.ylabel(r'Collision time $\tau~[s]$')
    title_str = r'Scaled using $\dot M={:.2f}\dot M_{{\rm Edd}}$ for $M=${:.1E}$M_\odot$'.format(bg.fraction_of_Mdot, bg.M_in_Msun)
    title_str += r", $T=${:.1E} eV".format(Te_in_eV)
    plt.title(title_str)
    plt.xlabel(r'$r/r_g$')
    plt.gca().tick_params(top=True, right=False, direction='in', which='both')
    secax.tick_params(top=False, right=True, direction='in', which='both')
    plt.yscale('log')
    # plt.gca().grid(color='.9', ls='--')
    plt.tight_layout()
    plt.savefig(figure_directory + "compare_tcollisions.png")
    # plt.show()

N_interp = 1000
n_cgs = 10**-5/Consts.mp_in_g
power_law_index = 3.0
p = power_law_index
gamma_min = 1.01
gamma_max = 10**3
t_collision = 0.01

kwargs = {}
# kwargs["gamma_max"] = 10**8
kwargs["t_collision"] = t_collision
pl = plasma(**kwargs)
rest_mass = pl.particle_mass_cgs * Consts.c_in_cgs**2.0
gamma_range = pl.gamma_range

powerlaw_f = power_law(pl, n_cgs, gamma_min, gamma_max, power_law_index)
density = get_number_density(pl, powerlaw_f)
avg_E = get_average_energy(pl, powerlaw_f)
maxwellian_f = maxwellian(pl, n_cgs, avg_E)
density_max = get_number_density(pl, maxwellian_f)
avg_E_max = get_average_energy(pl, maxwellian_f)

theory_E = (1-power_law_index)*rest_mass*(gamma_max**(2-p) - gamma_min**(2-p))/(gamma_max**(1-p) - gamma_min**(1-p))
print("Power-law density integrates to {:e}, should get {:e}".format(density, n_cgs))
print("Power-law energy integrates to {:e}, should get {:e}".format(avg_E, theory_E))
print("Maxwellian density integrates to {:e}, should get {:e}".format(density_max, n_cgs))
print("Maxwellian energy integrates to {:e}, should get {:e}".format(avg_E_max, theory_E))

plt.close()
plt.figure()
plt.plot(gamma_range, powerlaw_f)
plt.plot(gamma_range, maxwellian_f)
plt.gca().axvline([avg_E/rest_mass], color='black', ls='--')
plt.xscale('log')
plt.yscale('log')
plt.xlabel(r'$\gamma$')
plt.ylabel(r'$f(\gamma)$ [particles/cm3/erg]')
plt.ylim([1e0, 1e26])
plt.xlim([0.5, 5e3])
# plt.show()
# plt.figure()
# plt.plot(bg.r_range, pl.t_collision*bg.tg_in_s)
# plt.xlabel(r'$r/r_g$')
# plt.ylabel(r'$t_{\rm collision}/t_g$')
# plt.yscale('log')
# plt.show()

# --------------------------------------------------------------
# Test thermalization of power-law initial distribution function
# --------------------------------------------------------------
print("\n--------------------------------------------------------------------")
print("Testing thermalization of power-law initial distribution function...")
initial_f = powerlaw_f
if isinstance(t_collision, float):
    tcoll_str = "{:.2f}".format(t_collision)
else:
    tcoll_str = "Evolving"

kwargs["injection_type"] = "None"

allf_path = bg.path_to_reduced_data + "powerlaw_solve.txt"
if not os.path.exists(allf_path) or True:
    print("Evolving distribution function...")
    all_f = evolve_distribution_function(initial_f, bg, pl, **kwargs)
    np.savetxt(allf_path, all_f)
else:
    print("Loading distribution function from file...")
    all_f = np.loadtxt(allf_path)
figure_directory = bg.path_to_figures
figure_directory += "implementation_tests/"
figure_directory += "thermalize_initial_power_law/tcollision" + tcoll_str + "/"

dt = bg.t_infall/N_interp
time_range = np.arange(0, bg.t_infall, dt)

if not os.path.exists(figure_directory):
    os.makedirs(figure_directory)

t = 0
print("Making plots...")
while t < 90: # not np.allclose(all_f[t, 1:]/maxwellian_f[1:], 1, 1e-2):
    # for t in np.arange(all_f.shape[0]):
    time = time_range[t]
    figure_name = "t{:03d}".format(t)

    n_t = get_number_density(pl, all_f[t, :])
    avg_E_t = get_average_energy(pl, all_f[t, :])
    maxwellian_t = maxwellian(pl, n_t, avg_E_t)

    plt.figure()
    plt.plot(pl.gamma_range, all_f[t, :]/initial_f.max(), label=r"$f(t)$")
    plt.plot(pl.gamma_range, maxwellian_t/initial_f.max(), label=r"$f_M(t)$")
    plt.gca().axvline([avg_E/rest_mass], color='black', ls='--', label="Eavg initial")
    plt.gca().axvline([avg_E_t/rest_mass], color='blue', ls=':', label="Eavg current")
    # plt.plot(pl.gamma_range, maxwellian_f/initial_f.max(), 'k--', label="Maxwellian")
    plt.xlabel(r'$\gamma$')
    plt.ylabel(r'$f(E)$ (arb. units)')
    plt.gca().grid(color='.9', ls='--')
    # plt.gca().grid(which='minor', color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    if isinstance(t_collision, float):
        t_collision_tg = t_collision/bg.tg_in_s
        if t == 1:
            print(t_collision)
            print(bg.tg_in_s)
            print(t_collision_tg)
        title_str = r'$t={:.3f}~t_f={:.3e}~t_c$'.format(time/bg.t_infall, time/t_collision_tg)
    else:
        title_str = r'$t={:.3f}~t_f$'.format(time/bg.t_infall)

    plt.legend()

    plt.title(title_str)
    plt.yscale('log')
    plt.xscale('log')
    plt.ylim([1e-7, 1])
    plt.xlim([0.9, 1e2])

    plt.savefig(figure_directory + figure_name + '.png', bbox_inches='tight')
    plt.close()
    t += 1
print("Done. {:d} timesteps plotted.".format(t))
exit()


# --------------------------------------------------------------
# Test thermalization of power-law initial distribution function
# --------------------------------------------------------------
print("\n--------------------------------------------------------------------")
print("Testing thermalization of power-law initial distribution function...")
initial_f = powerlaw_f

# must delete powerlaw_solve.txt if changed!!
t_collision = pl.t_collision * bg.tg_in_s
# t_collision = 0.1 # in tg
# t_collision = 0.1*np.ones(N_interp)

kwargs["t_collision"] = t_collision
kwargs["injection_type"] = "None"

allf_path = bg.path_to_reduced_data + "initial_powerlaw_solve.txt"
if not os.path.exists(allf_path) or False:
    print("Evolving distribution function...")
    all_f = evolve_distribution_function(initial_f, bg, pl, **kwargs)
    np.savetxt(allf_path, all_f)
else:
    print("Loading distribution function from file...")
    all_f = np.loadtxt(allf_path)

if isinstance(t_collision, float):
    tcoll_str = "{:.2f}".format(t_collision)
else:
    tcoll_str = "Evolving"
figure_directory = bg.path_to_figures
figure_directory += "implementation_tests/"
figure_directory += "thermalize_initial_power_law/tcollision" + tcoll_str + "/"
dt = bg.t_infall/N_interp
time_range = np.arange(0, bg.t_infall, dt)

if not os.path.exists(figure_directory):
    os.makedirs(figure_directory)

t = 0
print("Making plots...")
while not np.allclose(all_f[t, 1:]/maxwellian_f[1:], 1, 1e-2):
    # for t in np.arange(all_f.shape[0]):
    time = time_range[t]
    figure_name = "t{:03d}".format(t)

    plt.figure()
    plt.plot(velocity_range/Consts.c_in_cgs, all_f[t, :]/initial_f.max())
    plt.plot(velocity_range/Consts.c_in_cgs, maxwellian_f/initial_f.max(), 'k--')
    plt.xlabel(r'$v/c$')
    plt.ylabel(r'$f(v)$ (arb. units)')
    plt.ylim([0, 1.1])
    plt.gca().grid(color='.9', ls='--')
    # plt.gca().grid(which='minor', color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    if isinstance(t_collision, float):
        title_str = r"$t_c = {:.2f}~t_f$".format(t_collision) + "\n"
        title_str += r'$t={:.3f}~t_f={:.3f}~t_c$'.format(time/bg.t_infall, time/bg.t_infall/t_collision)
    else:
        title_str = r"$t_c = {:.2f}~t_f$".format(t_collision[t]) + "\n"
        title_str += r'$t={:.3f}~t_f$'.format(time/bg.t_infall)

    plt.title(title_str)
    plt.yscale('log')
    plt.ylim([1e-4, 1])

    plt.savefig(figure_directory + figure_name + '.png', bbox_inches='tight')
    plt.close()
    t += 1
print("Done. {:d} timesteps plotted.".format(t))

# --------------------------------------------------------------
# Test thermalization of delta-function particle injection
# --------------------------------------------------------------
print("\n-----------------------------------------------------")
print("Testing thermalization of delta_function injection...")
# must delete powerlaw_solve.txt if changed!!
t_collision = 0.1 # in tg
kwargs["t_collision"] = t_collision
kwargs["injection_type"] = "delta_function"
initial_f = maxwellian_f

allf_path = bg.path_to_reduced_data + "delta_function_solve.txt"
if not os.path.exists(allf_path) or False:
    print("Evolving distribution function...")
    all_f = evolve_distribution_function(initial_f, bg, pl, **kwargs)
    np.savetxt(allf_path, all_f)
else:
    print("Loading distribution function from file...")
    all_f = np.loadtxt(allf_path)

if isinstance(t_collision, float):
    tcoll_str = "{:.2f}".format(t_collision)
else:
    tcoll_str = "Evolving"
figure_directory = bg.path_to_figures
figure_directory += "implementation_tests/"
figure_directory += "delta_function_injection/tcollision" + tcoll_str + "/"
dt = bg.t_infall/N_interp
time_range = np.arange(0, bg.t_infall, dt)

if not os.path.exists(figure_directory):
    os.makedirs(figure_directory)

t = 0
print("Making plots...")
while not np.allclose(all_f[t+2, -1]/all_f[1, -1], 0.01, 1e-1):
    # for t in np.arange(all_f.shape[0]):
    time = time_range[t]
    figure_name = "t{:03d}".format(t)

    plt.figure()
    plt.plot(velocity_range/Consts.c_in_cgs, all_f[t, :]/initial_f.max())
    plt.plot(velocity_range/Consts.c_in_cgs, maxwellian_f/initial_f.max(), 'k--')
    plt.xlabel(r'$v/c$')
    plt.ylabel(r'$f(v)$ (arb. units)')
    plt.ylim([0, 1.1])
    plt.gca().grid(color='.9', ls='--')
    # plt.gca().grid(which='minor', color='.9', ls='--')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    if isinstance(t_collision, float):
        title_str = r"$t_c={:.2f}~t_f$".format(t_collision) + "\n"
        title_str += r'$t={:.3f}~t_f={:.3f}~t_c$'.format(time/bg.t_infall, time/bg.t_infall/t_collision)
    else:
        title_str = r"$t_c={:.2f}~t_f$".format(t_collision[t])
        title_str += r'$t={:.3f}~t_f={:.3f}~t_c$'.format(time/bg.t_infall)
    plt.title(title_str)

    # plt.show()
    plt.savefig(figure_directory + figure_name + '.png', bbox_inches='tight')
    plt.close()

    t += 1

print("Done. {:d} timesteps plotted.".format(t))
