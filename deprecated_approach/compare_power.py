import sys
import numpy as np
import matplotlib
import pickle
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from dynamical_background_gammie1999 import *
from equilibrium_finder import *
from scipy import special

small_size = 22
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)
matplotlib.rc('text', usetex=True)

if __name__ == '__main__':
    overwrite = False
    # decrease_by_tau = True
    decrease_by_tau = False
    inc_angle_deg = 60
    # inc_angle_deg = None
    Fthetaphi_values = [6.0, 4.0, 2.0] # 1.0 down is wrong??
    # Fthetaphi_values = [6.0, 4.0, 2.0, 1.0, 0.6]
    # Fthetaphi_values = [6.0, 5.75, 5.5, 5.25, 5.0, 4.75, 4.5, 4.25, 4.0, 3.75, 3.5, 3.25, 3.0, 2.75, 2.5, 2.0, 1.75, 1.5, 1.25, 1.0, 0.6]
    spin_values = [0.6, 0.7, 0.8, 0.9, 0.95]
    figure_dir = "/mnt/c/Users/liaha/research/projects/summer2021/figures/multizone_model/compare/"
    figure_dir = "/mnt/c/Users/liaha/research/projects/summer2021/figures/multizone_model/compare/Qtest1/"
    if not os.path.exists(figure_dir):
        os.makedirs(figure_dir)

    redshift = inc_angle_deg is not None
    powerlaw = []
    thermal = []
    total_power = []
    plot_Fthetaphi = []
    fractional_error = []
    for Fthetaphi in Fthetaphi_values:
        for spin in spin_values:
            print("---------------------------------------")
            print("    Fthetaphi={:.2f}".format(Fthetaphi))
            print("    a={:.2f}".format(spin))
            kwargs = {}
            kwargs["a"] = spin
            kwargs["Fthetaphi"] = Fthetaphi
            kwargs["jump"] = 1

            bg = dynamical_background(**kwargs)

            kwargs["PL_type"] = "WUBCN18"
            kwargs["gamma2_type"] = "4sigma"

            equilibrium = equilibrium_finder(bg, **kwargs)
            spectrum_dict = equilibrium.retrieve_spectra(quiet=True, decrease_by_tau=decrease_by_tau, overwrite=overwrite, inc_angle_deg=inc_angle_deg)

            powerlaw.append(spectrum_dict["power_in_powerlaw"])
            thermal.append(spectrum_dict["power_in_thermal_pr"])
            if not redshift:
                fractional_error.append(spectrum_dict["fractional_error"])
            plot_Fthetaphi.append(Fthetaphi)
            try:
                total_power.append(equilibrium.volume_heating_rate*equilibrium.emitting_volume)
            except:
                total_power.append(equilibrium.heating_rate*equilibrium.emitting_volume)

    # Need to rewrite figs.
    powerlaw = np.array(powerlaw)
    thermal = np.array(thermal)
    total_power = np.array(total_power)
    plt.figure()
    plt.plot(plot_Fthetaphi, powerlaw/thermal, ls='None', marker='o')
    plt.xlabel(r'$F_{\theta\phi}$')
    plt.ylabel(r"$P_{\rm PL}/P_{\rm thermal}$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    # plt.yscale('log')
    # plt.ylim([None, None])
    plt.ylim([0, None])
    plt.gca().grid(color='.9', ls='--')
    if redshift:
        title_str = r"Redshifted, $i=${:.0f} degrees".format(inc_angle_deg)
    else:
        title_str = "Not Redshifted"
    if not decrease_by_tau:
        title_str += "\n WITHOUT 1/tau decrease (test)"
    plt.title(title_str)
    plt.tight_layout()
    fname = "power_fraction"
    if not decrease_by_tau:
        fname += "_noTauDecrease"
    if redshift:
        fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
    plt.savefig(figure_dir + fname)
    plt.close()

    if not redshift:
        fractional_error = np.array(fractional_error)
        plt.figure()
        plt.plot(plot_Fthetaphi, fractional_error, ls='None', marker='o')
        plt.xlabel(r'$F_{\theta\phi}$')
        plt.ylabel(r"$(L_{\rm tot} - Q^+)/Q^+$")
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.yscale('log')
        plt.gca().grid(color='.9', ls='--')
        title_str = "Not Redshifted"
        if not decrease_by_tau:
            title_str += "WITHOUT 1/tau decrease (test)"
        plt.title(title_str)
        plt.tight_layout()
        fname = "fractional_error"
        if not decrease_by_tau:
            fname += "_noTauDecrease"
        if redshift:
            fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
        plt.savefig(figure_dir + fname)
        plt.close()

    plt.figure()
    plt.plot(plot_Fthetaphi, powerlaw+thermal, ls='None', marker='o', label="Powerlaw + thermal Sum")
    plt.plot(plot_Fthetaphi, total_power, ls='None', marker='+', label="Q+*volume")
    plt.xlabel(r'$F_{\theta\phi}$')
    plt.ylabel(r'$\rm erg/s$')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.yscale('log')
    plt.gca().grid(color='.9', ls='--')
    plt.legend()
    if redshift:
        title_str = r"Redshifted, $i=${:.0f} degrees".format(inc_angle_deg)
    else:
        title_str = "Not Redshifted"
    if not decrease_by_tau:
        title_str += "WITHOUT 1/tau decrease (test)"
    plt.title(title_str)
    plt.tight_layout()
    fname = "total_power"
    if not decrease_by_tau:
        fname += "_noTauDecrease"
    if redshift:
        fname += "_redshifted_i{:d}.png".format(inc_angle_deg)
    plt.savefig(figure_dir + fname)
    plt.close()

    print("Saved figure " + figure_dir + fname)
