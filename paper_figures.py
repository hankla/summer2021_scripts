import sys
import numpy as np
import matplotlib
import os
import pickle
from matplotlib.legend import Legend
from matplotlib.lines import Line2D
from matplotlib import ticker, cm, colors
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from dynamical_background_gammie1999 import *
from equilibrium_finder import *
from compare_handler import *

small_size = 22
medium_size = 16
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=14)
# matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)
matplotlib.rc('text', usetex=True)

def make_figure_1(figdir):
    medium_size = 14
    matplotlib.rc('axes', labelsize=medium_size)
    matplotlib.rc('legend', fontsize=medium_size)
    matplotlib.rc('xtick', labelsize=medium_size)
    matplotlib.rc('ytick', labelsize=medium_size)
    print("Making Figure 1.")

    kwargs = {}
    kwargs["Fthetaphi"] = 6.0
    kwargs["minimum_gamma1"] = 2.0
    kwargs["a"] = 0.95
    # kwargs["a"] = 0.5
    bg = dynamical_background(**kwargs)

    kwargs["PL_type"] = "WUBCN18"
    kwargs["gamma2_type"] = "4sigma"
    kwargs["quick"] = False
    kwargs["jump"] = 1
    equilibrium = equilibrium_finder(bg, **kwargs)

    fig = plt.figure(figsize=(12,3))
    axs = fig.subplots(1, 4)
    line1, = axs[0].plot(equilibrium.r_range, -equilibrium.Br_over_r/1e8, label=r"$-B_r/B_0$")
    ax = axs[0].twinx()
    # ax.se
    ax.set_yscale('log')
    ax.set_ylim([1.0, 100])
    line2, = ax.plot(equilibrium.bg.r_range, equilibrium.bg.uTsolution, ls='--', label=r"Fluid $\gamma$", color="C1")
    # line2, = ax.plot(equilibrium.bg.r_range, uR_over_c, ls='--', label=r"$-u_r/c$", color="C1")
    axs[0].legend(handles=[line1, line2], frameon=False)
    axs[0].tick_params(top=True, right=False, direction='in', which='both')
    ax.tick_params(right=True, direction='in', which='both')
    axs[0].set_xlabel(r'$r/r_g$')
    axs[0].set_xlim([equilibrium.r_range.min(), equilibrium.r_range.max()])
    # axs[0].set_ylabel(r"$B_r$ [G]")
    equilibrium.ion_temperature_over_r = 2.0/5.0*Consts.G_in_cgs*equilibrium.M_in_Msun * Consts.solar_mass_in_g*Consts.mp_in_g/(Consts.kB_cgs*equilibrium.r_range*get_rg_in_cm(equilibrium.M_in_Msun))
    theta_i = Consts.kB_cgs * equilibrium.ion_temperature_over_r/Consts.restmass_proton_cgs
    equilibrium.ion_beta_over_r = 8.0*np.pi*equilibrium.number_density_over_r*Consts.kB_cgs*equilibrium.ion_temperature_over_r/equilibrium.Br_over_r**2.0
    line1, = axs[1].plot(equilibrium.r_range, theta_i, label=r"$\theta_i$")
    ax1 = axs[1].twinx()
    # ax1.set_yscale('log')
    ax1.set_ylim([0.0, 2.0])
    line2, = ax1.plot(equilibrium.r_range, equilibrium.ion_beta_over_r, ls='--', label=r"$\beta_i$", color="C1")
    axs[1].legend(handles=[line1, line2], frameon=False)
    axs[1].tick_params(top=True, right=False, direction='in', which='both')
    ax1.tick_params(right=True, direction='in', which='both')
    axs[1].set_xlabel(r'$r/r_g$')
    axs[1].set_xlim([equilibrium.r_range.min(), equilibrium.r_range.max()])

    axs[2].plot(equilibrium.r_range, equilibrium.tau_es)
    if equilibrium.tau_es.min() > 1.0:
        axs[2].set_ylim([1.0, None])
    else:
        axs[2].axhline([1.0], color='black', ls='--')
    axs[2].set_ylabel(r'$\tau_{\rm es}$')
    axs[2].set_yscale('log')
    axs[3].plot(equilibrium.r_range, equilibrium.ion_magnetization_over_r)
    axs[3].set_ylabel(r"$\sigma_{i, \rm{cold}}$")
    # axs[3].set_yscale('log')

    for ax in axs[2:]:
        ax.tick_params(top=True, right=True, direction='in', which='both')
        ax.set_xlabel(r'$r/r_g$')
        ax.set_xlim([equilibrium.r_range.min(), equilibrium.r_range.max()])
    plt.tight_layout()
    figname = "fiducial_background_figure1.png"
    plt.savefig(figdir + figname, bbox_inches='tight', dpi=300)
    plt.savefig(figdir + "pdfs/" + figname.replace("_figure1.png", ".pdf"), bbox_inches='tight', dpi=300)

    print(equilibrium.ion_magnetization_over_r.min())
    print(equilibrium.ion_magnetization_over_r.max())
    print(equilibrium.ion_magnetization_over_r.min()*2000)
    print(equilibrium.electron_magnetization_over_r.min())
    print(equilibrium.electron_magnetization_over_r.max())
    gamma_max_estimate = 0.1*Consts.e_in_cgs*equilibrium.Br_over_r*equilibrium.r_range/Consts.restmass_cgs
    print(gamma_max_estimate.max())
    print(gamma_max_estimate.min())

def make_figure_2(figdir):
    """
    Very similar to comparison.plot_timescales_line_over_accretion_efficiency results.
    Take for a=0.95.
    """
    print("Making Figure 2.")
    category = "sparse3spin4"
    kwargs = {}
    fig_comparison = comparison(category, **kwargs)
    fig_comparison.get_labels()
    fig_comparison.get_quantities_over_accretion_efficiency()

    timescale_quantities = ["_thermalization_times",
                            "_collision_times",
                            "_cooling_times",
                            "_accel_times"]
    n = len(timescale_quantities)

    regionStr = "middle"
    spin = 0.95
    grid = True
    # grid = False
    dpi = 300
    fignames = {spin: "timescales_" + regionStr + "_a{:.2f}_figure2.png".format(spin)}
    pdf_replace_str = "_figure2.png"
    figKwargs = {"regionStrs": [regionStr], "spins": [spin], "grid": grid,
                 "figdir": figdir, "fignames": fignames,
                 "pdf_replace_str": pdf_replace_str, "dpi": dpi}
    fig_comparison.plot_timescales_line_over_accretion_efficiency(**figKwargs)
    print(fig_comparison.quantities_over_accretion_efficiency["middle_infall_times"])
    print(fig_comparison.quantities_over_accretion_efficiency["accretion_efficiencies"])

def make_figure_3(figdir):
    print("Making Figure 3.")
    medium_size = 14
    matplotlib.rc('axes', labelsize=medium_size)
    matplotlib.rc('xtick', labelsize=medium_size)
    matplotlib.rc('ytick', labelsize=medium_size)

    kwargs = {}
    kwargs["Fthetaphi"] = 6.0
    kwargs["minimum_gamma1"] = 2.0
    kwargs["a"] = 0.95
    bg = dynamical_background(**kwargs)

    kwargs["PL_type"] = "WUBCN18"
    kwargs["gamma2_type"] = "4sigma"
    kwargs["quick"] = False
    kwargs["jump"] = 1
    equilibrium = equilibrium_finder(bg, **kwargs)
    total_cooling = equilibrium.nonthermal_cooling_rate_over_r + equilibrium.thermal_cooling_rate_over_r
    try:
        equilibrium.half_light_radius
    except:
        equilibrium.get_spectrum_over_r(quiet=True)

    fig = plt.figure(figsize=(12,3))
    axs = fig.subplots(1, 4)
    axs[0].plot(equilibrium.quick_r_range, equilibrium.quick_gamma2_over_r)
    axs[0].set_ylabel(r"High-energy cut-off $\gamma_2$")
    axs[0].set_yscale('log')
    if equilibrium.quick_gamma2_over_r.min() > 1e3:
        axs[0].set_ylim([1e3, None])
    axs[1].plot(equilibrium.quick_r_range, equilibrium.peff_over_r, label=r"$p_{\rm eff}$")
    axs[1].plot(equilibrium.quick_r_range, equilibrium.quick_PL_indices, ls=":", color='black', label=r"$p(\sigma_i)$")
    axs[1].legend(frameon=False)
    axs[2].plot(equilibrium.quick_r_range, equilibrium.theta_e_over_r)
    axs[2].set_ylabel(r'$\theta_e$')
    axs[3].plot(equilibrium.quick_r_range, equilibrium.nonthermal_cooling_rate_over_r/total_cooling, label=r"$Q^-_{\rm PL}/Q^-$", ls='-')
    axs[3].plot(equilibrium.quick_r_range, equilibrium.thermal_cooling_rate_over_r/total_cooling, label=r"$Q^-_{\rm MJ}/Q^-$", ls='--')
    # axs[3].axhline([1.0], color='black', ls='--')
    axs[3].legend(frameon=False)
    plt.yscale('log')
    for ax in axs:
        ax.tick_params(top=True, right=True, direction='in', which='both')
        ax.axvline([equilibrium.half_light_radius], color='black', ls='--')
        # ax.axvline([equilibrium.decoupling_radius], color='black', ls='-.')
        ax.set_xlabel(r'$r/r_g$')
        ax.set_xlim([equilibrium.quick_r_range.min(), equilibrium.quick_r_range.max()])
    plt.tight_layout()

    figname = "prescription_quantities_figure3.png"
    plt.savefig(figdir + figname, bbox_inches='tight', dpi=300)
    plt.savefig(figdir + "pdfs/" + figname.replace("_figure3.png", ".pdf"), bbox_inches='tight', dpi=300)

def make_figure_4(figdir):
    matplotlib.rc('legend', fontsize=medium_size)
    print("Making Figure 4.")
    grid = True
    # grid = False
    kwargs = {}
    kwargs["Fthetaphi"] = 6.0
    kwargs["minimum_gamma1"] = 2.0
    kwargs["a"] = 0.95
    bg = dynamical_background(**kwargs)

    kwargs["PL_type"] = "WUBCN18"
    kwargs["gamma2_type"] = "4sigma"
    kwargs["quick"] = False
    kwargs["jump"] = 1

    equilibrium = equilibrium_finder(bg, **kwargs)
    # Innermost radius
    last_index = equilibrium.quick_r_range.size - 1
    fPL_inner = equilibrium.f_dict["nonthermal"][last_index]
    fMJ_inner = equilibrium.f_dict["thermal"][last_index]
    # Initial power-law
    fPL0_norm_over_r = equilibrium.number_density_over_r * (equilibrium.PL_indices - 1.0)/(1.0 - equilibrium.gamma2_over_r**(1.0 - equilibrium.PL_indices))
    fPL0_inner = fPL0_norm_over_r[-1]*equilibrium.gamma_bins**(-equilibrium.PL_indices[-1])
    fPL0_inner[equilibrium.gamma_bins > equilibrium.gamma2_over_r[-1]] = 0

    plt.figure()
    plt.plot(equilibrium.gamma_bins, fPL0_inner, color=colorblind_colors[3], ls='--', label=r"$f_{\rm PL,0}(\gamma)$, $1<\gamma<\gamma_2$")
    plt.plot(equilibrium.gamma_bins, fPL_inner, color=colorblind_colors[0], ls='-', label=r"$f_{\rm PL}(\gamma)$, $\gamma_1<\gamma<\gamma_2$")
    plt.plot(equilibrium.gamma_bins, fMJ_inner, color=colorblind_colors[1], ls=':', label=r"$f_{\rm MJ}(\gamma)$")
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim([1e2, 1e20])
    plt.xlabel(r'Particle Lorentz Factor $\gamma$')
    plt.ylabel(r"Electron distribution function $f(\gamma)$")
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.legend(frameon=False, loc='upper right')
    if grid:
        plt.gca().grid(color='.9', ls='--')
    plt.tight_layout()
    figname = "inner_distribution_function_figure4.png"
    plt.savefig(figdir + figname, bbox_inches='tight', dpi=300)
    plt.savefig(figdir + "pdfs/" + figname.replace("_figure4.png", ".pdf"), bbox_inches='tight', dpi=300)

def make_figure_5(figdir):
    print("Making Figure 5.")
    matplotlib.rc('legend', fontsize=14)
    kwargs = {}
    kwargs["Fthetaphi"] = 6.0
    kwargs["minimum_gamma1"] = 2.0
    kwargs["a"] = 0.95
    bg = dynamical_background(**kwargs)

    kwargs["PL_type"] = "WUBCN18"
    kwargs["gamma2_type"] = "4sigma"
    kwargs["quick"] = False
    kwargs["jump"] = 1

    equilibrium = equilibrium_finder(bg, **kwargs)
    inc_angle_deg = 60
    dpi = 300
    figname = "spectrum_figure5.png"
    pdf_replace_str = "_figure5.png"
    xlims = [1e15, 2e23]
    MeV_in_Hz = kevToNu(1000.0)
    axvspans = [[MeV_in_Hz, None]]

    figKwargs = {"figdir": figdir, "figname": figname,
                 "xlims": xlims, "ylims": None,
                 "axvspans": axvspans, "title": False,
                 "pdf_replace_str": pdf_replace_str,
                 "dpi": dpi}

    quiet = True; spectrum_decrease_by_tau = False; overwrite = False
    equilibrium.plot_spectra(quiet, spectrum_decrease_by_tau,
                             overwrite, inc_angle_deg, **figKwargs)
    return

def make_figure_6(figdir):
    print("Making Figure 6.")
    kwargs = {}
    kwargs["Fthetaphi"] = 6.0
    kwargs["minimum_gamma1"] = 2.0
    kwargs["a"] = 0.95
    bg = dynamical_background(**kwargs)

    kwargs["PL_type"] = "WUBCN18"
    kwargs["gamma2_type"] = "4sigma"
    kwargs["quick"] = False
    kwargs["jump"] = 1

    figname = "luminosity_over_r_figure6.png"
    equilibrium = equilibrium_finder(bg, **kwargs)
    inc_angle_deg = 60
    num_bins = 100
    dpi = 300
    pdf_replace_str = "_figure6.png"
    figKwargs = {"figdir": figdir, "figname": figname,
                 "title": False, "ylims": None, "xlims": None,
                 "pdf_replace_str": pdf_replace_str,
                 "dpi": dpi}
    quiet = True; spectrum_decrease_by_tau = False; overwrite = False
    equilibrium.plot_spectra_over_r(quiet, spectrum_decrease_by_tau,
                                    overwrite, inc_angle_deg, num_bins,
                                    **figKwargs)
    return


def make_figure_7(figdir):
    print("Making Figure 7.")
    category = "sparse3spin4"
    kwargs = {}
    inc_angle = 60
    fig_comparison = comparison(category, **kwargs)
    fig_comparison.get_spectral_quantities_over_accretion_efficiency(inc_angle)

    # LPL/Ldisk over accretion efficiency. Can be found amidst the
    # figures/multizone_model/compare/gamma1minimum2.00/ --category--/line_over_accretionEfficiency
    # generated by plot_spectrum_fractions_line_over_accretion_efficiency in compare_handler.py
    spectral_type = "1keV-1MeV"
    quantity = "power_in_" + spectral_type + "_nonthermal"
    other_quantity = "power_in_" + spectral_type + "_disk"

    marker_fills = {0.95:[True, True, True, False, False],
                    0.85:[True, True, False, False, False],
                    0.70:[True, False, False, False, False],
                    0.50:[True, False, False, False, False]
                    }
    plt.figure()
    ax = plt.gca()
    # accretion efficiency plot
    n = fig_comparison.spin_values.size
    for i, spin in enumerate(fig_comparison.spin_values):
        data = fig_comparison.spectral_quantities_over_accretion_efficiency[quantity][spin]
        other_data = fig_comparison.spectral_quantities_over_accretion_efficiency[other_quantity][spin]
        accretion_efficiencies = fig_comparison.quantities_over_accretion_efficiency["accretion_efficiencies"][spin]
        for j, fill in enumerate(marker_fills[spin]):
            if fill:
                h1, = ax.plot(accretion_efficiencies[j], (data/other_data)[j], marker='o',
                        color='black', fillstyle='none', markersize=10)
        line, = ax.plot(accretion_efficiencies, data/other_data,
                       marker='o', label=r"$a={:.2f}$".format(spin),
                       color=cb_colors(n)[i], fillstyle='full')
    ax.set_xlabel(fig_comparison.labels["accretion_efficiencies"])
    # ylabel = fig_comparison.spectral_labels["nonthermal"] + r"$/$" + fig_comparison.spectral_labels["disk"] + fig_comparison.spectral_labels[spectral_type]
    ylabel = fig_comparison.spectral_labels["nonthermal"] + r"$/$" + fig_comparison.spectral_labels["disk"]
    ylabel = "Luminosity Ratio " + ylabel
    ax.set_ylabel(ylabel)
    ylims = ax.get_ylim()
    # ax.axhspan(ylims[0], 1e-2, color='gray', alpha=0.3, hatch='/')
    ax.set_yscale('log')
    plt.ylim([None, 2e-1])
    ax.tick_params(top=True, right=True, direction='in', which='both')
    ax.grid(color='.9', ls='--')
    leg1 = plt.legend(frameon=False)
    PL_dom = Line2D([0], [0], color='black', fillstyle='none', marker='o', markersize=10, ls='none')
    therm_dom = Line2D([0], [0], color='black', marker='o', ls='none')
    # leg2 = ax.legend([PL_dom, therm_dom], [r"$L_{\rm PL}>L_{\rm MJ}$", r"$L_{\rm PL}<L_{\rm MJ}$"], ncol=2, frameon=False, bbox_to_anchor=(0.8, 1.1))
    leg2 = ax.legend([PL_dom, therm_dom], [r"$L_{\rm PL}>L_{\rm MJ}$", r"$L_{\rm PL}<L_{\rm MJ}$"], frameon=False)
    ax.add_artist(leg1)

    figname = "LPL_over_Ldisk_figure7.png"
    plt.savefig(figdir + figname, bbox_inches='tight', dpi=300)
    plt.savefig(figdir + "pdfs/" + figname.replace("_figure7.png", ".pdf"), bbox_inches='tight', dpi=300)
    plt.close()

def make_figure_8(figdir):
    category = "sparse3spin4"
    kwargs = {}
    fig_comparison = comparison(category, **kwargs)

    fignames = {"PLF": "fraction_PLF_figure8.png"}
    pdf_replace_str = "_figure8.png"
    dpi = 300
    # Decided not to show observations since there's is no trend with inclination angle
    # (from my calculations and from Munoz-Darias+ 2013).
    astro_objects = []
    annotation_locs = []
    annotation_names = []
    # astro_objects = ["GRO1655", "J1550", "GX339"]
    # # annotation_locs = {"GRO1655": [57, 0.015], "J1550":[72, 0.008], "GX339":[30, 0.1]}
    # # annotation_locs = {"GRO1655": [57, 0.015], "J1550":[72, 0.03], "GX339":[30, 0.1]}
    annotation_names = {"GRO1655": "GRO J1655-40", "J1550":"XTE J1550-564", "GX339":"GX 339-4"}
    inc_values = ["Fthetaphi6.00_value"]
    legend_loc = "upper left"
    spectral_types = []
    spectral_quantities = []
    ratio_types = ["PLF"]
    ylims = [1e-3, 1]

    figKwargs = {"inc_values": inc_values,
                 "spectral_quantities": spectral_quantities,
                 "spectral_types": spectral_types,
                 "objects_to_annotate": astro_objects,
                 "ratio_types": ratio_types,
                 "ylims":ylims,
                 "annotation_locs": annotation_locs,
                 "annotation_names": annotation_names,
                 "legend_loc": legend_loc,
                 "fignames": fignames,
                 "figdir": figdir,
                 "figdir_val": figdir,
                 "dpi": dpi,
                 "pdf_replace_str": pdf_replace_str,
                 "title": False}

    inc_angles = [0, 15, 30, 45, 60, 75, 85, 90]
    fig_comparison.plot_spectral_fractions_line_over_inclination(inc_angles, **figKwargs)


def make_figure_8_old(figdir):
    print("Making Figure 8.")
    kwargs = {}
    category = "sparse3spin4"
    inc_angle = 60
    fig_comparison = comparison(category, **kwargs)
    fig_comparison.get_spectral_quantities_over_accretion_efficiency(inc_angle)

    marker_fills = {0.95:[True, True, False, False, False],
                    0.85:[True, True, False, False, False],
                    0.70:[True, False, False, False, False],
                    0.50:[True, False, False, False, False]
                    }
    # LPL/LMJ over accretion efficiency. Can be found amidst the
    # figures/multizone_model/compare/gamma1minimum2.00/ --category--/line_over_accretionEfficiency
    # generated by plot_spectrum_fractions_line_over_accretion_efficiency in compare_handler.py
    quantity = "power_in_powerlaws"
    other_quantity = "power_in_thermal_prs"
    spectral_type = "Xray_nopair_"
    fraction_name = quantity + "_over_" + other_quantity + "_" + spectral_type

    plt.figure()
    ax = plt.gca()
    n = fig_comparison.spin_values.size
    # accretion efficiency plot
    for i, spin in enumerate(fig_comparison.spin_values):
        data = fig_comparison.spectral_quantities_over_accretion_efficiency[spectral_type + quantity][spin]
        other_data = fig_comparison.spectral_quantities_over_accretion_efficiency[spectral_type + other_quantity][spin]
        accretion_efficiencies = fig_comparison.quantities_over_accretion_efficiency["accretion_efficiencies"][spin]
        for j, fill in enumerate(marker_fills[spin]):
            if fill:
                ax.plot(accretion_efficiencies[j], (data/other_data)[j], marker='o',
                        color='black', fillstyle='none', markersize=10)
        line, = ax.plot(accretion_efficiencies, data/other_data,
                            marker='o', label=r"$a={:.2f}$".format(spin), color=cb_colors(n)[i])
    ylims = ax.get_ylim()
    ax.axhspan(ylims[0], 1.0, color='gray', alpha=0.3, hatch='/')
    ax.set_xlabel(fig_comparison.labels["accretion_efficiencies"])
    ylabel = fig_comparison.spectral_labels[quantity] + r"$/$" + fig_comparison.spectral_labels[other_quantity] + fig_comparison.spectral_labels[spectral_type]
    ax.set_ylabel(ylabel)
    ax.set_yscale('log')
    ax.tick_params(top=True, right=True, direction='in', which='both')
    ax.grid(color='.9', ls='--')
    plt.legend(frameon=False)

    plt.tight_layout()

    figname = "LPL_over_LMJ_figure8.png"
    plt.savefig(figdir + figname, bbox_inches='tight', dpi=300)
    plt.savefig(figdir + "pdfs/" + figname.replace("_figure8.png", ".pdf"), bbox_inches='tight', dpi=300)
    plt.close()


if __name__ == '__main__':
    fig_dir = "/mnt/c/Users/liaha/research/projects/summer2021/figures/paper_figures/"
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    if not os.path.exists(fig_dir + "pdfs/"):
        os.makedirs(fig_dir + "pdfs/")

    # make_figure_1(fig_dir)
    make_figure_2(fig_dir)
    # make_figure_3(fig_dir)
    # make_figure_4(fig_dir)
    # make_figure_5(fig_dir)
    # make_figure_6(fig_dir)
    # make_figure_7(fig_dir)
    # make_figure_8(fig_dir)

    exit()
    category = "sparse3spin4"
    inc_angle = 60
    kwargs = {}
    comparison = comparison(category, **kwargs)
    comparison.get_spectral_quantities_over_accretion_efficiency(inc_angle)
    comparison.get_spectral_quantities_over_inclination([60])
    # Get table values
    All_nu = comparison.spectral_quantities_over_accretion_efficiency["power_in__nonthermal"][0.95][0]/comparison.spectral_quantities_over_accretion_efficiency["power_in__disk"][0.95][0]
    nu_above_1keV = comparison.spectral_quantities_over_accretion_efficiency["power_in_1keV_nonthermal"][0.95][0]/comparison.spectral_quantities_over_accretion_efficiency["power_in_1keV_disk"][0.95][0]
    nu_between_1keV1MeV = comparison.spectral_quantities_over_accretion_efficiency["power_in_1keV-1MeV_nonthermal"][0.95][0]/comparison.spectral_quantities_over_accretion_efficiency["power_in_1keV-1MeV_disk"][0.95][0]
    print(All_nu)
    print(nu_above_1keV)
    print(nu_between_1keV1MeV)
