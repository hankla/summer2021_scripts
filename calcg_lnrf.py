import numpy as np

def calcg(U,MU,Q2,L,A,TPM,TPR,SU,SM,VRL,VTL,VPL):
      ONE=1.
      TWO=2.
      TRES=3.
      THIRD=ONE/TRES
      R=ONE/U

      # Calculate the ISCO
      Z1=ONE+(ONE-A*A)**THIRD*((ONE+A)**THIRD+(ONE-A)**THIRD)
      Z2=np.sqrt(TRES*A*A+Z1*Z1)
      RMS=TRES+Z2-np.sign(A)*np.sqrt((TRES-Z1)*(TRES+Z1+TWO*Z2)) # ISCO
      # Metric coefficients
      D=R*R-TWO*R+A*A
      AR=(R*R+A*A)**2-A*A*D*(ONE-MU*MU)
      RHO=R*R+A*A*MU*MU
      ENU=np.sqrt(D*RHO/AR)
      EMU1=np.sqrt(RHO/D)
      EMU2=np.sqrt(RHO)
      EPSI=np.sqrt(ONE-MU*MU)*np.sqrt(AR/RHO)
      OM=TWO*A*R/AR
      # --------------------
      SR=(-ONE)**TPR*SU
      ST=(-ONE)**TPM*SM
      OMEGA=ENU/EPSI*VPL+OM
      GAM=ONE/np.sqrt(ONE-(VRL*VRL+VTL*VTL+VPL*VPL))
# Now, compute R(r) and Theta(theta):
      RR=R*R*np.sqrt(-A*A*Q2*U**4+TWO*U**3*(Q2+(A-L)**2)+U*U*(A*A-Q2-L*L)+ONE)
      TT=np.sqrt((Q2+MU*MU*(A*A-L*L-Q2)-A*A*MU**4)/(ONE-MU*MU))
#      IF(TOTAL(WHERE(TT NE TT))) NE -1 THEN TT(WHERE(TT NE TT))=0.
#      IF TOTAL(WHERE(RR NE RR)) NE -1 THEN RR(WHERE(RR NE RR))=0.
# Now, we can compute the redshift formula:
      G=ENU/GAM/(ONE-L*OMEGA-EMU1*ENU*VRL/RHO*SR*RR-EMU2*ENU*VTL/RHO*ST*TT)
      return G

def lnrf_frame(VR,VT,OMEGA,R,A,TH):
      # r, th are the spatial locations.
      # A is black hole spin
      # VR, VT are the moving frame 3-velocities v^r and v^\theta,
      # where v^r = u^r/u^t, v^\theta = u^\theta/u^t.
      # Omega is v^phi = u^\phi/u^t
      MU=np.cos(TH)
      # zero_inds = TH == np.pi/2.
      # MU[zero_inds] = 0.0
      D=R*R-2*R+A*A # Delta for Kerr metric
      AR=(R*R+A*A)**2-A*A*D*(1.-MU*MU) # A(r) for Kerr metric
      RHO=R*R+A*A*MU*MU # rho2 for Kerr metric
      ENU=np.sqrt(D*RHO/AR) # e^\nu (Viergutz 1993 below eq. 1)
      EMU1=np.sqrt(RHO/D)
      EMU2=np.sqrt(RHO)
      EPSI=np.sqrt(1.-MU*MU)*np.sqrt(AR/RHO)
      OM=2*A*R/AR # little omega
      # Get fluid velocity in LNRF (Viergutz eq. 15)
      VRL=EMU1/ENU*VR
      VTL=EMU2/ENU*VT
      VPL=EPSI/ENU*(OMEGA-OM)
      return VRL,VTL,VPL
