import sys
import numpy as np
import matplotlib
import pickle
from matplotlib.legend import Legend
from matplotlib.lines import Line2D
from matplotlib.cm import ScalarMappable
from matplotlib import ticker, cm, colors
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from dynamical_background_gammie1999 import *
from equilibrium_finder import *
import matplotlib.patches as patches
import scipy.interpolate as interpolate
import matplotlib.patheffects as mpe

small_size = 22
medium_size = 16
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=14)
# matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)
matplotlib.rc('text', usetex=True)

class comparison:
    def __init__(self, category, setup="lia_hp", **kwargs):
        """
        TODO:
        - generalize to different PL_type, gamma2_type
        """
        self.setup = setup
        self.category = category
        self.minimum_gamma1 = kwargs.get("minimum_gamma1", 2.0)
        overwrite_self = kwargs.get("overwrite_self", False)

        # --------------------------------------------------------------
        # For now, FIX PIC prescriptions
        # --------------------------------------------------------------
        # PL_type and gamma2_type determine the prescription used to set
        # the power-law index and high-energy cutoff, respectively.
        # They can be set to constant or to use a PIC prescription.
        # PL_type options: "WUBCN18", "WPU19", "constant"
        # gamma2_type options: "fixed", "4sigma", "6sigma"
        self.PL_type = kwargs.get("PL_type", "WUBCN18")
        self.gamma2_type = kwargs.get("gamma2_type", "4sigma")
        # --------------------------------------------------------------
        # Handle prescriptions for p and gamma2
        # --------------------------------------------------------------
        if self.PL_type == "constant":
            self.PL_index_constant = 2.9
            self.PL_filestr = "Constant{:.2f}".format(self.PL_index_constant)
            self.PL_titstr = "$p={:.2f}$".format(self.PL_index_constant)
        else:
            self.PL_filestr = self.PL_type
            self.PL_titstr = self.PL_type + " PL prescription"

        if self.gamma2_type == "fixed":
            self.gamma2_constant = kwargs.get("gamma2_constant", 1000)
            self.gamma2_str = "{:.2e}".format(self.gamma2_constant)
            self.gamma2_titstr = r"$\gamma_2={:.2e}$".format(self.gamma2_constant)
        else:
            self.gamma2_str = self.gamma2_type
            self.gamma2_titstr = r"$\gamma_2$ " + self.gamma2_str

        # -----------------------------------
        #      Add default setup information here
        # -----------------------------------
        if setup == "lia_hp":
            path_to_figures = "/mnt/c/Users/liaha/research/projects/"\
                "summer2021/figures/multizone_model/compare/gamma1minimum{:.2f}/".format(self.minimum_gamma1) + category + "/"
            path_to_reduced_data = "/mnt/c/Users/liaha/research/projects/"\
                "summer2021/data_reduced/comparison/gamma1minimum{:.2f}/".format(self.minimum_gamma1)
            pickle_path = path_to_reduced_data + category + ".p"

        # -----------------------------------
        # If paths are non-standard, set them here using kwargs
        # Otherwise, use the defaults from above
        # -----------------------------------
        self.path_to_figures = kwargs.get("path_to_figures", path_to_figures)
        self.path_to_reduced_data = kwargs.get("path_to_reduced_data",
                                               path_to_reduced_data)
        self.pickle_path = kwargs.get("pickle_path", pickle_path)

        if os.path.exists(self.pickle_path) and (not overwrite_self):
            print("Loading comparison from " + self.pickle_path)
            success = self._load_from_pickle()
        else:
            success = self._initialize(**kwargs)
        self.get_labels()

        # -----------------------------------
        # Dump to file
        # -----------------------------------
        with open(self.pickle_path, 'wb') as fh:
            pickle.dump(self, fh, pickle.HIGHEST_PROTOCOL)



    def _initialize(self, **kwargsOuter):
        print("Creating new comparison.")
        self.to_print = kwargsOuter.get("printing", True)

        # -----------------------------------
        # Create directories if need be
        # -----------------------------------
        if not os.path.exists(self.path_to_figures):
            os.makedirs(self.path_to_figures)
        if not os.path.exists(self.path_to_reduced_data):
            os.makedirs(self.path_to_reduced_data)

        self.get_category_values()
        self.get_labels()
        self.quantities_over_FthetaphiSpin = {}
        # Initialize zeros
        for quantity in self.labels.keys():
            self.quantities_over_FthetaphiSpin[quantity] = np.zeros((self.Fthetaphi_values.size, self.spin_values.size))

        # -----------------------------------
        # Get necessary simulation data
        # -----------------------------------
        for i, Fthetaphi in enumerate(self.Fthetaphi_values):
            for j, spin in enumerate(self.spin_values):
                print("Fthetaphi={:.2f}, a={:.2f}".format(Fthetaphi, spin))
                kwargs = {}
                kwargs["a"] = spin
                kwargs["Fthetaphi"] = Fthetaphi
                kwargs["PL_type"] = self.PL_type
                kwargs["gamma2_type"] = self.gamma2_type
                kwargs["minimum_gamma1"] = self.minimum_gamma1
                kwargs["jump"] = 1

                bg = dynamical_background(**kwargs)
                equilibrium = equilibrium_finder(bg, **kwargs)
                if equilibrium.PL_type != self.PL_type:
                    print("ERROR: PL type not the same!")
                if equilibrium.gamma2_type != self.gamma2_type:
                    print("ERROR: gamma2 type not the same!")
                equilibrium.get_quick_arrays()
                equilibrium.calculate_tEnergyLoss_at_gamma1_over_r()
                equilibrium.calculate_tThermalize_over_r()

                self.quantities_over_FthetaphiSpin["accretion_efficiencies"][i][j] = equilibrium.accretion_efficiency
                self.quantities_over_FthetaphiSpin["total_volume_heating_rates"][i][j] = equilibrium.volume_heating_rate
                self.quantities_over_FthetaphiSpin["rISCO"][i][j] = equilibrium.bg.rISCO
                self.quantities_over_FthetaphiSpin["rEH"][i][j] = equilibrium.bg.rEH

                tcoll_size = equilibrium.gamma1_ee_tEnergyLoss_over_r.size
                mid_ind = int(tcoll_size/2)
                # bg.times_at_radius[0] is at THE EVENT HORIZON but bg.r_range[0] is at the ISCO
                # so need to reverse
                tinfall_over_r_interp_f = interp1d(equilibrium.bg.r_range, equilibrium.bg.times_at_radius[::-1])
                tinfall_over_r = tinfall_over_r_interp_f(equilibrium.quick_r_range)
                tinfall_over_r_s = tinfall_over_r*equilibrium.bg.get_tg_in_s(equilibrium.M_in_Msun)
                tinfall_inner = tinfall_over_r_s[-1]
                tinfall_mid = tinfall_over_r_s[mid_ind]
                tinfall_outer = tinfall_over_r_s[0]

                inner_tcoll = equilibrium.gamma1_ee_tEnergyLoss_over_r[-1]/tinfall_inner
                outer_tcoll = equilibrium.gamma1_ee_tEnergyLoss_over_r[0]/tinfall_outer
                middle_tcoll = equilibrium.gamma1_ee_tEnergyLoss_over_r[mid_ind]/tinfall_mid
                mean_tcoll = (equilibrium.gamma1_ee_tEnergyLoss_over_r/tinfall_over_r_s).mean()
                std_tcoll = (equilibrium.gamma1_ee_tEnergyLoss_over_r/tinfall_over_r_s).std()

                inner_tcool = equilibrium.cooling_consts_over_r[-1]/equilibrium.gamma1_over_r[-1]/tinfall_inner
                middle_tcool = equilibrium.cooling_consts_over_r[mid_ind]/equilibrium.gamma1_over_r[mid_ind]/tinfall_mid
                outer_tcool = equilibrium.cooling_consts_over_r[0]/equilibrium.gamma1_over_r[0]/tinfall_outer
                mean_tcool = (equilibrium.cooling_consts_over_r/equilibrium.gamma1_over_r/tinfall_over_r_s).mean()
                std_tcool = (equilibrium.cooling_consts_over_r/equilibrium.gamma1_over_r/tinfall_over_r_s).std()

                inner_taccel = 100.0/equilibrium.plasma_freq_over_r[-1]/tinfall_inner
                middle_taccel = 100.0/equilibrium.plasma_freq_over_r[mid_ind]/tinfall_mid
                outer_taccel = 100.0/equilibrium.plasma_freq_over_r[0]/tinfall_outer
                mean_taccel = (100.0/equilibrium.plasma_freq_over_r/tinfall_over_r_s).mean()
                std_taccel = (100.0/equilibrium.plasma_freq_over_r/tinfall_over_r_s).std()

                inner_tTherm = equilibrium.t_thermalize[-1]/tinfall_inner
                middle_tTherm = equilibrium.t_thermalize[mid_ind]/tinfall_mid
                outer_tTherm = equilibrium.t_thermalize[0]/tinfall_outer
                mean_tTherm = (equilibrium.t_thermalize/tinfall_over_r_s).mean()
                std_tTherm = (equilibrium.t_thermalize/tinfall_over_r_s).std()

                decoupling_index = (np.abs(equilibrium.t_thermalize - tinfall_over_r_s)).argmin()
                decoupling_radius = equilibrium.quick_r_range[decoupling_index]
                decoupling_radius_fractional = (equilibrium.bg.rISCO - decoupling_radius)/(equilibrium.bg.rISCO - equilibrium.bg.rEH)
                r2 = equilibrium.r_range[-1] * get_rg_in_cm(equilibrium.M_in_Msun)
                dr2 = decoupling_radius * get_rg_in_cm(equilibrium.M_in_Msun)
                decoupled_volume = 4.0*np.pi/3.0*np.abs(dr2**3 - r2**3)
                heating_in_decoupled = decoupled_volume/equilibrium.emitting_volume
                self.quantities_over_FthetaphiSpin["inner_infall_times"][i][j] = tinfall_inner
                self.quantities_over_FthetaphiSpin["middle_infall_times"][i][j] = tinfall_mid
                self.quantities_over_FthetaphiSpin["outer_infall_times"][i][j] = tinfall_outer
                self.quantities_over_FthetaphiSpin["mean_infall_times"][i][j] = tinfall_over_r_s.mean()
                self.quantities_over_FthetaphiSpin["std_infall_times"][i][j] = tinfall_over_r_s.std()

                self.quantities_over_FthetaphiSpin["inner_collision_times"][i][j] = inner_tcoll
                self.quantities_over_FthetaphiSpin["middle_collision_times"][i][j] = middle_tcoll
                self.quantities_over_FthetaphiSpin["outer_collision_times"][i][j] = outer_tcoll
                self.quantities_over_FthetaphiSpin["mean_collision_times"][i][j] = mean_tcoll
                self.quantities_over_FthetaphiSpin["std_collision_times"][i][j] = std_tcoll

                self.quantities_over_FthetaphiSpin["inner_cooling_times"][i][j] = inner_tcool
                self.quantities_over_FthetaphiSpin["middle_cooling_times"][i][j] = middle_tcool
                self.quantities_over_FthetaphiSpin["outer_cooling_times"][i][j] = outer_tcool
                self.quantities_over_FthetaphiSpin["mean_cooling_times"][i][j] = mean_tcool
                self.quantities_over_FthetaphiSpin["std_cooling_times"][i][j] = std_tcool

                self.quantities_over_FthetaphiSpin["inner_accel_times"][i][j] = inner_taccel
                self.quantities_over_FthetaphiSpin["middle_accel_times"][i][j] = middle_taccel
                self.quantities_over_FthetaphiSpin["outer_accel_times"][i][j] = outer_taccel
                self.quantities_over_FthetaphiSpin["mean_accel_times"][i][j] = mean_taccel
                self.quantities_over_FthetaphiSpin["std_accel_times"][i][j] = std_taccel

                self.quantities_over_FthetaphiSpin["inner_thermalization_times"][i][j] = inner_tTherm
                self.quantities_over_FthetaphiSpin["middle_thermalization_times"][i][j] = middle_tTherm
                self.quantities_over_FthetaphiSpin["outer_thermalization_times"][i][j] = outer_tTherm
                self.quantities_over_FthetaphiSpin["mean_thermalization_times"][i][j] = mean_tTherm
                self.quantities_over_FthetaphiSpin["std_thermalization_times"][i][j] = std_tTherm

                self.quantities_over_FthetaphiSpin["decoupling_radius"][i][j] = decoupling_radius
                self.quantities_over_FthetaphiSpin["decoupling_radius_fractional"][i][j] = decoupling_radius_fractional
                self.quantities_over_FthetaphiSpin["heating_in_decoupled"][i][j] = heating_in_decoupled
                self.quantities_over_FthetaphiSpin["mean_temperatures"][i][j] = equilibrium.temperature_over_r.mean()
                self.quantities_over_FthetaphiSpin["std_temperatures"][i][j] = equilibrium.temperature_over_r.std()
                amplification_factor = calculate_amplification_factor(equilibrium.number_density_over_r, equilibrium.temperature_over_r)
                self.quantities_over_FthetaphiSpin["mean_amplification_factor"][i][j] = amplification_factor.mean()
                self.quantities_over_FthetaphiSpin["std_amplification_factor"][i][j] = amplification_factor.std()


        # -----------------------------------
        # Write in order of accretion efficiencies
        # -----------------------------------
        self.get_quantities_over_accretion_efficiency()

        # -----------------------------------
        # Dump to file
        # -----------------------------------
        with open(self.pickle_path, 'wb') as fh:
            pickle.dump(self, fh)
        return True

    def _load_from_pickle(self):
        try:
            with open(self.pickle_path, 'rb') as fh:
                self.__dict__.update(pickle.load(fh).__dict__)
                return True
        except:
            return False

    def get_spectral_data(self, inc_angle):
        # Do NOT save spectral data to full pickle file. Separate out into inclination angle files,
        # otherwise it gets reaaally unwieldy.
        to_calculate = False
        # to_calculate = True
        spectral_path = self.path_to_reduced_data + self.category + "_spectrum_i{:d}.p".format(inc_angle)
        if not os.path.exists(spectral_path):
            print("Initializing spectral data for i={:d}".format(inc_angle))
            self.spectral_data = {}
            self.spectral_data["inc_angle"] = inc_angle
            self.spectral_data["Fthetaphi_values"] = self.Fthetaphi_values
            self.spectral_data["spin_values"] = self.spin_values
            for quantity in self.list_of_spectral_quantities:
                for spectral_type in self.list_of_spectral_type:
                    full_quantity = "power_in_" + spectral_type + "_" + quantity
                    self.spectral_data[full_quantity] = np.zeros((self.Fthetaphi_values.size, self.spin_values.size))
                    to_calculate = True
        else:
            # print("Loading spectral data from comparison file for i={:d}: ".format(inc_angle) + spectral_path)
            with open(spectral_path, 'rb') as fh:
                self.spectral_data = pickle.load(fh)
            for quantity in self.list_of_spectral_quantities:
                for spectral_type in self.list_of_spectral_type:
                    full_quantity = "power_in_" + spectral_type + "_" + quantity
                    if full_quantity not in self.spectral_data:
                        self.spectral_data[full_quantity] = np.zeros((self.Fthetaphi_values.size, self.spin_values.size))
                        to_calculate = True

        # -----------------------------------
        # Get necessary simulation data
        # -----------------------------------
        if to_calculate:
            print("Compiling quantities from spectra...")
            quiet = True; spectrum_decrease_by_tau = False; overwrite = False
            # Have to do full calculation here
            for i, Fthetaphi in enumerate(self.Fthetaphi_values):
                for j, spin in enumerate(self.spin_values):
                    print("Fthetaphi={:.2f}, a={:.2f}".format(Fthetaphi, spin))
                    kwargs = {}
                    kwargs["a"] = spin
                    kwargs["Fthetaphi"] = Fthetaphi
                    kwargs["PL_type"] = self.PL_type
                    kwargs["gamma2_type"] = self.gamma2_type
                    kwargs["minimum_gamma1"] = self.minimum_gamma1
                    kwargs["jump"] = 1

                    bg = dynamical_background(**kwargs)
                    equilibrium = equilibrium_finder(bg, **kwargs)
                    if equilibrium.PL_type != self.PL_type:
                        print("ERROR: PL type not the same!")
                    if equilibrium.gamma2_type != self.gamma2_type:
                        print("ERROR: gamma2 type not the same!")

                    (spectrum_dict, disk_spectrum_dict) = equilibrium.retrieve_spectra(quiet, spectrum_decrease_by_tau, overwrite, inc_angle)
                    equilibrium.retrieve_spectral_power(quiet, spectrum_decrease_by_tau, overwrite, inc_angle)

                    Lnu_nonthermal = spectrum_dict["Lnu_nonthermal"]
                    Lnu_thermal = spectrum_dict["Lnu_thermal"]
                    Lnu_disk = disk_spectrum_dict["Lnu_disk"]
                    spectral_bins = spectrum_dict["spectral_bins"]

                    for quantity in equilibrium.list_of_spectral_quantities:
                        for spectral_type in equilibrium.list_of_spectral_type:
                            full_quantity = "power_in_" + spectral_type + "_" + quantity
                            self.spectral_data[full_quantity][i][j] = equilibrium.spectral_power[full_quantity]

        with open(spectral_path, 'wb') as fh:
                pickle.dump(self.spectral_data, fh)
        return

    def get_quantities_over_accretion_efficiency(self):
        try:
            return self.spectral_quantities_over_accretion_efficiency
        except:
            self.quantities_over_accretion_efficiency = {}
            for quantity in self.list_of_quantities:
                self.quantities_over_accretion_efficiency[quantity] = {}
                for i, spin in enumerate(self.spin_values):
                    self.quantities_over_accretion_efficiency[quantity][spin] = np.transpose(self.quantities_over_FthetaphiSpin[quantity])[i][:]
            return

    def get_spectral_quantities_over_accretion_efficiency(self, inc_angle_deg):
        # Do NOT save spectral data to full pickle file. Separate out into inclination angle files,
        # otherwise it gets reaaally unwieldy.
        # NOTE: can only hold one inclination angle at a time. But easy to re-calculate
        self.get_spectral_data(inc_angle_deg)
        self.spectral_quantities_over_accretion_efficiency = {}
        self.spectral_quantities_over_accretion_efficiency["inc_angle"] = inc_angle_deg
        for quantity in self.list_of_spectral_quantities:
            for spectral_type in self.list_of_spectral_type:
                full_quantity = "power_in_" + spectral_type + "_" + quantity
                self.spectral_quantities_over_accretion_efficiency[full_quantity] = {}
                for i, spin in enumerate(self.spin_values):
                    self.spectral_quantities_over_accretion_efficiency[full_quantity][spin] = np.transpose(self.spectral_data[full_quantity])[i][:]
        # -----------------------------------------------------------------------------------
        # Now, do the power-law fraction, defined in Dunn+ 2010 as LPL/(LPL + Ldisk)
        # where LPL is between 1 - 100 keV and Ldisk is between 1 eV and 100 keV.
        # Also do Remillard & McClintock 2006 definition, RM06 = LPL/(LPL + Ldisk)
        # where both luminosities are between 2 - 20 keV
        # -----------------------------------------------------------------------------------
        for full_quantity in ["PLF", "RM06"]:
            self.spectral_quantities_over_accretion_efficiency[full_quantity] = {}
            if full_quantity == "RM06":
                LPL = self.spectral_data["power_in_2keV-20keV_nonthermal"]
                Ldisk = self.spectral_data["power_in_2keV-20keV_disk"]
            elif full_quantity == "PLF":
                LPL = self.spectral_data["power_in_1keV-100keV_nonthermal"]
                Ldisk = self.spectral_data["power_in_1eV-100keV_disk"]
            PLF_over_FthetaphiSpin = LPL/(LPL + Ldisk)
            # Rearrange data
            for i, spin in enumerate(self.spin_values):
                self.spectral_quantities_over_accretion_efficiency[full_quantity][spin] = np.transpose(PLF_over_FthetaphiSpin)[i][:]
        return

    def get_spectral_quantities_over_inclination(self, inc_angles):
        inc_angles = np.array(inc_angles)
        self.spectral_quantities_over_inclination = {}

        for quantity in self.list_of_spectral_quantities:
            for spectral_type in self.list_of_spectral_type:
                # -----------------------------------------------------------------------------------
                # All quantities (not fractions)
                # -----------------------------------------------------------------------------------
                full_quantity = "power_in_" + spectral_type + "_" + quantity
                self.spectral_quantities_over_inclination[full_quantity + "_max"] = np.zeros((inc_angles.size, self.spin_values.size))
                self.spectral_quantities_over_inclination[full_quantity + "_min"] = np.zeros((inc_angles.size, self.spin_values.size))
                for Fthetaphi in self.Fthetaphi_values:
                    Fstr = "_Fthetaphi{:.2f}".format(Fthetaphi)
                    self.spectral_quantities_over_inclination[full_quantity + Fstr] = np.zeros((inc_angles.size, self.spin_values.size))

                for i, inc_angle in enumerate(inc_angles):
                    self.get_spectral_data(inc_angle)
                    q_over_FthetaphiSpin = self.spectral_data[full_quantity]

                    qmax_over_spin = q_over_FthetaphiSpin.max(axis=0)
                    qmin_over_spin = q_over_FthetaphiSpin.min(axis=0)
                    self.spectral_quantities_over_inclination[full_quantity + "_max"][i][:] = qmax_over_spin
                    self.spectral_quantities_over_inclination[full_quantity + "_min"][i][:] = qmin_over_spin
                    for j, Fthetaphi in enumerate(self.Fthetaphi_values):
                        Fstr = "_Fthetaphi{:.2f}".format(Fthetaphi)
                        self.spectral_quantities_over_inclination[full_quantity + Fstr][i][:] = q_over_FthetaphiSpin[j][:]

                # -----------------------------------------------------------------------------------
                # Now do fractions (so don't divide by quantities at different accretion efficiencies)
                # -----------------------------------------------------------------------------------
                other_quantities = (self.list_of_spectral_quantities).copy()
                other_quantities.remove(quantity)
                for other_quantity in other_quantities:
                    other_full_quantity = "power_in_" + spectral_type + "_" + other_quantity
                    fraction_name = "power_in_" + quantity + "_over_" + other_quantity + "_" + spectral_type
                    self.spectral_quantities_over_inclination[fraction_name + "_max"] = np.zeros((inc_angles.size, self.spin_values.size))
                    self.spectral_quantities_over_inclination[fraction_name + "_min"] = np.zeros((inc_angles.size, self.spin_values.size))
                    for Fthetaphi in self.Fthetaphi_values:
                        Fstr = "_Fthetaphi{:.2f}".format(Fthetaphi)
                        self.spectral_quantities_over_inclination[fraction_name + Fstr] = np.zeros((inc_angles.size, self.spin_values.size))

                    for i, inc_angle in enumerate(inc_angles):
                        self.get_spectral_data(inc_angle)
                        q_over_FthetaphiSpin = self.spectral_data[full_quantity]
                        q2_over_FthetaphiSpin = self.spectral_data[other_full_quantity]
                        Fthetaphi_values = self.spectral_data["Fthetaphi_values"]
                        q_over_q2 = q_over_FthetaphiSpin/q2_over_FthetaphiSpin
                        fraction_max_over_spin = q_over_q2.max(axis=0)
                        fraction_min_over_spin = q_over_q2.min(axis=0)

                        self.spectral_quantities_over_inclination[fraction_name + "_max"][i][:] = fraction_max_over_spin
                        self.spectral_quantities_over_inclination[fraction_name + "_min"][i][:] = fraction_min_over_spin
                        for j, Fthetaphi in enumerate(self.Fthetaphi_values):
                            Fstr = "_Fthetaphi{:.2f}".format(Fthetaphi)
                            self.spectral_quantities_over_inclination[fraction_name + Fstr][i][:] = q_over_q2[j]

        # -----------------------------------------------------------------------------------
        # Now, do the power-law fraction, defined in Dunn+ 2010 as LPL/(LPL + Ldisk)
        # where LPL is between 1 - 100 keV and Ldisk is between 1 eV and 100 keV.
        # Also do Remillard & McClintock 2006 definition, RM06 = LPL/(LPL + Ldisk)
        # where both luminosities are between 2 - 20 keV
        # -----------------------------------------------------------------------------------
        full_quantity = "PLF"
        full_quantity2 = "RM06"
        # Initialize arrays
        for val in ["_min", "_max"]:
            for quantity in [full_quantity, full_quantity2]:
                self.spectral_quantities_over_inclination[quantity + val] = np.zeros((inc_angles.size, self.spin_values.size))
        for j, Fthetaphi in enumerate(self.Fthetaphi_values):
            Fstr = "_Fthetaphi{:.2f}".format(Fthetaphi)
            self.spectral_quantities_over_inclination[full_quantity + Fstr] = np.zeros((inc_angles.size, self.spin_values.size))
            self.spectral_quantities_over_inclination[full_quantity2 + Fstr] = np.zeros((inc_angles.size, self.spin_values.size))

        # Re-arrange data
        for i, inc_angle in enumerate(inc_angles):
            self.get_spectral_data(inc_angle)
            LPL = self.spectral_data["power_in_1keV-100keV_nonthermal"]
            Ldisk = self.spectral_data["power_in_1eV-100keV_disk"]
            PLF_over_FthetaphiSpin = LPL/(LPL + Ldisk)

            PLF_max_over_spin = PLF_over_FthetaphiSpin.max(axis=0)
            PLF_min_over_spin = PLF_over_FthetaphiSpin.min(axis=0)
            self.spectral_quantities_over_inclination[full_quantity + "_max"][i][:] = PLF_max_over_spin
            self.spectral_quantities_over_inclination[full_quantity + "_min"][i][:] = PLF_min_over_spin

            for j, Fthetaphi in enumerate(self.Fthetaphi_values):
                Fstr = "_Fthetaphi{:.2f}".format(Fthetaphi)
                self.spectral_quantities_over_inclination[full_quantity + Fstr][i][:] = PLF_over_FthetaphiSpin[j][:]
            LPL = self.spectral_data["power_in_2keV-20keV_nonthermal"]
            Ldisk = self.spectral_data["power_in_2keV-20keV_disk"]
            PLF_over_FthetaphiSpin = LPL/(LPL + Ldisk)

            PLF_max_over_spin = PLF_over_FthetaphiSpin.max(axis=0)
            PLF_min_over_spin = PLF_over_FthetaphiSpin.min(axis=0)
            self.spectral_quantities_over_inclination[full_quantity2 + "_max"][i][:] = PLF_max_over_spin
            self.spectral_quantities_over_inclination[full_quantity2 + "_min"][i][:] = PLF_min_over_spin

            for j, Fthetaphi in enumerate(self.Fthetaphi_values):
                Fstr = "_Fthetaphi{:.2f}".format(Fthetaphi)
                self.spectral_quantities_over_inclination[full_quantity2 + Fstr][i][:] = PLF_over_FthetaphiSpin[j][:]
        return



    def get_labels(self):
        self.labels = {"accretion_efficiencies":r"Plunging region dissipation fraction $\Delta\epsilon$",
                       "rISCO":r"$r_{\rm ISCO}$",
                       "rEH":r"$r_{\rm EH}$",
                       "total_volume_heating_rates":r"$Q_+$",
                       "mean_infall_times":r"$t_{\rm infall}|_{\rm avg}$ [s]",
                       "std_infall_times":r"$t_{\rm infall}|_{\rm std} [s]$",
                       "inner_infall_times":r"$t_{\rm infall}|_{r_{\rm EH}}$ [s]",
                       "middle_infall_times":r"$t_{\rm infall}$ [s]",
                       "outer_infall_times":r"$t_{\rm infall}|_{r_{\rm ISCO}}$ [s]",
                       "mean_collision_times":r"$t_{\rm coll}^{ee}(\gamma_1)/t_{\rm infall}|_{\rm avg}$",
                       "std_collision_times":r"$t_{\rm coll}^{ee}(\gamma_1)/t_{\rm infall}|_{\rm std}$",
                       "inner_collision_times":r"$t_{\rm coll}^{ee}(\gamma_1)/t_{\rm infall}|_{r_{\rm EH}}$",
                       # "middle_collision_times":r"$t_{\rm coll}^{ee}(\gamma_1)/t_{\rm infall}|_{r_{\rm mid}}$",
                       "middle_collision_times":r"$t_{\rm coll}^{ee}(\gamma_1)/t_{\rm infall}$",
                       "outer_collision_times":r"$t_{\rm coll}^{ee}(\gamma_1)/t_{\rm infall}|_{r_{\rm ISCO}}$",
                       "mean_cooling_times":r"$t_{\rm cool}(\gamma_1)/t_{\rm infall}|_{\rm avg}$",
                       "std_cooling_times":r"$t_{\rm cool}(\gamma_1)/t_{\rm infall}|_{\rm std}$",
                       "inner_cooling_times":r"$t_{\rm cool}(\gamma_1)/t_{\rm infall}|_{r_{\rm EH}}$",
                       # "middle_cooling_times":r"$t_{\rm cool}(\gamma_1)/t_{\rm infall}|_{r_{\rm mid}}$",
                       "middle_cooling_times":r"$t_{\rm cool}(\gamma_1)/t_{\rm infall}$",
                       "outer_cooling_times":r"$t_{\rm cool}(\gamma_1)/t_{\rm infall}|_{r_{\rm ISCO}}$",
                       "mean_accel_times":r"$t_{\rm accel}/t_{\rm infall}|_{\rm avg}$",
                       "std_accel_times":r"$t_{\rm accel}/t_{\rm infall}|_{\rm std}$",
                       "inner_accel_times":r"$t_{\rm accel}/t_{\rm infall}|_{r_{\rm EH}}$",
                       # "middle_accel_times":r"$t_{\rm accel}/t_{\rm infall}|_{r_{\rm mid}}$",
                       "middle_accel_times":r"$t_{\rm accel}/t_{\rm infall}$",
                       "outer_accel_times":r"$t_{\rm accel}/t_{\rm infall}|_{r_{\rm ISCO}}$",
                       "mean_thermalization_times":r"$t_{\rm therm}^{ei}/t_{\rm infall}|_{\rm avg}$",
                       "std_thermalization_times":r"$t_{\rm therm}^{ei}/t_{\rm infall}|_{\rm std}$",
                       "inner_thermalization_times":r"$t_{\rm therm}^{ei}/t_{\rm infall}|_{r_{\rm EH}}$",
                       # "middle_thermalization_times":r"$t_{\rm therm}^{ei}/t_{\rm infall}|_{r_{\rm mid}}$",
                       "middle_thermalization_times":r"$t_{\rm therm}^{ei}/t_{\rm infall}$",
                       "outer_thermalization_times":r"$t_{\rm therm}^{ei}/t_{\rm infall}|_{r_{\rm ISCO}}$",
                       "decoupling_radius":r"$r_{\rm decoupling}/r_g$",
                       "decoupling_radius_fractional":r"$(r_{\rm ISCO} - r_{\rm decoupling})/\Delta r_{\rm PR}$",
                       "heating_in_decoupled":r"$Q_{\rm decoupled}/Q_{\rm tot}$",
                       "mean_temperatures":r"$T_e|_{\rm avg}$ K",
                       "std_temperatures":r"$T_e|_{\rm std}$ K",
                       "mean_amplification_factor":r"$A(\rho,T)|_{\rm avg}$",
                       "std_amplification_factor":r"$A(\rho,T)|_{\rm std}$"
        }
        self.filenames = {"accretion_efficiencies":"accretionEfficiencies",
                          "rISCO":"rISCO",
                          "rEH":"rEH",
                          "total_volume_heating_rates":"totalVolumeHeatingRate",
                          "mean_collision_times":"tcoll_mean",
                          "std_collision_times":"tcoll_std",
                          "inner_collision_times":"tcoll_rEH",
                          "middle_collision_times":"tcoll_rMiddle",
                          "outer_collision_times":"tcoll_rISCO",
                          "mean_cooling_times":"tcool_mean",
                          "std_cooling_times":"tcool_std",
                          "inner_cooling_times":"tcool_rEH",
                          "middle_cooling_times":"tcool_rMiddle",
                          "outer_cooling_times":"tcool_rISCO",
                          "mean_accel_times":"taccel_mean",
                          "std_accel_times":"taccel_std",
                          "inner_accel_times":"taccel_rEH",
                          "middle_accel_times":"taccel_rMiddle",
                          "outer_accel_times":"taccel_rISCO",
                          "mean_thermalization_times":"tTherm_mean",
                          "std_thermalization_times":"tTherm_std",
                          "inner_thermalization_times":"tTherm_rEH",
                          "middle_thermalization_times":"tTherm_rMiddle",
                          "outer_thermalization_times":"tTherm_rISCO",
                          "decoupling_radius":"decouplingRadius",
                          "decoupling_radius_fractional":"decouplingRadiusFractional",
                          "heating_in_decoupled":"heatingInDecoupled",
                          "mean_temperatures":"electronTemperature_mean",
                          "std_temperatures":"electronTemperature_std",
                          "mean_amplification_factor":"amplificationFactor_mean",
                          "std_amplification_factor":"amplificationFactor_std",
                          "nonthermal":"powerInPowerlaw",
                          "thermal":"powerInThermalPR",
                          "disk":"powerInDisk",
                          "PLF":"PLF",
                          "RM06":"PLF_RM06",
                          "":"",
                          "1eV-100keV": "_1eV-100keV",
                          "1keV": "_1keV",
                          "1keV-2keV": "_1keV-2keV",
                          "1keV-100keV": "_1keV-100keV",
                          "1keV-1MeV": "_1keV-1MeV",
                          "2keV-20keV": "_2keV-20keV",
                          "50keV": "_50keV"
        }

        self.limits = {"accretion_efficiencies":[0.0, None],
                       "spins":[0.0, 1.0],
                       "total_volume_heating_rates":[None, None],
                       "rISCO":[None, None],
                       "rEH":[None, None],
                        "mean_collision_times":[None, None],
                        "std_collision_times":[None, None],
                        "inner_collision_times":[None, None],
                        "middle_collision_times":[None, None],
                        "outer_collision_times":[None, None],
                        "mean_cooling_times":[None, None],
                        "std_cooling_times":[None, None],
                        "inner_cooling_times":[None, None],
                        "middle_cooling_times":[None, None],
                        "outer_cooling_times":[None, None],
                        "mean_accel_times":[None, None],
                        "std_accel_times":[None, None],
                        "inner_accel_times":[None, None],
                        "middle_accel_times":[None, None],
                        "outer_accel_times":[None, None],
                        "mean_thermalization_times":[None, None],
                        "std_thermalization_times":[None, None],
                        "inner_thermalization_times":[None, None],
                        "middle_thermalization_times":[None, None],
                        "outer_thermalization_times":[None, None],
                        "decoupling_radius":[None, None],
                        "decoupling_radius_fractional":[None, None],
                        "heating_in_decoupled":[0.0, 1.0],
                        "mean_temperatures":[None, None],
                        "std_temperatures":[None, None],
                        "mean_amplification_factor":[None, None],
                        "std_amplification_factor":[None, None]
        }
        self.logscale = {"accretion_efficiencies":False,
                          "total_volume_heating_rates":False,
                         "rISCO":False,
                         "rEH":False,
                          "mean_collision_times":True,
                          "std_collision_times":False,
                          "inner_collision_times":False,
                          "middle_collision_times":True,
                          "outer_collision_times":False,
                          "mean_cooling_times":False,
                          "std_cooling_times":False,
                          "inner_cooling_times":False,
                          "middle_cooling_times":False,
                          "outer_cooling_times":True,
                          "mean_accel_times":True,
                          "std_accel_times":False,
                          "inner_accel_times":False,
                          "middle_accel_times":True,
                          "outer_accel_times":True,
                          "mean_thermalization_times":False,
                          "std_thermalization_times":False,
                          "inner_thermalization_times":False,
                          "middle_thermalization_times":True,
                          "outer_thermalization_times":False,
                          "decoupling_radius":False,
                          "decoupling_radius_fractional":False,
                          "heating_in_decoupled":False,
                          "mean_temperatures":True,
                          "std_temperatures":False,
                          "mean_amplification_factor":False,
                          "std_amplification_factor":False
        }
        self.spectral_labels = {"nonthermal":r"$L_{\rm PL}$",
                               "thermal":r"$L_{\rm MJ}$",
                               "disk":r"$L_{\rm disk}$",
                                "PLF":r"Power-law Fraction $\mathcal{L}_{\rm PL}/(\mathcal{L}_{\rm PL}+\mathcal{L}_{\rm disk})$",
                                "RM06":r"$PLF({\rm 2 keV}<\nu<{\rm 20 keV})$",
                                "":"",
                                "1eV-100keV":r"$({\rm 1eV} <\nu<{\rm 100keV})$",
                                "1keV":r"$({\rm 1keV} <\nu)$",
                                "1keV-2keV":r"$({\rm 1keV} <\nu<{\rm 2keV})$",
                                "1keV-100keV":r"$({\rm 1keV} <\nu<{\rm 100keV})$",
                                "1keV-1MeV":r"$({\rm 1keV} <\nu<{\rm 1MeV})$",
                                "2keV-20keV":r"$({\rm 2keV} <\nu<{\rm 20keV})$",
                                "50keV":r"$({\rm 50keV} <\nu)$"
                               # "Xray_":r"$({\rm 1keV} < \nu)$",
                               # "Xray_nopair_":r"$({\rm 1keV} < \nu < {\rm 1MeV})$",
                               # "soft_Xray_":r"$({\rm 2keV} < \nu < {\rm 20keV})$",
        }

        self.list_of_spectral_quantities = [
            "nonthermal",
            "thermal",
            "disk"
        ]

        self.list_of_spectral_type = ["", "1eV-100keV",
                                      "1keV", "1keV-2keV", "1keV-100keV", "1keV-1MeV",
                                      "2keV-20keV",
                                      "50keV"]
        # self.list_of_spectral_type = ["Xray_", "Xray_nopair_", "", "soft_Xray_"]
        self.list_of_special_quantites = ["half_light_radius", "tenth_light_radius"]
        self.list_of_quantities = list(self.labels.keys())

        return

    def get_category_values(self):
        category = self.category
        if category == "test":
            self.Fthetaphi_values = np.array([6.0, 2.0])
            self.spin_values = np.array([0.95, 0.5])
            self.contour = True
        if category == "sparse1":
            self.Fthetaphi_values = np.array([6.0, 5.0, 4.0, 3.0, 2.0, 1.0])
            self.spin_values = np.array([0.95, 0.9, 0.8, 0.7, 0.6, 0.5])
            self.contour = True
        if category == "sparse2":
            self.Fthetaphi_values = np.array([6.0, 5.0, 4.0, 3.0, 2.0])
            self.spin_values = np.array([0.95, 0.9, 0.85, 0.8, 0.75, 0.7])
            self.contour = True
        if category == "sparse3":
            self.Fthetaphi_values = np.array([6.0, 5.0, 4.0, 3.0])
            self.spin_values = np.array([0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65])
            self.contour = True
        if category == "sparse3spin4":
            self.Fthetaphi_values = np.array([6.0, 5.0, 4.0, 3.0, 2.0])
            self.spin_values = np.array([0.95, 0.85, 0.7, 0.5])
            self.contour = True
        if category == "sparse3spin3":
            self.Fthetaphi_values = np.array([6.0, 5.0, 4.0, 3.0, 2.0])
            self.spin_values = np.array([0.95, 0.75, 0.5])
            self.contour = True
        if category == "sparse3spin2":
            self.Fthetaphi_values = np.array([6.0, 5.0, 4.0, 3.0, 2.0])
            self.spin_values = np.array([0.95, 0.5])
            self.contour = True
        if category == "sparse3spin5":
            self.Fthetaphi_values = np.array([6.0, 5.0, 4.0, 3.0, 2.0])
            self.spin_values = np.array([0.95, 0.85, 0.75, 0.65, 0.5])
            self.contour = True
        if category == "a0.85":
            self.Fthetaphi_values = np.array([6.0, 5.0, 4.0, 3.0, 2.0])
            self.spin_values = np.array([0.85])
            self.contour = False
        if category == "a0.95":
            self.Fthetaphi_values = np.array([6.0, 5.0, 4.0, 3.0, 2.0])
            self.spin_values = np.array([0.95])
            self.contour = False
        if category == "Fthetaphi1.00":
            self.Fthetaphi_values = np.array([1.0])
            self.spin_values = np.array([0.95, 0.9, 0.8, 0.7, 0.6, 0.5])
            self.contour = False
        if category == "Fthetaphi2.00":
            self.Fthetaphi_values = np.array([2.0])
            self.spin_values = np.array([0.95, 0.9, 0.8, 0.7, 0.6, 0.5])
            self.contour = False
        # Use for timescales plot.
        if category == "Fthetaphi6.00":
            self.Fthetaphi_values = np.array([6.0])
            # self.spin_values = np.array([0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.5, 0.4, 0.3])
            self.spin_values = np.array([0.95, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3])
            self.contour = False
        return

    def plot_quantities_line_over_accretion_efficiency(self):
        self.get_labels()
        self.get_quantities_over_accretion_efficiency()

        figdir = self.path_to_figures + "line_over_accretionEfficiency/"
        print("Saving quantities over accretion efficiency in " + figdir)
        if not os.path.exists(figdir):
            os.makedirs(figdir)
        if not os.path.exists(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        left_locs = [0.7, 0.5, 0.3, 0.1]
        width_locs = [0.4, 0.2, 0.2, 0.2]
        n = self.spin_values.size
        for quantity in self.list_of_quantities:
            plt.figure()
            for i, spin in enumerate(self.spin_values):
                accretion_efficiencies = self.quantities_over_accretion_efficiency["accretion_efficiencies"][spin]
                data = self.quantities_over_accretion_efficiency[quantity][spin]
                line, = plt.plot(accretion_efficiencies, data,
                                 marker='o', label=r"$a={:.2f}$".format(spin), color=cb_colors(n)[i])
                if quantity == "decoupling_radius":
                    rEHs = self.quantities_over_accretion_efficiency["rEH"][spin]
                    rISCOs = self.quantities_over_accretion_efficiency["rISCO"][spin]
                    double_check = (np.allclose(rEHs, rEHs[0])) and (np.allclose(rISCOs, rISCOs[0]))
                    if double_check:
                        # plt.gca().axhspan(rEHs[0], rISCOs[0], alpha=0.1, color=line.get_color())
                        left = left_locs[i]; width = width_locs[i]
                        bottom = rEHs[0]; height = rISCOs[0] - rEHs[0]
                        rect = plt.Rectangle((left, bottom), width, height, color=line.get_color(), alpha=0.1)
                        plt.gca().add_patch(rect)
                    else:
                        print("EHs and ISCOs not the same for the same spin:")
                        print(rEHs)
                        print(rISCOs)
            plt.legend(frameon=False)
            plt.gca().tick_params(top=True, right=True, direction='in', which='both')
            plt.gca().grid(color='.9', ls='--')
            plt.xlim(self.limits["accretion_efficiencies"])
            plt.ylim(self.limits[quantity])
            plt.xlabel(self.labels["accretion_efficiencies"])
            plt.ylabel(self.labels[quantity])
            if self.logscale[quantity]: plt.yscale('log')
            plt.tight_layout()
            figname = self.filenames[quantity] + ".png"
            plt.savefig(figdir + figname)
            plt.title('')
            plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"))
            plt.close()
        return


    def plot_spectrum_fractions_line_over_accretion_efficiency(self, inc_angle, **kwargs):
        title = kwargs.get("title", True)
        figdir = self.path_to_figures + "line_over_accretionEfficiency/i{:d}/".format(inc_angle)
        figdir = kwargs.get("figdir", figdir)
        spectral_quantities = kwargs.get("spectral_quantities", self.list_of_spectral_quantities)
        spectral_types = kwargs.get("spectral_types", self.list_of_spectral_type)

        self.get_spectral_quantities_over_accretion_efficiency(inc_angle)
        print("Saving spectral fractions over accretion efficiency in " + figdir)
        if not os.path.exists(figdir):
            os.makedirs(figdir)
        if not os.path.exists(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")

        n = self.spin_values.size
        for quantity in spectral_quantities:
            for spectral_type in spectral_types:
                other_quantities = (self.list_of_spectral_quantities).copy()
                other_quantities.remove(quantity)
                for other_quantity in other_quantities:
                    plt.figure()
                    for i, spin in enumerate(self.spin_values):
                        accretion_efficiencies = self.quantities_over_accretion_efficiency["accretion_efficiencies"][spin]
                        full_quantity = "power_in_" + spectral_type + "_" + quantity
                        other_full_quantity = "power_in_" + spectral_type + "_" + other_quantity
                        data = self.spectral_quantities_over_accretion_efficiency[full_quantity][spin]
                        other_data = self.spectral_quantities_over_accretion_efficiency[other_full_quantity][spin]
                        line, = plt.plot(accretion_efficiencies, data/other_data,
                                         marker='o', label=r"$a={:.2f}$".format(spin),
                                         color=cb_colors(n)[i])
                    plt.legend(frameon=False)
                    if "nonthermal" in quantity and "thermal" in other_quantity:
                        ylims = plt.gca().get_ylim()
                        plt.gca().axhspan(ylims[0], 1.0, color='gray', alpha=0.3, hatch='/')
                    if "thermal" in quantity and "nonthermal" in other_quantity:
                        ylims = plt.gca().get_ylim()
                        plt.gca().axhspan(1.0, ylims[1], color='gray', alpha=0.3, hatch='/')
                    if "nonthermal" in quantity and "disk" in other_quantity:
                        ylims = plt.gca().get_ylim()
                        plt.gca().axhspan(ylims[0], 1e-2, color='gray', alpha=0.3, hatch='/')
                    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
                    plt.gca().grid(color='.9', ls='--')
                    plt.xlim(self.limits["accretion_efficiencies"])
                    # plt.ylim(self.limits[quantity])
                    plt.xlabel(self.labels["accretion_efficiencies"])
                    ylabel = self.spectral_labels[quantity] + r"$/$" + self.spectral_labels[other_quantity] + self.spectral_labels[spectral_type]
                    plt.ylabel(ylabel)
                    plt.title(r"$i={:d}^\circ$".format(inc_angle))
                    plt.yscale('log')
                    plt.tight_layout()
                    figname = "fraction_" + self.filenames[quantity] + "_over_" + self.filenames[other_quantity] + self.filenames[spectral_type] + "_i{:d}.png".format(inc_angle)
                    plt.savefig(figdir + figname)
                    plt.title('')
                    plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"))
                    plt.close()
        # -----------------------------------------------------------------------------------
        # Now, do the power-law fraction, defined in Dunn+ 2010 as LPL/(LPL + Ldisk)
        # where LPL is between 1 - 100 keV and Ldisk is between 1 eV and 100 keV.
        # -----------------------------------------------------------------------------------
        objects_to_annotate = kwargs.get("objects_to_annotate", [])
        for full_quantity in ["PLF", "RM06"]:
            all_ae = np.array([])
            all_quantities = np.array([])
            all_spin = np.array([])
            plt.figure()
            for i, spin in enumerate(self.spin_values):
                accretion_efficiencies = self.quantities_over_accretion_efficiency["accretion_efficiencies"][spin]
                data = self.spectral_quantities_over_accretion_efficiency[full_quantity][spin]
                line, = plt.plot(accretion_efficiencies, data,
                                    marker='o', label=r"$a={:.2f}$".format(spin),
                                    color=cb_colors(n)[i])
                # Prep for interp
                all_ae = np.append(all_ae, accretion_efficiencies)
                all_quantities = np.append(all_quantities, data)
                all_spin = np.append(all_spin, spin*np.ones(data.shape))
            plt.legend(frameon=False)
            plt.gca().tick_params(top=True, right=True, direction='in', which='both')
            plt.gca().grid(color='.9', ls='--')
            plt.xlabel(self.labels["accretion_efficiencies"])
            ylabel = self.spectral_labels[full_quantity]
            plt.ylim([1e-4, 1])
            plt.ylabel(ylabel)
            plt.yscale('log')
            if objects_to_annotate:
                # Remillard & McClintock 2006 defines the soft state as having a disk fraction > 75%
                # in the 2 - 20 keV band. No definition on the 1 - 100 keV...
                # plt.gca().axhline([0.25], ls='--', color='black')
                # Interpolate spin values.
                interp_f = interpolate.interp2d(all_spin, all_quantities, all_ae)
                for system in objects_to_annotate:
                    hatch = AstroObjects[system]["hatch"]
                    color = AstroObjects[system]["color"]
                    max_inc = AstroObjects[system]["max_inc"]
                    min_inc = AstroObjects[system]["min_inc"]
                    if (max_inc + 5 < inc_angle) or (min_inc - 5 > inc_angle):
                        print("Note that " + system + "'s inclination angle doesn't match with this figure's.")
                        print("Skipping.")
                        continue
                    max_spin = AstroObjects[system]["max_spin"]
                    min_spin = AstroObjects[system]["min_spin"]
                    max_PLF = AstroObjects[system]["max_" + full_quantity]
                    min_PLF = AstroObjects[system]["min_" + full_quantity]
                    height = max_PLF - min_PLF
                    # Interpolate location of ae for bottom (min PLF, min spin)
                    bottom_left_ae = interp_f(min_spin, min_PLF)
                    bottom_right_ae = interp_f(max_spin, min_PLF)
                    top_left_ae = interp_f(min_spin, max_PLF)
                    top_right_ae = interp_f(max_spin, max_PLF)
                    width = top_right_ae - top_left_ae
                    vertices = [[bottom_left_ae, min_PLF], [bottom_right_ae, min_PLF],
                                [top_right_ae, max_PLF], [top_left_ae, max_PLF]]
                    polygon = patches.Polygon(vertices, color=color, alpha=0.1)
                    plt.gca().add_patch(polygon)
                    # plt.gca().annotate(system, xy=(bottom_right_ae, min_PLF*1.06), rotation=45)
                    if system == "GX339":
                        plt.gca().text(top_left_ae*1.4, min_PLF*1.0, system)
                    else:
                        plt.gca().text(bottom_right_ae, min_PLF*1.06, system, rotation=45)
                    # plt.gca().text(bottom_right_ae, min_PLF*1.06, system, rotation=45, bbox=dict(boxstyle='round', edgecolor=color, fill=False, alpha=0.1))

                    # Make an estimate of accretion efficiency from PLF, i.e. horizontal band
                    # plt.gca().axhspan(min_PLF, max_PLF, alpha=0.1, color=color)
                    # plt.gca().annotate(system, xy=(plt.gca().get_xlim()[-1]*0.95, min_PLF*1.06), rotation=270)
                    # Fill between interpolated spots
            if title:
                plt.title(r"$i={:d}^\circ$".format(inc_angle))
            plt.tight_layout()
            figname = "fraction_" + self.filenames[full_quantity] + ".png"
            plt.savefig(figdir + figname)
            plt.title('')
            plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"))
            plt.close()
        return

    def plot_spectral_quantities_line_over_inclination(self, inc_angles, **kwargs):
        normalize = kwargs.get("normalize", False)
        inc_angles = np.array(inc_angles)
        self.get_spectral_quantities_over_inclination(inc_angles)
        inc_values = ["min_over_Fthetaphi", "max_over_Fthetaphi"]
        for i, Fthetaphi in enumerate(self.Fthetaphi_values):
            inc_values.append("Fthetaphi{:.2f}_value".format(Fthetaphi))
        inc_values = kwargs.get("inc_values", inc_values)
        spectral_types = kwargs.get("spectral_types", self.list_of_spectral_type)
        quantities = kwargs.get("quantities", self.list_of_spectral_quantities)
        title = kwargs.get("title", True)
        figdir = self.path_to_figures + "line_over_inclination/"
        figdir = kwargs.get("figdir", figdir)
        legend_loc = kwargs.get("legend_loc", "best")
        print("Saving spectral quantities over inclination in " + figdir)
        if not os.path.exists(figdir):
            os.makedirs(figdir)
        for value in inc_values:
            val_path = figdir + value + "/"
            if not os.path.exists(val_path):
                os.makedirs(val_path)
            if not os.path.exists(val_path + "pdfs/"):
                os.makedirs(val_path + "pdfs/")
        n = self.spin_values.size
        for quantity in quantities:
            for spectral_type in spectral_types:
                for value in inc_values:
                    figdir_val = figdir + value + "/"
                    plt.figure()
                    for i, spin in enumerate(self.spin_values):
                        val = value.split("_")[0]
                        full_quantity = "power_in_" + spectral_type + "_" + quantity + "_" + val
                        data = np.transpose(self.spectral_quantities_over_inclination[full_quantity])[i][:]
                        if normalize:
                            data = data/data[0]
                        line, = plt.plot(inc_angles, data,
                                        marker='o', label=r"$a={:.2f}$".format(spin),
                                        color=cb_colors(n)[i])
                    plt.legend(frameon=False, loc=legend_loc)
                    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
                    plt.gca().grid(color='.9', ls='--')
                    # plt.xlim(self.limits["accretion_efficiencies"])
                    # plt.ylim(self.limits[quantity])
                    plt.xlabel("Inclination angle $i$ ($^\circ$)")
                    ylabel = self.spectral_labels[quantity] + self.spectral_labels[spectral_type]
                    if normalize:
                        ylabel += r"$/L(i=0^\circ)$"
                    else:
                        ylabel += " erg/s"
                    objects_to_annotate = kwargs.get("objects_to_annotate", [])
                    cmap = pl.cm.viridis_r
                    vmin = self.spin_values.min()
                    vmax = self.spin_values.max()
                    normed_spins = colors.Normalize(vmin=vmin, vmax=vmax)
                    for system in objects_to_annotate:
                        if "min_" + spectral_type in AstroObjects[system]:
                            max_inc = AstroObjects[system]["max_inc"]
                            min_inc = AstroObjects[system]["min_inc"]
                            max_spin = AstroObjects[system]["max_spin"]
                            min_spin = AstroObjects[system]["min_spin"]
                            color = AstroObjects[system]["color"]
                            min_power = AstroObjects[system]["min_" + spectral_type]
                            max_power = AstroObjects[system]["max_" + spectral_type]
                            max_spin_color = cmap(normed_spins(max_spin))
                            min_spin_color = cmap(normed_spins(min_spin))
                            starting_coord = (min_inc, min_power)
                            width = max_inc - min_inc
                            height = max_power - min_power
                            plt.gca().add_patch(patches.Rectangle(starting_coord, width, height, alpha=0.3, color=min_spin_color, lw=0.5))
                            plt.gca().annotate(system, xy=(min_inc, max_power))
                    plt.ylabel(ylabel)
                    plt.yscale('log')
                    if title:
                        plt.title(value.replace("_", " "))
                    plt.tight_layout()
                    figname = self.filenames[quantity] + self.filenames[spectral_type]
                    if normalize:
                        figname += "_normToI0"
                    figname += ".png"
                    plt.savefig(figdir_val + figname)
                    plt.title('')
                    plt.savefig(figdir_val + "pdfs/" + figname.replace(".png", ".pdf"))
                    plt.close()
        return

    def plot_spectral_fractions_line_over_inclination(self, inc_angles, **kwargs):
        inc_angles = np.array(inc_angles)
        self.get_spectral_quantities_over_inclination(inc_angles)
        figdir = self.path_to_figures + "line_over_inclination/"
        figdir = kwargs.get("figdir", figdir)
        dpi = kwargs.get("dpi", 150)
        figdir_val = kwargs.get("figdir_val", None)
        annotation_locs = kwargs.get("annotation_locs", None)
        annotation_names = kwargs.get("annotation_names", None)
        title = kwargs.get("title", True)
        ylims = kwargs.get("ylims", None)
        # fignames = None uses the default. fignames must be a dictionary with entries for each quantity
        fignames = kwargs.get("fignames", None)
        print("Saving spectral fractions over inclination in " + figdir)
        if not os.path.exists(figdir):
            os.makedirs(figdir)
        inc_values = ["min_over_Fthetaphi", "max_over_Fthetaphi"]
        for i, Fthetaphi in enumerate(self.Fthetaphi_values):
            inc_values.append("Fthetaphi{:.2f}_value".format(Fthetaphi))
        inc_values = kwargs.get("inc_values", inc_values)
        spectral_quantities = kwargs.get("spectral_quantities", self.list_of_spectral_quantities)
        pdf_replace_str = kwargs.get("pdf_replace_str", None)
        spectral_types = kwargs.get("spectral_types", self.list_of_spectral_type)
        ratio_types = kwargs.get("ratio_types", ["PLF", "RM06"])
        legend_loc = kwargs.get("legend_loc", "best")

        # Because the individual spectral quantities max over accretion efficiency
        # and PL and thermal PR have different trends with accretion efficiency,
        # must take directly from data rather than retroactively calculating.
        n = self.spin_values.size
        for quantity in spectral_quantities:
            for spectral_type in spectral_types:
                other_quantities = (self.list_of_spectral_quantities).copy()
                other_quantities.remove(quantity)
                for other_quantity in other_quantities:
                    other_full_quantity = "power_in_" + spectral_type + "_" + other_quantity
                    fraction_name = "power_in_" + quantity + "_over_" + other_quantity + "_" + spectral_type
                    # print(self.spectral_quantities_over_inclination.keys())
                    for value in inc_values:
                        plt.figure()
                        for i, spin in enumerate(self.spin_values):
                            val = value.split("_")[0]
                            data = np.transpose(self.spectral_quantities_over_inclination[fraction_name + "_" + val])[i][:]
                            line, = plt.plot(inc_angles, data,
                                             marker='o', label=r"$a={:.2f}$".format(spin), color=cb_colors(n)[i],
                                             path_effects=[mpe.Stroke(linewidth=1.5, foreground='k'), mpe.Normal()])
                            # min_data = np.transpose(self.spectral_quantities_over_inclination[fraction_name + "_min"])[i][:]
                            # plt.fill_between(inc_angles, min_data, data, alpha=0.1, color=colorblind_colors[i])
                            # if spin == 0.5 and fraction_name == "power_in_nonthermal_over_disk_":
                                # print(data)
                            if fraction_name == "power_in_nonthermal_over_disk_1keV-1MeV":
                                print(spin)
                                print(data)
                        plt.legend(frameon=False, loc=legend_loc)
                        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
                        plt.gca().grid(color='.9', ls='--')
                        # plt.xlim(self.limits["accretion_efficiencies"])
                        # plt.ylim(self.limits[quantity])
                        plt.xlabel("Inclination angle $i$ ($^\circ$)")
                        ylabel = self.spectral_labels[quantity] + r"$/$" + self.spectral_labels[other_quantity] + self.spectral_labels[spectral_type]
                        plt.ylabel(ylabel)
                        plt.yscale('log')
                        if title:
                            plt.title(value.replace("_", " "))
                        plt.tight_layout()
                        if fignames is None:
                            figname = "fraction_" + self.filenames[quantity] + "_over_" + self.filenames[other_quantity] + self.filenames[spectral_type] + ".png"
                        else:
                            figname = fignames[quantity + "_" + spectral_type]
                        if figdir_val is None:
                            figdir_val = figdir + value + "/"
                            if not os.path.exists(figdir_val):
                                os.makedirs(figdir_val)
                            if not os.path.exists(figdir_val + "pdfs/"):
                                os.makedirs(figdir_val + "pdfs/")
                        plt.savefig(figdir_val + figname, dpi=dpi)
                        plt.title('')
                        if pdf_replace_str:
                            plt.savefig(figdir_val + "pdfs/" + figname.replace(pdf_replace_str, ".pdf"), bbox_inches='tight', dpi=dpi)
                        else:
                            plt.savefig(figdir_val + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
                        plt.close()

        # -----------------------------------------------------------------------------------
        # Now, do the power-law fraction, defined in Dunn+ 2010 as LPL/(LPL + Ldisk)
        # where LPL is between 1 - 100 keV and Ldisk is between 1 eV and 100 keV.
        # -----------------------------------------------------------------------------------
        objects_to_annotate = kwargs.get("objects_to_annotate", [])
        for full_quantity in ratio_types:
            for value in inc_values:
                plt.figure()
                for i, spin in enumerate(self.spin_values):
                    val = value.split("_")[0]
                    data = np.transpose(self.spectral_quantities_over_inclination[full_quantity + "_" + val])[i][:]
                    line, = plt.plot(inc_angles, data, lw=2.0,
                                    marker='o', label=r"$a={:.2f}$".format(spin), color=cb_colors(n)[i],
                                             path_effects=[mpe.Stroke(linewidth=2.5, foreground='k', alpha=0.8), mpe.Normal()])
                    print(spin)
                    print(data)
                # plt.legend(frameon=False, loc='lower right')
                # plt.legend(frameon=False, loc='best')
                plt.legend(frameon=False, loc=legend_loc)
                plt.gca().tick_params(top=True, right=True, direction='in', which='both')
                plt.gca().grid(color='.9', ls='--')
                plt.xlabel("Inclination angle $i$ ($^\circ$)")
                plt.xlim([inc_angles.min(), inc_angles.max()])
                ylabel = self.spectral_labels[full_quantity]
                plt.ylabel(ylabel)
                plt.yscale('log')
                print(ylims)
                if ylims is None:
                    plt.ylim([1e-3, 1e0])
                else:
                    plt.ylim(ylims)
                if objects_to_annotate:
                    # Remillard & McClintock 2006 defines the soft state as having a disk fraction > 75%
                    # in the 2 - 20 keV band. No definition on the 1 - 100 keV...
                    # plt.gca().axhline([0.25], ls='--', color='black')
                    # Dunn+ 2010 defines a "disk-dominated state" as having a PLF < 0.2
                    # plt.gca().axhline([0.20], ls='--', color='black')
                    cmap = pl.cm.viridis_r
                    vmin = self.spin_values.min()
                    vmax = self.spin_values.max()
                    normed_spins = colors.Normalize(vmin=vmin, vmax=vmax)
                    for system in objects_to_annotate:
                        hatch = AstroObjects[system]["hatch"]
                        color = AstroObjects[system]["color"]
                        max_inc = AstroObjects[system]["max_inc"]
                        min_inc = AstroObjects[system]["min_inc"]
                        max_spin = AstroObjects[system]["max_spin"]
                        min_spin = AstroObjects[system]["min_spin"]
                        max_PLF = AstroObjects[system]["max_" + full_quantity]
                        min_PLF = AstroObjects[system]["min_" + full_quantity]
                        width = max_inc - min_inc
                        height = max_PLF - min_PLF
                        min_starting_coord = (min_inc, min_PLF)
                        min_spin_color = cmap(normed_spins(min_spin))
                        max_spin_color = cmap(normed_spins(max_spin))
                        max_starting_coord = (min_inc, min_PLF + height/2.0)
                        # plt.gca().add_patch(patches.Rectangle(starting_coord, width, height, alpha=0.3, hatch=hatch))
                        # plt.gca().add_patch(patches.Rectangle(min_starting_coord, width, height/2.0, alpha=0.3, color=min_spin_color, lw=0))
                        # plt.gca().add_patch(patches.Rectangle(max_starting_coord, width, height/2.0, alpha=0.3, color=max_spin_color, lw=0))
                        # plt.gca().add_patch(patches.Rectangle(min_starting_coord, width, height, alpha=0.3, edgecolor='black', fill=False, ls=':', lw=0.5))
                        # if system == "J1550":
                            # plt.gca().add_patch(patches.Rectangle(min_starting_coord, width, height, alpha=0.3, color=max_spin_color, lw=0.5))
                        # else:
                        # plt.gca().add_patch(patches.Rectangle(min_starting_coord, width, height, alpha=0.3, color=max_spin_color, lw=0.5))
                        plt.gca().add_patch(patches.Rectangle(min_starting_coord, width, height, alpha=0.3, color=min_spin_color, lw=0.5))
                        if annotation_locs is None or system not in annotation_locs:
                            text_loc = (min_inc, min_PLF + height)
                        else:
                            text_loc = annotation_locs[system]
                        if annotation_names is None or system not in annotation_names:
                            text = system
                        else:
                            text = annotation_names[system]
                        plt.gca().annotate(text, xy=text_loc)
                if title:
                    plt.title(value.replace("_", " "))
                plt.tight_layout()
                if fignames is None:
                    figname = "fraction_" + self.filenames[full_quantity] + ".png"
                else:
                    figname = fignames[full_quantity]
                if figdir_val is None:
                    figdir_val = figdir + value + "/"
                    if not os.path.exists(figdir_val):
                        os.makedirs(figdir_val)
                    if not os.path.exists(figdir_val + "pdfs/"):
                        os.makedirs(figdir_val + "pdfs/")
                plt.savefig(figdir_val + figname, dpi=dpi)
                plt.title('')
                if pdf_replace_str:
                    plt.savefig(figdir_val + "pdfs/" + figname.replace(pdf_replace_str, ".pdf"), bbox_inches='tight', dpi=dpi)
                else:
                    plt.savefig(figdir_val + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
                plt.close()
        return

    def plot_spectrum_quantities_line_over_accretion_efficiency(self, inc_angle):
        self.get_spectral_quantities_over_accretion_efficiency(inc_angle)
        figdir = self.path_to_figures + "line_over_accretionEfficiency/i{:d}/".format(inc_angle)
        dpi = kwargs.get("dpi", 150)
        print("Saving spectral quantities over accretion efficiency in " + figdir)
        if not os.path.exists(figdir):
            os.makedirs(figdir)
        if not os.path.exists(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        n = self.spin_values.size
        for quantity in self.list_of_spectral_quantities:
            for spectral_type in self.list_of_spectral_type:
                plt.figure()
                for i, spin in enumerate(self.spin_values):
                    accretion_efficiencies = self.quantities_over_accretion_efficiency["accretion_efficiencies"][spin]
                    full_quantity = "power_in_" + spectral_type + "_" + quantity
                    data = self.spectral_quantities_over_accretion_efficiency[full_quantity][spin]
                    line, = plt.plot(accretion_efficiencies, data,
                                     marker='o', label=r"$a={:.2f}$".format(spin),
                                     color=cb_colors(n)[i])
                    # if full_quantity == "power_in_1keV-1MeV_nonthermal":
                        # print(spin)
                        # print(data)
                plt.legend(frameon=False)
                plt.gca().tick_params(top=True, right=True, direction='in', which='both')
                plt.gca().grid(color='.9', ls='--')
                plt.xlim(self.limits["accretion_efficiencies"])
                # plt.ylim(self.limits[quantity])
                plt.xlabel(self.labels["accretion_efficiencies"])
                ylabel = self.spectral_labels[quantity] + self.spectral_labels[spectral_type] + " erg/s"
                plt.ylabel(ylabel)
                plt.yscale('log')
                plt.title(r"$i={:d}^\circ$".format(inc_angle))
                plt.tight_layout()
                figname = self.filenames[quantity] + self.filenames[spectral_type] + "_i{:d}.png".format(inc_angle)
                plt.savefig(figdir + figname, dpi=dpi)
                plt.title('')
                plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"))
                plt.close()
        return

    def plot_timescales_line_over_spin(self, **kwargs):
        self.get_labels()

        pdf_replace_str = kwargs.get("pdf_replace_str", None)
        regionStrs = kwargs.get("regionStrs", ["inner", "middle", "outer", "mean"])
        spins = kwargs.get("spins", self.spin_values)
        Fthetaphi = kwargs.get("Fthetaphi", self.Fthetaphi_values.max())
        # fignames = None uses the default. fignames must be a dictionary with entries for each spin
        fignames = kwargs.get("fignames", None)

        ind = np.where(self.Fthetaphi_values == Fthetaphi)[0][0]

        figdir = self.path_to_figures + "line_over_spin/"
        figdir = kwargs.get("figdir", figdir)
        print("Saving timescales over spin in " + figdir)
        if not os.path.exists(figdir):
            os.makedirs(figdir)
        if not os.path.exists(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        timescale_quantities = ["_thermalization_times",
                                "_collision_times",
                                "_cooling_times",
                                "_accel_times"]
        n = len(timescale_quantities)
        for regionStr in regionStrs:
            plt.figure()
            for i, quantity in enumerate(timescale_quantities):
                data = self.quantities_over_FthetaphiSpin[regionStr + quantity][ind, :]
                label_str = self.labels[regionStr + quantity]
                if quantity == "_accel_times":
                    data = data*100
                    label_str = r"$100\times $" + label_str
                plt.plot(spins, data, marker='o', label=label_str, color=cb_colors2(n+1)[i])
            plt.legend(frameon=False)
            plt.gca().tick_params(top=True, right=True, direction='in', which='both')
            plt.gca().grid(color='.9', ls='--')
            plt.xlim(self.limits["spins"])
            plt.xlabel(r"$a$")
            plt.gca().axhline([1.0], color='black', ls='--')
            plt.yscale('log')
            plt.title(r"$F_{{\theta\phi}}={:.2f}$".format(Fthetaphi))
            plt.tight_layout()
            if fignames is None:
                figname = "timescales_" + regionStr + "_Fthetaphi{:.2f}.png".format(Fthetaphi)
            else:
                figname = fignames[spin]
            plt.savefig(figdir + figname)
            plt.title('')
            if pdf_replace_str:
                plt.savefig(figdir + "pdfs/" + figname.replace(pdf_replace_str, ".pdf"))
            else:
                plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"))
            plt.close()

    def plot_timescales_line_over_accretion_efficiency(self, **kwargs):
        self.get_labels()
        self.get_quantities_over_accretion_efficiency()

        pdf_replace_str = kwargs.get("pdf_replace_str", None)
        regionStrs = kwargs.get("regionStrs", ["inner", "middle", "outer", "mean"])
        spins = kwargs.get("spins", self.spin_values)
        grid = kwargs.get("grid", True)
        dpi = kwargs.get("dpi", 150)
        # fignames = None uses the default. fignames must be a dictionary with entries for each spin
        fignames = kwargs.get("fignames", None)

        figdir = self.path_to_figures + "line_over_accretionEfficiency/"
        figdir = kwargs.get("figdir", figdir)
        print("Saving timescales over accretion efficiency in " + figdir)
        if not os.path.exists(figdir):
            os.makedirs(figdir)
        if not os.path.exists(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")
        timescale_quantities = ["_thermalization_times",
                                "_collision_times",
                                "_cooling_times",
                                "_accel_times"]
        n = len(timescale_quantities)
        for spin in spins:
            for regionStr in regionStrs:
                plt.figure()
                for i, quantity in enumerate(timescale_quantities):
                    accretion_efficiencies = self.quantities_over_accretion_efficiency["accretion_efficiencies"][spin]
                    data = self.quantities_over_accretion_efficiency[regionStr + quantity][spin]
                    label_str = self.labels[regionStr + quantity]
                    if quantity == "_accel_times":
                        data = data*100
                        label_str = r"$100\times $" + label_str
                    if regionStr == "mean":
                        std = self.quantities_over_accretion_efficiency["std" + quantity][spin]
                        # plt.fill_between(accretion_efficiencies, data - std, data + std, alpha=0.5)
                        plt.fill_between(accretion_efficiencies, data, data + std, alpha=0.5, color=cb_colors2(n+1)[i])
                    plt.plot(accretion_efficiencies, data, marker='o', label=label_str, color=cb_colors2(n+1)[i])
                plt.legend(frameon=False)
                plt.gca().tick_params(top=True, right=True, direction='in', which='both')
                if grid:
                    plt.gca().grid(color='.9', ls='--')
                plt.xlim(self.limits["accretion_efficiencies"])
                plt.xlabel(self.labels["accretion_efficiencies"])
                plt.gca().axhline([1.0], color='black', ls='--')
                plt.yscale('log')
                plt.ylabel("Timescales compared to infall time")
                plt.title(r"$a={:.2f}$".format(spin))
                plt.tight_layout()
                if fignames is None:
                    figname = "timescales_" + regionStr + "_a{:.2f}.png".format(spin)
                else:
                    figname = fignames[spin]
                plt.savefig(figdir + figname, dpi=dpi)
                plt.title('')
                if pdf_replace_str:
                    plt.savefig(figdir + "pdfs/" + figname.replace(pdf_replace_str, ".pdf"), bbox_inches='tight', dpi=dpi)
                else:
                    plt.savefig(figdir + "pdfs/" + figname.replace(".png", ".pdf"), bbox_inches='tight', dpi=dpi)
                plt.close()

    def plot_quantities_contour_over_FthetaphiSpin(self):
        self.get_labels()
        self.get_quantities_over_accretion_efficiency()

        figdir = self.path_to_figures + "contours_over_FthetaphiSpin/"
        print("Saving contours over Fthetaphi and spin in " + figdir)
        if not os.path.exists(figdir):
            os.makedirs(figdir)
        if not os.path.exists(figdir + "pdfs/"):
            os.makedirs(figdir + "pdfs/")

        X, Y = np.meshgrid(self.spin_values, self.Fthetaphi_values)
        for quantity in self.list_of_quantities:
            plt.figure()
            CF = plt.contourf(self.spin_values, self.Fthetaphi_values, self.quantities_over_FthetaphiSpin[quantity])
            plt.scatter(X, Y, marker='o', color='black')
            plt.xlabel('$a$')
            plt.ylabel(r"$F_{\theta\phi}$")
            cbar = plt.gcf().colorbar(CF)
            cbar.ax.set_ylabel(self.labels[quantity])
            plt.tight_layout()
            plt.xlim([self.spin_values.min(), self.spin_values.max()])
            plt.ylim([self.Fthetaphi_values.min(), self.Fthetaphi_values.max()])
            figname = self.filenames[quantity] + ".png"
            plt.savefig(figdir + figname)
            plt.close()

    def plot_spectral_fractions_contour_over_InclinationSpin(self, **kwargs):
        inc_angles = kwargs.get("inc_angles", [0, 15, 30, 45, 60, 75, 90])
        inc_angles = np.array(inc_angles)
        self.get_labels()
        self.get_spectral_quantities_over_inclination(inc_angles)
        figdir = self.path_to_figures + "contours_over_inclinationSpin/"
        figdir = kwargs.get("figdir", figdir)
        title = kwargs.get("title", True)
        inc_values = ["min_over_Fthetaphi", "max_over_Fthetaphi"]
        for i, Fthetaphi in enumerate(self.Fthetaphi_values):
            inc_values.append("Fthetaphi{:.2f}_value".format(Fthetaphi))
        inc_values = kwargs.get("inc_values", inc_values)
        quantities = kwargs.get("quantities", ["PLF", "RM06"])
        objects_to_annotate = kwargs.get("objects_to_annotate", [])

        print("Saving contours over inclination and spin in " + figdir)
        if not os.path.exists(figdir):
            os.makedirs(figdir)
        for value in inc_values:
            val_path = figdir + value + "/"
            if not os.path.exists(val_path):
                os.makedirs(val_path)
            if not os.path.exists(val_path + "pdfs/"):
                os.makedirs(val_path + "pdfs/")

        X, Y = np.meshgrid(inc_angles, self.spin_values)
        for value in inc_values:
            val_path = figdir + value + "/"
            val = value.split("_")[0]
            for quantity in quantities:
                name = quantity + "_" + val
                PLF = np.transpose(self.spectral_quantities_over_inclination[name])
                PLF_interp_func = interpolate.interp2d(inc_angles, self.spin_values, PLF)
                spin2 = np.linspace(self.spin_values.min(), self.spin_values.max(), 1000, endpoint=True)
                inc_angles2 = np.linspace(inc_angles.min(), inc_angles.max(), 1000, endpoint=True)
                PLF_interp = PLF_interp_func(inc_angles2, spin2)
                plt.figure()
                if title:
                    plt.title(value.replace("_", " "))
                if "min" in value:
                    vmin = 1e-6
                    vmax = 1.0
                    levels = [1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1]
                else:
                    vmax = 1.0
                    vmin = 1e-3
                    levels = [1e-3, 5e-3, 1e-2, 5e-2, 1e-1, 5e-1, 1]

                norm = colors.Normalize(vmin=vmin, vmax=vmax)
                norm = colors.LogNorm(vmin=vmin, vmax=vmax)

                CF = plt.contourf(inc_angles, self.spin_values, PLF, levels, norm=colors.LogNorm(vmin=vmin, vmax=vmax), extend='max', vmin=vmin, vmax=vmax)
                # CF = plt.pcolormesh(inc_angles2, spin2, PLF_interp, norm=norm)

                # Plot the Remillard & McClintock 2006 definition of soft state.
                # plt.contour(inc_angles2, spin2, PLF_interp, [0.25], linestyles=['dashed'], colors=['black'], linewidths=[1])
                plt.contour(inc_angles, self.spin_values, PLF, [0.25], linestyles=['dashed'], colors=['black'], linewidths=[1])
                cmap = CF.cmap
                # plt.scatter(X, Y, marker='o', color='black')
                plt.ylabel('$a$')
                plt.xlabel("Inclination angle $i$ ($^\circ$)")
                cbar = plt.gcf().colorbar(CF)
                cbar.ax.set_ylabel(self.spectral_labels[quantity])
                plt.tight_layout()
                plt.ylim([self.spin_values.min(), self.spin_values.max()])
                plt.xlim([inc_angles.min(), inc_angles.max()])
                for system in objects_to_annotate:
                    hatch = AstroObjects[system]["hatch"]
                    max_inc = AstroObjects[system]["max_inc"]
                    min_inc = AstroObjects[system]["min_inc"]
                    max_spin = AstroObjects[system]["max_spin"]
                    min_spin = AstroObjects[system]["min_spin"]
                    max_PLF = AstroObjects[system]["max_" + quantity]
                    min_PLF = AstroObjects[system]["min_" + quantity]
                    width = max_inc - min_inc
                    height = max_spin - min_spin
                    starting_coord = (min_inc + width/2.0, min_spin + height/2.0)

                    # Color ellipses: left side min PLF, right side max PLF
                    color1 = cmap(norm(min_PLF))
                    color2 = cmap(norm(max_PLF))
                    arc1 = arc_patch(starting_coord, width/2.0, height/2.0, 90, 270, alpha=1.0, facecolor=color1, edgecolor=None)
                    arc2 = arc_patch(starting_coord, width/2.0, height/2.0, 270, 270+180, alpha=1.0, facecolor=color2, edgecolor=None)
                    outline = patches.Ellipse(starting_coord, width, height, fill=False, edgecolor='black', ls="-", linewidth=0.5)
                    plt.gca().add_patch(arc1)
                    plt.gca().add_patch(arc2)
                    plt.gca().add_patch(outline)
                    # Label astrophysical objects
                    text_x = 1.01*(min_inc + width)
                    if system == "H1743" or system == "J1550":
                        text_y = 1.01*(min_spin + height)
                    else:
                        text_y = 1.01*(min_spin + height/2.0)
                    plt.gca().annotate(system, xy=(text_x, text_y))
                    figname = self.filenames[quantity] + ".png"
                plt.savefig(val_path + figname)
                plt.title('')
                plt.savefig(val_path + "pdfs/" + figname.replace(".png", ".pdf"))
                plt.close()

if __name__ == '__main__':
    category = "a0.95"
    category = "Fthetaphi6.00"
    category = "sparse3spin4"
    # category = "sparse3spin2"

    kwargs = {}
    comparison = comparison(category, **kwargs)

    # print(comparison.quantities_over_accretion_efficiency["accretion_efficiencies"])

    # -----------------------------
    # Quantities plots
    # -----------------------------
    # comparison.plot_quantities_line_over_accretion_efficiency()

    # -----------------------------
    # Timescales plots
    # -----------------------------
    # comparison.plot_timescales_line_over_accretion_efficiency()
    # comparison.plot_timescales_line_over_spin()

    # -----------------------------
    # Contour plots
    # -----------------------------
    # comparison.plot_quantities_contour_over_FthetaphiSpin()

    # -----------------------------
    # Spectrum plots over accretion efficiency (given inclination)
    # -----------------------------
    figKwargs = {}
    astro_objects = []
    astro_objects = ["GRO1655", "J1550", "GX339"]
    # astro_objects = ["J1550"]
    # figKwargs["objects_to_annotate"] = astro_objects
    # figKwargs["spectral_types"] = []
    inc_angles = [85]
    # inc_angles = [60, 0, 30, 45, 15, 85]
    # inc_angles = [0, 15, 30, 45, 60, 75, 85, 90]
    # for inc_angle in inc_angles:
        # comparison.plot_spectrum_quantities_line_over_accretion_efficiency(inc_angle)
        # comparison.plot_spectrum_fractions_line_over_accretion_efficiency(inc_angle, **figKwargs)
    inc_angle = 60
    # comparison.plot_spectrum_quantities_line_over_accretion_efficiency(inc_angle)
    # print(comparison.spectral_quantities_over_accretion_efficiency.keys())
    # comparison.plot_spectrum_fractions_line_over_accretion_efficiency(inc_angle)

    # -----------------------------
    # Spectrum plots over inclination angle
    # -----------------------------
    # inc_values = ["max_over_Fthetaphi", "min_over_Fthetaphi", "Fthetaphi6.00_value"]
    # inc_values = ["max_over_Fthetaphi", "min_over_Fthetaphi"]
    inc_values = ["Fthetaphi6.00_value"]
    # astro_objects = ["GRO1655", "GX339"]
    astro_objects = ["GRO1655", "J1550"]
    # astro_objects = ["GRO1655", "GX339", "H1743"]
    astro_objects = ["GRO1655", "J1550", "GX339", "H1743"]
    astro_objects = ["GRO1655", "J1550", "GX339"]
    astro_objects = ["GRO1655", "J1550", "4U1543"]
    astro_objects = ["GRO1655", "J1550", "4U1543", "GX339"]
    # astro_objects = ["GRO1655", "GRS1915", "4U1543"]
    figKwargs = {}
    figKwargs["inc_values"] = inc_values
    # figKwargs["objects_to_annotate"] = astro_objects
    figKwargs["spectral_types"] = []
    # figKwargs["spectral_types"] = ["50keV"]
    # figKwargs["normalize"] = True
    inc_angles = [0, 15, 30, 45, 60, 75, 85, 90]
    # inc_angles = [0, 15, 30, 45, 60, 85]
    # inc_angles = [60]
    # comparison.plot_spectral_quantities_line_over_inclination(inc_angles, **figKwargs)
    comparison.plot_spectral_fractions_line_over_inclination(inc_angles, **figKwargs)

    # -----------------------------
    # Contour spectrum plots over inclination angle
    # -----------------------------
    # comparison.plot_spectral_fractions_contour_over_InclinationSpin(**figKwargs)
