import sys
import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from dynamical_background_gammie1999 import *


# --------------------------------------------------------------
# Input parameters using rough estimates
# --------------------------------------------------------------
M_in_Msun = 10.0
mass_density_cgs = 10**(-5.0) # g/cm^3
Te_in_K = 10**9.0 # K
Te_in_eV = Te_in_K/(1.1*10**4)
rg_in_cm = rG_in_cm_from_solar_mass(M_in_Msun)
radial_size_cm = 3.0*rg_in_cm
number_density_cgs = mass_density_cgs/Consts.mp_in_g
luminosity = 10**38 # erg/s

kwargs = {}
kwargs["M_in_Msun"] = M_in_Msun
kwargs["radial_size_in_rg"] = 3.0
bg = dynamical_background(**kwargs)
print(bg.rISCO)

# --------------------------------------------------------------
# Calculated parameters
# --------------------------------------------------------------
print("--------------------------------------------")
print("Calculating cooling times for estimated temperature/density")
print("T = {:.1e} K, rho = {:.1e} g/cm^3".format(Te_in_K, mass_density_cgs))
print("--------------------------------------------")

theta_e = Consts.kB_cgs * Te_in_K/(Consts.me_in_g*Consts.c_in_cgs**2.0)
print("theta_e=kT/me c^2: {:.2f}".format(theta_e))
# Electron scattering optical depth
tau_es = number_density_cgs * Consts.thomson_cross_section_cgs * radial_size_cm
print("Electron scattering optical depth: {:.2f}".format(tau_es))

# Calculate importance of Compton cooling
# Compton y parameter (assuming non-relativistic thermal electrons)
compton_y = 4 * theta_e * np.max([tau_es, tau_es**2.0])
# Compton y parameter (assuming relativistic thermal electrons)
rel_compton_y = 16 * theta_e**2.0 * np.max([tau_es, tau_es**2.0])
print("Compton y (non-relativistic): {:.2f}".format(compton_y))
print("Compton y (relativistic): {:.2f}".format(rel_compton_y))

# Calculate importance of synchrotron radiation
equipartition_UB_cgs = number_density_cgs*Consts.kB_cgs*Te_in_K
equipartition_B_cgs = np.sqrt(8*np.pi*equipartition_UB_cgs)
print("Equipartition B: {:e} G".format(equipartition_B_cgs))
# Rybicki & Lightman eq. XX
t_sync = 5.1*10**8.0/(equipartition_B_cgs**2.0)
print("Cooling time for ultra-rel electrons: {:e}/gamma seconds = {:.2e}/gamma t_infall".format(t_sync, t_sync/bg.t_infall))

# Bremsstrahlung cooling estimates
photon_density = luminosity / (4*np.pi*radial_size_cm**2.0*Consts.c_in_cgs)
t_brems = 3*Consts.kB_cgs*Te_in_K/number_density_cgs*10**27/np.sqrt(Te_in_eV)
print("Uph/UB_equipartition: {:.2f}".format(photon_density/equipartition_UB_cgs))
print("Bremsstrahlung cooling time: {:e} seconds = {:e} t_infall".format(t_brems, t_brems/bg.t_infall))

# Thermalization time
# assuming very high-energy electron thermalizing
# via Coulomb collisions with thermal bulk
# NRL p. 33
thermalization_constant = (5.11*10**5)**1.5*10**6.0/7.7
coulomb_log = get_ee_coulomb_log(Te_in_eV, number_density_cgs)
print("e-e Coulomb log: {:.2f}".format(coulomb_log))
thermalization_time_constant = thermalization_constant/(number_density_cgs * coulomb_log)
print("High-energy electron thermalization time: {:e}*gamma^1.5".format(thermalization_time_constant))

# --------------------------------------------------------------
# Repeat but with dynamical background
# --------------------------------------------------------------
print("--------------------------------------------")
print("Calculating cooling times for Gammie 1999 model")
print("assuming ISCO values (pretty dense)")
print("--------------------------------------------")
print("B = {:.1e} G, rho = {:.1e} g/cm^3".format(bg.get_Br_in_cgs()[0], bg.get_mass_density_in_cgs()[0]))

# Since B is known already, get T from equipartition
Te_in_K = bg.get_equipartition_T()[0]
Te_in_eV = Te_in_K/(1.1*10**4)
theta_e = Consts.kB_cgs * Te_in_K/(Consts.me_in_g*Consts.c_in_cgs**2.0)
print("T at ISCO [K]: {:e}".format(Te_in_K))
print("theta_e=kT/me c^2 at ISCO: {:.2f}".format(theta_e))
number_density_cgs = bg.get_mass_density_in_cgs()[0]/Consts.mp_in_g
radial_size_cm = bg.radial_size_cm

# Electron scattering optical depth
tau_es = number_density_cgs * Consts.thomson_cross_section_cgs * radial_size_cm
print("Electron scattering optical depth at ISCO: {:.2f}".format(tau_es))

# Calculate importance of Compton cooling
# Compton y parameter (assuming non-relativistic thermal electrons)
compton_y = 4 * theta_e * np.max([tau_es, tau_es**2.0])
# Compton y parameter (assuming relativistic thermal electrons)
rel_compton_y = 16 * theta_e**2.0 * np.max([tau_es, tau_es**2.0])
print("Compton y (non-relativistic): {:.2f}".format(compton_y))
print("Compton y (relativistic): {:.2f}".format(rel_compton_y))

# Synchrotron cooling time
gammie_B_cgs = bg.get_Br_in_cgs()[0]
gammie_UB = gammie_B_cgs**2.0/(8.0*np.pi)
t_sync_gammie = 5.1*10**8.0/(gammie_B_cgs**2.0)
print("Cooling time for ultra-rel electrons at ISCO: {:e}/gamma seconds".format(t_sync_gammie))

# Bremsstrahlung cooling
luminosity = bg.L_edd*bg.efficiency
photon_density = luminosity / (4*np.pi*radial_size_cm**2.0*Consts.c_in_cgs)
t_brems = 3*Consts.kB_cgs*Te_in_K/number_density_cgs*10**27/np.sqrt(Te_in_eV)
UphB_ratio = photon_density/gammie_UB
print("Uph/UB_equipartition: {:.3f}".format(UphB_ratio))
print("Bremsstrahlung cooling time: {:e} seconds".format(t_brems))

# Thermalization time
thermalization_constant = (5.11*10**5)**1.5*10**6.0/7.7
coulomb_log = get_ee_coulomb_log(Te_in_eV, number_density_cgs)
print("e-e Coulomb log: {:.2f}".format(coulomb_log))
thermalization_time_constant = thermalization_constant/(number_density_cgs * coulomb_log)
print("High-energy electron thermalization time: {:e}*gamma^1.5".format(thermalization_time_constant))

# TODO:
# - make plots of cooling times as a function of radius.
