import sys
import numpy as np
import scipy.interpolate as interpolate
import pandas as pd
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *


def read_plf(file_dir, object_name):
    outburst_dict = AstroObjects[object_name]["outburst_data"]
    total_PLF = []
    for outburst in outburst_dict:
        print(outburst)
        csv_name = file_dir + outburst + "_plf.csv"
        df = pd.read_csv(csv_name, header=1)

        data = df.to_numpy()
        times = data[:, 0]
        PLF = data[:, 1]
        total_PLF.extend(PLF)

        PLF_mean = PLF.mean()
        PLF_median = np.median(PLF)
        PLF_std = PLF.std()
        PLF_min = PLF.min()
        PLF_max = PLF.max()
        # Calculate where 90% are within the median
        PLF_p90 = np.percentile(PLF, 90)
        PLF_p10 = np.percentile(PLF, 10)

        print("Number of points: {:d}".format(PLF.size))
        print("PLF median: {:.3f}".format(PLF_median))
        print("PLF mean: {:.3f}".format(PLF_mean))
        print("PLF std: {:.3f}".format(PLF_std))
        print("PLF min: {:.3f}".format(PLF_min))
        print("PLF max: {:.3f}".format(PLF_max))
        print("PLF 90th percentile: {:.3f}".format(PLF_p90))
        print("PLF 10th percentile: {:.3f}".format(PLF_p10))

        plt.figure()
        plt.scatter(times, PLF)
        plt.gca().set_yscale('log')
        plt.title(outburst)
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.gca().axhline([PLF_mean], color='black', ls='--')
        plt.gca().axhline([PLF_p90], color='black', ls='--')
        plt.gca().axhline([PLF_p10], color='black', ls='--')
        plt.gca().axhspan(PLF_mean - PLF_std, PLF_mean + PLF_std, alpha=0.1)
        plt.ylabel(r'PLF')
        plt.xlabel(r'$t$')
        plt.ylim([1e-3, 1e0])

        plt.figure()
        plt.hist(PLF)
        plt.xlabel(r'PLF')
        plt.title(outburst)
        # plt.show()
    print("Total PLF stats")
    print("Number of points: {:d}".format(len(total_PLF)))
    print("PLF mean: {:.3f}".format(np.mean(total_PLF)))
    print("PLF std: {:.3f}".format(np.std(total_PLF)))
    print("PLF min: {:.3f}".format(np.min(total_PLF)))
    print("PLF max: {:.3f}".format(np.max(total_PLF)))
    print("PLF 90th percentile: {:.3f}".format(np.percentile(total_PLF, 90)))
    print("PLF 10th percentile: {:.3f}".format(np.percentile(total_PLF, 10)))
    return

def read_file(file_dir, object_name):
    outburst_dict = AstroObjects[object_name]["outburst_data"]
    for outburst in outburst_dict:
        print(outburst)
        csv_name = file_dir + outburst + ".csv"
        df = pd.read_csv(csv_name, header=1)

        data = df.to_numpy()
        PL_time = data[:, 0]
        PL_lum = data[:, 1]
        disk_time = data[:, 2]
        disk_lum = data[:, 3]
        # Remove nans
        PL_time = PL_time[~np.isnan(PL_time)]
        disk_time = disk_time[~np.isnan(disk_time)]
        PL_lum = PL_lum[~np.isnan(PL_lum)]
        disk_lum = disk_lum[~np.isnan(disk_lum)]

        # Cut to appropriate times
        time_limits = outburst_dict[outburst]["time_limits"]
        PL_inds = (PL_time > time_limits[0])
        PL_time = PL_time[PL_inds]
        PL_lum = PL_lum[PL_inds]
        PL_inds = (PL_time < time_limits[1])
        PL_time = PL_time[PL_inds]
        PL_lum = PL_lum[PL_inds]
        disk_inds = (disk_time > time_limits[0])
        disk_time = disk_time[disk_inds]
        disk_lum = disk_lum[disk_inds]
        disk_inds = (disk_time < time_limits[1])
        disk_time = disk_time[disk_inds]
        disk_lum = disk_lum[disk_inds]

        # Have to interpolate since not taken at same times
        PL_interp_f = interpolate.interp1d(PL_time, PL_lum, fill_value='extrapolate')
        disk_interp_f = interpolate.interp1d(disk_time, disk_lum, fill_value='extrapolate')
        time_range = np.linspace(time_limits[0], time_limits[1], 1000)
        PL_interp = PL_interp_f(time_range)
        disk_interp = disk_interp_f(time_range)
        to_adjust = 50
        PLF = PL_interp/(PL_interp + disk_interp)
        PLF = PLF[to_adjust:-to_adjust]
        time_range = time_range[to_adjust:-to_adjust]
        PLF_mean = PLF.mean()
        PLF_median = np.median(PLF)
        PLF_std = PLF.std()
        PLF_min = PLF.min()
        PLF_max = PLF.max()

        print("PLF median: {:.3f}".format(PLF_median))
        print("PLF mean: {:.3f}".format(PLF_mean))
        print("PLF std: {:.3f}".format(PLF_std))
        print("PLF min: {:.3f}".format(PLF_min))
        print("PLF max: {:.3f}".format(PLF_max))
        plt.figure()
        plt.scatter(time_range, PLF)
        plt.gca().set_yscale('log')
        plt.title(outburst)
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.gca().axhline([PLF_mean], color='black', ls='--')
        plt.gca().axhspan(PLF_mean - PLF_std, PLF_mean + PLF_std, alpha=0.1)
        plt.ylabel(r'PLF')
        plt.xlabel(r'$t$')
        plt.ylim([1e-3, 1e-1])

        plt.figure()
        plt.hist(PLF)
        plt.xlabel(r'PLF')
        plt.title(outburst)
    return


if __name__ == '__main__':
    file_dir = "/mnt/c/Users/liaha/research/projects/summer2021/figures/dunn_plots/"
    # astro_objects = ["GRO1655", "J1550", "GX339"]
    astro_objects = ["GX339"]
    astro_objects = ["4U1543"]
    for astro in astro_objects:
        # read_file(file_dir, astro)
        read_plf(file_dir, astro)
