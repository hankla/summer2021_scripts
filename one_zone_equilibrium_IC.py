import sys
import numpy as np
import matplotlib
from scipy.optimize import fsolve
import scipy.integrate as integrate
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from dynamical_background_gammie1999 import *
from scipy import special


def calculate_tsync_constant():
    # Full cooling time = calculate_tsync_constant/gamma
    # Note this is in the ultrarelativistic limit!
    # Rybicki & Lightman problem 6.1
    t_sync_constant = 5.1e8/(B_cgs**2.0)
    return t_sync_constant

def calculate_tIC_constant():
    # Full cooling time = calculate_tIC_constant/gamma
    # Assume is IC cooling time, proportional using ratio of power loss
    ratio = UB_cgs/Uph_cgs
    return ratio*calculate_tsync_constant()

def collision_time_equations(gamma):
    restmass_eV = 5.11e5 # use only to check against NRL

    # Tabulate needed integrals
    integral_path = bg.path_to_reduced_data + "tabulated_integral.txt"
    if not os.path.exists(integral_path):
        # Tabulate \psi(x) on NRL p. 31
        x_values = np.logspace(-5, 2, 10000)
        psi_values = []
        for x in x_values:
            psi_value = psi(x)
            psi_values.append(psi_value)
        tabulated_integral = np.vstack([x_values, psi_values])
        np.savetxt(integral_path, tabulated_integral)
        print("Tabulated psi(x).")
    else:
        tabulated_integral = np.loadtxt(integral_path)

    # Kinetic_ratio is x
    kinetic_ratio = (gamma - 1)/theta_e
    x_index = (np.abs(kinetic_ratio - tabulated_integral[0, :])).argmin()
    psi_value = tabulated_integral[1, x_index]

    # Full collision time = 1/(2*nu0const*psi(x))*(gamma-1)^1.5
    equation_to_solve = cooling_time/gamma - 1.0/(2*nu0const*psi_value)*(gamma-1)**1.5

    # For the high-energy limit:
    high_energy_const = 1.0e6/(7.7*number_density_cgs*coulomb_log)*(5.11e5)**1.5
    # equation_to_solve = cooling_time/gamma - high_energy_const*(gamma-1)**1.5

    return equation_to_solve

def solve_for_gamma1(temperature, number_density, magnetic_field, Uph):
    # Temperature changes only the collisional time.
    # For the general collision time, we need to solve an equation
    # involving psi(x), where x changes with temperature.

    # Set gamma1 min to be the non-relativistic average gamma from theta,
    # i.e. 1.5theta + 1
    gamma1_min = 1.5*theta_e + 1.0
    gamma1 = fsolve(collision_time_equations, gamma1_min)[0]

    # TODO: change gamma1 min?
    if gamma1 < gamma1_min:
        print("Collisions are never important. Setting gamma1 to its minimum value of 1.5theta + 1 = {:.2f}".format(gamma1_min))
        gamma1 = gamma1_min
    return gamma1

def solve_for_PL_norm(gamma1, theta, number_density, PL_index=3.0):
    fMJ_at_gamma1 = maxwell_juettner_at_gamma(gamma1, number_density, theta)
    normalization = fMJ_at_gamma1/number_density * gamma1**(PL_index)
    return normalization

def calculate_heating_rate(radial_size_cm, dissipation_efficiency, nonthermal_heating_fraction, eddington_luminosity):
    volume = 4.0*np.pi/3.0*radial_size_cm**3.0
    total_energy_dissipated = dissipation_efficiency * eddington_luminosity
    heating_rate = nonthermal_heating_fraction * total_energy_dissipated / volume
    return heating_rate

def calculate_sync_cooling_rate(normalization, gamma2=100):
    # Calculation in notebook. Assumes PIC = (Uph/UB) Psync
    # TODO:
    # - generalize p index
    # - include lower boundary

    p = PL_index
    # First, calculate synchrotron power loss
    p_ind_const = 4.0*np.sqrt(3)*Consts.e_in_cgs**4.0/(Consts.me_in_g**2.0*Consts.c_in_cgs**3.0)
    if p == 3.0:
        p_dep_const = 2.0
        gamma_dep = np.log10(gamma2/gamma1)
    else:
        p_dep_const = 2.0**((p-3.0)/2.0)*2.0/(3.0-p)
        gamma_dep = gamma2**(3-p)# - gamma1**(3-p)
    p_dep_const = p_dep_const * special.gamma(p/4.0+19.0/12.0)*special.gamma(p/4.0-1.0/12.0)/(p+1.0)

    # print(B_cgs**2.0*p_ind_const*p_dep_const*gamma_dep)
    power_loss_sync = B_cgs**2.0* p_ind_const * p_dep_const*gamma_dep * normalization*number_density_cgs

    return power_loss_sync


def calculate_IC_cooling_rate(normalization, gamma2=100):
    # Calculation in notebook. Assumes PIC = (Uph/UB) Psync

    power_loss_sync = calculate_sync_cooling_rate(normalization, gamma2)
    ratio = Uph_cgs/UB_cgs
    power_loss_IC = ratio*power_loss_sync
    return power_loss_IC


if __name__ == '__main__':
    # --------------------------------------------------------------
    # Input parameters using rough estimates
    # --------------------------------------------------------------
    M_in_Msun = 10.0
    mass_density_cgs = 1.0e-5 # g/cm^3
    Tproton_in_K = 1.0e09
    eddington_luminosity = 1.26e38 * M_in_Msun
    eddington_fraction = 0.1 # erg/s
    luminosity = eddington_fraction * eddington_luminosity
    PL_index = 2.0
    nonthermal_heating_fraction = 0.1
    dissipation_efficiency = 1.04 # should really calculate from Gammie background
    coulomb_log = 10
    gamma2 = 10
    use_IC_cooling = True

    number_density_cgs = mass_density_cgs/Consts.mp_in_g

    nu0const1 = 4*np.pi*Consts.e_in_cgs**4/(np.sqrt(Consts.me_in_g)*Consts.restmass_cgs**1.5)
    nu0const = nu0const1 * coulomb_log * number_density_cgs

    kwargs = {}
    kwargs["M_in_Msun"] = M_in_Msun
    bg = dynamical_background(**kwargs)
    radial_size_cm = 1.0*bg.rISCO * bg.rg_in_cm

    # --------------------------------------------------------------
    # Calculated parameters
    # --------------------------------------------------------------
    # Uph, the magnetic field, and the heating rate are always the same
    Uph_cgs = luminosity / (4*np.pi*radial_size_cm**2.0*Consts.c_in_cgs)
    heating_rate = calculate_heating_rate(radial_size_cm, dissipation_efficiency, nonthermal_heating_fraction, eddington_luminosity)
    UB_cgs = number_density_cgs*Consts.kB_cgs*Tproton_in_K
    B_cgs = np.sqrt(8.0*np.pi*UB_cgs)
    tsync_cool = calculate_tsync_constant()
    tIC_cool = calculate_tIC_constant()

    if use_IC_cooling:
        cooling_time = tIC_cool
    else:
        cooling_time = tsync_cool

    # temperature will be adjusted
    # temperature = Tproton_in_K
    temperature = 2.5e8
    # temperature = 2.3e8
    dtemp = 0.0001*Tproton_in_K

    print("{:.2e}".format(Uph_cgs))
    print(number_density_cgs)

    print("Dynamical background has an infall time of {:.1e} rg/c, or {:.1e}s".format(bg.t_infall, bg.t_infall*bg.tg_in_s))
    print("For T={:.1e} K, UB/Uph = {:.1e}, t_sync = {:.1e}/gamma s and t_IC = {:.1e}/gamma s".format(temperature, UB_cgs/Uph_cgs, tsync_cool, tIC_cool))
    print("Collision time: (gamma-1)^1.5/(2 nu0const psi(x)) = (gamma-1)^1.5/psi(x) * {:.2e}".format(1.0/(2.0*nu0const)))
    print("Adjusting electron temperature.............")

    # ----------------------------------------------
    iteration = 0; cooling_rate = 0
    for t in np.arange(0, 50):
    # while not np.allclose(cooling_rate, heating_rate, 1e-2):
        # First calculate new magnetic field from equipartition
        theta_e = Consts.kB_cgs * temperature/Consts.restmass_cgs

        # Get lower bound based on cooling rates. Only collisional time
        # changes with temperature
        gamma1 = solve_for_gamma1(temperature, number_density_cgs, B_cgs, Uph_cgs)
        # print("For T={:.1e} K, gamma1 = {:.2f}".format(temperature, gamma1))

        # print(Consts.restmass_cgs)
        # print("{:.2e}".format(maxwell_juettner_at_gamma(gamma1, number_density_cgs, theta_e)))
        # print("{:.2e}".format(maxwell_juettner_at_gamma(4.9, number_density_cgs, theta_e)))
        # print("{:.2e}".format(maxwellian_at_gamma(gamma1, number_density_cgs, theta_e)))
        # Get normalization by requiring continuity of distribution function
        # This norm is a factor of mc^2 = 1e-6 smaller than in OneNote because it's f(gamma) rather than f(E)
        norm = solve_for_PL_norm(gamma1, theta_e, number_density_cgs, PL_index)
        # print("Normalization: {:.2e}".format(norm))
        if use_IC_cooling:
            cooling_rate = calculate_IC_cooling_rate(norm, gamma2)
        else:
            cooling_rate = calculate_sync_cooling_rate(norm, gamma2)
        # print("Cooling rate: {:.3e}".format(cooling_rate))

        # print("Iteration {:d} with T = {:.3e} K; cooling rate = {:.3e}".format(iteration, temperature, cooling_rate))

        if cooling_rate > heating_rate:
            temperature = temperature - dtemp
        else:
            temperature = temperature + dtemp

        iteration += 1

    print("For T={:.4e} K, cooling = heating. Gamma1 = {:.2f}".format(temperature, gamma1))
    print("Cooling rate: {:.3e}".format(cooling_rate))
    print("Heating rate: {:.3e}".format(heating_rate))
    print("Process took {:d} iterations".format(iteration))
    print(theta_e)
    print(np.allclose(cooling_rate, heating_rate, 1e-2))
    # MJ_vals = maxwell_juettner_at_gamma(gamma_values, number_density_cgs, theta_e)
    # M_vals = maxwellian_at_gamma(gamma_values, number_density_cgs, theta_e)
#
    # print("MJ: {:.2e}".format(integrate.trapz(MJ_vals, gamma_values)))
    # print("Maxwellian: {:.2e}".format(integrate.trapz(M_vals, gamma_values)))
    # print(number_density_cgs)
    # plt.figure()
    # plt.plot(gamma_values, MJ_vals, label=r"$f_{MJ}(\gamma)$")
    # plt.plot(gamma_values, M_vals, label=r"$f_M(\gamma)$")
    # plt.xscale('log')
    # plt.yscale('log')
    # plt.xlabel(r'$\gamma$')
    # plt.ylim([1e-7*number_density_cgs, 10*number_density_cgs])
    # plt.xlim([0.9, 10])
    # plt.legend()
    # plt.show()
