#!/usr/bin/env python
# coding: utf-8

# # Gammie 1999
# The goal of this notebook is to reproduce Gammie 1999 for a given value of F_{\theta\phi},
# which will provide the background electromagnetic fields and four-velocity (hence the trajectories)
# of the fluid.

import pickle
import numpy as np
import scipy.optimize as optimize
from scipy.optimize import fsolve
from scipy.optimize import least_squares
from scipy import integrate
from scipy.interpolate import interp1d
import matplotlib
import matplotlib.pyplot as plt
from scipy import signal
import os
import sys
sys.path.append('')
from tools import *

small_size = 22
medium_size = 14
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('figure', titlesize=small_size)
matplotlib.rc('text', usetex=True)
# matplotlib.rcParams['text.latex.preamble'] = [r'\usepackage[cm]{sfmath}']


class dynamical_background:
    def __init__(self, setup="lia_hp", **kwargs):
        """
        TODO:
        """
        overwrite_self = kwargs.get("overwrite_self", False)
        self.Fthetaphi = kwargs.get("Fthetaphi", 6.0)
        self.a = kwargs.get("a", 0.95)
        # -----------------------------------
        #      Add default setup information here
        # -----------------------------------
        if setup == "lia_hp":
            path_to_figures = "/mnt/c/Users/liaha/research/projects/"\
                "summer2021/figures/"
            path_to_figures2 = path_to_figures + "dynamical_background/Fthetaphi{:.2f}a{:.2f}/".format(self.Fthetaphi, self.a)
            path_to_reduced_data = "/mnt/c/Users/liaha/research/projects/"\
                "summer2021/data_reduced/Fthetaphi{:.2f}a{:.2f}/".format(self.Fthetaphi, self.a)
            pickle_path = path_to_reduced_data + "dynamical_background_Fthetaphi{:.2f}a{:.2f}.p".format(self.Fthetaphi, self.a)

        # -----------------------------------
        # If paths are non-standard, set them here using kwargs
        # Otherwise, use the defaults from above
        # -----------------------------------
        self.path_to_figures = kwargs.get("path_to_figures", path_to_figures)
        self.path_to_figures2 = kwargs.get("path_to_figures2", path_to_figures2)
        self.path_to_reduced_data = kwargs.get("path_to_reduced_data",
                                               path_to_reduced_data)
        self.pickle_path = kwargs.get("pickle_path", pickle_path)

        if os.path.exists(self.pickle_path) and (not overwrite_self):
            print("Loading dynamical background from " + self.pickle_path)
            self._load_from_pickle()

        else:
            self._initialize(**kwargs)


    def _initialize(self, **kwargs):
        print("Creating new dynamical background.")
        self.M = 1

        self.rISCO = self.get_ISCO()
        self.rEH = self.get_event_horizon()
        self.OmegaF = self.OmegaKep(self.rISCO)
        self.r_range = np.linspace(self.rEH+1e-12, self.rISCO, 1000)[::-1]
        self.to_print = kwargs.get("printing", True)
        if self.to_print:
            print('rEH={:.3f}'.format(self.rEH))
            print('rISCO={:.3f}'.format(self.rISCO))

        # -----------------------------------
        # Create directories if need be
        # -----------------------------------
        if not os.path.exists(self.path_to_figures2):
            os.makedirs(self.path_to_figures2)
        if not os.path.exists(self.path_to_reduced_data):
            os.makedirs(self.path_to_reduced_data)

        # -----------------------------------
        # Run solver
        # Solve for ISCO information
        # -----------------------------------
        self.find_ISCO_four_velocity(**kwargs)

        # -----------------------------------
        # Solve for critical point location
        # -----------------------------------
        self.get_fast_critical_point(**kwargs)

        # -----------------------------------
        # Solve for four velocity
        # -----------------------------------
        self.get_four_velocity_solutions(**kwargs)

        # -----------------------------------
        # Calculate infall time
        # -----------------------------------
        self.get_infall_time()

        # -----------------------------------
        # Do conversions to cgs
        # -----------------------------------
        # self.get_FM_in_cgs()
        # self.get_Fthetaphi_in_cgs()
        # self.get_Br_in_cgs()

        # -----------------------------------
        # Dump to file
        # -----------------------------------
        with open(self.pickle_path, 'wb') as fh:
            pickle.dump(self, fh)

    def _load_from_pickle(self):
        try:
            with open(self.pickle_path, 'rb') as fh:
                self.__dict__.update(pickle.load(fh).__dict__)
                return True
        except:
            return False


    def get_ISCO(self):
        self.rISCO = get_rISCO(self.a, self.M)
        return self.rISCO

    def get_event_horizon(self):
        """
        rEH in units of rg
        """
        self.rEH = get_rEH(self.a)
        return self.rEH

    def OmegaKep(self, r):
        return 1/(r**(3/2) + self.a)


    # -----------------------------------
    # Define system for finding the ISCO information
    # -----------------------------------
    def ISCO_system(self, variables):
        uTISCO = variables[0]
        uPhiISCO = variables[1]
        a = self.a
        M = self.M

        condition1 = self.OmegaF - uPhiISCO/uTISCO
        condition2 = gtt(a, M, self.rISCO)*uTISCO**2\
            + gphiphi(a, M, self.rISCO)*uPhiISCO**2\
            + 2*gtphi(a, M, self.rISCO)*uTISCO*uPhiISCO + 1
        return [condition1, condition2]


    def find_ISCO_four_velocity(self, **kwargs):
        # Guesses from Gammie 1999 Fig. 1
        # The final values are not sensitive to the initial guess
        # (see bg_guess_scan class)
        # uphiISCOguess = kwargs.get("uphiISCOguess", 1.9)
        # utISCOguess = kwargs.get("utISCOguess", -0.8)
        uTISCOguess = kwargs.get("uTISCOguess", 3.2)
        uPhiISCOguess = kwargs.get("uPhiISCOguess", 0.9)

        # root = fsolve(self.ISCO_system, [uTISCOguess, uPhiISCOguess])
        root = least_squares(self.ISCO_system, [uTISCOguess, uPhiISCOguess])

        self.uTISCO = root.x[0]
        self.uPhiISCO = root.x[1]

        # This is close to the guess off of Gammie 1999 Fig. 1!
        self.ell = uphi(self.rISCO, self.uTISCO, self.uPhiISCO, self.a, self.M)
        self.epsilon = -ut(self.rISCO, self.uTISCO, self.uPhiISCO, self.a, self.M)


    # -----------------------------------
    # Define functions for finding the fast critical point
    # -----------------------------------

    def f1(self, r, uR, FL):
        a = self.a
        M = self.M
        numerator = mathcalD(a, M, r)*self.Fthetaphi**2*self.OmegaF\
            - 2*uR*gtphi(a, M, r)
        denominator = mathcalD(a, M, r)*self.Fthetaphi**2 + 2*uR*gphiphi(a, M, r)
        return numerator/denominator

    def h1(self, r, uR, FL):
        a = self.a
        M = self.M
        numerator = -2*uR
        denominator = mathcalD(a, M, r)*self.Fthetaphi**2 + 2*uR*gphiphi(a, M, r)
        return numerator/denominator

    def a1(self, r, uR, FL):
        a = self.a
        M = self.M
        return gtt(a, M, r) + gphiphi(a, M, r)*self.f1(r, uR, FL)**2 + 2*gtphi(a, M, r)*self.f1(r, uR, FL)

    def b1(self, r, uR, FL):
        a = self.a
        M = self.M
        return 2*gphiphi(a, M, r)*self.f1(r, uR, FL)*self.h1(r, uR, FL)*FL + 2*gtphi(a, M, r)*self.h1(r, uR, FL)*FL

    def c1(self, r, uR, FL):
        a = self.a
        M = self.M
        return 1 + grr(a, M, r)*uR**2 + gphiphi(a, M, r)*self.h1(r, uR, FL)**2*FL**2


    def uT_positive_root(self, r, uR, FL):
        numerator = -self.b1(r, uR, FL) + np.sqrt(self.b1(r, uR, FL)**2 - 4*self.a1(r, uR, FL)*self.c1(r, uR, FL))
        denominator = 2*self.a1(r, uR, FL)
        return numerator/denominator

    def uT_negative_root(self, r, uR, FL):
        numerator = -self.b1(r, uR, FL) - np.sqrt(self.b1(r, uR, FL)**2 - 4*self.a1(r, uR, FL)*self.c1(r, uR, FL))
        denominator = 2*self.a1(r, uR, FL)
        return numerator/denominator

    def FE_ISCO(self, r, uR, FL):
        a = self.a
        M = self.M
        # I would think you'd want to use the positive root near the ISCO, but for some reason only the negative root (same as fast point) gives the right answer
        # return (gtphi(a, M, r)*self.f1(r, uR, FL)+self.OmegaF*gphiphi(a, M, r)*self.f1(r, uR, FL) + gtt(a, M, r) + self.OmegaF*gtphi(a, M, r))*self.uT_positive_root(r, uR, FL) + (gtphi(a, M, r)*self.h1(r, uR, FL) + self.OmegaF * gphiphi(a, M, r)*self.h1(r, uR, FL)+ self.OmegaF)*FL
        # Use the positive root near the ISCO
        return (gtphi(a, M, r)*self.f1(r, uR, FL)+self.OmegaF*gphiphi(a, M, r)*self.f1(r, uR, FL) + gtt(a, M, r) + self.OmegaF*gtphi(a, M, r))*self.uT_negative_root(r, uR, FL) + (gtphi(a, M, r)*self.h1(r, uR, FL) + self.OmegaF * gphiphi(a, M, r)*self.h1(r, uR, FL)+ self.OmegaF)*FL

    def FE_FP(self, r, uR, FL):
        a = self.a
        M = self.M
        # Use the negative root near the fast critical point
        return (gtphi(a, M, r)*self.f1(r, uR, FL)+self.OmegaF*gphiphi(a, M, r)*self.f1(r, uR, FL) + gtt(a, M, r) + self.OmegaF*gtphi(a, M, r))*self.uT_negative_root(r, uR, FL) + (gtphi(a, M, r)*self.h1(r, uR, FL) + self.OmegaF * gphiphi(a, M, r)*self.h1(r, uR, FL)+ self.OmegaF)*FL

    def dFEdr(self, r, uR, FL):
        # Only used near critical point, so use FE negative root
        delta_r = 0.0001
        return (self.FE_FP(r + delta_r/2, uR, FL) - self.FE_FP(r - delta_r/2, uR, FL))/delta_r

    def dFEduR(self, r, uR, FL):
        # Only used near critical point, so use FE negative root
        delta_uR = 0.0001
        return (self.FE_FP(r, uR + delta_uR/2.0, FL) - self.FE_FP(r, uR - delta_uR/2.0, FL))/delta_uR

    # Solve for the location of the fast critical point
    def critical_point_system(self, variables):
        # variables = [r, u^r, FL]
        r, uR, FL = variables
        eq9 = self.FE_FP(r, uR, FL) - self.FE_ISCO(self.rISCO, 0, FL)
        eq10 = self.dFEdr(r, uR, FL)
        eq11 = self.dFEduR(r, uR, FL)
        return np.array([eq9, eq10, eq11])
        # return np.abs(eq9) + np.abs(eq10) + np.abs(eq11)


    def get_fast_critical_point(self, **kwargs):
        # The solution can depend sensitively on the initial guesses.
        # Note that if the final location of the fast critical point
        # is at the ISCO, the guesses probably need to be adjusted.
        # Solving for FL requires taking either the positive or negative root
        # of uT...one is valid near the ISCO and one near the fast critical point.

        # These guesses should be adjusted according to the results of
        # the bg_guess_scan. The below default values are for Fthetaphi=6.0
        rFguess = kwargs.get("rFguess", 1.4) # Gammie value: 1.37
        uRFguess = kwargs.get("uRFguess", -0.36) # Gammie value: -0.26
        FLguess = kwargs.get("FLguess", -0.8) # Gammie value: 1.23

        if rFguess == "estimate":
            rFguess = self.rEH + 1.0e-3
        if rFguess > self.rISCO or rFguess < self.rEH:
            rFguess = (self.rISCO - self.rEH)/2.0 + self.rEH
        # print(self.FE_FP(rFguess, uRFguess, FLguess))
        # print(self.FE_ISCO(self.rISCO, 0, FLguess))
        # print(self.dFEdr(rFguess, uRFguess, FLguess))
        # print(self.dFEduR(rFguess, uRFguess, FLguess))
        # exit()
        # The upper bound for uR might need to be adjusted.
        lower_bounds = np.array([self.rEH, -1.0, -np.inf])
        upper_bounds = np.array([self.rISCO, -0.001, np.inf])
        bounds = (lower_bounds, upper_bounds)
        # print(self.b1(rFguess, uRFguess, FLguess)**2)
        # print(4.0*self.a1(rFguess,uRFguess, FLguess)*self.c1(rFguess, uRFguess, FLguess))
        root = least_squares(self.critical_point_system,
                             [rFguess, uRFguess, FLguess], bounds=bounds)
        bounds2 = optimize.Bounds(lower_bounds, upper_bounds)
        guess = np.array([rFguess, uRFguess, FLguess])
        # fast_sol = optimize.minimize(self.critical_point_system, guess, method='Powell',
        # bounds=bounds2)
        fast_sol2 = optimize.fsolve(self.critical_point_system, guess)
        if self.to_print:
            print("(rF, uRF, FL) = ({:.2f}, {:.5f}, {:.2f}) with least squares".format(*root.x))
            print("(rF, uRF, FL) = ({:.2f}, {:.5f}, {:.2f}) with fsolve".format(*fast_sol2))
        if root.success:
            print("LSQ was successful. Setting critical point values to root...")
            self.rFval = root.x[0]
            self.uRFval = root.x[1]
            self.FLval = root.x[2]
        print(self.critical_point_system(fast_sol2))

        # self.rFval = fast_sol2[0]
        # self.uRFval = fast_sol2[1]
        # self.FLval = fast_sol2[2]

        self.FEval = self.OmegaF*self.FLval - (self.epsilon - self.ell*self.OmegaF)
        if self.to_print:
            print("Found FE = {:.2f}".format(self.FEval))


    # -----------------------------------
    # Define functions for solving for uT(r), uR(r), and uPhi(r)
    # -----------------------------------

    def FE_equation(self, r, uT, uR, uPhi):
        a = self.a; M = self.M
        return ut(r, uT, uPhi, a, M) + mathcalD(a, M, r)*self.OmegaF*self.Fthetaphi**2*(self.OmegaF*uT - uPhi)/(2*uR)

    def FL_equation(self, r, uT, uR, uPhi):
        a = self.a; M = self.M
        return -uphi(r, uT, uPhi, a, M) + mathcalD(a, M, r)*self.Fthetaphi**2*(self.OmegaF*uT - uPhi)/(2*uR)

    def normalization(self, r, uT, uR, uPhi):
        a = self.a; M = self.M
        return gtt(a, M, r)*uT**2 + gphiphi(a, M, r)*uPhi**2 + grr(a, M, r)*uR**2 + 2*gtphi(a, M, r)*uT*uPhi

    def four_velocity_system(self, variables):
        uT = variables[0]
        uR = variables[1]
        uPhi = variables[2]

        condition1 = self.FE_equation(self.rSol, uT, uR, uPhi) - self.FEval
        condition2 = self.FL_equation(self.rSol, uT, uR, uPhi) - self.FLval
        condition3 = self.normalization(self.rSol, uT, uR, uPhi) + 1.0
        return [condition1, condition2, condition3]

    def get_four_velocity_solutions(self, **kwargs):
        four_velocity = np.array([[0, 0, 0]])
        overwrite_four_velocity = kwargs.get("overwrite_four_velocity", True)
        ISCO_guesses = kwargs.get("ISCO_guesses",
                                  [self.uTISCO, -0.1, self.uPhiISCO])

        for rSol in self.r_range:
            self.rSol = rSol

            # Set guesses to be values at previous radius
            if rSol == self.r_range[0]:
                guess = ISCO_guesses
            else:
                guess = four_velocity[-1, :]

            # Enforce monotonicity of uR
            uRmax = four_velocity[-1, 1]
            # uT (Lorentz factor) should be greater than 1
            min_values = [1.0, -np.inf, -np.inf]
            max_values = [np.inf, uRmax, np.inf]
            bounds = (min_values, max_values)

            # four_velocity_at_r = fsolve(self.four_velocity_system, guess)
            # print(four_vel_sol.success)
            # four_velocity_at_r = least_squares(self.four_velocity_system,
                                               # guess).x
            four_velocity_at_r = least_squares(self.four_velocity_system,
                                               guess, bounds=bounds).x
            four_velocity = np.concatenate((four_velocity,
                                        [four_velocity_at_r]), axis=0)
        four_velocity = four_velocity[1:, :]

        self.uTsolution = four_velocity[:, 0]
        self.uRsolution = four_velocity[:, 1]
        self.uPhisolution = four_velocity[:, 2]

        self.rhosolution = -1.0/(2.0*np.pi*self.r_range**2.0*self.uRsolution)
        self.uRsmooth = signal.savgol_filter(self.uRsolution, 25, 1)
        self.rhosmooth = -1.0/(2.0*np.pi*self.r_range**2.0*self.uRsmooth)

        # Write to files
        header = "Fthetaphi = {:.2f}. M={:.2f}, a={:.2f}".format(self.Fthetaphi, self.M, self.a)
        uTpath = self.path_to_reduced_data + "uT_Fthetaphi{:.2f}.txt".format(self.Fthetaphi)
        uRpath = self.path_to_reduced_data + "uR_Fthetaphi{:.2f}.txt".format(self.Fthetaphi)
        uPhipath = self.path_to_reduced_data + "uPhi_Fthetaphi{:.2f}.txt".format(self.Fthetaphi)
        uRsmoothpath = self.path_to_reduced_data + "uRsmooth_Fthetaphi{:.2f}.txt".format(self.Fthetaphi)
        rhosmoothpath = self.path_to_reduced_data + "rhosmooth_Fthetaphi{:.2f}.txt".format(self.Fthetaphi)
        rhopath = self.path_to_reduced_data + "rho_Fthetaphi{:.2f}.txt".format(self.Fthetaphi)
        if not os.path.exists(uTpath) or overwrite_four_velocity:
            np.savetxt(uTpath, self.uTsolution, header=header)
        if not os.path.exists(uRpath) or overwrite_four_velocity:
            np.savetxt(uRpath, self.uTsolution, header=header)
        if not os.path.exists(uPhipath) or overwrite_four_velocity:
            np.savetxt(uPhipath, self.uPhisolution, header=header)
        if not os.path.exists(uRsmoothpath) or overwrite_four_velocity:
            np.savetxt(uRsmoothpath, self.uRsmooth, header=header)
        if not os.path.exists(rhosmoothpath) or overwrite_four_velocity:
            np.savetxt(rhosmoothpath, self.rhosmooth, header=header)
        if not os.path.exists(rhopath) or overwrite_four_velocity:
            np.savetxt(rhopath, self.rhosolution, header=header)

        return (self.uTsolution, self.uRsolution, self.uPhisolution, self.uRsmooth)

    # -----------------------------------
    # After solution has been found,
    # can calculate other quantities
    # -----------------------------------
    def get_infall_time(self):
        # Returns infall time in rg/c
        # Note that t_infall depends sensitively on uR. Here we smooth...
        self.times_at_radius = np.hstack([0, integrate.cumtrapz(1/self.uRsmooth, self.r_range)])
        self.t_infall = self.times_at_radius[-1]
        return self.t_infall

    def get_radii_interp_f(self, N=1000):
        # interpolate radii based off of times
        radii_interp_f = interp1d(self.times_at_radius, self.r_range)
        return radii_interp_f

    def get_density_interp_f(self, N=1000):
        # interpolate density based off of times
        density_interp_f = interp1d(self.times_at_radius, self.rhosmooth)
        return density_interp_f


    def Br_cgs_interp_f(self, N=1000):
        # interpolate Br based off of times
        Br_interp_f = interp1d(self.times_at_radius, self.get_Br_in_cgs())
        return Br_interp_f


    # -----------------------------------
    # Converting to CGS
    # -----------------------------------
    # The dimensionalizing quantities (L_edd, fraction of Mdot, etc.)
    # should NOT be considered properties of the dynamical background
    # itself, hence taking them as arguments rather than an attribute
    def get_tg_in_s(self, M_in_Msun):
        return get_rg_in_cm(M_in_Msun)/Consts.c_in_cgs

    def get_FM_in_cgs(self, M_in_Msun, f_Ledd=0.1, f_Mdot=0.1, H_over_R=1.0):
        # Estimate mdot as some fraction of MdotEdd
        # FM_in_cgs has units of g/s
        L_edd = get_eddington_lum(M_in_Msun)
        Mdot_edd = L_edd/(f_Ledd*Consts.c_in_cgs**2.0)
        Mdot = f_Mdot*Mdot_edd  # g/s
        FM_in_cgs = -Mdot/H_over_R/2.0  # g/s
        return FM_in_cgs

    def get_mass_density_in_cgs(self, M_in_Msun, f_Ledd=0.1, f_Mdot=0.1, H_over_R=1.0):
        rg_in_cm = get_rg_in_cm(M_in_Msun)
        rho_conversion = -self.get_FM_in_cgs(M_in_Msun, f_Ledd, f_Mdot, H_over_R)/(rg_in_cm**2.0 * Consts.c_in_cgs)
        # mass_density_cgs = self.rhosmooth * rho_conversion
        mass_density_cgs = self.rhosolution * rho_conversion
        return mass_density_cgs


    def get_Fthetaphi_in_cgs(self, M_in_Msun, f_Ledd=0.1, f_Mdot=0.1, H_over_R=1.0):
        FM_in_cgs = self.get_FM_in_cgs(M_in_Msun, f_Ledd, f_Mdot, H_over_R)
        conversion_factor = Consts.G_in_cgs * M_in_Msun * Consts.solar_mass_in_g * np.sqrt(-FM_in_cgs)/Consts.c_in_cgs**1.5
        Fthetaphi_in_cgs = self.Fthetaphi*conversion_factor
        return Fthetaphi_in_cgs

    def get_Br_in_cgs(self, M_in_Msun, f_Ledd=0.1, f_Mdot=0.1, H_over_R=1.0):
        """
        Br = -Fthetaphi/r^2
        """
        rg_in_cm = get_rg_in_cm(M_in_Msun)
        r_in_cm = self.r_range * rg_in_cm
        Br_in_cgs = -self.get_Fthetaphi_in_cgs(M_in_Msun, f_Ledd, f_Mdot, H_over_R)/(r_in_cm**2.0)
        return Br_in_cgs


    # -----------------------------------
    # Plotting functions
    # (mostly just for main function in this file)
    # -----------------------------------
    def plot_uT_solution(self):
        figure_dir = self.path_to_figures2
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)
        plt.figure()
        plt.plot(self.r_range, self.uTsolution)
        plt.xlabel('$r/r_G$')
        plt.ylabel('$u^t=\gamma$')
        plt.ylim([1.0, 100])
        plt.xlim([self.rEH, self.rISCO])
        plt.yscale('log')
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.title(r'$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$'.format(self.Fthetaphi, self.a))
        plt.gca().grid(color='.9', ls='--')
        plt.tight_layout()
        plt.savefig(figure_dir + "uT_solution.png")
        plt.close()


    def plot_uR_solution(self):
        figure_dir = self.path_to_figures2
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)
        plt.figure()
        plt.plot(self.r_range, self.uRsolution)
        plt.plot(self.r_range, self.uRsmooth)
        plt.plot(self.rFval, self.uRFval, 'ko')
        plt.xlabel('$r/r_G$')
        plt.ylabel('$u^r$')
        # plt.ylim([-0.5, 0])
        plt.xlim([self.rEH, self.rISCO])
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.title(r'$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$'.format(self.Fthetaphi, self.a))
        plt.tight_layout()
        plt.savefig(figure_dir + "uR_solution.png")
        plt.close()

    def plot_uphi_solution(self):
        figure_dir = self.path_to_figures2
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)
        plt.figure()
        plt.plot(self.r_range, uphi(self.r_range, self.uTsolution, self.uPhisolution, self.a, self.M))
        plt.xlabel('$r/r_G$')
        plt.ylabel('$u_\phi$')
        # plt.ylim([1.2, 2.4])
        plt.xlim([self.rFval, self.rISCO])
        plt.title(r'$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$'.format(self.Fthetaphi, self.a))
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.tight_layout()
        plt.savefig(figure_dir + "uphi_solution.png")
        plt.close()

    def plot_vPhi_solution(self):
        figure_dir = self.path_to_figures2
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)
        plt.figure()
        plt.plot(self.r_range, self.uPhisolution/self.uTsolution * self.r_range)
        plt.xlabel('$r/r_G$')
        plt.ylabel('$v^\phi$')
        plt.ylim([-1.0, 1.0])
        plt.xlim([self.rFval, self.rISCO])
        plt.title(r'$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$'.format(self.Fthetaphi, self.a))
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.tight_layout()
        plt.savefig(figure_dir + "vPhi_solution.png")
        plt.close()

    def plot_mass_density_solution(self, scaled=False, **kwargs):
        M_in_Msun = kwargs.get("M_in_Msun", 10.0)
        f_Ledd = kwargs.get("f_Ledd", 0.1)
        f_Mdot = kwargs.get("f_Mdot", 0.1)
        H_over_R = kwargs.get("H_over_R", 1.0)

        plt.figure()
        figure_dir = self.path_to_figures2
        title_str = r'$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$'.format(self.Fthetaphi, self.a)
        # title_str = r'$F_{\theta\phi}=$' + ' {:.1f}'.format(self.Fthetaphi)
        if scaled:
            figure_dir += "scaled/"
            figure_dir += "h{:.2f}Msun{:.1f}/".format(H_over_R, M_in_Msun)
            mass_density = self.get_mass_density_in_cgs(M_in_Msun, f_Ledd, f_Mdot, H_over_R)
            plt.ylabel(r'$\rho~[{\rm g/cm^3}]$')
            title = r'Scaled using $\dot M={:.2f}\dot M_{{\rm Edd}}$ for $M=${:.1E}$M_\odot$'.format(f_Mdot, M_in_Msun)
            title += '\n' +  r'$H/R={:.2f}$, '.format(H_over_R)
            title += title_str
            title_str = title
            ls='solid'
        else:
            mass_density = self.rhosmooth
            plt.ylabel(r'$\rho$');
            # plt.ylim([0, 1.0])
            ls='dashed'
        plt.yscale('log')
        plt.gca().grid(color='.9', ls='--')
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)

        plt.plot(self.r_range, mass_density, ls=ls)
        plt.xlabel('$r/r_G$')
        plt.title(title_str)
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.tight_layout()
        plt.savefig(figure_dir + "mass_density_solution.png")
        plt.close()

    def plot_tau_es(self, scaled=False, **kwargs):
        if not scaled:
            print("Why would I plot optical depth without physical units...exiting.")
            return
        M_in_Msun = kwargs.get("M_in_Msun", 10.0)
        f_Ledd = kwargs.get("f_Ledd", 0.1)
        f_Mdot = kwargs.get("f_Mdot", 0.1)
        H_over_R = kwargs.get("H_over_R", 1.0)

        plt.figure()
        figure_dir = self.path_to_figures2
        title_str = r'$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$'.format(self.Fthetaphi, self.a)
        # title_str = r'$F_{\theta\phi}=$' + ' {:.1f}'.format(self.Fthetaphi)
        figure_dir += "scaled/"
        figure_dir += "h{:.2f}Msun{:.1f}/".format(H_over_R, M_in_Msun)
        mass_density = self.get_mass_density_in_cgs(M_in_Msun, f_Ledd, f_Mdot, H_over_R)
        title = r'Scaled using $\dot M={:.2f}\dot M_{{\rm Edd}}$ for $M=${:.1E}$M_\odot$'.format(f_Mdot, M_in_Msun)
        title += '\n' +  r'$H/R={:.2f}$, '.format(H_over_R)
        title += title_str
        title_str = title
        ls='solid'
        plt.gca().grid(color='.9', ls='--')
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)

        tau_es = mass_density/Consts.mp_in_g * Consts.thomson_cross_section_cgs * H_over_R * self.r_range * get_rg_in_cm(M_in_Msun)
        plt.plot(self.r_range, tau_es, ls=ls)
        plt.xlabel('$r/r_G$')
        plt.ylabel(r'$\tau_{\rm es}$')
        # plt.ylim([1, np.max(tau_es)])
        # plt.yscale('log')
        # plt.ylim(bottom=0)
        plt.ylim([0, 10])
        plt.title(title_str)
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.tight_layout()
        plt.savefig(figure_dir + "tau_es.png")
        plt.close()

    def plot_radius_over_time(self):
        figure_dir = self.path_to_figures2
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)
        plt.figure()
        plt.plot(self.times_at_radius/self.t_infall, self.r_range, marker='o', ls='None')
        plt.xlim([0, 1])
        plt.ylabel('$r/r_G$')
        plt.xlabel('$t/t_f$')
        plt.title(r'$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$'.format(self.Fthetaphi, self.a))
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.tight_layout()
        plt.savefig(figure_dir + "radius_over_time.png")
        plt.close()

    def plot_orbits(self):
        figure_dir = self.path_to_figures2
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)
        phi_over_time = (integrate.cumtrapz(self.uPhisolution/self.uRsmooth, self.r_range) + 0) % (2.0*np.pi)
        # Adjust to plot periodic: https://stackoverflow.com/questions/14357104/plot-periodic-trajectories
        # Make a masked array with jump points masked
        abs_d_data = np.abs(np.diff(phi_over_time))
        mask = np.hstack([ abs_d_data > abs_d_data.mean()+3*abs_d_data.std(), [False]])
        masked_phi = np.ma.MaskedArray(phi_over_time, mask)
        N = self.r_range.size - 1
        cmap = matplotlib.cm.viridis(range(N))

        plt.figure()
        for i in np.arange(N):
            plt.plot(self.r_range[i], masked_phi[i], color=cmap[i], marker='o')
        plt.ylim([0, 2*np.pi])
        plt.xlim([self.rEH, self.rISCO])
        plt.xlabel('$r/r_G$')
        plt.ylabel(r'$\phi$');
        plt.title(r'$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$'.format(self.Fthetaphi, self.a))
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.gca().grid(color='.9', ls='--')
        plt.tight_layout()
        plt.savefig(figure_dir + "orbits.png")
        plt.close()

    def plot_Br_solution(self, scaled=False, **kwargs):
        M_in_Msun = kwargs.get("M_in_Msun", 10.0)
        f_Ledd = kwargs.get("f_Ledd", 0.1)
        f_Mdot = kwargs.get("f_Mdot", 0.1)
        H_over_R = kwargs.get("H_over_R", 1.0)

        plt.figure()
        figure_dir = self.path_to_figures2
        title_str = r'$F_{{\theta\phi}}={:.2f}$, $a={:.2f}$'.format(self.Fthetaphi, self.a)
        if scaled:
            figure_dir += "scaled/"
            figure_dir += "h{:.2f}Msun{:.1f}/".format(H_over_R, M_in_Msun)
            Br = self.get_Br_in_cgs(M_in_Msun, f_Ledd, f_Mdot, H_over_R)
            plt.ylabel(r"$B_r$ [G]")
            title = r'Scaled using $\dot M={:.2f}\dot M_{{\rm Edd}}$ for $M=${:.1E}$M_\odot$'.format(f_Mdot, M_in_Msun)
            title += '\n' +  r'$H/R={:.2f}$, '.format(H_over_R)
            title += title_str
            title_str = title
            plt.gca().grid(color='.9', ls='--')
        else:
            Br = -self.Fthetaphi/self.r_range**2.0
            plt.ylabel(r"$B_r$")
            ls='dashed'
        ls='solid'
        if not os.path.exists(figure_dir):
            os.makedirs(figure_dir)

        plt.plot(self.r_range, Br, ls=ls)
        plt.xlabel('$r/r_G$')
        plt.title(title_str)
        plt.gca().tick_params(top=True, right=True, direction='in', which='both')
        plt.tight_layout()
        plt.savefig(figure_dir + "Br_solution.png")
        plt.close()


if __name__ == "__main__":
    """
    Run some plots.
    """
    kwargs = {}
    kwargs["overwrite_self"] = False
    kwargs["Fthetaphi"] = 2.0
    # For other values of Fthetaphi, might need to scan guesses
    # to find a physical fast critical point location
    # kwargs["Fthetaphi"] = 1.5
    # kwargs["uRFguess"] = -0.2
    # kwargs["rFguess"] = 1.5
    # kwargs["Fthetaphi"] = 1.0
    # kwargs["uRFguess"] = -1.4
    # kwargs["rFguess"] = 1.4
    # kwargs["Fthetaphi"] = 0.6
    # kwargs["uRFguess"] = -1.3
    # kwargs["rFguess"] = 1.4
    kwargs["a"] = 0.50
    # kwargs["rFguess"] = 2.67
    # kwargs["rFguess"] = "estimate"
    # kwargs["rFguess"] = 1.869
    # kwargs["uRFguess"] = -0.50
    bg = dynamical_background(**kwargs)
    print(bg.rISCO)
    print(bg.rEH)

    # -----------------------------------
    # Reproduce Gammie 1999 Fig. 1
    # -----------------------------------
    bg.plot_uT_solution()
    bg.plot_uR_solution()
    bg.plot_uphi_solution()
    bg.plot_mass_density_solution()

    # -----------------------------------
    # Plot orbits/infall times
    # -----------------------------------
    bg.plot_radius_over_time()
    bg.plot_orbits()

    # -----------------------------------
    # Plot scaled background
    # -----------------------------------
    M_in_Msun = 10.0
    scaled = True
    kwargs["H_over_R"] = 1.0
    kwargs["M_in_Msun"] = M_in_Msun
    # kwargs["f_Mdot"] = 0.01
    print("({:.3f}, {:.3f}, {:.3f}, {:.3f})".format(bg.rFval, bg.uRFval, bg.FLval, bg.FEval))
    # bg.plot_mass_density_solution(scaled, **kwargs)
    # bg.plot_Br_solution(scaled, **kwargs)
    # bg.plot_tau_es(scaled, **kwargs)

    # FM_cgs = bg.get_FM_in_cgs(M_in_Msun)
    # rg_cgs = bg.get_rg_in_cm(M_in_Msun)
    # rISCO_cgs = bg.rISCO*rg_cgs
