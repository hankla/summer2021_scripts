# Diagnosing why the disk emission is non-monotonic with inclination angle/spin
import numpy as np
import os
import matplotlib
import pickle
import matplotlib.pyplot as plt
import sys
sys.path.append('')
from tools import *
from dynamical_background_gammie1999 import *
from equilibrium_finder import *

large_size = 22
medium_size = 16
# medium_size = 18
matplotlib.rc('axes', labelsize=medium_size, titlesize=large_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
# matplotlib.rc('legend', fontsize=16)
matplotlib.rc('legend', fontsize=medium_size)
# matplotlib.rc('figure', titlesize=large_size)
matplotlib.rc('text', usetex=True)

def plot_disk_spectra_over_spin(**kwargs):
    figdir = "/mnt/c/Users/liaha/research/projects/summer2021/figures/disk_spectra/"
    figdir = kwargs.get("figdir", figdir)
    print("Saving in " + figdir)
    # inc_angles = kwargs.get("inc_angles", [0, 30, 60, 85])
    inc_angles = kwargs.get("inc_angles", [30, 0, 60, 85, 15, 45, 75, 90])
    spins = kwargs.get("spins", [0.5, 0.7, 0.85, 0.95])
    # spins = kwargs.get("spins", [0.5, 0.9, 0.998])
    xlims = kwargs.get("xlims", None)
    ylims = kwargs.get("ylims", None)
    normalize = kwargs.get("normalize", False)
    for inc_angle in inc_angles:
        max_val = 0.0
        figname = "i{:.2f}_over_spin".format(inc_angle)
        if normalize:
            figname += "_norm"
        figname += ".png"

        spectrum_dict = {}
        for spin in spins:
            disk_spectrum_dict = retrieve_disk_spectrum(spin, inc_angle, **kwargs)
            Lnu_disk = disk_spectrum_dict["Lnu_disk"]
            spectrum_dict[spin] = Lnu_disk
            if "frequencies" not in spectrum_dict:
                frequencies = disk_spectrum_dict["spectral_bins"]
                spectrum_dict["frequencies"] = frequencies
            inc_max = (frequencies*Lnu_disk).max()
            if inc_max > max_val:
                max_val = inc_max

        plt.figure()
        frequencies = spectrum_dict["frequencies"]
        for spin in spins:
            Lnu_disk = spectrum_dict[spin]
            if normalize:
                Lnu_disk = Lnu_disk/max_val
            plt.plot(frequencies, frequencies*Lnu_disk, label=r"$a={:.2f}$".format(spin))
        plt.xlabel(r"$\nu/{\rm Hz}$ ")
        ylabel = r"$\nu L_\nu$"
        if normalize:
            ylabel += " normalized"
        plt.ylabel(ylabel)
        plt.xscale('log')
        plt.yscale('log')
        plt.legend()
        if xlims is None:
            # plt.xlim([1e16, 2e19])
            plt.xlim([kevToNu(0.1), kevToNu(11)])
        else:
            plt.xlim(xlims)
        if ylims is None:
            if normalize:
                plt.ylim([0.01, 1.1])
            else:
                plt.ylim([1e34, max_val*1.1])
                # plt.ylim([1e34, 2e38])
                # plt.ylim([1e30, 3e38])
        else:
            plt.ylim(ylims)
        ax = plt.gca()
        plt.gca().tick_params(right=True, direction='in', which='both')
        secax = plt.gca().secondary_xaxis('top', functions=(nuToKev, kevToNu))
        secax.tick_params(top=True, direction='in', which='both')
        secax.set_xlabel(r'$\nu/{\rm keV}$')
        plt.gcf().canvas.draw()
        for x in secax.get_xticks():
            ax.axvline(kevToNu(x), color='gray', ls='--', alpha=0.4)
        plt.title(r"$i={:d}^\circ$".format(inc_angle))
        plt.tight_layout()
        plt.savefig(figdir + figname)
        plt.close()
    return

def plot_disk_spectra_over_i(**kwargs):
    figdir = "/mnt/c/Users/liaha/research/projects/summer2021/figures/disk_spectra/"
    figdir = kwargs.get("figdir", figdir)
    print("Saving in " + figdir)
    inc_angles = kwargs.get("inc_angles", [0, 30, 60, 85])
    spins = kwargs.get("spins", [0.5, 0.7, 0.85, 0.95])
    xlims = kwargs.get("xlims", None)
    ylims = kwargs.get("ylims", None)
    normalize = kwargs.get("normalize", False)

    for spin in spins:
        max_val = 0.0
        figname = "a{:.2f}_over_inclination".format(spin)
        if normalize:
            figname += "_norm"
        figname += ".png"

        spectrum_dict = {}
        for inc_angle in inc_angles:
            disk_spectrum_dict = retrieve_disk_spectrum(spin, inc_angle, **kwargs)
            Lnu_disk = disk_spectrum_dict["Lnu_disk"]
            spectrum_dict[inc_angle] = Lnu_disk
            if "frequencies" not in spectrum_dict:
                frequencies = disk_spectrum_dict["spectral_bins"]
                spectrum_dict["frequencies"] = frequencies
            inc_max = (frequencies*Lnu_disk).max()
            if inc_max > max_val:
                max_val = inc_max

        plt.figure()
        frequencies = spectrum_dict["frequencies"]
        for inc_angle in inc_angles:
            Lnu_disk = spectrum_dict[inc_angle]
            if normalize:
                Lnu_disk = Lnu_disk/max_val
            plt.plot(frequencies, frequencies*Lnu_disk, label=r"$i={:d}^\circ$".format(inc_angle))
        plt.xlabel(r"$\nu/{\rm Hz}$ ")
        ylabel = r"$\nu L_\nu$"
        if normalize:
            ylabel += " normalized"
        plt.ylabel(ylabel)
        plt.xscale('log')
        plt.yscale('log')
        plt.legend()
        if xlims is None:
            # plt.xlim([1e16, 2e19])
            plt.xlim([kevToNu(0.1), kevToNu(11)])
        else:
            plt.xlim(xlims)
        if ylims is None:
            if normalize:
                plt.ylim([0.01, 1.1])
            else:
                plt.ylim([1e34, max_val*1.1])
                # plt.ylim([1e34, 2e38])
                # plt.ylim([1e30, 3e38])
        else:
            plt.ylim(ylims)
        ax = plt.gca()
        plt.gca().tick_params(right=True, direction='in', which='both')
        secax = plt.gca().secondary_xaxis('top', functions=(nuToKev, kevToNu))
        secax.tick_params(top=True, direction='in', which='both')
        secax.set_xlabel(r'$\nu/{\rm keV}$')
        plt.gcf().canvas.draw()
        for x in secax.get_xticks():
            ax.axvline(kevToNu(x), color='gray', ls='--', alpha=0.4)
        plt.title(r"$a={:.2f}$".format(spin))
        plt.tight_layout()
        plt.savefig(figdir + figname)
        plt.close()
    return

def retrieve_disk_spectrum(spin, inc_angle_deg, **kwargs):
    # disk_spectrum_path = "/mnt/c/Users/liaha/research/projects/summer2021/data_reduced/disk_spectra/"
    # disk_spectrum_path += "a{:.2f}/".format(spin)
    # disk_spectrum_path += "redshifted_i{:d}.p".format(inc_angle_deg)
    # disk_spectrum_path = kwargs.get("disk_spectrum_path", disk_spectrum_path)
    overwrite = kwargs.get("overwrite_disk_spectrum", False)
    quiet = kwargs.get("quiet", False)

    eqkwargs = {}
    eqkwargs["a"] = spin
    eqkwargs["Fthetaphi"] = kwargs.get("Fthetaphi", 6.0)
    bg = dynamical_background(**eqkwargs)
    eq = equilibrium_finder(bg, **eqkwargs)

    (spectrum, disk_spectrum_dict) = eq.retrieve_spectra(quiet, False, overwrite, inc_angle_deg, disk_only=True)
    # return eq.retrieve_disk_spectrum
    # if overwrite or (not os.path.exists(disk_spectrum_path)):
        # # print("Calculating disk spectrum " + disk_spectrum_path)
        # print("Whoops for " + disk_spectrum_path)
        # # disk_spectrum_dict = self.calculate_redshifted_disk_spectrum(inc_angle_deg)
    # else:
        # print("Loading disk spectrum from pickle file " + disk_spectrum_path)
        # with open(disk_spectrum_path, 'rb') as file:
            # disk_spectrum_dict = pickle.load(file)
    return disk_spectrum_dict

def plot_disk_redshifts_over_r(spin, inc_angle, **kwargs):
    figdir = "/mnt/c/Users/liaha/research/projects/summer2021/figures/disk_diagnostics/redshifts/a{:.2f}/".format(spin)
    figdir = kwargs.get("figdir", figdir)
    if not os.path.exists(figdir):
        os.makedirs(figdir)
    eqkwargs = {}
    eqkwargs["a"] = spin
    eqkwargs["Fthetaphi"] = kwargs.get("Fthetaphi", 6.0)
    bg = dynamical_background(**eqkwargs)
    eq = equilibrium_finder(bg, **eqkwargs)
    redshift_dict = eq.trace_geodesics(inc_angle)
    plt.plot(redshift_dict["disk_emitted_radii"], redshift_dict["disk_redshifts"], ls='None', marker='o')
    plt.ylabel(r"Redshift $g$")
    plt.xlabel(r"$r/r_g$")
    plt.title(r"$i={:.0f}^\circ$, {:d} total photons".format(inc_angle, redshift_dict["disk_redshifts"].size))
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.tight_layout()
    plt.savefig(figdir + "redshifts_over_r_i{:d}.png".format(inc_angle))
    plt.close()

    plt.figure()
    plt.plot(redshift_dict["photon_emitted_radii"], redshift_dict["redshifts"], ls='None', marker='o')
    plt.ylabel(r"Redshift $g$")
    plt.xlabel(r"$r/r_g$")
    plt.title(r"$i={:.0f}^\circ$, {:d} total photons".format(inc_angle, redshift_dict["redshifts"].size))
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.tight_layout()
    plt.savefig(figdir + "plunging_redshifts_over_r_i{:d}.png".format(inc_angle))
    plt.close()
    return

def plot_disk_velocities_over_r(spin, **kwargs):
    figdir = "/mnt/c/Users/liaha/research/projects/summer2021/figures/disk_diagnostics/four_velocities_over_r/"
    figdir = kwargs.get("figdir", figdir)
    if not os.path.exists(figdir):
        os.makedirs(figdir)
    mass = kwargs.get("mass", 1.0) # dimensionless
    M_in_Msun = kwargs.get("M_in_Msun", 10.0)
    rg_cm = get_rg_in_cm(M_in_Msun)
    disk_inner_edge = get_rISCO(spin, mass)*rg_cm
    disk_outer_edge = 1.0e3*rg_cm
    disk_inner_edge = kwargs.get("disk_inner_edge", disk_inner_edge)
    disk_outer_edge = kwargs.get("disk_outer_edge", disk_outer_edge)
    radii = np.logspace(np.log10(disk_inner_edge), np.log10(disk_outer_edge), 1000)
    r_in_rg = radii/rg_cm

    # LNRF observer has constant r, theta and phi = omega*t + const.
    # i.e. omega is angular velocity of LNRF in coordinate frame.
    # omega = -gtphi/gphiphi (Bardeen, Press, Teukolsky p. 354 top)
    omega = -gtphi(spin, mass, r_in_rg)/gphiphi(spin, mass, r_in_rg)
    # Omega is the coordinate angular velocity of a circular equatorial orbit
    # Omega = dphi/dt, BPT Eq. 2.16
    Omega = np.sqrt(mass)/(r_in_rg**1.5 + spin*np.sqrt(mass))
    plt.figure()
    plt.plot(r_in_rg, Omega, label=r"$\Omega$ (Circular orbit)")
    plt.plot(r_in_rg, omega, label=r"$\omega$ (LNRF)")
    plt.legend()
    plt.xlabel(r"$r/r_g$")
    plt.xscale('log')
    plt.yscale('log')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(r"$a={:.2f}$".format(spin))
    plt.tight_layout()
    figname = "angular_velocities_over_r_a{:.2f}.png".format(spin)
    figname = kwargs.get("figname", figname)
    plt.savefig(figdir + figname)
    plt.close()

    # Note that the gas velocity in the coordinate frame is not important for
    # calculating the redshift...for that we need the velocity in the LNRF.
    # Novikov Thorne gives the thin disk velocity already in the LNRF,
    # so we don't need to transform it. Eq. 5.4.4a
    # Let's compare those four velocities...
    # ----------------------------
    # OLD WAY
    [vR_lnrf, vTheta_lnrf, vPhi_lnrf] = get_thin_disk_lnrf_velocities_old(spin, mass, r_in_rg)
    # ----------------------------
    # NT WAY
    [vR_lnrf, vTheta_lnrf, vPhi_lnrf_NT] = get_thin_disk_lnrf_velocities(spin, mass, r_in_rg)
    print("Old and NT way agree: " + np.str(np.allclose(vPhi_lnrf, vPhi_lnrf_NT)))
    plt.figure()
    plt.plot(r_in_rg, vPhi_lnrf, label=r"Old way")
    plt.plot(r_in_rg, vPhi_lnrf_NT, label=r"NT way")
    plt.legend()
    plt.xlabel(r"$r/r_g$")
    plt.ylabel(r"$v^\phi$")
    plt.xscale('log')
    plt.yscale('log')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(r"$a={:.2f}$".format(spin))
    plt.tight_layout()
    figname = "vPhi_lnrf_over_r_a{:.2f}.png".format(spin)
    figname = kwargs.get("figname", figname)
    plt.savefig(figdir + figname)
    plt.close()

    [uT, uR, uTheta, uPhi] = get_thin_disk_four_velocity(spin, mass, r_in_rg)

    print(uT.max())
    print(uT.min())
    gamma_lnrf = Bfunc(spin, r_in_rg)*np.sqrt(Dfunc(spin, r_in_rg)/Afunc(spin, r_in_rg)/Cfunc(spin, r_in_rg))
    plt.figure()
    plt.plot(r_in_rg, uT, label=r"Coordinate $\gamma$")
    plt.plot(r_in_rg, gamma_lnrf, label=r"LNRF $\gamma$")
    plt.legend()
    plt.xlabel(r"$r/r_g$")
    plt.xscale('log')
    plt.yscale('log')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(r"$a={:.2f}$".format(spin))
    plt.tight_layout()
    figname = "gamma_over_r_a{:.2f}.png".format(spin)
    figname = kwargs.get("figname", figname)
    plt.savefig(figdir + figname)
    plt.close()

    # Definition of lorentz factor
    betaPhi = np.sqrt(1.0 - 1.0/uT**2.0)
    newtonianPhi = r_in_rg/(r_in_rg**1.5 + spin)
    plt.figure()
    plt.plot(r_in_rg, betaPhi, label="Coordinate $v^\phi$")
    plt.plot(r_in_rg, vPhi_lnrf, label="LNRF $v^\phi$")
    plt.plot(r_in_rg, newtonianPhi, label="Newtonian")
    plt.legend()
    plt.xlabel(r"$r/r_g$")
    plt.ylabel(r"$v_\phi/c$")
    plt.xscale('log')
    plt.yscale('log')
    # plt.ylim([5e7, None])
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(r"$a={:.2f}$".format(spin))
    plt.tight_layout()
    figname = "betaPhi_over_r_a{:.2f}.png".format(spin)
    figname = kwargs.get("figname", figname)
    plt.savefig(figdir + figname)
    plt.close()
    return

def plot_disk_temperature_over_r(spin, Mdot, **kwargs):
    figdir = "/mnt/c/Users/liaha/research/projects/summer2021/figures/disk_diagnostics/temperature_over_r/"
    figdir = kwargs.get("figdir", figdir)
    if not os.path.exists(figdir):
        os.makedirs(figdir)
    mass = kwargs.get("mass", 1.0) # dimensionless
    M_in_Msun = kwargs.get("M_in_Msun", 10.0)
    rg_cm = get_rg_in_cm(M_in_Msun)
    disk_inner_edge = get_rISCO(spin, mass)*rg_cm
    disk_outer_edge = 1.0e2*rg_cm
    disk_inner_edge = kwargs.get("disk_inner_edge", disk_inner_edge)
    disk_outer_edge = kwargs.get("disk_outer_edge", disk_outer_edge)
    disk_radial_bins = np.logspace(np.log10(disk_inner_edge), np.log10(disk_outer_edge), 1000)
    r_in_rg = disk_radial_bins/rg_cm
    disk_temp_over_r = (3*Consts.G_in_cgs*M_in_Msun*Consts.solar_mass_in_g*Mdot/(8*np.pi*disk_radial_bins**3*Consts.SB_cgs))**0.25
    # rstar = get_rEH(spin)*rg_cm
    rstar = disk_inner_edge*0.9
    newtonian_temp = disk_temp_over_r * (1.0 - np.sqrt(rstar/disk_radial_bins))**0.25
    relativistic_flux_correction = funcFromHell(spin, r_in_rg)/Bfunc(spin, r_in_rg)/np.sqrt(Cfunc(spin, r_in_rg))
    relativistic_temp_correction = relativistic_flux_correction**0.25
    rel_disk_temp_over_r = disk_temp_over_r * relativistic_temp_correction
    plt.plot(r_in_rg, newtonian_temp, label="Newtonian T(r)")
    plt.plot(r_in_rg, rel_disk_temp_over_r, label="With NT corrections")
    plt.legend()
    plt.xlabel(r"$r/r_g$")
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim([5e7, None])
    plt.gca().tick_params(right=True, direction='in', which='both')
    plt.title(r"$a={:.2f}$, $\dot M={:.2e}$ g/s".format(spin, Mdot))
    plt.tight_layout()
    figname = "temperature_over_r_a{:.2f}_Mdot{:.2e}.png".format(spin, Mdot)
    figname = kwargs.get("figname", figname)
    plt.savefig(figdir + figname)
    print("Max temp: {:.2e} K".format(rel_disk_temp_over_r.max()))
    return


if __name__ == '__main__':
    # plot_disk_spectra_over_i()
    spin = 0.95
    # spin = 0.50
    Mdot = 1.0e28 # g/s
    Fthetaphi = 6.0
    inc_angle = 60
    plot_disk_temperature_over_r(spin, Mdot)
    # plot_disk_velocities_over_r(spin)
    # plot_disk_redshifts_over_r(spin, inc_angle)
    inc_angles = [0, 15, 30, 45, 60, 75, 85, 90]
    # inc_angles = [5]
    # for angle in inc_angles:
        # plot_disk_redshifts_over_r(spin, angle)

    kwargs = {}
    kwargs["a"] = spin
    kwargs["Fthetaphi"] = Fthetaphi
    # bg = dynamical_background(**kwargs)
    # equilibrium = equilibrium_finder(bg, **kwargs)
    # plot_disk_spectra_over_i(normalize=True)
    # plot_disk_spectra_over_i(normalize=False)
    # plot_disk_spectra_over_spin(normalize=True)
    plot_disk_spectra_over_spin(normalize=False)
