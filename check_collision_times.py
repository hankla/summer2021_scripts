import sys
import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
sys.path.append('')
from tools import *
from dynamical_background_gammie1999 import *
from equilibrium_finder import *
from scipy import special

small_size = 22
medium_size = 14
medium_size = 18
matplotlib.rc('axes', labelsize=medium_size)
matplotlib.rc('xtick', labelsize=medium_size)
matplotlib.rc('ytick', labelsize=medium_size)
# matplotlib.rc('legend', fontsize=medium_size)
matplotlib.rc('legend', fontsize=14)
matplotlib.rc('figure', titlesize=small_size)
matplotlib.rc('text', usetex=True)

def check_cooling_equals_collision_freq(ndensity, theta, cooling_const, beta_values):
    turnover_beta, turnover_gamma = get_turnover_beta_gamma(theta)
    coll_freq_solution = calculate_max_energy_loss_freq(theta, ndensity)
    temperature = theta*Consts.restmass_cgs/Consts.kB_cgs

    beta2, beta1_sol = beta_values
    coll_freq_solution = calculate_max_energy_loss_freq(theta, ndensity)
    max_nu_coll = -coll_freq_solution.fun
    min_coll_beta = coll_freq_solution.x[0]

    beta_range = np.logspace(np.log10(min_coll_beta), np.log10(beta2), 1000)
    gamma_range = 1.0/np.sqrt(1.0 - beta_range**2)

    cooling_freq = gamma_range/cooling_const
    collision_freq = -minus_energy_loss_frequency(beta_range, ndensity, theta)

    plt.figure()
    plt.plot(beta_range, cooling_freq, label=r"$\nu_{\rm cool}$")
    plt.plot(beta_range, collision_freq, label=r"$\nu_{\rm coll}$")
    plt.gca().axvline([beta1_sol], ls='--', color='black', label=r"$\beta_{{1, sol}}={:.2f}$".format(beta1_sol))
    plt.gca().axvline([min_coll_beta], ls=':', color='black', label=r"$\beta(t_{{\rm coll, min}})={:.4f}$".format(min_coll_beta))
    # plt.gca().axvline([turnover_beta], ls=':', color='black', label=r"$\beta_{{\rm turnover}}={:.2f}$".format(turnover_beta))
    # plt.gca().axvline([minimum_beta1], ls='-.', color='black', label=r"$\beta_{{\rm min}}={:.2f}$".format(minimum_beta1))
    plt.legend()
    plt.ylim([0.0, collision_freq.max()*1.1])
    # plt.xlim([turnover_beta*0.99, turnover_beta*1.1])
    # plt.xlim([turnover_gamma*0.99, gamma2])
    plt.xlabel(r"$\beta$")
    plt.ylabel(r"$\nu_\epsilon$")
    plt.xscale('log')
    # plt.yscale('log')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(r"T={:.2e} K".format(temperature))
    plt.tight_layout()
    plt.show()


def check_max_nuEnergyLoss(ndensity, theta, beta_values):
    turnover_beta, turnover_gamma = get_turnover_beta_gamma(theta)
    coll_freq_solution = calculate_max_energy_loss_freq(theta, ndensity)
    temperature = theta*Consts.restmass_cgs/Consts.kB_cgs

    minimum_beta1, beta2 = beta_values
    beta_range = np.logspace(np.log10(turnover_beta), np.log10(beta2), 1000)

    plt.figure()
    plt.plot(beta_range, -minus_energy_loss_frequency(beta_range, ndensity, theta), label=r"$\nu_{\rm coll}$")
    plt.gca().axvline([coll_freq_solution.x[0]], ls='--', color='black', label=r"$\beta(\nu_{{\rm coll, max}})={:.2f}$".format(coll_freq_solution.x[0]))
    plt.gca().axvline([turnover_beta], ls=':', color='black', label=r"$\beta_{{\rm turnover}}={:.2f}$".format(turnover_beta))
    plt.gca().axvline([minimum_beta1], ls='-.', color='black', label=r"$\beta_{{\rm min}}={:.2f}$".format(minimum_beta1))
    plt.legend()
    plt.ylim([0.0, None])
    plt.xlim([turnover_beta*0.99, beta2])
    plt.xlabel(r"$\beta$")
    plt.ylabel(r"$\nu_\epsilon$")
    plt.xscale('log')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(r"T={:.2e} K".format(temperature))
    plt.tight_layout()
    plt.show()

def timescales_over_gamma(equilibrium, index):
    """
    Plot various infall times and collision times as a function of Lorentz factor.
    """
    bg = equilibrium.bg
    gamma_range = np.logspace(np.log10(1.5), np.log10(1.0e2), 1000)
    # Infall time.
    # bg.times_at_radius[0] is at THE EVENT HORIZON but bg.r_range[0] is at the ISCO
    # so need to reverse
    radius = equilibrium.quick_r_range[index]
    tinfall_over_r_interp_f = interp1d(bg.r_range, bg.times_at_radius[::-1])
    tinfall_at_r = tinfall_over_r_interp_f(radius)
    tinfall_at_r_s = tinfall_at_r * equilibrium.bg.get_tg_in_s(equilibrium.M_in_Msun)

    cooling_const_at_r = equilibrium.quick_cooling_consts[index]
    cooling_times = cooling_const_at_r/gamma_range

    Te_at_r = equilibrium.temperature_over_r[index]
    theta_e_at_r = Consts.kB_cgs*Te_at_r/Consts.restmass_cgs
    ndensity_at_r = equilibrium.quick_number_density_over_r[index]
    beta_range = np.sqrt(1.0 - 1.0/gamma_range**2)

    print("Temperature: {:.2e} K".format(Te_at_r))

    tEnergyLoss = energy_loss_timescale(beta_range, ndensity_at_r, theta_e_at_r)
    tThermalize = equilibrium.t_thermalize[index]

    plasma_freq = equilibrium.plasma_freq_over_r[index]
    tAccel = 300/equilibrium.ion_magnetization_over_r[index]/plasma_freq

    plt.figure()
    plt.gca().axhline([tThermalize/tinfall_at_r_s], ls=':', color='black', label=r"$t_{\rm therm}^{ep}/t_{\rm infall}$")
    # plt.gca().axhline([1.0], ls='--', color='black', label=r"$t_{\rm infall}/t_{\rm infall}$")
    plt.gca().axhline([1.0], ls='--', color='black', label=None)
    plt.plot(gamma_range, tEnergyLoss/tinfall_at_r_s, label=r"$t_{\rm coll}^{ee}(\gamma)/t_{\rm infall}$")
    plt.gca().axhline([tAccel/tinfall_at_r_s], ls='-.', color='black', label=r"$t_{\rm accel}/t_{\rm infall}$")
    plt.plot(gamma_range, cooling_times/tinfall_at_r_s, label=r"$t_{\rm cool}(\gamma)/t_{\rm infall}$")

    plt.xlabel(r"$\gamma$")
    plt.ylabel(r"$t/t_{\rm infall}$")
    plt.xlim([gamma_range.min(), gamma_range.max()])
    # plt.xlim([1.0, gamma_range.max()])
    plt.xscale('log')
    plt.yscale('log')
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    # plt.gca().grid(color='.9', ls='--')
    plt.title(r"$T_e={:.2e}$ K at $r={:.2f}r_g$".format(Te_at_r, radius))
    # plt.legend(loc='lower right', frameon=False, ncol=2)
    plt.legend(loc='best', frameon=False, ncol=2)
    # plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left', frameon=False)
    # plt.legend(bbox_to_anchor=(0.5, 1.15), loc='upper center', frameon=False, ncol=2)
    plt.ylim([1e-8, 2e1])
    plt.tight_layout()
    # plt.show()

    figure_dir2 = equilibrium.path_to_figures2
    fname = "timescales_over_gamma_r{:.2f}.png".format(radius)
    plt.savefig(figure_dir2 + fname)
    if not os.path.isdir(figure_dir2 + "pdfs/"):
        os.makedirs(figure_dir2 + "pdfs/")
    plt.title('')
    plt.savefig(figure_dir2 + "pdfs/" + fname.replace(".png", ".pdf"), bbox_inches='tight')
    print("Saving " + figure_dir2 + fname)
    plt.close()


if __name__ == '__main__':
    kwargs = {}
    kwargs["Fthetaphi"] = 6.0
    kwargs["minimum_gamma1"] = 2.0
    # kwargs["a"] = 0.8
    # kwargs["Fthetaphi"] = 1.0
    bg = dynamical_background(**kwargs)

    kwargs["PL_type"] = "WUBCN18"
    # kwargs["PL_type"] = "WPU19"
    # kwargs["PL_type"] = "constant"
    # kwargs["gamma2_type"] = "fixed"
    kwargs["gamma2_type"] = "4sigma"

    kwargs["quick"] = True
    kwargs["jump"] = 1
    decrease_by_tau = True
    decrease_by_tau = False
    # inc_angle_deg = None
    inc_angle_deg = 60

    equilibrium = equilibrium_finder(bg, **kwargs)
    timescales_over_gamma(equilibrium, 500)
