#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import sys
sys.path.append('')
from dynamical_background_gammie1999 import dynamical_background


class guess_scan(dynamical_background):
    def __init__(self, setup="lia_hp", **kwargs):
        """
        TODO:
        """
        overwrite_self = kwargs.get("overwrite_self", False)
        self.to_print = kwargs.get("printing", False)
        self.Fthetaphi = kwargs.get("Fthetaphi", 6.0)
        self.a = kwargs.get("a", 0.95)
        # -----------------------------------
        #      Add default setup information here
        # -----------------------------------
        if setup == "lia_hp":
            path_to_figures = "/mnt/c/Users/liaha/research/projects/"\
                "summer2021/figures/"
            path_to_reduced_data = "/mnt/c/Users/liaha/research/projects/"\
                "summer2021/data_reduced/"
            pickle_path = path_to_reduced_data + "bg_guess_scan_Fthetaphi{:.2f}a{:.2f}.p".format(self.Fthetaphi, self.a)

        # -----------------------------------
        # If paths are non-standard, set them here using kwargs
        # Otherwise, use the defaults from above
        # -----------------------------------
        self.path_to_figures = kwargs.get("path_to_figures", path_to_figures)
        self.path_to_reduced_data = kwargs.get("path_to_reduced_data",
                                               path_to_reduced_data)
        self.pickle_path = kwargs.get("pickle_path", pickle_path)

        if os.path.exists(self.pickle_path) and (not overwrite_self):
            print("Loading guess scan from " + self.pickle_path)
            self._load_from_pickle()

        else:
            self._initialize(**kwargs)


    def _initialize(self, **kwargs):
        print("Creating new dynamical background.")
        self.M = 1

        self.rISCO = self.get_ISCO()
        self.rEH = self.get_event_horizon()
        self.OmegaF = self.OmegaKep(self.rISCO)
        self.r_range = np.linspace(self.rEH+1e-10, self.rISCO, 1000)[::-1]

        # -----------------------------------
        # Create directories if need be
        # -----------------------------------
        if not os.path.exists(self.path_to_figures):
            os.makedirs(self.path_to_figures)
        if not os.path.exists(self.path_to_reduced_data):
            os.makedirs(self.path_to_reduced_data)

        # -----------------------------------
        # Run guess scan for ISCO four-velocity
        # -----------------------------------
        self.scan_uPhiISCOguess(**kwargs)
        self.scan_uTISCOguess(**kwargs)
#
        # -----------------------------------
        # Run guess scan for critical point location
        # -----------------------------------
        self.scan_rFguess(**kwargs)
        self.scan_uRFguess(**kwargs)
        self.scan_FLguess(**kwargs)

        # -----------------------------------
        # Dump to file
        # -----------------------------------
        with open(self.pickle_path, 'wb') as fh:
            pickle.dump(self, fh)

    def _load_from_pickle(self):
        try:
            with open(self.pickle_path, 'rb') as fh:
                self.__dict__.update(pickle.load(fh).__dict__)
                return True
        except:
            return False

    def scan_uPhiISCOguess(self, **kwargs):
        uPhi_grid = np.linspace(0, 3, 100)
        list_of_uTISCO = []
        list_of_uPhiISCO = []
        for guess in uPhi_grid:
            kwargs["uPhiISCOguess"] = guess
            self.find_ISCO_four_velocity(**kwargs)
            list_of_uTISCO.append(self.uTISCO)
            list_of_uPhiISCO.append(self.uPhiISCO)

        self.uPhiISCOguess = {'uPhi_grid': uPhi_grid,
                              'uTISCO_result': np.array(list_of_uTISCO),
                              'uPhiISCO_result': np.array(list_of_uPhiISCO)}

    def scan_uTISCOguess(self, **kwargs):
        uT_grid = np.linspace(0.001, 3, 100)
        list_of_uTISCO = []
        list_of_uPhiISCO = []
        for guess in uT_grid:
            kwargs["uTISCOguess"] = guess
            self.find_ISCO_four_velocity(**kwargs)
            list_of_uTISCO.append(self.uTISCO)
            list_of_uPhiISCO.append(self.uPhiISCO)

        self.uTISCOguess = {'uT_grid': uT_grid,
                              'uTISCO_result': np.array(list_of_uTISCO),
                              'uPhiISCO_result': np.array(list_of_uPhiISCO)}


    def scan_rFguess(self, **kwargs):
        guess_grid = np.linspace(self.rEH+1e-10, self.rISCO, 100)
        print("Scanning rF guess from {:.2f} to {:.2f}".format(guess_grid[0], guess_grid[-1]))
        list_of_rF = []
        list_of_uRF = []
        list_of_FL = []
        for guess in guess_grid:
            kwargs["rFguess"] = guess
            self.get_fast_critical_point(**kwargs)
            list_of_rF.append(self.rFval)
            list_of_uRF.append(self.uRFval)
            list_of_FL.append(self.FLval)

        self.rFguess = {'guess_grid': guess_grid,
                              'rF_result': np.array(list_of_rF),
                              'uRF_result': np.array(list_of_uRF),
                              'FL_result': np.array(list_of_FL)}

    def scan_uRFguess(self, **kwargs):
        guess_grid = np.linspace(-1.5, 0.0, 100)
        print("Scanning uRF guess from {:.2f} to {:.2f}".format(guess_grid[0], guess_grid[-1]))
        list_of_rF = []
        list_of_uRF = []
        list_of_FL = []

        guess_indices_to_remove = []
        for i, guess in enumerate(guess_grid):
            kwargs["uRFguess"] = guess

            # Solving for the fast critical point can crash for some values.
            # Catch these and don't add them to the list.
            try:
                self.get_fast_critical_point(**kwargs)
            except:
                guess_indices_to_remove.append(i)
                continue

            list_of_rF.append(self.rFval)
            list_of_uRF.append(self.uRFval)
            list_of_FL.append(self.FLval)

        # Remove guesses that crashed
        guess_grid = np.delete(guess_grid, guess_indices_to_remove)

        self.uRFguess = {'guess_grid': guess_grid,
                              'rF_result': np.array(list_of_rF),
                              'uRF_result': np.array(list_of_uRF),
                              'FL_result': np.array(list_of_FL)}

    def scan_FLguess(self, **kwargs):
        guess_grid = np.linspace(-4.0, 4.0, 100)
        print("Scanning FL guess from {:.2f} to {:.2f}".format(guess_grid[0], guess_grid[-1]))
        list_of_rF = []
        list_of_uRF = []
        list_of_FL = []
        for guess in guess_grid:
            kwargs["FLguess"] = guess
            self.get_fast_critical_point(**kwargs)
            list_of_rF.append(self.rFval)
            list_of_uRF.append(self.uRFval)
            list_of_FL.append(self.FLval)

        self.FLguess = {'guess_grid': guess_grid,
                              'rF_result': np.array(list_of_rF),
                              'uRF_result': np.array(list_of_uRF),
                              'FL_result': np.array(list_of_FL)}


if __name__ == "__main__":
    """
    Run some plots.
    """
    kwargs = {}
    kwargs["overwrite_self"] = False
    kwargs["Fthetaphi"] = 6.0
    kwargs["a"] = 0.3
    bg = guess_scan(**kwargs)

    figure_dir = bg.path_to_figures + "dynamical_background/Fthetaphi{:.2f}a{:.2f}/guess_scans/".format(bg.Fthetaphi, bg.a)
    if not os.path.exists(figure_dir):
        os.makedirs(figure_dir)
    # uPhiISCOguess = bg.uPhiISCOguess
    # uTISCOs = uPhiISCOguess['uTISCO_result']
    # uPhiISCOs = uPhiISCOguess['uPhiISCO_result']
    # print(np.allclose(uTISCOs, uTISCOs[0]))
    # print(np.allclose(uPhiISCOs, uPhiISCOs[0]))
    # plt.figure()
    # plt.plot(uPhiISCOguess['uPhi_grid'], uPhiISCOs, label=r'$u^\phi$')
    # plt.plot(uPhiISCOguess['uPhi_grid'], uTISCOs, label=r'$u^t$')
    # plt.xlabel(r'$u^\phi$ guess')
    # plt.legend()
    # plt.show()
#
    # uTISCOguess = bg.uTISCOguess
    # uTISCOs = uTISCOguess['uTISCO_result']
    # uPhiISCOs = uTISCOguess['uPhiISCO_result']
    # print(np.allclose(uTISCOs, uTISCOs[0]))
    # print(np.allclose(uPhiISCOs, uPhiISCOs[0]))
    # plt.figure()
    # plt.plot(uTISCOguess['uT_grid'], uPhiISCOs, label=r'$u^\phi$')
    # plt.plot(uTISCOguess['uT_grid'], uTISCOs, label=r'$u^t$')
    # plt.xlabel(r'$u^t$ guess')
    # plt.legend()
    # plt.show()

    guess_dict = bg.rFguess
    guess_grid = guess_dict['guess_grid']
    rFs = guess_dict['rF_result']
    uRFs = guess_dict['uRF_result']
    FLs = guess_dict['FL_result']

    plt.figure()
    plt.plot(guess_grid, rFs, label=r'$r_F$', color='C0')
    plt.plot(guess_grid, uRFs, label=r'$u^r_F$', color='C1')
    plt.plot(guess_grid, FLs, label=r'$F_L$', color='C2')
    plt.axhline([1.37], color='C0', ls='--')
    plt.axhline([-0.26], color='C1', ls='--')
    plt.axhline([1.23], color='C2', ls='--')
    plt.xlabel(r'$r_F$ guess')
    plt.legend()
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(r'$F_{{\theta\phi}}={:.1f}$, $u^R_F$ guess = -0.26, $F_L$ guess = 1.2'.format(bg.Fthetaphi))
    plt.tight_layout()
    plt.savefig(figure_dir + "rF_guess_scan.png")

    guess_dict = bg.uRFguess
    guess_grid = guess_dict['guess_grid']
    rFs = guess_dict['rF_result']
    uRFs = guess_dict['uRF_result']
    FLs = guess_dict['FL_result']
    plt.figure()
    plt.plot(guess_grid, rFs, label=r'$r_F$', color='C0')
    plt.plot(guess_grid, uRFs, label=r'$u^r_F$', color='C1')
    plt.plot(guess_grid, FLs, label=r'$F_L$', color='C2')
    plt.axhline([1.37], color='C0', ls='--')
    plt.axhline([-0.26], color='C1', ls='--')
    plt.axhline([1.23], color='C2', ls='--')
    plt.xlabel(r'$u^r_F$ guess')
    plt.legend()
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(r'$F_{{\theta\phi}}={:.1f}$, $r_F$ guess = 1.4, $F_L$ guess = 1.2'.format(bg.Fthetaphi))
    plt.tight_layout()
    plt.savefig(figure_dir + "uRF_guess_scan.png")

    guess_dict = bg.FLguess
    guess_grid = guess_dict['guess_grid']
    rFs = guess_dict['rF_result']
    uRFs = guess_dict['uRF_result']
    FLs = guess_dict['FL_result']
    plt.figure()
    plt.plot(guess_grid, rFs, label=r'$r_F$', color='C0')
    plt.plot(guess_grid, uRFs, label=r'$u^r_F$', color='C1')
    plt.plot(guess_grid, FLs, label=r'$F_L$', color='C2')
    plt.axhline([1.37], color='C0', ls='--')
    plt.axhline([-0.26], color='C1', ls='--')
    plt.axhline([1.23], color='C2', ls='--')
    plt.xlabel(r'$F_L$ guess')
    plt.legend()
    plt.gca().tick_params(top=True, right=True, direction='in', which='both')
    plt.gca().grid(color='.9', ls='--')
    plt.title(r'$F_{{\theta\phi}}={:.1f}$, $r_F$ guess = 1.4, $u^r_F$ guess = -0.26'.format(bg.Fthetaphi))
    plt.tight_layout()
    plt.savefig(figure_dir + "FL_guess_scan.png")

    print("Saving figures in " + figure_dir)
    # plt.show()
