# README
## Files
- dynamical_background_gammie1999.py: a class containing information about the four-velocity/density/EM fields obtained by solving the Gammie 1999 model. Running this script will generate plots of the four-velocity at each radius.

- tools.py: a set of useful functions/constants/etc (e.g. metric components). Not to be run on its own.

- cooling_timescales.py: calculations of various timescales in the Gammie model, including the infall time, synchrotron cooling time, bremsstrahlung cooling time, and collision/thermalization times.

- one_zone_equilibrium_IC.py: a one-zone model to see if a steady-state solution could be found where cooling due to radiation balances heating due to the dissipation calculated from the Gammie model.

- multi_zone_equilibrium.py: current best model. Solves for equilibrium at every radius using the Gammie model values to set magnetic field strengths. Programatically choses between IC and synchrotron based on UB/Uph. Stitches a power-law distribution onto a Maxwell-Juettner with temperature T (to be solved for); starts at gamma1 (determined by setting tcollisional(gamma1) = tcool(gamma1)), ends at gamma2 (set by PIC-motivated cut-off of 4sigma, where sigma is the magnetization. See Werner, Philippov, Uzdensky 2018(9?)). Power-law index is currently constant but will ultimately be magnetization-dependent, motivated by PIC simulations. The normalization of the power-law distribution is set by continuity of the power-law distribution with the Maxwell-Juettner distribution. The cooling rate is calculated from Rybicki & Lightman Eq. 6.36 (see latest issue). Power loss due to IC is assumed to scale as (Uph/UB) Psync. Note we use leastsquares rather than fsolve since there's no derivatives involved.

### Deprecated files
The previous approach was to directly evolve the distribution function. This approach is now deprecated (directory: deprecated_approach/) as are the files below that use this approach.
- implementation_tests.py: a set of tests checking that the evolution of the distribution function is working properly. Running this script will output various information about collisional timescales and generate example plots.
- reproduce_nayakshin_fig6.py: an attempt to replicate the evolution of the distribution function under cooling as outlined in Nayakshin & Melia 1998, Fig. 6
