#!/usr/bin/env python
# coding: utf-8

import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pylab as pl
import matplotlib.patches as patches
import scipy.integrate as integrate
import scipy.special as special
from scipy.optimize import root
from scipy.optimize import fsolve
from scipy.optimize import curve_fit
import scipy.optimize as optimize
import pickle
import os
import calcg_lnrf as calcg

class Consts:
    pass


Consts.G_in_cgs = 6.67*10**(-8) # cm^3/g/s^2
Consts.solar_mass_in_g = 1.99*10**33 # g
Consts.c_in_cgs = 3*10**10 # cm/s
Consts.h_cgs = 6.626*10**(-27) # erg s
Consts.me_in_g = 9.1*10**(-28) # g
Consts.mp_in_g = 1.67*10**(-24) # g
Consts.thomson_cross_section_cgs = 6.6*10**(-25)
Consts.kB_cgs = 1.3*10**(-16) # erg/K
Consts.e_in_cgs = 3.3*10**(-10) # statC
Consts.restmass_cgs = Consts.me_in_g * Consts.c_in_cgs**2.0
Consts.restmass_proton_cgs = Consts.mp_in_g * Consts.c_in_cgs**2.0
Consts.nu0const1 = 16.0*np.sqrt(np.pi)*Consts.e_in_cgs**4/(Consts.me_in_g**2)
Consts.coulomb_log = 10.0
Consts.SB_cgs = 5.6e-5
Consts.gaunt_factor = 1.2

colorblind_colors = ["#2691db", "#2e692c", "C1", "#723b7a", "#e41a1c"]
def cb_colors(n):
    return pl.cm.viridis(np.linspace(0, 1, n))
def cb_colors2(n):
    return pl.cm.magma(np.linspace(0, 1, n))
def cb_colors3(n):
    return pl.cm.viridis(np.linspace(0, 1, n))
# -----------------------------------
# Define metric components.
# Lower case -- > lower index; upper case -- > upper index, e.g. uR is u^r. Convention is -+++. Note that Gammie's a parameter for spin is dimensionless, so need to take conventional a (with units of length) and divide by M.
# -----------------------------------
def gtt(a, M, r):
    return -1 + 2*M/r


def grr(a, M, r):
    return r**2/(r**2-2*M*r+a**2*M**2)


def gphiphi(a, M, r):
    return ((r**2+a**2*M**2)**2-a**2*M**2*(r**2-2*M*r+a**2*M**2))/(r**2)


def gtphi(a, M, r):
    return -2*a*M**2/r


def gTT(a, M, r):
    return -(2 + r + r**3)/(r*(r - 1)**2)


def gTPhi(a, M, r):
    return -2/(r*(r-1)**2)


def mathcalD(a, M, r):
    return 1 - 2*M/r + a**2*M**2/(r**2)


def uphi(r, uT, uPhi, a, M):
    return gphiphi(a, M, r)*uPhi + gtphi(a, M, r)*uT


def ut(r, uT, uPhi, a, M):
    return gtphi(a, M, r)*uPhi + gtt(a, M, r)*uT


# -----------------------------------
# Define some quantities necessary for finding the collision time
# -----------------------------------
def get_ee_coulomb_log(Te_in_eV, number_density):
    # Assuming thermal electron-electron collisions
    # From NRL p. 34 eq. a
    # Te_in_eV in eV. number_density in 1/cm^3
    # See also https://farside.ph.utexas.edu/teaching/plasma/Plasma/node39.html
    coulomb_log = 23.5 - np.log(np.sqrt(number_density)*Te_in_eV**(-5.0/4.0)) - np.sqrt(10**(-5.0) + (np.log(Te_in_eV) - 2.0)**2 / 16.0)
    if (coulomb_log < 10).any():
        print("Warning: the coulomb logarithm is getting close to 1!")
    return coulomb_log


def get_ii_coulomb_log(Ti_in_eV, number_density, Te_in_eV=None):
    # Mixed ion-ion collisions
    # From NRL p. 34 eq. c
    # Assuming proton-proton thermal collisions
    # Assume quasineutrality density_e = density_i
    # or proton-electron thermal collisions

    # protron-proton collisions
    if Te_in_eV is None:
        Te_in_eV = Ti_in_eV
        mu_e = 1.0
    else:
        mu_e = Consts.me_in_g / Consts.mp_in_g
    mu_p = 1.0

    coulomb_log = 23 - np.log((mu_p + mu_e)/(mu_p*Ti_in_eV + mu_e*Te_in_eV) * np.sqrt(number_density/Ti_in_eV + number_density / Te_in_eV))
    if (coulomb_log < 10).any():
        print("Warning: the coulomb logarithm is getting close to 1!")
    return coulomb_log


def get_ei_coulomb_log(Te_in_eV, Ti_in_eV, number_density):
    # Assuming thermal electron-proton plasma
    # From NRL p. 34 eq. b
    # Te_in_eV, Ti_in_eV in eV, density in 1/cm^3
    # Assume quasineutrality density_e = density_i
    # See also https://farside.ph.utexas.edu/teaching/plasma/Plasma/node39.html
    mass_ratio = Consts.me_in_g / Consts.mp_in_g
    if Ti_in_eV*mass_ratio < Te_in_eV:
        if Te_in_eV < 10:
            coulomb_log = 23 - np.log(np.sqrt(number_density)/Te_in_eV**(1.5))
        else:
            # should be using this one for equal temperatures
            coulomb_log = 24 - np.log(np.sqrt(number_density)/Te_in_eV)
    elif Te_in_eV < Ti_in_eV*mass_ratio:
        coulomb_log = 30 - np.log(np.sqrt(number_density)/Ti_in_eV**(1.5))
    else:
        print("Using mixed ion-ion coulomb logarithm")
        coulomb_log = get_ii_coulomb_log(Ti_in_eV, number_density, Te_in_eV)

    if (coulomb_log < 10).any():
        print("Warning: the coulomb logarithm is getting close to 1!")
    return coulomb_log


def get_ee_collision_time(Te_in_eV, number_density):
    # From NRL p. 37
    # Te_in_eV in eV, density in 1/cm^3
    ee_coulomb_log = get_ee_coulomb_log(Te_in_eV, number_density)
    if isinstance(number_density, float):
        print("e-e Coulomb log: {:.2f}".format(ee_coulomb_log))
    return 3.44*10**5*Te_in_eV**1.5/number_density/ee_coulomb_log


def get_ei_collision_time(Te_in_eV, Ti_in_eV, number_density):
    # From NRL p. 37
    # Te_in_eV in eV, density in 1/cm^3
    ei_coulomb_log = get_ei_coulomb_log(Te_in_eV, Ti_in_eV, number_density)
    # ei_coulomb_log = 15
    if isinstance(number_density, float):
        print("e-i Coulomb log: {:.2f}".format(ei_coulomb_log))
    return 2.09*10**7*Ti_in_eV**1.5/number_density/ei_coulomb_log

# def maxwell_juettner(plasma, number_density_cgs, average_energy_cgs):
    # """
    # Isotropic Maxwell-Juettner distribution function
    # whose zeroth moment integrates to number_density
    # and whose second moment integrates to average_energy.
    # Returns in energy-space.
    # """
    # # T in K, mass in g, velocities in cm/s
    # # TODO: change to be relativistic coefficient?
#
#
    # gamma = plasma.gamma_range
    # def MJ_equations(p):
        # theta = p
        # return integrate.simps(gamma * gamma**2.0 * np.sqrt(1.0-1/gamma**2.0)/theta/special.kn(2, 1/theta)*np.exp(-gamma/theta), gamma) - average_energy_cgs
    # theta_sol = fsolve(MJ_equations, (0.2))
    # print(theta_sol)
def maxwell_juettner_over_gamma(gamma_bins, number_density, theta):
    # """
    # Isotropic Maxwell-Juettner distribution function
    # whose zeroth moment integrates to number_density
    # """
    fMJ_over_gamma = gamma_bins**2.0*np.sqrt(1.0-1.0/(gamma_bins**2.0))/(theta*special.kn(2, 1.0/theta))*np.exp(-gamma_bins/theta)*number_density
    return fMJ_over_gamma

def maxwell_juettner_at_gamma(gamma, number_density, theta, quiet=True):
    # """
    # Isotropic Maxwell-Juettner distribution function
    # whose zeroth moment integrates to number_density
    # """
    temp = theta*Consts.restmass_cgs/Consts.kB_cgs
    if isinstance(gamma, float):
        if gamma < 1:
            print("ERROR: gamma is less than one!!")
            exit()
    else:
        if (gamma < 1).any():
            print("ERROR: gamma is less than one!!")
            exit()

    import warnings
    warnings.filterwarnings('error')
    try:
        fMJ_at_gamma = gamma**2.0*np.sqrt(1.0-1.0/(gamma**2.0))/(theta*special.kn(2, 1.0/theta))*np.exp(-gamma/theta)*number_density
    except RuntimeWarning:
        if special.kn(2, 1.0/theta) == 0.0:
            if not quiet:
                print("Modified bessel function is zero. Using MB. T={:.2e} K".format(temp))
            # The Maxwell-Boltzmann overestimates the number of particles compared
            # to the MJ, but it's more accurate at low temperatures anyway.
            # Switching to MB instead of just setting MJ = small number
            # yields much smoother solutions below T\approx 9e6 K.
            return maxwellian_at_gamma(gamma, number_density, theta, quiet)
    small_number = np.finfo(np.float128).eps # 1e-19
    if fMJ_at_gamma.size > 1:
        small_number_array = small_number*np.ones(fMJ_at_gamma.shape) # 1e-19
        if (fMJ_at_gamma < small_number_array).any():
            if not quiet:
                print("Setting Maxwell-Juettner value to small number. T={:.2e} K".format(temp))
            fMJ_at_gamma[fMJ_at_gamma < small_number_array] = small_number
    elif fMJ_at_gamma < small_number:
        if not quiet:
            print("Setting Maxwell-Juettner value to small number.")
        fMJ_at_gamma = small_number
    return fMJ_at_gamma


def maxwellian_at_gamma(gamma, number_density, theta, quiet=True):
    # """
    # Isotropic Maxwell-Juettner distribution function
    # whose zeroth moment integrates to number_density.
    # Note that we must use fMB(gamma)dgamma, i.e. convert
    # the differential too.
    # """
    temp = theta*Consts.restmass_cgs/Consts.kB_cgs
    beta = np.sqrt(1.0 - 1.0/gamma**2)
    fM_at_gamma = np.sqrt(2.0/np.pi)*beta/theta**1.5*np.exp(-beta**2/2.0/theta)*number_density
    small_number = np.finfo(np.float128).eps
    if fM_at_gamma.size > 1:
        small_number_array = small_number*np.ones(fM_at_gamma.shape) # 1e-19
        if (fM_at_gamma < small_number_array).any():
            if not quiet:
                print("Setting Maxwellian value to small number: T={:.2e}.".format(temp))
            fM_at_gamma[fM_at_gamma < small_number_array] = small_number
    elif fM_at_gamma < small_number:
        if not quiet:
            print("Setting Maxwellian value to small number: T={:.2e}.".format(temp))
        fM_at_gamma = small_number
    return fM_at_gamma


def maxwellian_fit_f(gamma_values, A, theta):
    """
    f_M(gamma) = A sqrt(gamma) * exp(-gamma / theta)
    returns number density, average energy of best-fit maxwellian
    """
    return A*np.sqrt(gamma_values) * np.exp(-gamma_values/theta)


def power_law_over_gamma(gamma1, gamma2, peff, norm, gamma_bins):
    # NOTE: norm = norm*ndensity!
    ind1 = (np.abs(gamma_bins - gamma1)).argmin()
    ind2 = (np.abs(gamma_bins - gamma2)).argmin()
    fPL = norm * gamma_bins**(-peff)
    fPL[:ind1] = 0
    fPL[ind2:] = 0
    return fPL


def get_eddington_lum(M_in_Msun):
    return 1.26e38 * M_in_Msun

def psi_function(x):
    """
    Gives psi(x)-psi'(x), where psi(x) is the lower incomplete gamma function
    Note that the 2/sqrt(pi) has been absorbed into nu0const1
    Also note that gammainc is normalized to the gamma function.
    BIG NOTE: this psi function is for the electron-electron energy loss
    frequency; for electron-proton there's a factor of me/mp!
    """
    return special.gammainc(1.5, x)*special.gamma(1.5) - np.exp(-x)*np.sqrt(x)

def psi_function_EP(x):
    """
    Gives me/mp*psi(x)-psi'(x), where psi(x) is the lower incomplete gamma function
    Note that the 2/sqrt(pi) has been absorbed into nu0const1
    Also note that gammainc is normalized to the gamma function.
    BIG NOTE: this psi function is for the electron-proton energy loss
    """
    mass_ratio = Consts.me_in_g/Consts.mp_in_g
    return mass_ratio*special.gammainc(1.5, x)*special.gamma(1.5) - np.exp(-x)*np.sqrt(x)

# turnover_x is the x where the electron-electron energy loss frequency is zero,
# where energy loss freq is \nu_\epsilon on NRL p. 31
Consts.turnover_x = root(psi_function, 1.0).x[0]
Consts.max_temp = Consts.restmass_cgs/(2.0*Consts.kB_cgs*Consts.turnover_x)
Consts.turnover_x_EP = root(psi_function_EP, 1.0).x[0]
Consts.max_temp_EP = Consts.restmass_proton_cgs/(2.0*Consts.kB_cgs*Consts.turnover_x_EP)

def nuToKev(nu):
    return 4.1e-18 * nu
def kevToNu(keV):
    return keV/(4.1e-18)

def calculate_xcoh(ndensity, temperature):
    # RL eq. 7.65b
    xcoh = 2.4e17 * np.sqrt(Consts.me_in_g*ndensity)*(temperature)**(-9.0/4.0)
    return xcoh

def calculate_amplification_factor(ndensity, temperature):
    # RL eq. 7.74b
    xcoh = calculate_xcoh(ndensity, temperature)
    amplification_factor = 0.75*(np.log(2.25/xcoh))**2
    return amplification_factor

def calculate_bremsstrahlung_emissivity(ndensity, temperature):
    # RL eq. 5.15b
    bremsstrahlung_emissivity = 1.4e-27*np.sqrt(temperature)*ndensity**2*Consts.gaunt_factor
    return bremsstrahlung_emissivity

def minus_energy_loss_frequency(beta, ndensity, theta):
    """
    NOTE that this energy loss frequency is for electron-electron collisions.
    """
    x_val = 0.5*beta**2/theta
    nu0const = Consts.nu0const1*ndensity*Consts.coulomb_log
    energy_loss_freq = nu0const*psi_function(x_val)/(beta*Consts.c_in_cgs)**3
    return -energy_loss_freq

def minus_energy_loss_frequency_EP(beta, ndensity, theta):
    """
    NOTE that this energy loss frequency is for electron-proton collisions.
    theta must be kTp/mp*c^2.
    """
    x_val = 0.5*beta**2/theta
    nu0const = Consts.nu0const1*ndensity*Consts.coulomb_log
    energy_loss_freq = nu0const*psi_function_EP(x_val)/(beta*Consts.c_in_cgs)**3
    return -energy_loss_freq

def energy_loss_timescale(beta, ndensity, theta):
    return -1.0/minus_energy_loss_frequency(beta, ndensity, theta)

def energy_loss_timescale_EP(beta, ndensity, theta):
    return -1.0/minus_energy_loss_frequency_EP(beta, ndensity, theta)

def abs_energy_loss_timescale(beta, ndensity, theta):
    return np.abs(1.0/minus_energy_loss_frequency(beta, ndensity, theta))

def get_turnover_beta_gamma(theta):
    # Find the velocity where the electron-electron energy loss frequency is zero
    turnover_beta = np.sqrt(2.0*Consts.turnover_x*theta)
    if np.isscalar(turnover_beta):
        if turnover_beta <= 0.0:
            print("Turnover beta: {:.2f}".format(turnover_beta))
            exit()
        # TODO: this limit indicates that the NRL energy loss frequency assumption
        # of a Maxwell-Boltzmann thermal plasma is breaking down.
        # Need to revise limit or something here.
        if turnover_beta >= 1.0:
            print("Turnover beta greater than c: T={:.2e}".format(theta*Consts.restmass_cgs/Consts.kB_cgs))
            turnover_beta = 0.9999999
    else:
        if (turnover_beta <= 0.0).any():
            print("Turnover beta: {:.2f}".format(turnover_beta))
            exit()
        else:
            turnover_beta[turnover_beta >= 1.0] = 0.9999999
    turnover_gamma = 1.0/np.sqrt(1.0 - turnover_beta**2.0)
    return (turnover_beta, turnover_gamma)

def calculate_max_energy_loss_freq(theta, ndensity):
    # Bound from beta = turnover beta to 1.
    # NOTE this does not include manual limits set on gamma1 for the power law
    # because its purpose is to find the max energy loss frequency regardless of
    # power-law model constraints.
    minimum_beta, g = get_turnover_beta_gamma(theta)
    maximum_beta = 1.0
    nu_solution = optimize.minimize(minus_energy_loss_frequency, x0=(minimum_beta), bounds=[(minimum_beta, maximum_beta)], args=(ndensity, theta))
    return nu_solution

def get_fixed_spectral_bins():
    nu_max = 5.0e23
    nu_min = 1.0e14
    spectral_bins = np.logspace(np.log10(nu_min), np.log10(nu_max), 10000)
    return spectral_bins

def get_rISCO(spin, mass):
    chi = spin
    Z1 = 1.0 + (1.0 - chi**2.0)**(1.0/3.0)*((1.0 + chi)**(1.0/3.0) + (1.0 - chi)**(1.0/3.0))
    Z2 = np.sqrt(3.0*chi**2.0 + Z1**2.0)
    # rISCO, as per r_ms on Wiki.
    rISCO = mass*(3.0 + Z2 - np.sqrt((3.0 - Z1)*(3.0 + Z1 + 2*Z2)))
    return rISCO

def get_rEH(spin):
    """
    rEH in units of rg
    """
    rEH = 1.0 + np.sqrt(1.0 - spin**2)
    return rEH

def get_rg_in_cm(M_in_Msun):
    return (Consts.G_in_cgs*Consts.solar_mass_in_g*M_in_Msun)/(Consts.c_in_cgs**2.0)

def get_thin_disk_lnrf_velocities(spin, mass, radii_in_rg):
    # mass = dimensionless. Usually 1.0
    # Novikov Thorne gives the thin disk velocity already in the LNRF,
    # so we don't need to transform it like we did in the old way.
    # Verified that the two methods are allclose 2022-04-05.
    # Eq. 5.4.4a
    vPhi_lnrf = np.sqrt(mass)*Ffunc(spin, radii_in_rg)/(Bfunc(spin, radii_in_rg)*np.sqrt(radii_in_rg*Dfunc(spin, radii_in_rg)))
    vR_lnrf = 0.0*np.ones(vPhi_lnrf.shape)
    vTheta_lnrf = 0.0*np.ones(vPhi_lnrf.shape)
    return [vR_lnrf, vTheta_lnrf, vPhi_lnrf]

def get_thin_disk_four_velocity(spin, mass, radii_in_rg):
    # Calculate from Kerr circular orbit velocity Omega.
    # Omega = dphi/dt = dphi/dtau (dtau/dt) = uPhi/gamma. (NT 5.4.3, probably in Bardeen 1973)
    Omega = np.sqrt(mass)/(radii_in_rg**1.5 + spin*np.sqrt(mass))
    # Setting u^mu u_mu = -1 and plugging in uPhi = gamma Omega = uT Omega
    # gives an equation for uT in terms of Omega.
    denominator = (gtt(spin, mass, radii_in_rg) + 2.0*gtphi(spin, mass, radii_in_rg)*Omega + gphiphi(spin, mass, radii_in_rg)*Omega**2)
    # We MUST have denominator < 0 to give a real number!
    if (denominator > 0).any():
        print("ERROR: denominator is positive! Will result in imaginary gamma.")
    uT_disk = np.sqrt(-1.0/denominator)
    uPhi_disk = uT_disk*Omega
    uTheta_disk = 0.0*np.ones(uPhi_disk.shape)
    uR_disk = 0.0*np.ones(uPhi_disk.shape)
    return [uT_disk, uR_disk, uTheta_disk, uPhi_disk]

def get_thin_disk_lnrf_velocities_old(spin, mass, radii_in_rg):
    # mass = dimensionless. Usually 1.0
    uT_disk, uR_disk, uTheta_disk, uPhi_disk = get_thin_disk_four_velocity(spin, mass, radii_in_rg)
    # ------------------------------
    # Transform to locally non-rotating frame
    # ------------------------------
    theta_array = np.pi/2.*np.ones(uR_disk.shape)
    # Calculate three-velocities
    vR_bl = uR_disk/uT_disk
    vTheta_bl = uTheta_disk/uT_disk
    vPhi_bl = uPhi_disk/uT_disk
    vR_lnrf, vTheta_lnrf, vPhi_lnrf = calcg.lnrf_frame(vR_bl, vTheta_bl, vPhi_bl, radii_in_rg, spin, theta_array)
    return [vR_lnrf, vTheta_lnrf, vPhi_lnrf]

def redshifted_disk_spectrum(spin, mass, inc_angle, **kwargs):
    eq = kwargs.get("equilibrium", None)
    if eq is not None:
        disk_spectrum_path = eq.dir_to_disk_spectrum + "redshifted_i{:d}.p".format(inc_angle)
    else:
        disk_spectrum_path = "/mnt/c/Users/liaha/research/projects/summer2021/data_reduced/disk_spectra/a{:.2f}/".format(spin)
    disk_spectrum_path = kwargs.get("disk_spectrum_path", disk_spectrum_path)

    if os.path.exists(disk_spectrum_path):
        with open(disk_spectrum_path, 'rb') as file:
            disk_spectrum_dict = pickle.load(file)
    else:
        disk_spectrum_dict = {}

    return disk_spectrum_dict

#---------------------------------------
# Relativistic correction functions.
# From Novikov & Thorne eqns. 5.4.1
# Note x=r/rg
#---------------------------------------

def Afunc(a, r):
    return 1.0 + a**2/r**2 + 2.0*a**2/r**3
def Bfunc(a, r):
    return 1.0 + a/r**1.5
def Cfunc(a, r):
    return 1.0 - 3.0/r + 2.0*a/r**1.5
def Dfunc(a, r):
    return 1.0 - 2.0/r + a**2/r**2
def Efunc(a, r):
    return 1.0 + 4.0*a**2/r**2 - 4.0*a**2/r**3 + 3.0*a**4/r**4
def Ffunc(a, r):
    return 1.0 - 2.0*a/r**1.5 + a**2/r**2
def funcFromHell(a, r):
    # Note NT expresses this as an integral.
    # Page and Thorne expresses algebraically,
    # and to be as obnoxious as possible they set
    # x = sqrt(dimensionless radius)
    x = np.sqrt(r)
    x0 = np.sqrt(get_rISCO(a, 1.0))
    x1 = 2.0*np.cos(1.0/3.0*np.arccos(a) - np.pi/3.0)
    x2 = 2.0*np.cos(1.0/3.0*np.arccos(a) + np.pi/3.0)
    x3 = -2.0*np.cos(1.0/3.0*np.arccos(a))

    bracket_one = x - x0 - 1.5*a*np.log(x/x0)
    bracket_two = -3.0*(x1 - a)**2/(x1*(x1 - x2)*(x1 - x3))*np.log((x - x1)/(x0 - x1))
    bracket_three = -3.0*(x2 - a)**2/(x2*(x2 - x1)*(x2 - x3))*np.log((x - x2)/(x0 - x2))
    bracket_four = -3.0*(x3 - a)**2/(x3*(x3 - x1)*(x3 - x2))*np.log((x - x3)/(x0 - x3))
    bracket = bracket_one + bracket_two + bracket_three + bracket_four

    return Bfunc(a, r)/np.sqrt(Cfunc(a, r))/x*bracket

def arc_patch(xy, width, height, theta1=0., theta2=180., resolution=50, **kwargs):
    # From https://stackoverflow.com/questions/36789304/how-to-plot-half-ellipses-in-python
    # generate the points
    theta = np.linspace(np.radians(theta1), np.radians(theta2), resolution)
    points = np.vstack((width*np.cos(theta)  + xy[0],
                        height*np.sin(theta) + xy[1]))
    # build the polygon and add it to the axes
    poly = patches.Polygon(points.T, closed=True, **kwargs)

    return poly

class AstroObjects:
    pass

AstroObjects = {}
# Also see nice Table 1 in Salvesen & Miller 2021 for inclination,
# and different spin measuring methods (continuum vs. Fe line).
# Reynolds 2021 has quite a few spin measurements too.
# In these tables, I'll take the min/max over both methods
# because their ranges don't always overlap.
# Zeros indicate I haven't found data for those entries yet.
AstroObjects["CygX1"] = {"min_inc": 26.3, "max_inc": 27.9,
                           "max_PLF": 0.0, "min_PLF":0.0,
                           "max_RM06": 0.0, "min_RM06":0.0,
                           "min_spin": 0.95, "max_spin": 0.99,
                           "hatch": '/', "color": "C0"}
AstroObjects["GRO1655"] = {"min_inc": 69, "max_inc": 71.4,
                           "min_50keV":5.1e36, "max_50keV":9.5e36, # Grove+ 1998
                           # "max_PLF": 0.18, "min_PLF":0.007, # pure estimate
                           # "min_PLF": 0.011, "max_PLF":0.031, # mean +- 1 std
                           "min_PLF": 0.011, "max_PLF":0.190, # absolute max/min over all outbursts
                           "max_RM06": 0.0, "min_RM06":0.0,
                           "min_spin": 0.60, "max_spin": 0.94,
                           "outburst_data":{"GRO1655-2":{"time_limits":[3550, 3600]},
                                            "GRO1655-1":{"time_limits":[475, 637]}}, # note is +50000
                           "hatch": '/', "color": "C0"}
AstroObjects["GX339"] = {"min_inc": 29, "max_inc": 31, # Parker 2016
                         # "max_PLF": 0.14, "min_PLF":0.02, # pure estimate
                         # "min_PLF": 0.013, "max_PLF":0.131, # mean +- 1 std from outburst 2
                         # "min_PLF": 0.008, "max_PLF":0.168, # mean +- 1 std from all outbursts
                         "min_PLF": 0.006, "max_PLF":0.272, # absolute max/min from outburst 2
                         "max_RM06": 0.0, "min_RM06":0.0,
                         "max_spin": 0.97, "min_spin": 0.87, # Parker 2016
                         "outburst_data":{"GX339-2":{"time_limits":[2418, 2513]}, "GX339-3":{}},
                         "hatch": '\\', "color":"C1"}
AstroObjects["J1550"] = {"min_inc": 71, "max_inc": 79,
                         # "max_PLF": 0.19, "min_PLF":0.003, # pure estimate
                         # "min_PLF": 0.027, "max_PLF":0.141, # mean +- 1 std
                         "min_PLF": 0.004, "max_PLF":0.277, # absolute max/min
                         "min_RM06": 0.0, "max_RM06":0.0,
                         "min_spin": 0.07, "max_spin": 0.70,
                         "outburst_data":{"J1550-1":{"time_limits":[1164, 1238]}},
                         "hatch": '\\', "color":"C3"}
AstroObjects["GRS1915"] = {"min_inc": 64, "max_inc": 68,
                           "min_50keV":27e36, "max_50keV":45e36, # Grove+ 1998
                           "min_spin": 0.95, "max_spin": 0.99,
                           "hatch": '\\', "color":"C2"}
AstroObjects["4U1543"] = { # Note there's a very low count rate. Munoz-Darias+ 2013 excludes it.
    "min_inc": 19.2, "max_inc": 22.2,
    "min_50keV":0.5e36, "max_50keV":1.7e36, # Grove+ 1998
    "min_spin": 0.59, "max_spin": 0.90,
    # "min_PLF": 0.029, "max_PLF":0.129, # mean +- 1 std
    "min_PLF": 0.021, "max_PLF":0.188, # absolute max/min
    "min_RM06": 0.0, "max_RM06":0.0,
    "outburst_data":{"4U1543-1":{}},
    "hatch": '\\', "color":"C2"}
AstroObjects["4U1630"] = { # note than chatterjee+ 2022 suggests this is a triple system
    "min_inc": 60, "max_inc": 70, # seifina+ 2014, tomsick+ 1998; liu liu+ find i=0-10??
    "max_PLF": 0.18, "min_PLF":0.02,
    "max_RM06": 0.0, "min_RM06":0.0,
    "min_spin": 0.80, "max_spin": 0.99, # reynolds 2021, liu, liu, bambi, ji 2022
    "hatch": '\\', "color":"C2"}
# Note H1743 only has continuum fitting spin measurements (no Fe line),
# which tend to be smaller? Maybe?
AstroObjects["H1743"] = {"min_inc": 72, "max_inc": 78,
                         "max_PLF": 0.18, "min_PLF":0.02,
                         "max_RM06": 0.0, "min_RM06":0.0,
                         "min_spin": -0.13, "max_spin": 0.54,
                         "hatch": '\\', "color":"C2"}
